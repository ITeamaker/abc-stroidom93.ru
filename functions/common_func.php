<?php

//обрезание обратных слешев в $_REQUEST данных
function stripslashes_smart($post) {
	if (get_magic_quotes_gpc()) {
		if (is_array($post)) {
			foreach ($post as $k=>$v) {
				$q[$k] = stripslashes_smart($v);
			}
		}
		else $q = stripslashes($post);
	}
	else $q = $post;
	return $q;
}

function push_crm($template,$post=array()) {
	if(/*$template=='make_call'*/1){
		if($post){
			$params = array();
			$shop_parameters = mysql_select("SELECT * FROM shop_parameters WHERE 1 ORDER BY rank DESC",'rows_id');
			foreach ($shop_parameters as $k=>$v){
				if(@$post['p'.$k]){
					$value = unserialize($v['values']);
					$val=explode(',',$post['p'.$k]);
					$string_val = array();
					foreach ($val as $k2=>$v2){
						if(@$value[$v2]){
							$string_val[] = $value[$v2];
						}
					}
					unset($post['p'.$k]);
					$post[$v['key']]= implode('; ',$string_val);
				}
			}
			log_add('push_crm.txt',http_build_query($post));
			$url = 'http://portal.dev-platform.ru/Modules/RequestsForm/Listening/Index/5b07fefab4ee4';
			$result = file_get_contents($url, false, stream_context_create(array(
				'http' => array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query($post)
				)
			)));
			//log_add('push_crm_result.txt',$result);
		}
	}
}



//создание урл из $_GET
function build_query($key = '') {
	$get = $_GET;
	if ($key) {
		$array = explode(',',$key);
		foreach ($array as $k=>$v) unset($get[$v]);
	}
	return http_build_query($get);
}

function dd($data,$die=false) {
	echo '<pre>';
	print_r($data);
	echo '</pre>';
	if ($die) die();
}

//создание файла лога в папке logs
/**
 * @param $file - название файла в папке /logs/
 * @param $string - строка или массив данных который будут записаны в лог
 * @param bool $debug - в значении true логи будут писываться только если $config['debug'] = true
 */
function log_add($file,$string,$debug=false) {
	global $config;
	//логи с пометкой дебаг не создаются при выключеном $config['debug']
	if ($debug==false OR $config['debug'] == true) {
		if (!is_dir(ROOT_DIR . 'logs')) mkdir(ROOT_DIR . 'logs');
		$fp = fopen(ROOT_DIR . 'logs/' . $file, 'a');
		//если в лог передан массив то делаем из него строку
		if (is_array($string)) {
			$content = '';
			foreach ($string as $k=>$v) {
				if (is_array($v)) $content.= $k.':'.serialize($v)."\t";
				else $content.= $k.':'.$v."\t";
			}
			$string = $content;
		}
		fwrite($fp, $string . PHP_EOL);
		fclose($fp);
	}
}

//получить ИП
function get_ip(){
	$ip = '';
	if(!empty($_SERVER['HTTP_X_REAL_IP'])) {//check ip from share internet
		$ip = $_SERVER['HTTP_X_REAL_IP'];
	}
	elseif(!empty($_SERVER['HTTP_CLIENT_IP'])) { //check ip from share internet
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}
	elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { //to check ip is pass from proxy
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

/**
 * Transpose input arrays and save input keys.
 * 
 * Example inputs: 
 * 
 * <code>
 * <input name="name[]" value="Alex"><br>
 * <input name="post[]" value="Actor"><br>
 * <input name="email[]" value="alex_actor@mail.dev"><br>
 * </code>
 * 
 * input as
 * 
 * <code>
 * [
 *  'name' => ['Alex', 'Born', 'Cindal'],
 *  'post' => ['Actor', 'Banker', 'Conductor'],
 *  'email' => ['alex_actor@mail.dev', 'born_banker@mail.dev', 'cindal_conductor.dev']   
 * ];
 * </code>
 * output as 
 * <code>
 * [
 *  0 => [
 *      'name'  => 'Alex',
 *      'post'  => 'Actor',
 *      'email' => 'alex_actor@mail.dev'
 *  ],
 *       1 => [
 *           'name'  => 'Born',
 *           'post'  => 'Banker',
 *           'email' => 'born_banker@mail.dev'
 *       ],
 *       2 => [
 *           'name'  => 'Cindal',
 *           'post'  => 'Conductor',
 *           'email' => 'cindal_conductor.dev'
 *       ],
 *   ];
 * </code>
 * 
 * @param array $inputArray
 * @return array
 */
function transposeArray(array $inputArray){
	$outputArray = array();
	foreach ($inputArray as $dataKey=>$dataValues) {
		foreach ($dataValues as $k=>$v) {
			$outputArray[$k][$dataKey] = $v;
		}
	}
	return $outputArray;
}

/*
 * Функция для сжатия нтмл кода
 * @param $body - простой нтмл код
 * @return mixed - сжатый нтмл код
 * @version v1.2.11
 * v.1.1.8 - добавлена
 * v.1.2.11 - полностью обновлена
*/
function html_minify ($body) {
	//remove redundant (white-space) characters
	$replace = array(
		//remove tabs before and after HTML tags
		'/\>[^\S ]+/s'   => '>',
		'/[^\S ]+\</s'   => '<',
		//shorten multiple whitespace sequences; keep new-line characters because they matter in JS!!!
		'/([\t ])+/s'  => ' ',
		//remove leading and trailing spaces
		'/^([\t ])+/m' => '',
		'/([\t ])+$/m' => '',
		// remove JS line comments (simple only); do NOT remove lines containing URL (e.g. 'src="http://server.com/"')!!!
		'~//[a-zA-Z0-9 ]+$~m' => '',
		//remove empty lines (sequence of line-end and white-space characters)
		'/[\r\n]+([\t ]?[\r\n]+)+/s'  => "\n",
		//remove empty lines (between HTML tags); cannot remove just any line-end characters because in inline JS they can matter!
		'/\>[\r\n\t]+\</s'    => '><',
		//все пробелы между тегами нельзя удалять
		'/\>[ ]+\</s'    => '> <',
		//remove "empty" lines containing only JS's block end character; join with next line (e.g. "}\n}\n</script>" --> "}}</script>"
		'/}[\r\n\t ]+/s'  => '}',
		'/}[\r\n\t ]+,[\r\n\t ]+/s'  => '},',
		//remove new-line after JS's function or condition start; join with next line
		'/\)[\r\n\t ]?{[\r\n\t ]+/s'  => '){',
		'/,[\r\n\t ]?{[\r\n\t ]+/s'  => ',{',
		//remove new-line after JS's line end (only most obvious and safe cases)
		'/\),[\r\n\t ]+/s'  => '),',
		//remove quotes from HTML attributes that does not contain spaces; keep quotes around URLs!
		'~([\r\n\t ])?([a-zA-Z0-9]+)="([a-zA-Z0-9_/\\-]+)"([\r\n\t ])?~s' => '$1$2=$3$4', //$1 and $4 insert first white-space character found before/after attribute
	);
	$body = preg_replace(array_keys($replace), array_values($replace), $body);
	//remove optional ending tags (see http://www.w3.org/TR/html5/syntax.html#syntax-tag-omission )
	$remove = array(
		'</option>', '</li>', '</dt>', '</dd>', '</tr>', '</th>', '</td>'
	);
	$body = str_ireplace($remove, '', $body);
	return $body;
}

//создание урл из $_GET
function calculator($data = array(),$debug=false) {
	global $config;
	$result = array();
    $data_cost = '';
	// Считаем площадь одного этажа
	$area = ($data['width']>0 ? $data['width'] : 1) * ($data['length']>0 ? $data['length'] : 1);
	//  Считаем площадь дома
	$sum_area = $area * ($data['storeys']==1 ? 1 : 2);
    if ($data['storeys']==2){
        $sum_area = $sum_area*0.9;
    }
    else{
        $sum_area = $sum_area;
    }
	// начальная стоимость метра
	$cost = 0;

    // параметры которые влияют на стоимость 1 кв.м.
    // этажность
    if ($data['storeys']>0 && isset($config['calculator_storeys'][$data['storeys']])) {
        $cost = $cost + $config['calculator_storeys'][$data['storeys']]['value'];
        if ($debug) $data_cost .= 'этажность'.$cost.'<br>';
    }
    // высота потолков
    if ($data['сeiling_height']>0 && isset($config['calculator_сeiling_height'][$data['сeiling_height']])) {
        $cost = $cost + $config['calculator_сeiling_height'][$data['сeiling_height']]['value'];
        if ($debug) $data_cost .= 'высота потолков '.$cost.'<br>';
    }
    // Кровля
    if ($data['roof']>0 && isset($config['calculator_roof'][$data['roof']])) {
        $cost = $cost + $config['calculator_roof'][$data['roof']]['value'];
        if ($debug) $data_cost .= 'Кровля '.$cost.'<br>';
    }
    //Окна
    if ($data['windows']>0 && isset($config['calculator_windows'][$data['windows']])) {
        $cost = $cost + $config['calculator_windows'][$data['windows']]['value'];
        if ($debug) $data_cost .= 'Окна '.$cost.'<br>';
    }
    //Фасад
    if ($data['facade']>0 && isset($config['calculator_facade'][$data['facade']])) {
        $cost = $cost + $config['calculator_facade'][$data['facade']]['value'];
        if ($debug) $data_cost .= 'Фасад '.$cost.'<br>';
    }
    //Коммуникации
    if ($data['communications']>0 && isset($config['calculator_communications'][$data['communications']])) {
        $cost = $cost + $config['calculator_communications'][$data['communications']]['value'];
        if ($debug) $data_cost .= 'Коммуникации '.$cost.'<br>';
    }
    // стены
    if ($data['walls']>0 && isset($config['calculator_walls'][$data['walls']])) {
        $cost = $cost + $config['calculator_walls'][$data['walls']]['value'];
        if ($debug) $data_cost .= 'стены '.$cost.'<br>';
    }
    // от стен зависит стоимость фундамента
    if ($data['foundation']==1 && $data['walls']>0 && isset($config['calculator_walls'][$data['walls']])) {
        $cost = $cost + $config['calculator_walls'][$data['walls']]['foundation'];
        if ($debug) $data_cost .= 'стоимость фундамента '.$cost.'<br>';
    }
    //Внутренняя отделка зависит от типа стен trim
    if ($data['trim']>0 && isset($config['calculator_trim'][$data['trim']]) && $data['walls']>0 && isset($config['calculator_walls'][$data['walls']])) {
        /*
        не требуется
        -для каркасных СИП и брусовых домов - минус 1500 руб.
        - для каменных 0 руб.
        */
        if ($data['trim']==1){
            if($data['walls']==4){
                $cost = $cost + 0;
            }else{
                $cost = $cost + 0;
            }
        }
        /*
        предчистовая - 0 руб.
        для каркасных, СИП и брусовых домов - 0 руб.
        для каменных - плюс 4000 руб.
        */
        elseif($data['trim']==2){
            if($data['walls']==4){
                $cost = $cost + 4000;
            }else{
                $cost = $cost + 0;
            }
        }
        //Под ключ - плюс 3000 руб
        else{
            if($data['walls']==4){
                $cost = $cost + 7000;
            }else{
                $cost = $cost + 3000;
            }
        }
        //$cost = $cost + $config['calculator_walls'][$data['walls']]['value'];
        if ($debug) $data_cost .= 'Внутренняя отделка зависит от типа стен '.$cost.'<br>';
    }
    //echo $cost;

	/* [4.64] Этапность пунктов калькулятора и информация в селекторах
	// стоимость одного м2
	$cost_m = 9000;
	$cost_m_s = $cost_m_c = 0;
	// параметры которые влияют на стоимость 1 кв.м.
	// этажность
	if ($data['storeys']>0 && isset($config['calculator_storeys'][$data['storeys']])) {
		$cost_m_s = $config['calculator_storeys'][$data['storeys']]['value']*$cost_m;
		//echo 'этажность'.$cost_m.'<br>';
	}
	// высота потолков
	if ($data['сeiling_height']>0 && isset($config['calculator_сeiling_height'][$data['сeiling_height']])) {
		$cost_m_c = $config['calculator_сeiling_height'][$data['сeiling_height']]['value']*$cost_m;
		//echo 'высота потолков'.$cost_m.'<br>';
	}
	$cost_m = $cost_m + $cost_m_s +$cost_m_c;
	$cost_area = $sum_area * $cost_m;

	$cost_roof = $cost_facade = $cost_windows = $cost_communications = $cost_foundation = $cost_trim = 0;
	// параметры которые не влияют на стоимость 1 кв.м.
	// кровля
	if ($data['roof']>0 && isset($config['calculator_roof'][$data['roof']])) {
		$cost_roof = $config['calculator_roof'][$data['roof']]['value']*$area;
		//echo 'кровля'.$cost_roof.'<br>';
	}
	// Фасад
	if ($data['facade']>0 && isset($config['calculator_facade'][$data['facade']])) {
		// стоимость типа отделки
		$cost_facade = $config['calculator_facade'][$data['facade']]['value']
			* (
				(($data['width']>0 ? $data['width'] : 1) // ширина
					+ ($data['length']>0 ? $data['length'] : 1)) // длина
				* ($data['сeiling_height']>0 ? $config['calculator_сeiling_height'][$data['сeiling_height']]['name'] : 2.5) // высота
				* 2
			)
			* ($data['storeys']>0 ? $data['storeys'] : 1); // этажность
		//echo 'Фасад '.$cost_facade.'<br>';
	}
	// окна
	$cost_windows = ($data['windows']>0 && isset($config['calculator_windows'][$data['windows']])) ? $config['calculator_windows'][$data['windows']]['value'] : 0;
	// Коммуникации
	$cost_communications = ($data['communications']>0 && isset($config['calculator_communications'][$data['communications']])) ? $config['calculator_communications'][$data['communications']]['value'] : 0;
	// Фундамент
	$cost_foundation = ($data['foundation']>0 && isset($config['calculator_foundation'][$data['foundation']])) ? $config['calculator_foundation'][$data['foundation']]['value'] : 0;
	// Внутренняя отделка
	$cost_trim = ($data['trim']>0 && isset($config['calculator_trim'][$data['trim']])) ? $config['calculator_trim'][$data['trim']]['value'] * $sum_area : 0;
	//echo $cost_trim.'<br>';

	$total = $cost_area + $cost_roof + $cost_facade + $cost_windows + $cost_communications + $cost_foundation + $cost_trim;
	*/

    $total = $cost*$sum_area;
    /*
    if ($data['storeys']==2){
        $total_a = $total*0.9;
    }
    else{
        $total_a = $total;
    }/**/
	$result = array(
		'sum_area' => $sum_area,
		'total' => $total,
        'data' => $data_cost,
	);
	return $result;
}