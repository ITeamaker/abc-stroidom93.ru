<?php
/**
 * файл генерирует xml документ для поисковиков
 * доступен по адресу /sitemap.xml
 * в .htaccess есть настройка RewriteRule ^sitemap.xml$ sitemap.php [L]
 *
 * есть три варианта генерациия файла /admin.php?m=config#4
 * $config['sitemap_generation']==0 - файл не генерируется, ручная настройка sitemap.xml /admin.php?m=seo_sitemap
 * $config['sitemap_generation']==1 - файл генерируется из всех страниц, которые есть на сайте
 * $config['sitemap_generation']==2 - файл генерируется только из непроиндексированных страниц /admin.php?m=config#3
 *
 * todo:
 * нужно добавить обработку мультиязычных сайтов
 */

// загрузка настроек *********************************************************
define('ROOT_DIR', dirname(__FILE__).'/');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

// загрузка функций **********************************************************
require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
//require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
//require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
//require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
//require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
//require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
//require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

$pages = mysql_select("SELECT * FROM pages WHERE 1",'rows');

foreach ($pages as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','pages',$data);
}
//articles
$articles = mysql_select("SELECT * FROM articles WHERE 1",'rows');

foreach ($articles as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','articles',$data);
}
//buildings
$buildings = mysql_select("SELECT * FROM buildings WHERE 1",'rows');

foreach ($buildings as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','buildings',$data);
}
//designing_houses
$designing_houses = mysql_select("SELECT * FROM designing_houses WHERE 1",'rows');

foreach ($designing_houses as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','designing_houses',$data);
}
//projects
$projects = mysql_select("SELECT * FROM projects WHERE 1",'rows');

foreach ($projects as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','projects',$data);
}
//internal_finish
$internal_finish = mysql_select("SELECT * FROM internal_finish WHERE 1",'rows');

foreach ($internal_finish as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','internal_finish',$data);
}
//exterior_finish
$exterior_finish = mysql_select("SELECT * FROM exterior_finish WHERE 1",'rows');

foreach ($exterior_finish as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','exterior_finish',$data);
}
//plots
$plots = mysql_select("SELECT * FROM plots WHERE 1",'rows');

foreach ($plots as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','plots',$data);
}
//investment_projects
$investment_projects = mysql_select("SELECT * FROM investment_projects WHERE 1",'rows');

foreach ($investment_projects as $k=>$v){
    //print_r ($v);
    $data = array();
    $data['id'] = $v['id'];
    $data['lastmod'] = $v['lastmod']=='0000-00-00 00:00:00' ? date('Y-m-d H:i:s') : $v['lastmod'];
    mysql_fn('update','investment_projects',$data);
}
