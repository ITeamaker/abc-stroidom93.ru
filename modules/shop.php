<?php
$html['filter'] = $html['product_list'] = '';
$product = false;

//404 если есть $u[4]
if ($u[3]) {
	$error++;
}
// ТОВАР *******************************************************************
elseif ($u[2] AND $product = mysql_select("
		SELECT sp.*,sm.name material_name
		FROM shop_products sp
		LEFT JOIN shop_materials sm ON sm.id=sp.material
		WHERE sp.display = 1 AND sp.url = '".mysql_res($u[2])."'
		LIMIT 1
	",'row')) {
	if ($config['multilingual']) {
		$product['name'] = $product['name'.$lang['i']];
		$product['title'] = $product['title'.$lang['i']];
		$product['keywords'] = $product['keywords'.$lang['i']];
		$product['description'] = $product['description'.$lang['i']];
		$product['text'] = $product['text'].$lang['i'];
	}
	$page = array_merge($page,$product);

	$html['content'] = html_array('shop/product_text',$page);
	//вложенный breadcrumb
	$breadcrumb['module'][] = array($page['name'],get_url('shop_products',$page));
}
//список товаров
else {
    /*
	//загрузка функций для формы
	require_once(ROOT_DIR.'functions/form_func.php');
	//определение значений формы
	$fields = array(
		'areas'		=> 'text',
		'material'	=> 'text',
		'costs'		=> 'text',
		'garage'	=> 'int',
		'loft'		=> 'int',
		'terrace'	=> 'int',
		'floor'		=> 'text',
	);

	//создание массива $post
	$post = form_smart($fields,stripslashes_smart($_GET)); //print_r($post);

	$where = '';
	// площадь
	if ($post['areas'] AND $areas = explode(',',$post['areas'])){
		if (count($areas)>1){
			//print_r ($areas);
			$count_areas = 0;
			$where_area = '';
			foreach ($areas as $ka =>$va){
				if ($config['filter_areas'][$va]){
					$count_areas++;
					$min = $config['filter_areas'][$va]['min'] ? " sp.area>=" . $config['filter_areas'][$va]['min'] : '';
					$max = $config['filter_areas'][$va]['max'] ? ( $config['filter_areas'][$va]['min'] ? " AND sp.area<".$config['filter_areas'][$va]['max'] : " sp.area<".$config['filter_areas'][$va]['max']) : '';
					if ($count_areas==1) $where_area.= ' ( '.$min.' '.$max.' ) ';
					else $where_area.= ' OR ( '.$min.' '.$max.' ) ';
				}
			}
			$where .= ' AND ('.$where_area.')';
			//echo $where;
		}
		elseif ($config['filter_areas'][$areas[0]]){
			$where.= $config['filter_areas'][$areas[0]]['min'] ? " AND sp.area>=".$config['filter_areas'][$areas[0]]['min'] : '';
			$where.= $config['filter_areas'][$areas[0]]['max'] ? " AND sp.area<".$config['filter_areas'][$areas[0]]['max'] : '';
		}
	}
	// Материал
	if ($post['material']){
		$where.= " AND sp.material IN (".$post['material'].")";
	}
	// Стоимость
	if ($post['costs'] AND $costs = explode(',',$post['costs'])){
		if (count($costs)>1){
			//print_r ($costs);
			$count_costs = 0;
			$where_costs = '';
			foreach ($costs as $kc =>$vc){
				if ($config['filter_price'][$vc]){
					$count_costs++;
					$min = $config['filter_price'][$vc]['min'] ? " sp.price>=" . $config['filter_price'][$vc]['min'] : '';
					$max = $config['filter_price'][$vc]['max'] ? ( $config['filter_price'][$vc]['min'] ? " AND sp.price<".$config['filter_price'][$vc]['max'] : " sp.price<".$config['filter_price'][$vc]['max']) : '';
					if ($count_costs==1) $where_costs.= ' ( '.$min.' '.$max.' ) ';
					else $where_costs.= ' OR ( '.$min.' '.$max.' ) ';
				}
			}
			$where .= ' AND ('.$where_costs.')';
			//echo $where_costs;
		}
		elseif ($config['filter_price'][$costs[0]]){
			$where.= $config['filter_price'][$costs[0]]['min'] ? " AND sp.price>=".$config['filter_price'][$costs[0]]['min'] : '';
			$where.= $config['filter_price'][$costs[0]]['max'] ? " AND sp.price<".$config['filter_price'][$costs[0]]['max'] : '';
		}
	}
	//Параметры
	if ($post['garage']){
		$where.= " AND sp.garage =1";
	}
	if ($post['loft']){
		$where.= " AND sp.loft = 1";
	}
	if ($post['terrace']){
		$where.= " AND sp.terrace = 1";
	}
	//Этажность
	if ($post['floor']){
		$where.= " AND sp.floors IN (".$post['floor'].")";
	}
*/
	//фильтр
	//$html['filter'] = html_array('shop/product_filter',$post);
	//список товаров
	//$where
    /*
	$query = "
		SELECT sp.*, sm.name material_name
		FROM shop_products sp
		LEFT JOIN shop_materials sm ON sm.id=sp.material
		WHERE sp.display = 1
			$where
		GROUP BY sp.id
		ORDER BY sp.price
	"; //echo $query;

	$query_map = "
		SELECT sp.id, sp.id name
		FROM shop_products sp
		WHERE sp.display = 1
			$where
		GROUP BY sp.id
	"; //echo $query;
    */
    $query = "
        SELECT sp.*, sb.url category_url
        FROM projects sp
        LEFT JOIN projects_categories sb ON sb.id=sp.category
        WHERE sp.display = 1 AND sb.display=1 AND sp.portfolio=1
        ORDER BY sp.price ASC
	";
	//$coordinate = mysql_select($query_map,'array');
	//$html['product_list_map'] = html_array('shop/product_list_map',$coordinate);
	$html['product_list'] = html_query('shop/product_list shop',$query);
    $html['slider_portfolio'] = html_query('projects/projects_slider_top','
        SELECT *
		FROM portfolio_slider
		WHERE display = 1
		ORDER BY rank DESC
    ','');
}