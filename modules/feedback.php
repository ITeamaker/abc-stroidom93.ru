<?php
//загрузка функций для формы
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами

if ($u[2]) $error++;

//print_r ($_POST);
//die();
// задать вопрос
if (isset($_GET['action']) && $_GET['action']=='make_call' && count($_POST)>0){
//определение значений формы
	$fields = array(
		'name'			=>	'text',
		'phone'			=>	'required text',
        'form_page_url'	=>	'required text',
        'form_page_name'=>	'required text',
	);
	//создание массива $post
	$post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);

	//сообщения с ошибкой заполнения
	$message = form_validate($fields,$post);

	//если нет ошибок то отправляем сообщение
	if (count($message)==0) {
		$post['date'] = date('Y-m-d H:i:s');
		$post['id'] = mysql_fn('insert','feedback',$post);
		if ($post['id']){
			echo 1;
			require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
			mailer('make_call',$lang['id'],$post);
			//mailer('feedback',$lang['id'],$post,false,false,$post['email'],$files);
		}
		else echo i18n('validate|error_order');
	}
	else {
		echo html_array('form/message',$message);
	}
	die();
}
elseif (isset($_GET['action']) && $_GET['action']=='make_call_calculator' && count($_POST)>0){
//определение значений формы
		$fields = array(
			'name'			=>	'required text',
			'phone'			=>	'required text',
			'form_page_url'	=>	'required text',
			'form_page_name'=>	'required text',
			'width_calc'=>	'int',
			'length_calc'=>	'int',
			'storeys_calc'=>	'int',
			'сeiling_height_calc'=>	'int',
			'walls_calc'=>	'int',
			'foundation_calc'=>	'int',
			'roof_calc'=>	'int',
			'windows_calc'=>	'int',
			'facade_calc'=>	'int',
			'communications_calc'=>	'int',
			'trim_calc'=>	'int',

		);
	//$fields['communications_calc'] = select(@$fields['communications_calc'], $config['calculator_communications']);
	//print_r($fields['communications_calc']);

	//создание массива $post
		$post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);
		//сообщения с ошибкой заполнения
		$message = form_validate($fields,$post);

		//если нет ошибок то отправляем сообщение
		if (count($message)==0) {
			$post['storeys_calc'] = $config['calculator_storeys'][$post['storeys_calc']]['name'];
			$post['сeiling_height_calc'] = $config['calculator_сeiling_height'][$post['сeiling_height_calc']]['name'];
			$post['walls_calc'] = $config['calculator_walls'][$post['walls_calc']]['name'];
			$post['foundation_calc'] = $config['calculator_foundation'][$post['foundation_calc']]['name'];
			$post['roof_calc'] = $config['calculator_roof'][$post['roof_calc']]['name'];
			$post['windows_calc'] = $config['calculator_windows'][$post['windows_calc']]['name'];
			$post['facade_calc'] = $config['calculator_facade'][$post['facade_calc']]['name'];
			$post['communications_calc'] = $config['calculator_communications'][$post['communications_calc']]['name'];
			$post['trim_calc'] = $config['calculator_trim'][$post['trim_calc']]['name'];

			$post['date'] = date('Y-m-d H:i:s');
			$post['id'] = mysql_fn('insert','feedback_calc',$post);
			if ($post['id']){
				echo 1;
				require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
				mailer('make_call_calculator',$lang['id'],$post);

			}
			else echo i18n('validate|error_order');
		}
		else {
			echo html_array('form/message',$message);
		}
		die();

}
//обрабока формы
elseif (count($_POST)>0) {
	//определение значений формы
	$fields = array(
		'email'			=>	'required email',
		'name'			=>	'required text',
		'text'			=>	'required text',
		'captcha'		=>	'required captcha2'
	);
	//создание массива $post
	$post = form_smart($fields,stripslashes_smart($_POST));
	//print_r($post);

	//сообщения с ошибкой заполнения
	$message = form_validate($fields,$post);

	//если нет ошибок то отправляем сообщение
	if (count($message)==0) {
		unset($_SESSION['captcha'],$post['captcha']); //убиваем капчу чтобы второй раз не отправлялось
		//прикрепленные файлы
		$files = array();
		$post['files'] = array();
		if (isset($_FILES['attaches']['name']) AND is_array($_FILES['attaches']['name'])) {
			foreach ($_FILES['attaches']['name'] as $k=>$v) if ($v) {
				$name = trunslit($v);
				$files[$name] = $_FILES['attaches']['tmp_name'][$k];
				$post['files'][] = array(
					'name'=>$v,
					'file'=>$name
				);
			}
		}
		//запись сообщения в базу вместе с файлами
		$post['files'] = count($post['files']) ? serialize($post['files']) : '';
		$post['date'] = date('Y-m-d H:i:s');
		$post['id'] = mysql_fn('insert','feedback',$post);
		if ($post['files']) {
			$i = 0;
			foreach ($files as $k=>$v) {
				$path = ROOT_DIR.'files/feedback/'.$post['id'].'/files/'.$i.'/';
				mkdir($path,0755,true);
				copy($v,$path.$k);
				$i++;
			}

		}
		//6-й параметр кому ответить
		require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
		mailer('feedback',$lang['id'],$post,false,false,$post['email'],$files);
		$post['success'] = 1;
	}
	else {
		echo html_array('form/message',$message);
	}
	die();
	//if (count($message)>0) $post['message'] = $message;
}
$error++;
