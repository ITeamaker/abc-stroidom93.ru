<?php

$plots = false;

//404 если есть $u[3]
if ($u[3]) {
	$error++;
}
//одна запись
elseif ($u[2]) {
	if ($plots = mysql_select("
		SELECT pl.*, rg.name name_region
		FROM plots pl
		LEFT JOIN regions rg ON rg.id=pl.region
		WHERE pl.url = '".mysql_res($u[2])."' AND pl.display = 1 AND rg.display = 1 
	",'row')) {
		$page = array_merge($page,$plots);
		//print_r($page);
		$html['content'] = html_array('plots/text',$page);
		$breadcrumb['module'][] = array($page['name'],get_url('plots',$page));
	}
	else $error++;
}
// страница модуля
else {
	//загрузка функций для формы
	require_once(ROOT_DIR.'functions/form_func.php');
	//определение значений формы
	$fields = array(
		'regions'	=> 'text',
		'price'		=> 'int',
		'area'		=> 'int',
	);

	//создание массива $post
	$post = form_smart($fields,stripslashes_smart($_GET));

	$where = '';
	if ($post['regions']) $where.=" AND pl.region IN (".$post['regions'].")";

	$order = '';
	$sort = isset($_GET['sort'])?htmlspecialchars($_GET['sort']):'';

	$order = ' pl.price, pl.area';
	if($sort == 'price'){
		$order = ' pl.price ';
	}elseif ($sort == 'price_desc'){
		$order = ' pl.price DESC ';
	}elseif($sort == 'area'){
		$order = ' pl.area ';
	}elseif($sort == 'area_desc'){
		$order = ' pl.area DESC ';
	}

	$html['filter'] = html_array('plots/filter',$post);

	$query = "
		SELECT pl.*,rg.name name_region
		FROM plots pl
		LEFT JOIN regions rg ON rg.id=pl.region
		WHERE pl.display = 1 AND rg.display = 1
			$where
		ORDER BY ".$order."
	";

	//echo $query;
	$html['list'] = html_query('plots/list shop',$query);
}