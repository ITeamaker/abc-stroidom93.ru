<?php
//загрузка функций для формы
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами

// задать вопрос
if (isset($_GET['action']) && $_GET['action']=='order_services' && count($_POST)>0){
    //определение значений формы
    //[4.101] Правки в открытых формах на сайте.
	$fields = array(
		'name'			=>	'required text',
		'phone'			=>	'required text',
		'services'		=>	' text',
		'type'			=>	' int',
		'text'			=>	' text',
        'form_page_url'	=>	' text',
        'form_page_name'=>	' text',
		//'captcha'		=>	'required captcha2'
	);

	//создание массива $post
	$post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);

	//сообщения с ошибкой заполнения
	$message = form_validate($fields,$post);

	//если нет ошибок то отправляем сообщение
	if (count($message)==0) {
		$post['date'] = date('Y-m-d H:i:s');
        $post['services_text'] = $post['services'];
        unset($post['services']);
		$post['id'] = mysql_fn('insert','order_services',$post);
		if ($post['id']){
			echo 1;
			require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
			mailer('order_services',$lang['id'],$post);
		}
		else echo i18n('validate|error_order');
	}
	else {
		echo html_array('form/message',$message);
	}
	die();
}
// задать вопрос
elseif (isset($_GET['action']) && $_GET['action']=='order_investment_projects' && count($_POST)>0){
//определение значений формы
    $fields = array(
        'name'			=>	'required text',
        'phone'			=>	'required text',
        'services'		=>	' int',
        'text'			=>	' text',
        'form_page_url'	=>	'required text',
        'form_page_name'=>	'required text',
        //'captcha'		=>	'required captcha2'
    );
    //создание массива $post
    $post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);

    //сообщения с ошибкой заполнения
    $message = form_validate($fields,$post);

    //если нет ошибок то отправляем сообщение
    if (count($message)==0) {
        $post['date'] = date('Y-m-d H:i:s');
        $post['order_type'] = 10;
        $post['id'] = mysql_fn('insert','order_services',$post);
        if ($post['id']){
            echo 1;
            require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
            mailer('order_investment_projects',$lang['id'],$post);
        }
        else echo i18n('validate|error_order');
    }
    else {
        echo html_array('form/message',$message);
    }
    die();
}
// задать вопрос
elseif (isset($_GET['action']) && $_GET['action']=='order_buildings' && count($_POST)>0){
//определение значений формы
    //определение значений формы
    //[4.101] Правки в открытых формах на сайте.
    $fields = array(
        'name'			=>	'required text',
        'phone'			=>	'required text',
        'services'		=>	' int',
        'type'			=>	' int',
        'text'			=>	' text',
        'form_page_url'	=>	'required text',
        'form_page_name'=>	'required text',
        //'captcha'		=>	'required captcha2'
    );


    //создание массива $post
    $post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);

    //сообщения с ошибкой заполнения
    $message = form_validate($fields,$post);

    //если нет ошибок то отправляем сообщение
    if (count($message)==0) {
        $post['date'] = date('Y-m-d H:i:s');
        $post['id'] = mysql_fn('insert','order_services',$post);
        if ($post['id']){
            echo 1;
            require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
            mailer('order_services',$lang['id'],$post);
        }
        else echo i18n('validate|error_order');
    }
    else {
        echo html_array('form/message',$message);
    }
    die();
}