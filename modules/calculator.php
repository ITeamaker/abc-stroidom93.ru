<?php

$calculator = false;

//404 если есть $u[3]
	if ($u[2]) {
		$error++;
	}
	if (count($_POST) > 0) {
		$json['done'] = false;
		//загрузка функций для формы
		require_once(ROOT_DIR . 'functions/form_func.php');    //функции для работы со формами
		//определение значений формы
		$fields = array(
			'width' => 'required decimal',
			'length' => 'required decimal',
			'foundation' => 'required int',
			'storeys' => 'required int',
			'сeiling_height' => 'required int',
			'roof' => 'required int',
			'facade' => 'required int',
			'windows' => 'required int',
			'communications' => 'required int',
			'trim' => 'required int',
            'walls'=>'required int',
		);
		//создание массива $post
		$post = form_smart($fields, stripslashes_smart($_POST)); //print_r($_POST);

		//сообщения с ошибкой заполнения
		$message = form_validate($fields, $post);

		// формула рассчета если нет ошибок
		if (count($message) == 0) {
			$data = calculator($post);
			$json = array(
				'done' => true,
				'html' => '<span class="result_cost_main">' . number_format($data['total'], 0, '.', ' ') . ' р.</span><span class="cost_sq">(' . number_format($data['total'] / $data['sum_area'], 0, '.', ' ') . ' р./м2)</span>',
			);
		}else {
			$post = array();
			$json['message'] = html_array('form/message', $message);
		}
		echo json_encode($json);
		die();
	}

// данные по умолчанию
	$data = array(
		'width' => 10,
		'length' => 9,
		'foundation' => 1,
		'storeys' => 1,//
		'сeiling_height' => 1,//
		'roof' => 1,//
		'facade' => 1,//
		'windows' => 1,//
		'communications' => 1,//
		'trim' => 2,
        'walls'=>1,
	);

	$data_calculator = calculator($data);
	$data['total'] = $data_calculator['total'];
	$data['sum_area'] = $data_calculator['sum_area'];

	$html['content'] = html_array('calculator/calculator', $data);

