<?php
//require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
//require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
//require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
//require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
//require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

//основной язык
$lang = lang(1);

$json = array();

$json['done'] = false;

$fields = array(
    'id'			=>	'required int',
);
//создание массива $post
$post = form_smart($fields,stripslashes_smart($_GET)); //print_r($post);

//сообщения с ошибкой заполнения
$message = form_validate($fields,$post);

//если нет ошибок то отправляем сообщение
if (count($message)==0) {
	$link = $post['id'] == 5 ? '<a class="ya-phone-1" href="tel:'.i18n('common|phone'.$post['id'].'_link').'">'.i18n('common|phone'.$post['id']).'</a>' : ($post['id'] == 4 ? '<a class="ya-phone-2" href="tel:'.i18n('common|phone'.$post['id'].'_link').'">'.i18n('common|phone'.$post['id']).'</a>' : '<a href="tel:'.i18n('common|phone'.$post['id'].'_link').'">'.i18n('common|phone'.$post['id']).'</a>');
    $json = array(
        'done'	        =>	true,
        'address_data'	    =>	'<div class="address" data-address="'.$post['id'].'"><span class="name_wrap"><span class="name">'.i18n('common|address_city'.$post['id']).'</span></span><span style="margin-left: 10px;">'.$link.'</span></div>',
        'email_data'    =>	'<a href="mailto:'.i18n('common|email'.$post['id']).'">'.i18n('common|email'.$post['id']).'</a>',
        'working_hours_data'=>	i18n('common|working_days_name').' '.i18n('common|working_hours'.$post['id']),
    );
}
//print_r ($json);
echo json_encode($json);

//<?=($office_type==5 ? '<span class="ya-phone-1" >'.i18n('common|phone'.$office_type).'</span>' : '<a href="tel:'.i18n('common|phone'.$office_type.'_link').'">'.i18n('common|phone'.$office_type).'</a>')