<?php

require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
//require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

//основной язык
$lang = lang(1);

//список модулей на сайте
$modules = mysql_select("SELECT url name,module id FROM pages WHERE module!='pages' AND language=".$lang['id']." AND display=1",'array',60*60);

//определение значений формы
$fields = array(
	'email'			=> 'required email',
	'form_page_url'	=>	'required text',
	'form_page_name'=>	'required text',
);
//создание массива $post
$post = form_smart($fields,stripslashes_smart($_POST)); //print_r($post);

//сообщения с ошибкой заполнения
$message = form_validate($fields,$post);

//если нет ошибок то отправляем сообщение
if (count($message)==0) {
	$data_files = unserialize($lang['contract']);
	/*
	 * a:1:{i:1;a:3:{s:4:"temp";s:0:"";s:4:"file";s:15:"dogovor-smr.doc";s:4:"name";s:11:"dogovor-smr";}}
	 * */
	if (count($data_files)>0){
		$files = array();
		foreach ($data_files as $k=>$v) if ($v['file']){
			$files[$v['file']] = ROOT_DIR.'files/languages/1/contract/'.$k.'/'.$v['file'];
		}
	}
	//запись сообщения в базу вместе с файлами
	$post['files'] = $lang['contract'];
	$post['date'] = date('Y-m-d H:i:s');
	$post['id'] = mysql_fn('insert','contract_application',$post);

	//print_r ($files);
	//die();
	//6-й параметр кому ответить
	mailer('contract_aplication',$lang['id'],$post,false,false,$post['email'],$files);/**/
	mailer('contract_aplication',$lang['id'],$post,$post['email'],false,false,$files);/**/
	echo 1;
}
else {
	echo html_array('form/message',$message);
}
