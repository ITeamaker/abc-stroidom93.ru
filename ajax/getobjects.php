<?php
//require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
//require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
//require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
//require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
//require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

//основной язык
$lang = lang(1);

//список модулей на сайте
$modules = mysql_select("SELECT url name,module id FROM pages WHERE module!='pages' AND language=".$lang['id']." AND display=1",'array',60*60);

$json = array();

switch ($_GET['type'])
{
	case 'portfolio':
		$fields = array(
			'id'	=> 'text',
		);
		$post = form_smart($fields,stripslashes_smart($_GET)); //print_r($post);
		if ($post['id'] && $post['id']!=''){
			$query = "
				SELECT * 
				FROM shop_products 
				WHERE 1 "
					." AND id IN (".$post['id'].") "
					." AND `lat` !=0 AND `lng` !=0 
					AND `display` = 1 
				ORDER BY id ASC";
			$objects = mysql_select($query,'rows');
			$json['objects']['type'] = 'FeatureCollection';
			$number = 0;
			foreach ( $objects as $k=>$obj )
			{
				$json['objects']['features'][$number] = array(
					'type' => 'Feature',
					'id' => (int)$number,
					'geometry' => array(
						'type' => 'Point',
						'coordinates' => array((float)$obj['lat'],(float)$obj['lng'])
					),
					'properties' => array(
						//'balloonContentHeader' => htmlspecialchars($obj['name']),
						'balloonContent' => html_array("shop/yaMapBallon",$obj),//htmlspecialchars($obj['name']),
						'hintContent' => $obj['title'],
					),
					'options' => array(
						'iconLayout' => 'default#image',
						//'iconImageHref' => '/files/infrastructure_types/{$obj['type']}/{$obj['icon']}',
						'iconImageHref' => '/templates/images/point_map.png',
						'iconImageSize' => array(51,52),
						'iconImageOffset' => array(-25, -52),
					),
				);
				$number++;
			}
		}
		break;
    default:
        break;
}
//print_r ($json);
echo json_encode($json);