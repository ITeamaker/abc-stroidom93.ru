<?php

$config['cms_version'] = '1.1.13-02';

$config['multilingual'] = false; //многоязычный сайт

//database
$config['mysql_server']		= 'localhost';
$config['mysql_username']	= 'ck01672_stroydom';
$config['mysql_password']	= 'NxdsIMft';
$config['mysql_database']	= 'ck01672_stroydom';
//исключение для локальной версии
if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' AND $_SERVER['SERVER_ADDR']=='127.0.0.1') {
	$config['mysql_server'] = 'localhost';
	$config['mysql_username'] = 'root';
	$config['mysql_password'] = '';
	$config['mysql_database'] = 'abc-stroidom93.ru';
}
$config['mysql_charset']	= 'UTF8';
$config['mysql_connect']	= false; //по умолчанию база не подключена
$config['mysql_error']		= false; //ошибка подключения к базе

//timezone
$config['timezone']			= 'Europe/Moscow';

//папка со стилями
$config['style'] = 'templates';

//charset
$config['charset']			= 'UTF-8';

//debug
$config['debug'] = false; //если поставить true то будут писаться все логи log_add

$config['offices'] = array(
	//3=>	'Адлер',
	3=> 'Сочи',
    //2=> 'Гагра',
    //5=> 'Москва',
    //4=> 'С.Петербург',
);

$config['services_types'] = array(
	1=> array(
		'name'	=> 'Строительство домов',
		'type' 	=> 'buildings'
	),
	2=> array(
		'name'	=> 'Проектирование домов',
		'type' 	=> 'designing_houses'
	),
	3=> array(
		'name'	=> 'Внутренняя отделка',
		'type' 	=> 'internal_finish'
	),
	4=> array(
		'name'	=> 'Внешняя отделка',
		'type' 	=> 'exterior_finish'
	),
	5=> array(
		'name'	=> 'Кладка стен',
		'type' 	=> 'masonry_walls'
	),
	6=> array(
		'name'	=> 'Заливка фундамента',
		'type' 	=> 'pouring_foundation'
	),
	7=> array(
		'name'	=> 'Монтаж кровли',
		'type' 	=> 'installation_roof'
	),
	8=> array(
		'name'	=> 'Продажа участков',
		'type' 	=> 'plots'
	),
);

// этажность
$config['shop_product_floor'] = array(
	1=> '1 этаж',
	2=> '2 этажа',
	3=>	'3 этажа',
	//3=> 'Много этажный дом',
	//4=> 'Гостинница',
);
// этажность
$config['shop_product_floor_filter'] = array(
	1=> '1 этажные дома',
	2=> '2х этажные дома',
	3=> '3х этажные дома',
	//3=> 'Много этажные дома',
	//4=> 'Гостинницы',
);

// площадь
$config['filter_areas'] = array(
	1=> array(
		'name' => 'меньше 40',
		'max' => 40,
	),
	2=> array(
		'name' => '40-60',
		'min' => 40,
		'max' => 60,
	),
	3=> array(
		'name' => '61-90',
		'min' => 61,
		'max' => 90
	),
	4=> array(
		'name' => '91-120',
		'min' => 91,
		'max' => 120,
	),
	5=> array(
		'name' => '121-150',
		'min' => 121,
		'max' => 150,
	),
	6=> array(
		'name' => '151-200',
		'min' => 151,
		'max' => 200,
	),
	7=> array(
		'name' => 'более 200 соток',
		'min' => 200,
	),
);

// цена
// 1-2 млн. 2-5 млн. 5-10 млн.
$config['filter_price'] = array(
	1=> array(
		'name' => '1-2 млн.',
		'min' => 1000000,
		'max' => 2000000,
	),
	2=> array(
		'name' => '2-5 млн.',
		'min' => 2000000,
		'max' => 5000000,
	),
	3=> array(
		'name' => '5-10 млн.',
		'min' => 5000000,
		'max' => 10000000,
	),
	4=> array(
		'name' => 'более 10 млн.',
		'min' => 10000000,
	),
);

/*калькулятор*/
// этажность
$config['calculator_storeys'] = array(
	1=> array(
		'name'	=> 1,
		'value'	=> 1500,

	),
    2=> array(
        'name'	=> '1+ мансарда',
        'value'	=> 0,
    ),
	3=> array(
		'name'	=> 2,
		'value'	=> 0,
	),

);
// высота потолков
$config['calculator_сeiling_height'] = array(
	1=> array(
		'name'	=> 2.5,
		'value'	=> 0,
	),
	2=> array(
		'name'	=> 2.7,
		'value'	=> 300,
	),
	3=> array(
		'name'	=> 3,
		'value'	=> 500,
	),
);
//кровля
$config['calculator_roof'] = array(
	1=> array(
		'name'	=> 'Металлочерепица',
		'value'	=> 1500,
	),
	2=> array(
		'name'	=> 'Мягкая черепица',
		'value'	=> 2000,
	),
);

// Отделка внешних стен
$config['calculator_facade'] = array(
	1=> array(
		'name'	=> 'Сайдинг',
		'value'	=> 1500,
	),
	2=> array(
		'name'	=> 'Имитация бруса',
		'value'	=> 1500,
	),
	3=> array(
		'name'	=> 'Имитация кирпича',
		'value'	=> 1500,
	),
	4=> array(
		'name'	=> 'Короед',
		'value'	=> 2500,
	),
    5=> array(
        'name'	=> 'Не требуется',
        'value'	=> 0,
    ),
    //не требуется
);
// стены и от стен зависит стоимость фундамента
$config['calculator_walls'] = array(
    1=> array(
        'name'	=> 'Каркасный',
        'value'	=> 5500,
        'foundation' => 1200,
    ),
    2=> array(
        'name'	=> 'Сип панель',
        'value'	=> 6500,
        'foundation' => 2200,
    ),
    3=> array(
        'name'	=> 'Брус',
        'value'	=> 6500,
        'foundation' => 2200,
    ),
    4=> array(
        'name'	=> 'Каменный',
        'value'	=> 7500,
        'foundation' => 3200,
    ),
);
//foundation
$config['calculator_foundation'] = array(
	1=> array(
		'name'	=> 'Требуется ',
	),
	2=> array(
		'name'	=> 'Не требуется',
	),
);
$config['calculator_windows'] = array(
	1=> array(
		'name'	=> 'Требуются ',
		'value'	=> 800,
	),
	2=> array(
		'name'	=> 'Не требуются',
		'value'	=> 0,
	),
);
//foundation
$config['calculator_trim'] = array(
	1=> array(
		'name'	=> 'Не требуется ',
	),
	2=> array(
		'name'	=> 'Предчистовая',
	),
    3=> array(
        'name'	=> 'Под ключ',
    ),
);
//Коммуникации
$config['calculator_communications'] = array(
	1=> array(
		'name'	=> 'Требуются ',
		'value'	=> 1500,
	),
	2=> array(
		'name'	=> 'Не требуются',
		'value'	=> 0,
	),
);

//виды оплат (мерчанты) - по умолчанию закомментированы
/**/
$config['payments'] = array(
	//без оплаты
	1=> 'безналичный рассчет',
	//robokassa
	//100 => 'robokassa',
	//101 => 'robokassa [терминал]',
	//102 => 'robokassa [qiwi]',
	//103 => 'robokassa [карта]',
	//104 => 'robokassa [wmr]',
	//105 => 'robokassa [yandex]',
	//yandex
	//200 => 'yandex',
	//201 => 'yandex [yandexmoney]',
	//202 => 'yandex [карта]',
	//203 => 'yandex [webmoney]',
	//204 => 'yandex [qiwi]',
	//todo qiwi
	//300 => 'qiwi',
	//todo alfabank
	//400 => 'alfabanki',
	//liqpay privatbank
	//500 => 'liqpay',//v.1.1.2
	//paypal
	//600 => 'paypal',
	//todo 2checkout
	//700 => '2checkout',
);
/**/

//массив всех подключаемых css и js файлов
//{localization} - будет заменяться на $lang['localization']
//? будет заменятся на гет параметр времени создания сайта
$config['sources'] = array(
	'bootstrap.css'             => '/plugins/bootstrap/css/bootstrap.min.css',
	'bootstrap.js'              => '/plugins/bootstrap/js/bootstrap.min.js',
	'slick.css'             	=> '/plugins/slick/slick.css',
	'slick.js'              	=> '/plugins/slick/slick.min.js',
	'fancybox.css'             	=> '/plugins/fancy/source/jquery.fancybox.css?',
	'fancybox.js'              	=> '/plugins/fancy/source/jquery.fancybox.pack.js',
	'mask.js'					=> '/plugins/jQuery-Mask/dist/jquery.mask.min.js',
	'common.css'				=> '/templates/css/common.css?',
	'common.js'				    => '/templates/scripts/common.js?',
	'nouislider.css'			=> '/plugins/noUiSlider/nouislider.css?',
	'newslider.css'				=> '/templates/css/newslider.css?',
	'newslider.js'				=> '/templates/scripts/newslider.js?',
	'nouislider.js'				=> array(
		'/plugins/noUiSlider/wNumb.js',
		'/plugins/noUiSlider/nouislider.js?',
	),
	'editable.js'				=> '/templates/scripts/editable.js',
	'font.css'				    => '/templates/css/font.css',
	'highslide'					=> array(
		'/plugins/highslide/highslide.packed.js',
		'/plugins/highslide/highslide.css',
	),
	'highslide_gallery.css' 	=> array(
		'/plugins/highslide/highslide.css',
	),
	'highslide_gallery' 		=> array(
		'/plugins/highslide/highslide-with-gallery.js',
		'/templates/scripts/highslide.js',
	),
	'jquery.js'					=> '/plugins/jquery/jquery-1.11.3.min.js',
	'jquery_cookie.js'			=> '/plugins/jquery/jquery.cookie.js',
	'jquery_ui.js'				=> '/plugins/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js',
	'jquery_ui.css'			    => '/plugins/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.css',
	'jquery_localization.js'	=> '/plugins/jquery/i18n/jquery.ui.datepicker-{localization}.js',
	'jquery_form.js'			=> '/plugins/jquery/jquery.form.min.js',
	'jquery_uploader.js'		=> '/plugins/jquery/jquery.uploader.js',
	'jquery_validate.js'		=> array(
		'/plugins/jquery/jquery-validation-1.8.1/jquery.validate.min.js',
		'/plugins/jquery/jquery-validation-1.8.1/additional-methods.min.js',
		'/plugins/jquery/jquery-validation-1.8.1/localization/messages_{localization}.js',
	),
	'jquery_multidatespicker.js'=> '/plugins/jquery/jquery-ui.multidatespicker.js',
	'reset.css'					=> '/templates/css/reset.css',
	'tinymce.js'				=> '/plugins/tinymce/tinymce.min.js',//старый тинумайс
	'tinymce.js'				=> '/plugins/tinymce_4.3.11/tinymce.min.js',
);

error_reporting(E_ALL);
error_reporting(0);
//исключение для локальной версии
if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' AND $_SERVER['SERVER_ADDR']=='127.0.0.1') {
	set_error_handler('error_handler');
}
else {
	set_error_handler('error_handler');
}

date_default_timezone_set($config['timezone']);
ini_set('session.cookie_lifetime', 0);
ini_set('magic_quotes_gpc', 0);

header('Content-type: text/html; charset='.$config['charset']);
header('X-UA-Compatible: IE=edge,chrome=1');

//обработчик ошибок
function error_handler($errno,$errmsg,$file,$line) {
	// Этот код ошибки не включен в error_reporting
	if (in_array($errno,array(8192,8))) return;
	//if (!(error_reporting() & $errno)) return;
	//не фиксируем простые ошибки
	if ($errno==E_USER_NOTICE) return true;
	//запись в файл
	$log_file_name = 'error_'.date('Y-m').'.txt';
	$err_str = date('d H:i');
	$err_str.= "\t".$errno;
	$err_str.= "\tfile://".$file;
	$err_str.= "\t".$line;
	$err_str.= "\thttp://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	$err_str.= "\t".$errmsg;
	$err_str.= "\r\n";
	if (!is_dir(ROOT_DIR.'logs')) mkdir(ROOT_DIR.'logs');
	$fp = fopen(ROOT_DIR.'logs/'.$log_file_name, 'a');
	fwrite($fp,$err_str);
	fclose($fp);
	//фатальная ошибка
	if ($errno==E_USER_ERROR) exit(1);
	//не запускаем внутренний обработчик ошибок PHP
	return true;
}

/**
 * v.1.1.0 - генерация урл
 * @param $type - тип урл
 * @param bool $q - массив значений
 * @return string - урл
 */
function get_url($type='',$q=false){
	global $modules,$config,$lang;
	$url = '';
	//старницы
	if ($type=='page') {
		if (strstr($q['url'],'?')) $url = '/' . $q['url'] . '';
		else $url = $q['module'] == 'index' ? '/' : '/' . $q['url'] . '/';
	}
	//товары
	elseif ($type=='shop_product') {
		if (@$modules['shop']) {
			$url = '/' . $modules['shop'] . '/' . $q['url'.$lang['i']] . '/';
		}
		else $url = '#';
	}
	/*
	//категории
	elseif ($type=='shop_category') {
		if (@$modules['shop']) {
			$url = '/' . $modules['shop'] . '/' . $q['id'] . '-' . $q['url' .$lang['i']]. '/';
		}
		else $url = '#';
	}
	*/
    elseif ($type=='projects') {
        if (@$modules['projects']) {
            $url = '/' . $modules['projects'] . '/' . $q['category_url'] . '/' . $q['url'] . '/';
        }
        else $url = '#';
    }
    //категории
    elseif ($type=='projects_category') {
        if (@$modules['projects']) {
            $url = '/' . $modules['projects'] . '/' . $q['url']. '/';
        }
        else $url = '#';
    }/*
	//новости
	elseif ($type=='projects') {
		if (@$modules['projects']) {
			$url = '/' . $modules['projects'] . '/' . $q['url']. '/';
		}
		else $url = '#';
	}
*/
	//Готовые обьекты
	elseif ($type=='finished_objects') {
		if (@$modules['finished_objects']) {
			$url = '/' . $modules['finished_objects'] . '/' . $q['url']. '/';
		}
		else $url = '#';
	}
	//Готовые обьекты
	elseif ($type=='finished_objects_page') {
		if (@$modules['finished_objects']) {
			$url = '/' . $modules['finished_objects'] . '/' . $q['url']. '/';
		}
		else $url = '#';
	}


	//новости
	elseif ($type=='news') {
		if (@$modules['news']) {
			$url = '/' . $modules['news'] . '/' . $q['url']. '/';
		}
		else $url = '#';
	}
    //статьи
    elseif ($type=='articles') {
        if (@$modules['articles']) {
            $url = '/' . $modules['articles'] . '/' . $q['url']. '/';
        }
        else $url = '#';
    }
	//отзывы
	elseif ($type=='reviews') {
		if (@$modules['reviews']) {
			$url = '/' . $modules['reviews'] . '/' . $q['url']. '/';
		}
		else $url = '#';
	}
	//строительство
	elseif ($type=='buildings') {
		if (@$modules['buildings']) {
			$url = '/'.$modules['buildings'].'/'.$q['url'].'/';
		}
		else $url = '#';
	}
	//выгоди инвестиций
	elseif ($type=='investments') {
		if (@$modules['investments']) {
			$url = '/'.$modules['investments'].'/benefit/'.$q['url'].'/';
		}
		else $url = '#';
	}
	//программы инвестиций
	elseif ($type=='investment_projects') {
		if (@$modules['investments']) {
			$url = '/'.$modules['investments'].'/'.$q['url'].'/';
		}
		else $url = '#';
	}
	// проектирование
	elseif ($type=='designing_houses') {
		if (@$modules['designing_houses']) {
			$url = '/'.$modules['designing_houses'].'/'.$q['url'].'/';
		}
		else $url = '#';
	}
	// проекты
	elseif ($type=='projects') {
		if (@$modules['projects']) {
			$url = '/'.$modules['projects'].'/'.$q['url'].'/';
		}
		else $url = '#';
	}
	// внутренняя отделка
	elseif ($type=='internal_finish') {
		if (@$modules['internal_finish']) {
			$url = '/'.$modules['internal_finish'].'/'.$q['url'].'/';
		}
		else $url = '#';
	}
	// внешняя отделка
	elseif ($type=='exterior_finish') {
		if (@$modules['exterior_finish']) {
			$url = '/'.$modules['exterior_finish'].'/'.$q['url'].'/';
		}
		else $url = '#';
	}
	// участки
	elseif ($type=='plots') {
		if (@$modules['plots']) {
			$url = '/'.$modules['plots'].'/'.$q['url'].'/';
		}
		else $url = '#';
	}
	//галлерея
	elseif ($type=='gallery') {
		if (@$modules['gallery']) {
			$url = '/' . $modules['gallery'] . '/' .  $q['id'] . '-'.$q['url']. '/';
		}
		else $url = '#';
	}
	//корзина
	elseif ($type=='basket') {
		if (@$modules['basket']) {
			$url = '/' . $modules['basket'] . '/';
			if ($q) {
				$url.= $q['id'].'/'.md5($q['id'].$q['date']).'/';
			}
		}
		else $url = '#';
	}
	//добавляем в коце язык
	if ($config['multilingual']) {
		$url = '/'.$lang['url'].$url;
	}
	return $url;
}