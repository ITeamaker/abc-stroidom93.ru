<?php

//собирает все css в один бандл
// /api/bundle/common.css
//define('ROOT_DIR', dirname(__FILE__).'/');


header('Content-type: text/css; charset=UTF-8');

define('ROOT_DIR', dirname(__FILE__).'/');


include_once(ROOT_DIR . 'plugins/bootstrap/css/bootstrap.min.css');
include_once(ROOT_DIR . 'plugins/slick/slick.css');
include_once(ROOT_DIR . 'plugins/noUiSlider/nouislider.css');
include_once(ROOT_DIR . 'plugins/fancy/source/jquery.fancybox.css');
include_once(ROOT_DIR . 'templates/css/common.css');
include_once(ROOT_DIR . 'templates/css/newslider.css');


die();