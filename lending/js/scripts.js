
$(document).ready(function() {
	
		$('.callbackkiller.cbk-phone').click(function () {
			ga('send', 'event', 'order', 'click', 'phone'); 
			yaCounter40299509.reachGoal('phone');
			return true;
		});

		//$('input[name=phone]').mask('+?(999) 999-?99-99');

    $("#owl2").owlCarousel({
        navigation : false, // Show next and prev buttons
        slideSpeed : 1000,
        paginationSpeed: 1000,
        pagination: false,
        stopOnHover: true,
        mouseDrag: false,
        touchDrag: false,
        items: 3
        // "singleItem:true" is a shortcut for:
        // items : 1,
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });

    $("#owl3").owlCarousel({
        navigation : false, // Show next and prev buttons
        slideSpeed : 1000,
        paginationSpeed: 1000,
        pagination: false,
        stopOnHover: true,
        mouseDrag: false,
        touchDrag: false,
        items: 3
    });

    $("#owl1").owlCarousel({
        navigation : false, // Show next and prev buttons
        slideSpeed : 1000,
        paginationSpeed: 1000,
        pagination: false,
        stopOnHover: true,
        mouseDrag: false,
        touchDrag: false,
        singleItem:true
    });

    var owl2 = $("#owl2").data('owlCarousel');

    $("#owl2buttons .prev").click(function(){
        owl2.prev();
        return false;
    });
    $("#owl2buttons .next").click(function(){
        owl2.next();
        return false;
    });

    var owl1 = $("#owl1").data('owlCarousel');

    $("#owl1buttons .prev").click(function(){
        owl1.prev();
        return false;
    });
    $("#owl1buttons .next").click(function(){
        owl1.next();
        return false;
    });

    var owl3 = $("#owl3").data('owlCarousel');

    $("#owl3buttons .prev").click(function(){
        owl3.prev();
        return false;
    });
    $("#owl3buttons .next").click(function(){
        owl3.next();
        return false;
    });

    $("#owl2 a[rel=nofollow]").colorbox();

    $(".inline").colorbox({inline:true,fixed:true});
	
	$(document).scroll(function () {

        var y = $(this).scrollTop();
        var nav = $("#main");
        var off = $(nav).offset().top;
        if (y > off) {
            $("#nav").css("position","fixed");
            $("#nav").css("top","0");
            $("#nav").css("left","0");
            $("#nav").css("width","100%");
			$("#nav").css("background","rgba(194,194,194,0.9)");
            

        } else {
            $("#nav").css("position","absolute");
            $("#nav").css("top","110px");
            $("#nav").css("background","rgba(255,255,255,0.7)");
          
        }
    });
	
	$('nav a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top-50
				}, 1000);
				return false;
			}
		}
	});
	$('.tehno a').click(function() {
		var act = 'mail.php?act=projects&house='+$(this).parent().attr('id');
		$('#f2').attr('action',act);	
	});
	$('.spec a').click(function() {
		var act = 'mail.php?act=consult';
		$('#f2').attr('action',act);	
	});
	
	$( ".ajaxForm" ).submit(function( event ) {        
        event.preventDefault();
        var form = $(this);
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: form.serialize(),
            success: function (html) {
                $("#restext").html(html);
                console.log("asda1111");
                $("#reslink").click();
            }
        });
    });
	
  /*  $(document).scroll(function () {

        var y = $(this).scrollTop();
        var nav = $("#head");
        var off = $(nav).offset().top;
        if (y > off) {
            $("#nav").css("position","fixed");
            $("#nav").css("top","0");
            $("#nav").css("left","0");
            $("#nav").css("width","100%");
            $("#nav").fadeTo(500,0.7);

        } else {
            $("#nav").css("position","relative");
            $("#nav").fadeTo(500,1);
        }

        checkanim();
    });
    $("#nav").hover(function(){
            $("#nav").fadeTo(300,1);
        },
        function(){
            $("#nav").fadeTo(300,0.7);
        }
    );

    

*/

});
