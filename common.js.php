<?php

//собирает все js в один бандл
// /api/bundle/common.js

header('Content-Type: application/javascript; charset=UTF-8');
define('ROOT_DIR', dirname(__FILE__).'/');
include_once(ROOT_DIR . 'plugins/jquery/jquery-1.11.3.min.js');
//include_once(ROOT_DIR . 'plugins/tinymce/tinymce.min.js');
include_once(ROOT_DIR . 'plugins/slick/slick.js');
include_once(ROOT_DIR . 'plugins/slick/slick.min.js');
include_once(ROOT_DIR . 'plugins/bootstrap/js/bootstrap.min.js');
include_once(ROOT_DIR . 'plugins/fancy/source/jquery.fancybox.pack.js');
include_once(ROOT_DIR . 'plugins/jQuery-Mask/dist/jquery.mask.min.js');
include_once(ROOT_DIR . 'templates/scripts/newslider.js');
include_once(ROOT_DIR . 'plugins/noUiSlider/wNumb.js');
include_once(ROOT_DIR . 'plugins/noUiSlider/nouislider.js');
include_once(ROOT_DIR . 'templates/scripts/common.js');
include_once(ROOT_DIR . 'plugins/jquery/jquery-validation-1.8.1/jquery.validate.min.js');
include_once(ROOT_DIR . 'plugins/jquery/jquery-validation-1.8.1/additional-methods.min.js');
include_once(ROOT_DIR . 'plugins/jquery/jquery-validation-1.8.1/localization/messages_ru.js');
include_once(ROOT_DIR . 'plugins/jquery/jquery.form.min.js');

/*
include_once(ROOT_DIR . 'plugins/tinymce_4.3.11/tinymce.min.js');


/**/
die();