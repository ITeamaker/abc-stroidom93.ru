<?php

/**
 * файл генерирует xml документ для поисковиков
 * доступен по адресу /sitemap.xml
 * в .htaccess есть настройка RewriteRule ^sitemap.xml$ sitemap.php [L]
 *
 * есть три варианта генерациия файла /admin.php?m=config#4
 * $config['sitemap_generation']==0 - файл не генерируется, ручная настройка sitemap.xml /admin.php?m=seo_sitemap
 * $config['sitemap_generation']==1 - файл генерируется из всех страниц, которые есть на сайте
 * $config['sitemap_generation']==2 - файл генерируется только из непроиндексированных страниц /admin.php?m=config#3
 *
 */

// загрузка настроек *********************************************************
define('ROOT_DIR', dirname(__FILE__).'/');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

// загрузка функций **********************************************************
//require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
//require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
//require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
//require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
//require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
//require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
//require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

$config['cache'] = false;

header('Content-type: text/xml; charset=UTF-8');

$query = mysql_select("SELECT * FROM pars WHERE datebegin_check=1", "rows_id");

$content = " <Ads formatVersion=\"3\" target=\"Avito.ru\"> ";

 foreach ($query as $k=>$v){

	$images = unserialize($v['images']);

	$content.= "<Ad>";
	$content.= "<Id>".$v['id']."</Id>";


	/**/
	if(@isset($v['datebegin']) && $v['datebegin']!=''){
		$content.= "<DateBegin>".$v['datebegin']."</DateBegin>";
	}
	if(@isset($v['dateend']) && $v['dateend']!=''){
		//$content.= "<DateEnd>".$v['dateend']."</DateEnd>";
	}
	/**/

	$content.= "<Category>".$v['category']."</Category>";
	$content.= "<OperationType>".$v['operationtype']."</OperationType>";
	$content.= "<PropertyRights>".$v['propertyrights']."</PropertyRights>";
	if($v['category']=='Дома, дачи, коттеджи'){
		$content.= "<ObjectType>".$v['objecttype']."</ObjectType>";
		$content.= "<WallsType>".$v['wallstype']."</WallsType>";
		$content.= "<DistanceToCity>".$v['distancetocity']."</DistanceToCity>";
		$content.= "<LandArea>".$v['landarea']."</LandArea>";

	}else{
		if($v['category']!='Комнаты'){
			$content.= "<MarketType>".$v['markettype']."</MarketType>";
		}
		$content.= "<HouseType>".$v['housetype']."</HouseType>";
		$content.= "<Floor>".$v['floor']."</Floor>";
		$content.= "<Rooms>".$v['rooms']."</Rooms>";
	}

	$content.= "<Region>Краснодарский край</Region>";
	$content.= "<City>".$v['city']."</City>";
	$content.= "<Street>".$v['address']."</Street>";
	$content.= "<District>".$v['region']."</District>";

	//<![CDATA[ This text contains a CEND ]]>
//	$content.= "<Address>".$v['address']."</Address>";
	$content.= "<Description>".$v['description']."</Description>";
	$content.= "<Price>".round($v['price'])."</Price>";
	$content.= "<CompanyName>".$v['companyname']."</CompanyName>";
	if(isset($v['managername']) && $v['managername']!=''){
		$content.= "<ManagerName>".$v['managername']."</ManagerName>";
	}
	$content.= "<ContactPhone>".$v['contactphone']."</ContactPhone>";
	$content.= "<Images>";

	foreach ($images as $key=>$val) {
		//$content.= "<Image name=' ".$images[$key]." '/>";
		$content.= "<Image url='https://stroidom93.ru/files/pars/".$v['id']."/images/".$key."/".$images[$key]['file']." '/>";
	}

	$content.= "
		</Images>";

	$content.= "<Square>".$v['square']."</Square>";
	$content.= "<Floors>".$v['floors']."</Floors>";
	$content.= "</Ad>";
}

$content.= "</Ads>";

//запись в файл
$fp = fopen(ROOT_DIR.'stroidom.xml', 'w');
fwrite($fp, $content);
/**/

echo $content
?>