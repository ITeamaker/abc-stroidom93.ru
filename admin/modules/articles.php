<?php

$module['save_as'] = true;
$projects = mysql_select("SELECT id,name FROM `projects` ORDER BY rank DESC",'rows_id');

if ($get['u']=='edit') {
    $post['lastmod'] = date('Y-m-d H:i:s');
}

$a18n['project_1']	= 'проект 1';
$a18n['project_2']	= 'проект 1';
$a18n['project_3']	= 'проект 1';

$a18n['project_1_address']	= 'адрес проекта 1';
$a18n['project_2_address']	= 'адрес проекта 2';
$a18n['project_3_address']	= 'адрес проекта 3';

$a18n['project_1_link']	= 'название ссылки проекта 1';
$a18n['project_2_link']	= 'название ссылки проекта 2';
$a18n['project_3_link']	= 'название ссылки проекта 3';

$a18n['lastmod']	= 'дата обновления страницы';
$a18n['priority']	= 'приоритет страницы';

$table = array(
	'id'		=>	'date:desc name url title id',
	'img'		=>	'img',
    'name'		=>	'',
	'title'		=>	'',
	'url'		=>	'',
    'priority'		=>	'',
	'date'		=>	'date',
    'lastmod'	=>	'date',
	'display'	=>	'boolean'
);
$tabs = array(
    1=>'Общее',
    5=>'Карта сайта',
);

$form[1][] = array('input td7','name',true);
$form[1][] = array('input td3','date',true);
$form[1][] = array('checkbox td2','display',true);

$form[1][] = array('select td4','project_1',array(true,$projects,''),array('help'=>'Первый роект в блоке'));
$form[1][] = array('select td4','project_2',array(true,$projects,''),array('help'=>'Второй проект в блоке'));
$form[1][] = array('select td4','project_3',array(true,$projects,''),array('help'=>'Третий проект в блоке'));

$form[1][] = array('input td4','project_1_address',true);
$form[1][] = array('input td4','project_2_address',true);
$form[1][] = array('input td4','project_3_address',true);

$form[1][] = array('input td4','project_1_link',true,array('help'=>'Название для ссылки, если не заполнить будет указана назва проекта'));
$form[1][] = array('input td4','project_2_link',true,array('help'=>'Название для ссылки, если не заполнить будет указана назва проекта'));
$form[1][] = array('input td4','project_3_link',true,array('help'=>'Название для ссылки, если не заполнить будет указана назва проекта'));

$form[1][] = '<h2>Для вставки блока с первым проектом, необходимо вставить непосредственно в текст {project_item_1}, где 1 выбранный проект.</h2>';
$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('seo','seo url title keywords description',true);
$form[1][] = array('file','img','основное фото',array(''=>'resize 1000x1000','p-'=>'cut 332x148'));

$form[5][] = array('input td3','lastmod',true,array('help'=>'дата обновления страницы, изменяется автоматически'));
$form[5][] = array('input td3','priority',true,array('help'=>'Для главной страницы 1.00, для страниц категорий 0.8 для страниц с картинками и видео 0.6-0.2(приоритеты устанавливаются в зависимости от важности индексации той или иной страницы)'));