<?php

$module['save_as'] = true;

//исключение при редактировании модуля
if ($get['u']=='edit') {
	//$post['price_table'] = serialize($post['price_table']);
}

$a18n['text']	= 'краткое описание';
$a18n['text2']	= 'текст';
$a18n['img_about1']	= 'картинка 1 блока';
$a18n['text_about1']	= 'текст для 1 блока';
$a18n['img_about2']	= 'картинка 2 блока';
$a18n['text_about2']	= 'текст для 2 блока';
$a18n['img_about3']	= 'картинка 3 блока';
$a18n['text_about3']	= 'текст для 3 блока';
$a18n['img_about4']	= 'картинка 4 блока';
$a18n['text_about4']	= 'текст для 4 блока';
$a18n['img_about5']	= 'картинка 5 блока';
$a18n['text_about5']	= 'текст для 5 блока';
$a18n['category_block']	= 'строительстро на категории';
$a18n['img_list']	= 'картинка в списке';

$a18n['area']	= 'площадь';
$a18n['floors']	= 'этажность';
$a18n['type_roof']	= 'тип кровли';
$a18n['style']	= 'стиль';
$a18n['covering']	= 'перекрытия';
$a18n['finishing_facade']	= 'отделка фасада';
$a18n['dimensions']	= 'габариты';
$a18n['garage']	= 'гараж';
$a18n['bedrooms']	= 'спальни';
$a18n['type_foundation']	= 'тип фундамента';
$a18n['wall_materials']	= 'материалы стен';
$a18n['img_list']	= 'материал кровли';
$a18n['roof_material']	= 'материал кровли';

$table = array(
	'id'		=>	'rank:desc ',
	'name'		=>	'',
	'img'		=>	'img',
	//'img_list'		=>	'img',
	'rank'		=>	'',
	'url'		=>	'',
	'display'	=>	'boolean'
);


$tabs = array(
	1=>'Общее',
	4=>'Картинки проекта',
	2=>'Параметры',
	//3=>'Блоки с описанием',
);

$form[1][] = array('input td5','name',true);
$form[1][] = array('input td1','rank',true);
$form[1][] = array('checkbox','display',true);
$form[1][] = array('textarea td12','text',true);
$form[1][] = array('tinymce td12','text2',true);
//$form[1][] = array('file td6','img_list','картинка для списка',array(''=>'','p-'=>'cut 360x200'));
$form[1][] = array('seo','seo url title keywords description',true);

$form[4][] = array('file td6','img','основное фото',array(''=>'resize 1000x1000','l-'=>'cut 850x500','p-'=>'cut 555x360','s-'=>'cut 262x150'));
$form[4][] = array('file_multi','imgs','Дополнительные картинки',array(''=>'resize 1000x1000','l-'=>'cut 850x500','p-'=>'cut 262x150'));
$form[4][] = array('file_multi','imgs_plan','картинки планировки',array(''=>'resize 1000x1000','l-'=>'resize 740x500'));
$form[4][] = array('file_multi','imgs_facade','картинки фасада',array(''=>'resize 1000x1000','l-'=>'resize 740x500'));

$form[2][] = array('input td4','area',true);
$form[2][] = array('select td4','floors',array(true,$config['shop_product_floor'],''));
$form[2][] = array('input td4','type_roof',true); //Тип кровли
$form[2][] = array('input td4','style',true); //Стиль
$form[2][] = array('input td4','covering',true); //Перекрытия
$form[2][] = array('input td4','finishing_facade',true); //Отделка фасада
$form[2][] = array('input td4','dimensions',true); //Габариты
$form[2][] = array('input td4','garage',true); //Габариты
$form[2][] = array('input td4','bedrooms',true); //Спальни
$form[2][] = array('input td4','type_foundation',true); //Тип фундамента
$form[2][] = array('input td4','wall_materials',true); //Материалы стен
$form[2][] = array('input td4','roof_material',true); //Материал кровли


/*
$form[2][] = '<div style="clear:both; background:#E9E9E9; padding:5px 10px; width:875px; margin:0 -10px">';
$form[2][] = '<table class="product_list">';
$form[2][] = '<tr data-i="0">';
$form[2][] = '<th class="product_name_head">Название параметра</th>';
$form[2][] = '<th class="product_name_head">Значение</th>';
$form[2][] = '<th><a href="#" style="background:#35B374; display:inline-block; padding:2px; border-radius:10px"><span class="sprite plus"></span></a></th>';
$form[2][] = '</tr>';

$template['product'] = '
	<tr data-i="{i}">
		<td><input name="price_table[{i}][name]" value="{name}" class="product_name" /></td>
		<td><input name="price_table[{i}][price]" value="{price}" class="product_name"/></td>
		<td><a href="#" class="sprite boolean_0"></a></td>
	</tr>
';
if (isset($post['price_table'])) {
	$basket = unserialize($post['price_table']); //print_r ($basket);
	if ($basket) foreach ($basket as $key=>$val) {
		$val['i'] = $key;
		$form[2][] = template($template['product'],$val);
	}
}
$form[2][] = '</table>';
$form[2][] = '<div class="clear"></div></div>';


//шаблоны товара используются для js
$content = '<div style="display:none">';
$content.= '<textarea id="template_product">'.htmlspecialchars($template['product']).'</textarea>';
$content.= '</div>';
$content.= '<style type="text/css">
.form .product_list {width:100%}
.form .product_list th {text-align:left; padding:0 0 5px;}
.form .product_list td {border-top:1px solid #F3F3F3; padding:5px 0; vertical-align:top;}
.form .product_list input {text-align:right; border:1px solid gray; margin:0; padding:0 2px; height:19px; width:70px}
.form .product_list .product_name_head {width:410px; text-align: center;}
.form .product_list .product_name {width:380px; text-align:left;}
.form .product_list td td {border:none}
</style>';
$content.= '<script type="text/javascript">
$(document).ready(function(){
	$(document).on("click",".product_list th a",function(){
		var i = $(this).parents("table").find("tr:last").data("i");
		i++;
		var content = $("#template_product").val();
		content = content.replace(/{i}/g,i);
		content = content.replace(/{[\w]*}/g,"");
		$(this).parents("table").append(content);
		return false;
	});
	$(document).on("click",".product_list td a",function(){
		$(this).parents("tr").remove();
		return false;
	});
});
</script>';
*/
/*
$form[3][] = array('file td6','img_about1',' фото',array(''=>'','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about1',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about2',' фото',array(''=>'','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about2',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about3',' фото',array(''=>'','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about3',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about4',' фото',array(''=>'','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about4',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about5',' фото',array(''=>'','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about5',true);
$form[3][] = '<div class="clear"></div>';

*/