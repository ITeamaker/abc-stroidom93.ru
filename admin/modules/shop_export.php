<?php
/*
 * сохранение товаров в файл
 * динамические параметры тут - /admin/modules/shop_parameters.php
 * история обновления
 * v1.1.19
 */

//ini_set('memory_limit', '512M');
//ini_set('max_execution_time', 20*60);
$a18n['form_page_name'] = 'страница заявки';
$a18n['number'] = 'Номер';

//папка куда будут складываться результаты
$dir = 'files/temp/shop_export';

$table = array(
	'number',
	'id',
	'name',
	'phone',
//	'form_page_name',
	'date',
	//'name',
	//'special',
	//'text',
);

//дополнительный массив для параметров
$table2 = array();

//добавляем динамические параметры у которых стоит галочка синхронизация
if ($shop_parameters = mysql_select("SELECT * FROM shop_parameters WHERE import=1 ORDER BY rank DESC,id",'rows')) {
	foreach ($shop_parameters as $k => $v) {
		$table2[] = 'p' . $v['id'];
		$a18n['p' . $v['id']] = $v['name'];
	}
}
/**/
if (isset($_POST['export'])) {
	//dd($_POST);
	if (isset($_POST['date_start']) AND isset($_POST['date_finish']) AND $_POST['date_start'] !='' AND $_POST['date_finish'] !='' ){
		if($_POST['date_finish'] < $_POST['date_start'] ){
			$wheredate = '';
		}
		else{
			$wheredate = ' AND date>= "'.$_POST['date_start'].' 00:00:00"  AND date<= "'.$_POST['date_finish'].' 23:59:59" ';
		}
	}else{
		$wheredate = '';
	}
	/**/
	//dd($wheredate);
	if (is_dir(ROOT_DIR.$dir) || mkdir(ROOT_DIR.$dir,0755,true)) {
		$file = $dir.'/'.date('Y-m-d_H_i').'.'.$_POST['type'];
		$str = '';
		$content = '<h2>Содержание файла</h2>';
		$content.= '<a href="/'.$file.'">Скачать файл</a> &nbsp; <a href="">вернуться</a><br />';
		$content.= '<br /><table class="table"><tr>';
		$data = array();
		foreach ($table as $k=>$v) {
			$data[0][] = $a18n[$v];
			$content.= '<th>'.$a18n[$v].'</th>';
		}
		foreach ($table2 as $k=>$v) {
			$data[0][] = $a18n[$v];
			$content.= '<th>'.$a18n[$v].'</th>';
		}

		$content.= '</tr>';

		// таблиці для експорта
		// chose_objects
 		// feedback
		// feedback_calc
		// order_services
		// contract_application

		$chose_objects = mysql_select("SELECT id,name,phone,date FROM chose_objects WHERE 1 ".$wheredate." ",'rows');
		$feedback = mysql_select("	SELECT id,name,phone,date FROM feedback WHERE 1 ".$wheredate." ",'rows');
		$feedback_calc = mysql_select("SELECT id,name,phone,date FROM feedback_calc WHERE 1 ".$wheredate." ",'rows');
		$order_services = mysql_select("SELECT id,name,phone,date FROM order_services WHERE 1 ".$wheredate." ",'rows');
		$contract_application = mysql_select("	SELECT id,name,phone,date FROM contract_application WHERE 1 ".$wheredate." ",'rows');

		if(empty($chose_objects)) $chose_objects = array();
		if(empty($feedback)) $feedback = array();
		if(empty($feedback_calc)) $feedback_calc = array();
		if(empty($order_services)) $order_services = array();
		if(empty($contract_application)) $contract_application = array();

		$shop_products = array_merge($chose_objects, $feedback, $feedback_calc, $order_services,$contract_application);
		//dd($shop_products);

		if ($shop_products) {

			$i = 0;
			foreach ($shop_products as $q) {
				$i++;
				$q['number']= $i;
				$content .= '<tr valign="top">';

				foreach ($table as $k => $v) {
					$data[$i][] = $q[$v];

					$content .= '<td>' . $q[$v] . '</td>';
				}

				$content .= '</tr>';
			}
		}
		$content.= '</table>';
		if ($_POST['type']=='csv') {
			foreach ($data as $key=>$val) {
				foreach ($val as $k=>$v) {
					$str .= '"' . str_replace('"', "&quot;", $v) . '";';
				}
				$str.= "\n";
			}
			$str = iconv("UTF-8", "cp1251//TRANSLIT", $str);
			$fp = fopen(ROOT_DIR.$file, 'w');
			fwrite($fp,$str);
			fclose($fp);
			unset($table);
		}
		else {
			include (ROOT_DIR.'plugins/phpexcel/PHPExcel.php');
			include (ROOT_DIR.'plugins/phpexcel/PHPExcel/Writer/Excel2007.php');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw");
			$objPHPExcel->getProperties()->setLastModifiedBy("Maarten Balliauw");
			$objPHPExcel->getProperties()->setTitle("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setSubject("Office 2007 XLSX Test Document");
			$objPHPExcel->getProperties()->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.");
			$objPHPExcel->getActiveSheet()
				->fromArray(
					$data,  // The data to set
					NULL,        // Array values with this value will not be set
					'A1'         // Top left coordinate of the worksheet range where
				//    we want to set these values (default is A1)
				);
			$objPHPExcel->getActiveSheet()->setTitle('Simple');
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			//v1.2.40 запретить формулы при генерации excel
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save(ROOT_DIR.$file);

		}
	}
	else $content = 'не удалось создать каталог';

}
else {
	$content = '<br /><h2>Подтверждение создания файла</h2>';
	$content.= 'Будет сгенерирован файл excel c такой структурой:<br />';
	$content.= '<br /><table class="table"><tr>';
	foreach ($table as $k=>$v) {
		//$str.= '"'.str_replace('"',"&quot;",$fieldset[$v]).'";';
		$content.= '<th>'.$a18n[$v].'</th>';
	}
	foreach ($table2 as $k=>$v) {
		$content.= '<th>'.$a18n[$v].'</th>';
	}
	$content.= '</tr></table>';
	$content.= '<form method="post" action="">';
	$content.= '<br /><select name="type"><option value="csv">csv</option><option value="xlsx">xlsx</option></select> &nbsp; ';
	$content.= '<input class="datepicker" name="date_start" value="" placeholder="Начальная дата" style="margin-right: 5px">';
	$content.= '<input class="datepicker" name="date_finish" value="" placeholder="Конечная дата" style="margin-right: 5px">';

	$content.= '<input type="submit" name="export" value=" Сгенерировать файл ">';
	$content.= '</form>

<script type="text/javascript">

 $(document).ready(function() {
 	$(\'.datepicker\').datepicker({dateFormat:"yy-mm-dd"});
  $.datepicker.setDefaults(
   $.extend($.datepicker.regional["ru"]={
    dateFormat: \'yy-mm-dd\',
    
    //minDate:0,
    
   })
  );
 })
</script>
';

}

unset($table);
