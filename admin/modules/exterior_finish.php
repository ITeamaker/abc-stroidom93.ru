<?php

$module['save_as'] = true;

//исключение при редактировании модуля
if ($get['u']=='edit') {
	$post['price_table'] = serialize($post['price_table']);
    $post['lastmod'] = date('Y-m-d H:i:s');
}

$a18n['text']	= 'краткое описание';
$a18n['text2']	= 'текст';
$a18n['img_about1']	= 'картинка 1 блока';
$a18n['text_about1']	= 'текст для 1 блока';
$a18n['img_about2']	= 'картинка 2 блока';
$a18n['text_about2']	= 'текст для 2 блока';
$a18n['img_about3']	= 'картинка 3 блока';
$a18n['text_about3']	= 'текст для 3 блока';
$a18n['img_about4']	= 'картинка 4 блока';
$a18n['text_about4']	= 'текст для 4 блока';
$a18n['img_about5']	= 'картинка 5 блока';
$a18n['text_about5']	= 'текст для 5 блока';
$a18n['category_block']	= 'строительстро на категории';
$a18n['img_list']	= 'картинка в списке';
$a18n['seo_name']	= 'второй заголовок';
$a18n['seo_text']	= 'второй текстовый блок';
$a18n['text_seo_1']	= 'SEO текст';
$a18n['name_type']	= 'дополнительное название';
$a18n['lastmod']	= 'дата обновления страницы';
$a18n['priority']	= 'приоритет страницы';


$table = array(
	'id'		=>	'rank:desc ',
	'name'		=>	'',
	'img'		=>	'img',
	'img_list'	=>	'img',
	'rank'		=>	'',
	'url'		=>	'',
    'priority'		=>	'',
    'lastmod'	=>	'date',
	'display'	=>	'boolean'
);


$tabs = array(
	1=>'Общее',
	4=>'Текст',
	2=>'Цены',
	3=>'Блоки с описанием',
    5=>'Карта сайта',
);

$form[1][] = array('input td4','name',true);
$form[1][] = array('input td4','name_type',true,array('help'=>'второе название, будет отображаться в меню и селекте формы'));
$form[1][] = array('input td2','rank',true);
$form[1][] = array('checkbox td2','display',true);
$form[1][] = array('file td6','img','основное фото',array(''=>'resize 1000x1000','p-'=>'cut 555x360'));
$form[1][] = array('file td6','img_list','картинка для списка',array(''=>'resize 1000x1000','p-'=>'cut 360x200'));
$form[1][] = array('seo','seo url title keywords description',true);

$form[4][] = array('textarea td12','text',true);
$form[4][] = array('tinymce td12','text2',true,array('attr'=>'style="height:300px"'));
$form[4][] = array('input td12','seo_name',true);
$form[4][] = array('tinymce td12','seo_text',true,array('attr'=>'style="height:300px"'));

$form[2][] = '<div style="clear:both; background:#E9E9E9; padding:5px 10px; width:875px; margin:0 -10px">';
$form[2][] = '<table class="product_list">';
$form[2][] = '<tr data-i="0">';
$form[2][] = '<th class="product_name_head">площадь</th>';
$form[2][] = '<th class="product_name_head">цена</th>';
$form[2][] = '<th><a href="#" style="background:#35B374; display:inline-block; padding:2px; border-radius:10px"><span class="sprite plus"></span></a></th>';
$form[2][] = '</tr>';

$template['product'] = '
	<tr data-i="{i}">
		<td><input name="price_table[{i}][name]" value="{name}" class="product_name" /></td>
		<td><input name="price_table[{i}][price]" value="{price}" class="product_name"/></td>
		<td><a href="#" class="sprite boolean_0"></a></td>
	</tr>
';
if (isset($post['price_table'])) {
	$basket = unserialize($post['price_table']); //print_r ($basket);
	if ($basket) foreach ($basket as $key=>$val) {
		$val['i'] = $key;
		$form[2][] = template($template['product'],$val);
	}
}
$form[2][] = '</table>';
$form[2][] = '<div class="clear"></div></div>';


//шаблоны товара используются для js
$content = '<div style="display:none">';
$content.= '<textarea id="template_product">'.htmlspecialchars($template['product']).'</textarea>';
$content.= '</div>';
$content.= '<style type="text/css">
.form .product_list {width:100%}
.form .product_list th {text-align:left; padding:0 0 5px;}
.form .product_list td {border-top:1px solid #F3F3F3; padding:5px 0; vertical-align:top;}
.form .product_list input {text-align:right; border:1px solid gray; margin:0; padding:0 2px; height:19px; width:70px}
.form .product_list .product_name_head {width:410px; text-align: center;}
.form .product_list .product_name {width:380px; text-align:left;}
.form .product_list td td {border:none}
</style>';
$content.= '<script type="text/javascript">
$(document).ready(function(){
	$(document).on("click",".product_list th a",function(){
		var i = $(this).parents("table").find("tr:last").data("i");
		i++;
		var content = $("#template_product").val();
		content = content.replace(/{i}/g,i);
		content = content.replace(/{[\w]*}/g,"");
		$(this).parents("table").append(content);
		return false;
	});
	$(document).on("click",".product_list td a",function(){
		$(this).parents("tr").remove();
		return false;
	});
});
</script>';


$form[3][] = array('file td6','img_about1',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about1',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about2',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about2',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about3',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about3',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about4',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about4',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about5',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about5',true);
$form[3][] = '<div class="clear"></div>';

$form[4][] = array('tinymce td12','text_seo_1',true,array('attr'=>'style="height:300px"'));
//$form[4][] = array('tinymce td12','text_seo_2',true,array('attr'=>'style="height:300px"'));

$form[5][] = array('input td3','lastmod',true,array('help'=>'дата обновления страницы, изменяется автоматически'));
$form[5][] = array('input td3','priority',true,array('help'=>'Для главной страницы 1.00, для страниц категорий 0.8 для страниц с картинками и видео 0.6-0.2(приоритеты устанавливаются в зависимости от важности индексации той или иной страницы)'));