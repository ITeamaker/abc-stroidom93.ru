<?php

$table = array(
	'id'		=> 'date:desc id email',
	'date'		=> '',
	'product'	=> '[{product}] {product_name}',
	'rating' 	=> '',
	'name'		=> '',
	'email'		=> '',
	//'text'		=> 'strip_tags',
	'display'	=> 'display'
);

$where = @$_GET['product']>0 ? ' AND product='.intval($_GET['product']) : '';

$query = "
	SELECT shop_reviews.*,sp.name product_name
	FROM shop_reviews
	LEFT JOIN shop_products sp ON sp.id=shop_reviews.product
	WHERE 1 $where
";

$form[] = array('input td3','name',true);
$form[] = array('input td3','email',true);
$form[] = array('input td2','date',true);
$form[] = array('input td1','product',true,array('help'=>'ID товара'));
$form[] = array('input td1','rating',true);
$form[] = array('checkbox','display',true);
$form[] = array('tinymce td12','text',true);

if ($get['u']=='post') {
	//пересчет рейтинга товара
	if (in_array($get['name'],array('rating','display'))) {
		//быстрое сохранение
		mysql_fn('update',$module['table'],array($get['name']=>$get['value'],'id'=>$get['id']));
		//пересчет
		$post['product'] = mysql_select("SELECT product FROM shop_reviews WHERE id=".intval($get['id']),'string');
		after_save ();
	}
}

//пересчет рейтинга товара
function after_save () {
	global $post;
	if ($post['product']>0) {
		$post['product'] = intval($post['product']);
		$data = array(
			'id' => $post['product'],
			'rating' => mysql_select("SELECT SUM(rating)/COUNT(id) FROM shop_reviews WHERE display=1 AND product=" . $post['product'], 'string'),
		);
		//print_r($data);
		mysql_fn('update', 'shop_products', $data);
	}
}