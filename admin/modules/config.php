<?php

/**
 * динамические настройки сайта
 * хранятся в /config.php
 */

$module['one_form'] = true;

$get['id']='config';
if ($get['u']!='edit') $post = $config;
if ($get['u']=='edit') {
	$content = "<?php\r\n";
	foreach($post as $k=>$v)
		$content.= '$config[\''.$k.'\']=\''.str_replace("'","\'",$v).'\';'."\r\n";
	$fp = fopen(ROOT_DIR.'_config.php', 'w');
	fwrite($fp,$content);
	fclose($fp);
	if($post['cache']==0) {
		delete_all('cache',true);
	}
	$data['error']	= '';
	//print_r($data);
	echo '<textarea>'.htmlspecialchars(json_encode($data)).'</textarea>';
	die();
}

$tabs = array(
	1=>'Общее',
	//2=>'Платежные агрегаторы',
	//3=>'Индексация', //по умолчанию заккоментирована
	4=>'Sitemap',
	5=>'Avito-xml'
);

$form[1][] = array('input td4','sender',true,array('name'=>'глобальный email отправителя письма'));
$form[1][] = array('input td4','receiver',true,array('name'=>'глобальный email получателя письма'));
$form[1][] = '<br /><a href="/admin.php?m=letter_templates">настроить</a>';

$form[1][] = array('checkbox td12','cache',true,array('name'=>'включить кеширование сайта'));
$form[1][] = array('checkbox td12','redirects',true,array('name'=>'включить редиректы <a target="_blank" href="/admin.php?m=redirects">настроить пути</a>'));
$form[1][] = array('checkbox td12','editable',true,array('name'=>'включить быстрое редактирование с сайта <a target="_blank" href="/admin.php?m=user_types">настроить права доступа</a>'));
$form[1][] = array('checkbox td12','dummy',true,array('name'=>'включить заглушку для сайта (доступ на сайт будут иметь только администраторы)'));
$form[1][] = array('checkbox td12','uploader',true,array('name'=>'включить загрузку файлов через html5'));
$form[1][] = array('checkbox td12','html_minify',true,array('name'=>'включить сжатие html'));

//Платежные агрегаторы
//не показываем вкладку если нет оплаты
if (!isset($config['payments'])) unset($tabs[2]);
$modules['basket'] = mysql_select("SELECT url FROM pages WHERE module='basket'",'string');
$payment_action = 'http://'.$_SERVER['HTTP_HOST'].'/payments.php?payment_action=';
$payment_success = 'http://'.$_SERVER['HTTP_HOST'].'/'.$modules['basket'].'/success/';
$payment_fail = 'http://'.$_SERVER['HTTP_HOST'].'/'.$modules['basket'].'/fail/';
//робокасса
if (isset($config['payments'][100])) {
	$form[2][] = '<div style="clear:both"><br /><b>ROBOKASSA</b> <a href="http://robokassa.ru/" target="_blank">http://robokassa.ru/</a></div>';
	$form[2][] = array('input td4', 'robokassa_login', true, array('name' => 'логин'));
	$form[2][] = array('input td4', 'robokassa_password1', true, array('name' => 'пароль1'));
	$form[2][] = array('input td4', 'robokassa_password2', true, array('name' => 'пароль2'));
	$form[2][] = '<br /><b>Result URL:</b> ' . $payment_action . 'robokassa_result';
	$form[2][] = '<br /><b>Success URL:</b> ' . $payment_success;
	$form[2][] = '<br /><b>Fail URL:</b> ' . $payment_fail;
}
//yandex
if (isset($config['payments'][200])) {
	$form[2][] = '<div style="clear:both"><br /><br /><b>YANDEX</b> <a href="https://kassa.yandex.ru/" target="_blank">https://kassa.yandex.ru/</a></div>';
	$form[2][] = array('input td3', 'yandex_shopId', true, array('name' => 'идентификатор магазина'));
	$form[2][] = array('input td3', 'yandex_scid', true, array('name' => 'идентификатор витрины магазина'));
	$form[2][] = array('input td3', 'yandex_customerNumber', true, array('name' => 'идентификатор плательщика'));
	$form[2][] = array('input td3', 'yandex_shopPassword', true, array('name' => 'пароль'));
	$form[2][] = '<br /><b>paymentAviso URL:</b> ' . $payment_action . 'yandex_paymentAviso';
	$form[2][] = '<br /><b>checkOrder URL:</b> ' . $payment_action . 'yandex_checkOrder';
	$form[2][] = '<br /><b>Success URL:</b> ' . $payment_success;
	$form[2][] = '<br /><b>Fail URL:</b> ' . $payment_fail;
}
//liqpay privat24 v.1.1.2
if (isset($config['payments'][500])) {
	$form[2][] = '<div style="clear:both"><br /><br /><b>LIQPAY (Privat24)</b> <a href="https://liqpay.com/" target="_blank">https://liqpay.com/</a></div>';
	$form[2][] = array('input td6', 'liqpay_public_key', true, array('name' => 'публичный ключ'));
	$form[2][] = array('input td6', 'liqpay_private_key', true, array('name' => 'приватный ключ'));
	$form[2][] = '<br /><b>Result URL:</b> ' . $payment_action . 'liqpay_result';
	$form[2][] = '<br /><b>Return URL:</b> ' . $payment_success;
}
//paypal
if (isset($config['payments'][600])) {
	$form[2][] = '<div style="clear:both"><br /><br /><b>PAYPAL</b> <a href="https://paypal.ru/" target="_blank">https://paypal.ru/</a></div>';
	$form[2][] = array('input td12', 'paypal_email', true, array('name' => 'email'));
	$form[2][] = '<br /><b>Result URL:</b> ' . $payment_action . 'paypal_result';
	$form[2][] = '<br /><b>Success URL:</b> ' . $payment_success;
	$form[2][] = '<br /><b>Fail URL:</b> ' . $payment_fail;
}
//2checkout
if (isset($config['payments'][700])) {
	$form[2][] = '<div style="clear:both"><br /><br /><b>2CHECKOUT</b> <a href="https://2checkout.com/" target="_blank">https://2checkout.com/</a></div>';
	$form[2][] = array('input td12', '2checkout_id', true, array('name' => '2checkout_id'));
	$form[2][] = '<br /><b>Result URL:</b> ' . $payment_action . '2checkout_result';
	$form[2][] = '<br /><b>Success URL:</b> ' . $payment_success;
	$form[2][] = '<br /><b>Fail URL:</b> ' . $payment_fail;
}

//Индексация
if (isset($tabs[3])) {
	$form[3][] = '<div style="clear:both"><br /><b>ЯНДЕКС</b> <a href="https://xml.yandex.ru/settings/" target="_blank">https://xml.yandex.ru/settings/</a></div>';
	$form[3][] = array('input td4','yandex_user',true,array('name'=>'логин яндекс для xmlsearch'));
	$form[3][] = array('input td8','yandex_key',true,array('name'=>'ключ яндекс для xmlsearch'));
	$form[3][] = '<br />Для автоматической проверки страниц в индексе яндекса, нужно включить задачу cron: <a target="_blank" href="http://'.$_SERVER['HTTP_HOST'].'/cron.php?file=yandex_index">http://'.$_SERVER['HTTP_HOST'].'/cron.php?file=yandex_index</a>';
}

$sitemap_generation = array(
	0=>'не генерировать',
	1=>'все страницы',
);
if (isset($tabs[3]))  $sitemap_generation[2] = 'только не проиндексированные';
$form[4][] = array('select td4','sitemap_generation',array(true,$sitemap_generation),array('name'=>'генерация sitemap.xml'));
$form[4][] = '<div class="clear"></div>';
$form[4][] = '<b>1. не генерировать</b> - статический файл <a target="_blank" href="/sitemap.xml">sitemap.xml</a>';
$form[4][] = '<br /><b>2. все страницы</b> - файл <a target="_blank" href="/sitemap.xml">sitemap.xml</a> будет генерироваться автоматически с url всех страниц';
if (isset($tabs[3])) {
	$form[4][] = '<br /><b>3. только не проиндексированные</b> - файл <a target="_blank" href="/sitemap.xml">sitemap.xml</a> будет генерироваться автоматически с урл не проиндексированных страниц';
	$form[4][] = '(<a target="_blank" href="/admin.php?m=config#3">настроить индексацию</a>)';
}

$form[5][] = '<a target="_blank" href="/avito_xml_30_29_05.php">Сгенерировать XML Авито 30</a><br>';
$form[5][] = '<a target="_blank" href="/avito_xml_100.php">Сгенерировать XML Авито 100</a><br>';
$form[5][] = '<a target="_blank" href="/avito_xml.php">Сгенерировать XML Авито 500</a><br>';

//$form[5][] = '<br><a target="_blank" href="/stroidom.xml" download>Скачать XML файл</a><br>';
//$form[5][] = '<br><a target="_blank" href="/_/update_pars.php">Обновить объекты Авито</a> - <b>Обновляется информация об объектах, в которых отсутсвует Авито id </b><br>';