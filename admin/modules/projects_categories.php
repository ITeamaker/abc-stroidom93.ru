<?php

$module['save_as'] = true;

$a18n['filter_name']	= 'название в фильтре';

$table = array(
	'id'				=>	'rank:desc name',
	'name'				=>	'',
	'rank'				=>	'',
	'url'				=>	'',
	'display'			=>	'boolean'
);

$delete = array(
    'confirm'	=>	array(
        'projects'		=>	'category'
    ),
);

$form[] = array('input td5','name',true);
$form[] = array('input td3','filter_name',true);
$form[] = array('input td2','rank',true);
$form[] = array('checkbox td2','display',true);
$form[] = array('seo','seo url title keywords description',true);
