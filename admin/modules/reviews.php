<?php

$a18n['name']= 'имя';

$table = array(
	'id'		=>	'date:desc name url title id',
	'photo'		=>	'img',
	'img'		=>	'img',
	'name'		=>	'',
	'title'		=>	'',
	'url'		=>	'',
	'date'		=>	'date',
	'display'	=>	'boolean'
);



$form[] = array('input td7','name',true);
$form[] = array('input td3','date',true,array('attr'=>'class="datepicker"'));
$form[] = array('checkbox','display',true);
$form[] = array('tinymce td12','text',true);
$form[] = array('seo','seo url title keywords description',true);
$form[] = array('file td6','photo','Фото',array(''=>'resize 1000x1000','p-'=>'cut 130x130'));
$form[] = array('file td6','img','Изображение',array(''=>'resize 1000x1000','l-'=>'resize 262x500','p-'=>'resize 130x180'));
