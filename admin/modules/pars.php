<?php

$a18n['id_advert'] = 'id Avito';
$a18n['datebegin_check'] = 'xml Авито 500';
$a18n['dateend'] = 'публикации До даты';
$a18n['datebegin'] = 'публикации С даты';
$a18n['avito_30'] = 'xml Авито 30';
$a18n['avito_100'] = 'xml Авито 100';

$avito_type = array(
	'1'=>'Авито 30',
	'2'=>'Авито 100',
	'3'=>'Авито 500',
);

$table = array(
	'id'		=>	'id:desc name id price id_advert',
	'name'		=>	'',
	'region'		=>	'',
	'address'		=>	'',
	//'url'		=>	'',
	'price'		=>	'',
	'id_advert'		=>	'',
	//'datebegin'		=>	'',
	//'dateend'	=>	'',
	'avito_30'		=> 'boolean',
	'avito_100'		=> 'boolean',
	'datebegin_check'		=>	'boolean',

);

$filter[] = array('search');
$count[] = array();
$filter[] = array('avito',$avito_type,'все объявления');

$where = '';
if(isset($get['avito'])){
	if($get['avito']==1) {
		$where.= "AND avito_30 = 1";
	}
	elseif($get['avito']==2) {
		$where.= "AND avito_100 = 1";
	}
	elseif($get['avito']==3) {
		$where.= "AND datebegin_check = 1";
	}
	else{
		$where.= '';
	}
}

if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(pars.id_advert) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(pars.name) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(pars.id) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(pars.price) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
		)
";

$query = "
	SELECT *
	FROM pars
	WHERE 1 $where
";

$form[] = array('input td4','name',true);
$form[] = array('input td2','id_advert',true);
$form[] = array('checkbox','avito_30',true);
$form[] = array('checkbox','avito_100',true);
$form[] = array('checkbox','datebegin_check',true);
$form[] = array('input td2','datebegin',true);
$form[] = array('input td2','dateend',true);

//$form[] = array('input td3','date',true,array('attr'=>'class="datepicker"'));
//$form[] = array('checkbox','display',true);

$form[] = array('input td8','url',true);
$form[] = array('input td4','category',true);
$form[] = array('input td4','operationtype',true);

$form[] = array('input td4','markettype',true);
$form[] = array('input td4','propertyrights',true);
$form[] = array('input td4','companyname',true);
$form[] = array('input td12','address',true);
$form[] = array('input td6','region',true);
$form[] = array('input td6','city',true);
$form[] = array('textarea td12','description',true,array('help'=>'<![CDATA[ Ваш текст, вы можете использовать дополнительное форматирование с помощью HTML-тегов — строго из указанного списка: p, br, strong, em, ul, ol, li ]]> '));
$form[] = array('input td6','price',true);

$form[] = array('input td6','managername',true);
$form[] = array('input td6','contactphone',true);
$form[] = array('input td6','rooms',true);
$form[] = array('input td6','square',true);
$form[] = array('input td6','floor',true);
$form[] = array('input td6','floors',true);
$form[] = array('input td6','housetype',true);

$form[] = '<h2>Для вида объекта "Дома, дачи, коттеджи"</h2>';

$form[] = array('input td3','objecttype',true);
$form[] = array('input td3','wallstype',true);
$form[] = array('input td3','distancetocity',true);
$form[] = array('input td3','landarea',true);

//operationtype
$form[] = array('file_multi','images','картинки',array(''=>'resize 700x700','p-'=>'cut 250x175'));
/*
if($post['images'] !=''){
	dd(unserialize($post['images']));
	die();
}
/**/
//$form[] = array('tinymce td12','text',true);
//$form[] = array('seo','seo url title keywords description',true);