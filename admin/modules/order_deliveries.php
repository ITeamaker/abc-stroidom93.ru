<?php

$table = array(
	'id'		=>	'rank',
	'name'.$lang['i'] => '',
	'rank'		=>	'',
	'cost'		=>	'right',
	'free'		=>	'right',
	'display'	=>	'display'
);

$delete = array();

$tabs = array(
	1=>'Общее',
);
//если мультиязычный то нужно добавляем вкладки языков кроме главного языка
if ($config['multilingual']) {
	//вкладку с главным языком не показываем
	foreach ($config['languages'] as $k => $v) if ($k>0) {
		//вкладки
		$tabs['1' . $v['id']] = $v['name'];
		//поля
		$form['1' . $v['id']][] = array('input td12', 'name' . $v['id'], @$post['name' . $v['id']], array('name' => $a18n['name']));
		$form['1' . $v['id']][] = array('textarea td12', 'text' . $v['id'], @$post['text' . $v['id']], array('name' => $a18n['text']));
	}
}

$form[1][] = array('input td5','name'.$lang['i'],true);
$form[1][] = array('input td1 right','rank',true);
$form[1][] = array('input td2 right','cost',true,array('name'=>'стоимость доставки'));
$form[1][] = array('input td2 right','free',true,array('help'=>'укажите минимальную стоимость заказа для которого будет доставка бесплатной'));
$form[1][] = array('checkbox','display',true);
$form[1][] = array('textarea td12','text'.$lang['i'],true);