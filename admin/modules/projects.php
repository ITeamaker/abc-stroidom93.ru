<?php

$module['save_as'] = true;

if ($get['u']=='edit') {
    $post['lastmod'] = date('Y-m-d H:i:s');
}

$a18n['text']	= 'краткое описание';
$a18n['text2']	= 'текст';

$a18n['category_block']	= 'строительстро на категории';
$a18n['img_list']	= 'картинка в списке';

$a18n['area']	= 'площадь';
$a18n['floors']	= 'этажность фильтр';
$a18n['number_floors']	= 'количество этажей';
$a18n['garage']	= 'гараж';
$a18n['hotel']	= 'гостиница';
$a18n['apartment_buildings']	= 'многоквартирные дома ';
$a18n['country_house']	= 'дачный дом';
$a18n['with_attic']	= 'дом с мансардным этажом';
$a18n['townhouse_dupliksy']	= 'таунхаус или дупликс';
$a18n['bedrooms']	= 'спальни';
$a18n['price_metr']	= 'цена за 1 м2';

$a18n['portfolio']	= 'проект в портфолио';
$a18n['portfolio_index']	= 'проект в слайдере на главной';
$a18n['lastmod']	= 'дата обновления страницы';
$a18n['priority']	= 'приоритет страницы';
$a18n['flats']	= 'количество квартир';
$a18n['flats_area']	= 'площадь квартир';
$a18n['status']	= 'статус';

$a18n['video1']	= 'видео';
//video1

$categories = mysql_select("SELECT id,name FROM `projects_categories` ORDER BY rank DESC",'rows_id');
$statuses = mysql_select("SELECT id,name FROM `project_statuses` ORDER BY rank DESC",'rows_id');

$table = array(
	'id'				=>	'rank:desc price area',
	'name'				=>	'',
	'img'				=>	'img',
    'category:'	        =>	$categories,
	'price'				=>	'',
	'price_metr'		=>	'',
	'area'				=>	'',
	//'floors'			=>	$config['shop_product_floor'],
	'number_floors:'	=>	$config['shop_product_floor'],
	//'flats'				=>	'',
	'flats_area'				=>	'',
	//'bedrooms'			=>	'',
	//'garage'			=>	'',
	'rank'				=>	'',
	//'url'				=>	'',
    'status'		=>	'',
    'priority'		=>	'',
   // 'lastmod'	=>	'date',
	'hotel'				=>	'boolean',
	'apartment_buildings'		=>	'boolean',
	//'with_attic'		=>	'boolean',
	'townhouse_dupliksy'=>	'boolean',
    'portfolio'			=>	'boolean',
    'portfolio_index'	=>	'boolean',
	'display'			=>	'boolean'
);

$where = (isset($get['category']) && $get['category']>0) ? " AND category = '".intval($get['category'])."' " : "";
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(name) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";

$query = "
	SELECT *
	FROM projects
	WHERE id IS NOT NULL $where
";

$filter[] = array('search');
$filter[] = array('category',$categories,'категории',true);

$tabs = array(
	1=>'Общее',
	4=>'Картинки проекта',
    5=>'Карта сайта',
);

$form[1][] = array('input td3','name',true);
$form[1][] = array('input td2','price',true);
$form[1][] = array('input td2','price2',true);
$form[1][] = array('input td2','price_metr',true,array('help'=>'цена за 1 м2 для многоквартирных домов'));
$form[1][] = array('input td2','area',true);
$form[1][] = array('input td1','rank',true);

$form[1][] = array('select td3','status',array(true,$statuses,''));
$form[1][] = array('select td3','category',array(true,$categories,''));
$form[1][] = array('checkbox','portfolio',true);
$form[1][] = array('checkbox','portfolio_index',true);
$form[1][] = array('checkbox','display',true);
/*
$config['shop_product_floor_f'] = $config['shop_product_floor'];
unset ($config['shop_product_floor_f'][3]);
/*
$form[1][] = '<h2>Параметры для фильтра</h2>';
$form[1][] = array('select td3','floors',array(true,$config['shop_product_floor_f'],''));
$form[1][] = array('checkbox td3','hotel',true);
$form[1][] = array('checkbox td3','apartment_buildings',true);
//$form[1][] = array('checkbox td3','with_attic',true);
$form[1][] = array('checkbox td3','townhouse_dupliksy',true);
*/
$form[1][] = '<h2>Параметры проекта</h2>';
$form[1][] = array('select td3','number_floors',array(true,$config['shop_product_floor'],''));
$form[1][] = array('input td3','flats',true); //квартиры
$form[1][] = array('input td3','flats_area',true); //площадь квартир
$form[1][] = array('input td3','bedrooms',true); //Спальни
$form[1][] = array('input td3','garage',true); //Габариты
$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('seo','seo url title keywords description',true);

$form[4][] = array('input td12','video1',true); //Спальни
$form[4][] = array('file td6','img','основное фото',array(''=>'resize 1200x1200','l-'=>'resize 850x500','p-'=>'cut 555x360','s-'=>'cut 262x150','ss-'=>'cut 132x77'));
$form[4][] = array('file td6','img_list','картинка для списка',array(''=>'resize 1200x1200','p-'=>'cut 360x200'));
$form[4][] = array('file_multi','imgs','Дополнительные картинки',array(''=>'resize 1200x1200','l-'=>'resize 850x500','p-'=>'cut 132x77'));
$form[4][] = array('file_multi','imgs_plan','картинки планировки',array(''=>'resize 1200x1200','l-'=>'resize 740x500'));
$form[4][] = array('file_multi','imgs_facade','картинки фасада',array(''=>'resize 1200x1200','l-'=>'resize 740x500'));

$form[5][] = array('input td3','lastmod',true,array('help'=>'дата обновления страницы, изменяется автоматически'));
$form[5][] = array('input td3','priority',true,array('help'=>'Для главной страницы 1.00, для страниц категорий 0.8 для страниц с картинками и видео 0.6-0.2(приоритеты устанавливаются в зависимости от важности индексации той или иной страницы)'));

