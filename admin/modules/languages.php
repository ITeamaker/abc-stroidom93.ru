<?php

$locales = array(
	'en'	=>	'Английский',
	'ar'	=>	'Арабский',
	'bg'	=>	'Болгарский',
	'ca'	=>	'Каталанский',
	'cn'	=>	'Китайский',
	'cs'	=>	'Чешский',
	'da'	=>	'Датский',
	'de'	=>	'Немецкий',
	'el'	=>	'Греческий',
	'es'	=>	'Испанский',
	'eu'	=>	'Баскский',
	'fa'	=>	'Фарси',
	'fi'	=>	'Финский',
	'fr'	=>	'Французский',
	'he'	=>	'Иврит',
	'hu'	=>	'Венгерский',
	'it'	=>	'Итальянский',
	'ja'	=>	'Японский',
	'kk'	=>	'Казахский',
	'lt'	=>	'Литовский',
	'lv'	=>	'Латышский',
	'nl'	=>	'Голландский',
	'no'	=>	'Норвежский',
	'pl'	=>	'Польский',
	'ptbr'	=>	'Португальский (Бразилия)',
	'ptpt'	=>	'Португальский',
	'ro'	=>	'Румынский',
	'ru'	=>	'Русский',
	'si'	=>	'Словенский',
	'sk'	=>	'Словацкий',
	'sl'	=>	'Словенский',
	'sr'	=>	'Сербский',
	'th'	=>	'Таиландский',
	'tr'	=>	'Турецкий',
	'tw'	=>	'Тайванский',
	'ua'	=>	'Украинский',
	'vi'	=>	'Вьетнамский',
);


//многоязычный
if ($config['multilingual']) {
	$module['save_as'] = true;
	$table = array(
		'id'			=>	'rank:desc name id',
		'name'			=>	'',
		'rank'			=>	'',
		'url'			=>	'',
		'localization'	=>	$locales,
		'display'		=>	'display'
	);
	$form[0][] = array('input td4','name',true);
	$form[0][] = array('input td2','rank',true);
	$form[0][] = array('input td2','url',true);
	$form[0][] = array('select td2','localization',array(true,$locales));
	$form[0][] = array('checkbox td2','display',true);
}

//одноязычный
else {
	$module['one_form'] = true;
	$get['id'] = 1;
	if ($get['u']!='edit') {
		$post = mysql_select("
			SELECT *
			FROM languages
			WHERE id = 1
			LIMIT 1
		",'row');
	}
}

$a18n['localization'] = 'localization';

//исключения
if ($get['u']=='edit') {
	unset($post['dictionary']);
}
else {
	if ($get['id']>0) {
		$root = ROOT_DIR . 'files/languages/' . $get['id'] . '/dictionary';
		if (is_dir($root) && $handle = opendir($root)) {
			while (false !== ($file = readdir($handle))) {
				if (strlen($file) > 2)
					include(ROOT_DIR . 'files/languages/' . $get['id'] . '/dictionary/' . $file);
			}
		}
	}
}

//правила удаления для многоязычного
$delete['confirm'] = array('pages'=>'language');
$delete['delete'] = array(
	//товары
	"ALTER TABLE `shop_products` DROP `name".$get['id']."`",
	"ALTER TABLE `shop_products` DROP `text".$get['id']."`",
	"ALTER TABLE `shop_products` DROP `url".$get['id']."`",
	"ALTER TABLE `shop_products` DROP `title".$get['id']."`",
	"ALTER TABLE `shop_products` DROP `keywords".$get['id']."`",
	"ALTER TABLE `shop_products` DROP `description".$get['id']."`",
	//категории
	"ALTER TABLE `shop_categories` DROP `name".$get['id']."`",
	"ALTER TABLE `shop_categories` DROP `text".$get['id']."`",
	"ALTER TABLE `shop_categories` DROP `url".$get['id']."`",
	"ALTER TABLE `shop_categories` DROP `title".$get['id']."`",
	"ALTER TABLE `shop_categories` DROP `keywords".$get['id']."`",
	"ALTER TABLE `shop_categories` DROP `description".$get['id']."`",
	//параметры пользователя
	"ALTER TABLE `user_fields` DROP `name".$get['id']."`",
	"ALTER TABLE `user_fields` DROP `hint".$get['id']."`",
	//статусы заказа
	"ALTER TABLE `order_types` DROP `name".$get['id']."`",
	"ALTER TABLE `order_types` DROP `text".$get['id']."`",
	//доставка
	"ALTER TABLE `order_deliveries` DROP `name".$get['id']."`",
	"ALTER TABLE `order_deliveries` DROP `text".$get['id']."`",
);

function after_save() {
	global $get,$config;
	if (is_dir(ROOT_DIR . 'files/languages/' . $get['id'] . '/dictionary') || mkdir(ROOT_DIR . 'files/languages/' . $get['id'] . '/dictionary', 0755, true)) {
		$post = stripslashes_smart($_POST);
		foreach ($post['dictionary'] as $key => $val) {
			$str = '<?php' . PHP_EOL;
			$str .= '$lang[\'' . $key . '\'] = array(' . PHP_EOL;
			foreach ($val as $k => $v) {
				$str .= "	'" . $k . "'=>'" . str_replace("'", "\'", $v) . "'," . PHP_EOL;
			}
			$str .= ');';
			$str .= '?>';
			$fp = fopen(ROOT_DIR . 'files/languages/' . $get['id'] . '/dictionary/' . $key . '.php', 'w');
			fwrite($fp, $str);
			fclose($fp);
		}
	}
	//если мультиязычный то нужно добавлять колонки в мультиязычные таблицы
	if ($config['multilingual']) {
		if ($_GET['id'] == 'new') {
			//товары
			mysql_fn('query',"ALTER TABLE `shop_products` ADD `name".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `name`");
			mysql_fn('query',"ALTER TABLE `shop_products` ADD `text".$get['id']."` TEXT NOT NULL AFTER `text`");
			mysql_fn('query',"ALTER TABLE `shop_products` ADD `url".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `url`");
			mysql_fn('query',"ALTER TABLE `shop_products` ADD `title".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `title`");
			mysql_fn('query',"ALTER TABLE `shop_products` ADD `keywords".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `keywords`");
			mysql_fn('query',"ALTER TABLE `shop_products` ADD `description".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `description`");
			//категории
			mysql_fn('query',"ALTER TABLE `shop_categories` ADD `name".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `name`");
			mysql_fn('query',"ALTER TABLE `shop_categories` ADD `text".$get['id']."` TEXT NOT NULL AFTER `text`");
			mysql_fn('query',"ALTER TABLE `shop_categories` ADD `url".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `url`");
			mysql_fn('query',"ALTER TABLE `shop_categories` ADD `title".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `title`");
			mysql_fn('query',"ALTER TABLE `shop_categories` ADD `keywords".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `keywords`");
			mysql_fn('query',"ALTER TABLE `shop_categories` ADD `description".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `description`");

			//параметры пользователя
			mysql_fn('query',"ALTER TABLE `user_fields` ADD `name".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `name`");
			mysql_fn('query',"ALTER TABLE `user_fields` ADD `hint".$get['id']."` TEXT NOT NULL AFTER `hint`");
			//статутсы заказа
			mysql_fn('query',"ALTER TABLE `order_types` ADD `name".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `name`");
			mysql_fn('query',"ALTER TABLE `order_types` ADD `text".$get['id']."` TEXT NOT NULL AFTER `text`");
			//доставка
			mysql_fn('query',"ALTER TABLE `order_deliveries` ADD `name".$get['id']."` VARCHAR( 255 ) NOT NULL AFTER `name`");
			mysql_fn('query',"ALTER TABLE `order_deliveries` ADD `text".$get['id']."` TEXT NOT NULL AFTER `text`");
		}
	}
}



//вкладки
$tabs = array(
	0 => 'Общее',
	1 => 'Формы',
	8 => 'Главная',
	9 => 'Услуги',
	3 => 'Портфолио',
	10 => 'О нас',
	11 => 'Инвестиции',
	13 => 'Подбор обьектов',
    12 => 'Контакты',
    14 => 'Инвестиции',
	//2 => 'Профайл',
	//4 => 'Корзина',
	//5 => 'Яндекс-маркет',
	//6 => 'Рассылка',
	//7 => 'Календарь'
);

$form[0][] = lang_form('input td12','common|site_name','название сайта');
$form[0][] = lang_form('textarea td12','common|script_head','metatag (внутри тега head)');
$form[0][] = lang_form('textarea td12','common|script_body_start','после открывающегося тега body');
$form[0][] = lang_form('textarea td12','common|script_body_end','перед закрывающимся тегом body');

$form[12][] = lang_form('input td4','common|email_name','email');
$form[12][] = lang_form('input td4','common|address_name','адрес');
$form[12][] = lang_form('input td4','common|phone_name','телефон');
$form[12][] = lang_form('input td4','common|working_days_name','рабочие дни');
$form[12][] = lang_form('input td4','common|working_hours_name','время роботы');
$form[12][] = lang_form('input td4','common|email','email');
//$form[12][] = lang_form('input td4','common|address','адрес');
//$form[12][] = lang_form('input td4','common|address_abkhazia','адрес Абхазия');


$form[12][] = '<h2>Сочи</h2>';
$form[12][] = lang_form('input td4','common|address_city1','Сочи');
$form[12][] = lang_form('input td4','common|address1','адрес Сочи');
$form[12][] = lang_form('input td4','common|email1','email Сочи');
$form[12][] = lang_form('input td4','common|phone1','телефон Сочи');
$form[12][] = lang_form('input td4','common|phone1_link','телефон для ссылки Сочи');
$form[12][] = lang_form('input td4','common|working_hours1','время роботы Сочи');
$form[12][] = lang_form('input td4','common|coordinates1','координаты Сочи');

$form[12][] = '<h2>Гагра</h2>';
$form[12][] = lang_form('input td4','common|address_city2','Гагра');
$form[12][] = lang_form('input td4','common|address2','адрес Гагра');
$form[12][] = lang_form('input td4','common|email2','email Гагра');
$form[12][] = lang_form('input td4','common|phone2','телефон Гагра');
$form[12][] = lang_form('input td4','common|phone2_link','телефон для ссылки Гагра');
$form[12][] = lang_form('input td4','common|working_hours2','время роботы Гагра');
$form[12][] = lang_form('input td4','common|coordinates2','координаты Гагра');

$form[12][] = '<h2>Адлер</h2>';
$form[12][] = lang_form('input td4','common|address_city3','Адлер');
$form[12][] = lang_form('input td4','common|address3','адрес Адлер');
$form[12][] = lang_form('input td4','common|email3','email Адлер');
$form[12][] = lang_form('input td4','common|phone3','телефон Адлер');
$form[12][] = lang_form('input td4','common|phone3_link','телефон для ссылки Адлер');
$form[12][] = lang_form('input td4','common|working_hours3','время роботы Адлер');
$form[12][] = lang_form('input td4','common|coordinates3','координаты Адлер');

$form[12][] = '<h2>Санкт-Петербург</h2>';
$form[12][] = lang_form('input td4','common|address_city4','Санкт-Петербург');
$form[12][] = lang_form('input td4','common|address4','адрес Санкт-Петербург');
$form[12][] = lang_form('input td4','common|email4','email Санкт-Петербург');
$form[12][] = lang_form('input td4','common|phone4','телефон Санкт-Петербург');
$form[12][] = lang_form('input td4','common|phone4_link','телефон для ссылки Санкт-Петербург');
$form[12][] = lang_form('input td4','common|working_hours4','время роботы Санкт-Петербург');
$form[12][] = lang_form('input td4','common|coordinates4','координаты Санкт-Петербург');

$form[12][] = '<h2>Москва</h2>';
$form[12][] = lang_form('input td4','common|address_city5','Москва');
$form[12][] = lang_form('input td4','common|address5','адрес Москва');
$form[12][] = lang_form('input td4','common|email5','email Москва');
$form[12][] = lang_form('input td4','common|phone5','телефон Москва');
$form[12][] = lang_form('input td4','common|phone5_link','телефон для ссылки Москва');
$form[12][] = lang_form('input td4','common|working_hours5','время роботы Москва');
$form[12][] = lang_form('input td4','common|coordinates5','координаты Москва');

//$form[0][] = lang_form('input td4','common|txt_head3','время роботы');
$form[0][] = lang_form('input td4','common|get_free_consultation','получить бесплатную консультацию');
$form[0][] = lang_form('input td4','common|txt_head4','текст в шапке кнопка');

$form[0][] = lang_form('input td4','common|make_call','заказать звонок');
$form[0][] = lang_form('input td4','common|make_call_shop','заказать звонок портфолио');
$form[0][] = lang_form('input td4','common|make_call2','заказать обратный звонок');

$form[0][] = lang_form('input td4','common|menu_footer_name1','СтройДом');
$form[0][] = lang_form('input td4','common|menu_footer_name2','Строительство');
$form[0][] = lang_form('input td4','common|menu_footer_name3','Проекты домов');
$form[0][] = lang_form('input td4','common|menu_footer_name4','Услуги');


$form[0][] = lang_form('input td4','common|wrd_more','подробнее');
$form[0][] = lang_form('input td4','common|wrd_more1','Показать');
$form[0][] = lang_form('input td4','common|wrd_more2','подробнее о ценах');
$form[0][] = lang_form('input td4','common|back_review_btn','назад к отзывам');
$form[0][] = lang_form('input td4','common|msg_no_results','нет результатов');
$form[0][] = lang_form('input td4','common|wrd_no_photo','нет картинки');
$form[0][] = lang_form('input td4','common|breadcrumb_index','хлебные крошки: на главную');
$form[0][] = lang_form('input td4','common|breadcrumb_separator','хлебные крошки: разделитель');
$form[0][] = lang_form('input td4','common|make_selection','сделайте выбор');
$form[0][] = lang_form('input td4','common|pagination_prev','&#171;');
$form[0][] = lang_form('input td4','common|pagination_next','&#187;');
$form[0][] = lang_form('input td4','common|pagination_count_all','все');


$form[0][] = lang_form('input td4','common|requisites','реквизиты');
$form[0][] = lang_form('textarea td12','common|requisites_value','реквизиты');

//$form[0][] = lang_form('textarea td12','common|txt_index','текст на главной');
$form[0][] = lang_form('textarea td12','common|social','социальные кнопки');
$form[0][] = lang_form('textarea td12','common|txt_footer','текст в подвале');
$form[0][] = lang_form('input td12','common|str_no_page_name','название страницы 404');
$form[0][] = lang_form('textarea td12','common|txt_no_page_text','текст страницы 404');

$form[0][] = array('file_multi','contract','файлы договора','',array('name'=>'input'));

$form[8][] = lang_form('input td3','common|txt_slider','текст в слайдере');
$form[8][] = lang_form('input td3','common|txt_slider_round','текст в слайдере сбоку');

$form[8][] = lang_form('input td3','common|txt_slider_price','текст в слайдере цена');
$form[8][] = lang_form('input td3','common|txt_slider_price_und','текст в слайдере от');
$form[8][] = lang_form('input td3','common|txt_slider_count','текст в слайдере количество');
$form[8][] = lang_form('input td3','common|txt_slider_count_name','текст в слайдере количество домов');
$form[8][] = lang_form('input td3','common|txt_slider_count_link','кнопка в слайдере показать');

$form[8][] = lang_form('input td3','common|we_builds','мы строим');
$form[8][] = lang_form('input td3','common|why_we','почему мы');
$form[8][] = lang_form('input td3','common|our_services','наши услуги');
$form[8][] = lang_form('input td3','common|our_prices','наши цены');
$form[8][] = lang_form('input td3','common|portfolio','портфолио');
//$form[8][] = lang_form('input td3','common|order_service','заказать услугу');
$form[8][] = lang_form('input td3','common|certificates','сертификаты');
$form[8][] = lang_form('input td3','common|partners','партнеры');

$form[8][] = lang_form('input td3','common|order_project','заказать проект');
$form[8][] = lang_form('input td3','common|order_consultation','заказать консультацию');
$form[8][] = lang_form('input td3','common|order_consultation_vacancy','получить работу');
$form[8][] = lang_form('input td3','common|order_consultation_shop','записаться на экскурсию');
$form[8][] = lang_form('input td3','common|ask_questions_txt','Остались вопросы');
$form[8][] = lang_form('input td3','common|ask_questions_btn','кнопка Узнать больше');
$form[8][] = lang_form('input td3','common|wrd_more_price','Подробнее о ценах');

//$form[8][] = lang_form('tinymce td12','common|main_text','текст на главной');
//$form[8][] = lang_form('tinymce td12','common|main_text2','текст на главной 2');

$form[9][] = lang_form('input td4','common|we_also_build','также строим');
$form[9][] = lang_form('input td4','common|we_also_designing','также проектируем');
$form[9][] = lang_form('input td4','common|we_also_do','другие работы');
$form[9][] = lang_form('input td4','common|building_stages','стадии строительства');
$form[9][] = lang_form('input td4','common|designing_stages','стадии проектирование');
$form[9][] = lang_form('input td4','common|work_stages','стадии работы');
$form[9][] = lang_form('input td4','common|we_designing','мы проектируем');
$form[9][] = lang_form('input td4','common|we_do','мы выполняем');
$form[9][] = lang_form('input td4','common|style','стиль');
$form[9][] = lang_form('input td4','common|bedrooms','спальни');
$form[9][] = lang_form('input td4','common|type_roof','тип кровли');
$form[9][] = lang_form('input td4','common|covering','перекрытия');
$form[9][] = lang_form('input td4','common|finishing_facade','отделка фасада');
$form[9][] = lang_form('input td4','common|dimensions','габариты');
$form[9][] = lang_form('input td4','common|garage','гараж');
$form[9][] = lang_form('input td4','common|type_foundation','тип фундамента');
$form[9][] = lang_form('input td4','common|wall_materials','материалы стен');
$form[9][] = lang_form('input td4','common|roof_material','материал кровли');
$form[9][] = lang_form('input td4','common|plan','планирование');
$form[9][] = lang_form('input td4','common|facade','фасады');
$form[9][] = lang_form('tinymce td12','common|services_list','услуги');



$form[1][] = '<h2>Форма обратной связи</h2>';
$form[1][] = lang_form('input td4','common|order_service','заказать услугу');
$form[1][] = lang_form('input td4','common|order_service_vacancy','задать вопрос вакансии');
$form[1][] = lang_form('input td4','common|order_building','заказать строительство');
$form[1][] = lang_form('input td4','common|order_designing','заказать проектирование');
$form[1][] = lang_form('input td4','common|order_repairs','заказать ремонтные работы');
$form[1][] = lang_form('input td4','feedback|name_contract','получить контракт');
$form[1][] = lang_form('input td4','feedback|name','представтесь');
$form[1][] = lang_form('input td4','feedback|phone','контактный телефон');
$form[1][] = lang_form('input td4','feedback|services','выбирете услугу');
$form[1][] = lang_form('input td4','feedback|services2','укажите услугу');

//$form[1][] = lang_form('input td12','feedback|email','еmail');
$form[1][] = lang_form('input td4','feedback|text','сообщение');
$form[1][] = lang_form('input td4','feedback|send','отправить');
$form[1][] = lang_form('input td4','feedback|send_contract','отправить контракт');
//$form[1][] = lang_form('input td12','feedback|attach','прикрепить файл');
$form[1][] = lang_form('input td6','feedback|order_is_sent','услуга заказана');
$form[1][] = lang_form('input td6','feedback|order_is_sent_vacancy','вопрос по вакансии');
//order_is_sent_vacancy
$form[1][] = lang_form('input td6','feedback|order_building_is_sent','строительство заказано');
$form[1][] = lang_form('input td6','feedback|order_designing_is_sent','проектирование заказано');
$form[1][] = lang_form('input td6','feedback|order_repairs_is_sent','ремонтные работы заказаны');
$form[1][] = lang_form('input td6','feedback|message_is_sent','сообщение отправлено');
$form[1][] = lang_form('input td6','feedback|contract_is_sent','контракт отправлен');


$form[1][] = '<h2>Сообщения в формах</h2>';
$form[1][] = lang_form('input td12','validate|no_required_fields','не заполнены обязательные поля');
$form[1][] = lang_form('input td12','validate|short_login','короткий логин');
$form[1][] = lang_form('input td12','validate|not_valid_login','некорректный логин');
$form[1][] = lang_form('input td12','validate|not_valid_email','некорректный email');
$form[1][] = lang_form('input td12','validate|not_valid_password','некорректный пароль');
$form[1][] = lang_form('input td12','validate|not_valid_captcha','некорректный защитный код');
$form[1][] = lang_form('input td12','validate|not_valid_captcha2','отключены скрипты');
$form[1][] = lang_form('input td12','validate|error_email','ошибка при отправке письма');
$form[1][] = lang_form('input td12','validate|error_order','ошибка при заказе');
$form[1][] = lang_form('input td12','validate|no_email','в базе нету такого email');
$form[1][] = lang_form('input td12','validate|duplicate_login','дублирование логина');
$form[1][] = lang_form('input td12','validate|duplicate_email','дублирование email');
$form[1][] = lang_form('input td12','validate|not_match_passwords','пароли не совпадают');

//$form[2][] = lang_form('input td12','profile|hello','здравствуйте');
//$form[2][] = lang_form('input td12','profile|link','личный кабинет');
//$form[2][] = lang_form('input td12','profile|user_edit','личные данные');
//$form[2][] = lang_form('input td12','profile|exit','выйти');
$form[2][] = '<h2>Форма авторизации/регистрации/редактирования</h2>';
$form[2][] = lang_form('input td3','profile|email','еmail');
$form[2][] = lang_form('input td3','profile|password','пароль');
$form[2][] = lang_form('input td3','profile|password2','подтв. пароль');
$form[2][] = lang_form('input td3','profile|new_password','новый пароль');
$form[2][] = lang_form('input td3','profile|save','сохранить');
$form[2][] = lang_form('input td3','profile|registration','регистрация');
$form[2][] = lang_form('input td3','profile|enter','войти');
$form[2][] = lang_form('input td3','profile|remember_me','запомнить меня');
$form[2][] = lang_form('input td3','profile|auth','авторизация');
$form[2][] = lang_form('input td3','profile|remind','забыли пароль');
$form[2][] = lang_form('input td12','profile|successful_registration','успешная регистрация');
$form[2][] = lang_form('input td12','profile|successful_auth','успешная авторизация');
$form[2][] = lang_form('input td12','profile|error_auth','ошибка авторизации');
$form[2][] = lang_form('input td12','profile|msg_exit','Вы вышли!');
$form[2][] = lang_form('input td12','profile|go_to_profile','перейти в профиль');
$form[2][] = '<h2>Восстановление пароля</h2>';
$form[2][] = lang_form('input td12','profile|remind_button','отправить письмо по восстановлению пароля');
$form[2][] = lang_form('input td12','profile|successful_remind','отправлено письмо по восстановлению пароля');

//$form[3][] = lang_form('input td3','shop|catalog','каталог');
//$form[3][] = lang_form('input td3','shop|new','новинки');
//$form[3][] = lang_form('input td3','shop|brand','производитель');
//$form[3][] = lang_form('input td3','shop|article','артикул');
$form[3][] = lang_form('input td3','shop|areas','площадь');
$form[3][] = lang_form('input td3','shop|material','материал');

$form[3][] = lang_form('input td3','shop|parameters','комплектация');
$form[3][] = lang_form('input td3','shop|garage','с гаражом');
$form[3][] = lang_form('input td3','shop|loft','с мансандрой');
$form[3][] = lang_form('input td3','shop|terrace','с терассой');

$form[3][] = lang_form('input td3','shop|floor','этажность');
$form[3][] = lang_form('input td3','shop|floor_unit','этажный');

$form[3][] = lang_form('input td3','shop|price','цена');
$form[3][] = lang_form('input td3','shop|price_from','цена от');
$form[3][] = lang_form('input td3','shop|price_for_sq','цена за сотку');
$form[3][] = lang_form('input td3','shop|area','общая площадь');
$form[3][] = lang_form('input td3','shop|our_price','цена у нас');
$form[3][] = lang_form('input td3','shop|competitors_price','цена у конкурентов');
$form[3][] = lang_form('input td3','shop|name_region','название района');

$form[3][] = lang_form('input td3','shop|currency','валюта');
$form[3][] = lang_form('input td3','shop|currency_metr','валюта за 1м2');
$form[3][] = lang_form('input td3','shop|sq','единицы площи');
$form[3][] = lang_form('input td3','shop|number_months','время строительства');
$form[3][] = lang_form('input td3','shop|months1','месяц');
$form[3][] = lang_form('input td3','shop|months2','месяца');
$form[3][] = lang_form('input td3','shop|months5','месяцев');
$form[3][] = lang_form('input td3','shop|floor1','этаж');
$form[3][] = lang_form('input td3','shop|floor2','этажа');
$form[3][] = lang_form('input td3','shop|floor5','этажей');
$form[3][] = lang_form('input td3','shop|costs','стоимость');
$form[3][] = lang_form('input td3','shop|size','размер');
//$form[3][] = lang_form('input td3','shop|product_random','случайный товар');
$form[3][] = lang_form('input td3','shop|filter_button','искать');
$form[3][] = lang_form('input td3','shop|want_same','хочу также');

$form[3][] = lang_form('input td3','shop|hotel','гостиницы');
$form[3][] = lang_form('input td3','shop|apartment_buildings','многоквартирные дома');

$form[3][] = lang_form('input td3','shop|country_house','дачные дома');
$form[3][] = lang_form('input td3','shop|with_attic','дома с мансардным этажом');
$form[3][] = lang_form('input td3','shop|townhouse_dupliksy','таунхаус и дупликсы');
$form[3][] = lang_form('input td6','shop|we_do_for_you','выполнить для Вас');

/*
$form[3][] = '<h2>Отзывы</h2>';
$form[3][] = lang_form('input td3','shop|reviews','Отзывы');
$form[3][] = lang_form('input td3','shop|review_add','Оставить отзыв');
$form[3][] = lang_form('input td3','shop|review_name','имя');
$form[3][] = lang_form('input td3','shop|review_email','еmail');
$form[3][] = lang_form('input td3','shop|review_text','сообщение');
$form[3][] = lang_form('input td3','shop|review_send','отправить');
$form[3][] = lang_form('input td3','shop|want_same','хочу также');
$form[3][] = lang_form('input td12','shop|review_is_sent','отзыв добавлен');
*/
$form[4][] = lang_form('input td3','basket|buy','купить');
$form[4][] = lang_form('input td3','basket|basket','корзина');
$form[4][] = lang_form('input td12','basket|empty','пустая корзина');
$form[4][] = lang_form('input td12','basket|go_basket','перейти в корзину');
$form[4][] = lang_form('input td12','basket|go_next','продолжить покупки');
$form[4][] = lang_form('input td12','basket|product_added','товар добавлен');
$form[4][] = '<h2>Оплата</h2>';
$form[4][] = lang_form('input td12','order|payments','оплата');
$form[4][] = lang_form('input td12','order|pay','оплатить');
$form[4][] = lang_form('input td12','order|paid','оплачен');
$form[4][] = lang_form('input td12','order|not_paid','не плачен');
$form[4][] = lang_form('textarea td12','order|success','успешная оплата');
$form[4][] = lang_form('textarea td12','order|fail','отказ оплаты');

$form[4][] = '<h2>Таблица товаров</h2>';
$form[4][] = lang_form('input td3','basket|product_id','id товара');
$form[4][] = lang_form('input td3','basket|product_name','название товара');
$form[4][] = lang_form('input td3','basket|product_price','цена');
$form[4][] = lang_form('input td3','basket|product_count','количество');
$form[4][] = lang_form('input td3','basket|product_summ','сумма');
$form[4][] = lang_form('input td3','basket|product_cost','стоимость');
$form[4][] = lang_form('input td3','basket|product_delete','удалить');
$form[4][] = lang_form('input td3','basket|total','итого');
$form[4][] = '<h2>Параметры заказа</h2>';
$form[4][] = lang_form('input td3','basket|profile','личные данные');
$form[4][] = lang_form('input td3','basket|delivery','доставка');
$form[4][] = lang_form('input td3','basket|delivery_cost','стоимость доставки');
$form[4][] = lang_form('input td3','basket|comment','коммен к заказу');
$form[4][] = lang_form('input td3','basket|order','оформить заказ');
$form[4][] = '<h2>Статистика заказов</h2>';
$form[4][] = lang_form('input td3','basket|orders','статистика заказов');
$form[4][] = lang_form('input td3','basket|order_name','заказ');
$form[4][] = lang_form('input td3','basket|order_from','от');
$form[4][] = lang_form('input td3','basket|order_status','статус');
$form[4][] = lang_form('input td3','basket|order_date','дата');
$form[4][] = lang_form('input td3','basket|view_order','просмотр заказа');

$form[5][] = 'Полное описание можно найти на странице <a target="_balnk" href="http://help.yandex.ru/partnermarket/shop.xml">http://help.yandex.ru/partnermarket/shop.xml</a><br /><br />';
$form[5][] = lang_form('input td12','market|name','Короткое название магазина');
$form[5][] = lang_form('input td12','market|company','Полное наименование компании');
$form[5][] = lang_form('input td12','market|currency','Валюта магазина');

$form[6][] = '<h2>Основной шаблон автоматического письма</h2>';
$form[6][] = lang_form('textarea td12','common|letter_top','Текст в шапке письма');
$form[6][] = lang_form('textarea td12','common|letter_footer','Текст в подвале письма');
$form[6][] = '<h2>Основной шаблон письма рассылки</h2>';
$form[6][] = lang_form('textarea td12','subscribe|top','Текст в шапке рассылки');
$form[6][] = lang_form('textarea td12','subscribe|bottom','Текст в подвале рассылки');
$form[6][] = lang_form('input td8','subscribe|letter_failure_str','Если вы хотите отписаться от рассылки нажмите на');
$form[6][] = lang_form('input td4','subscribe|letter_failure_link','ссылку');
$form[6][] = '<h2>Подписка</h2>';
$form[6][] = lang_form('input td12','subscribe|on_button','Подписаться');
$form[6][] = lang_form('input td12','subscribe|on_success','Вы успешно подписаны');
$form[6][] = lang_form('input td12','subscribe|failure_text','Подтвердите, что хотите отписаться');
$form[6][] = lang_form('input td12','subscribe|failure_button','Отписаться');
$form[6][] = lang_form('input td12','subscribe|failure_success','Вы отписаны');

$form[7][] = lang_form('input td3','calendar|year','год');
$form[7][] = lang_form('input td3','calendar|y','г.');
$form[7][] = lang_form('input td3','calendar|month','месяц');
$form[7][] = lang_form('input td3','calendar|m','m.');
$form[7][] = lang_form('input td3','calendar|day','день');
$form[7][] = lang_form('input td3','calendar|d','д.');
$form[7][] = '<h2>Полные названия месяцев</h2>';
$form[7][] = lang_form('input td3','calendar|month_01','январь');
$form[7][] = lang_form('input td3','calendar|month_02','февраль');
$form[7][] = lang_form('input td3','calendar|month_03','март');
$form[7][] = lang_form('input td3','calendar|month_04','апрель');
$form[7][] = lang_form('input td3','calendar|month_05','май');
$form[7][] = lang_form('input td3','calendar|month_06','июнь');
$form[7][] = lang_form('input td3','calendar|month_07','июль');
$form[7][] = lang_form('input td3','calendar|month_08','август');
$form[7][] = lang_form('input td3','calendar|month_09','сенябрь');
$form[7][] = lang_form('input td3','calendar|month_10','октябрь');
$form[7][] = lang_form('input td3','calendar|month_11','ноябрь');
$form[7][] = lang_form('input td3','calendar|month_12','декабрь');
$form[7][] = '<h2>Полные названия месяцев в родительном падеже</h2>';
$form[7][] = lang_form('input td3','calendar|month2_01','января');
$form[7][] = lang_form('input td3','calendar|month2_02','февраля');
$form[7][] = lang_form('input td3','calendar|month2_03','марта');
$form[7][] = lang_form('input td3','calendar|month2_04','апреля');
$form[7][] = lang_form('input td3','calendar|month2_05','мая');
$form[7][] = lang_form('input td3','calendar|month2_06','июня');
$form[7][] = lang_form('input td3','calendar|month2_07','июля');
$form[7][] = lang_form('input td3','calendar|month2_08','августа');
$form[7][] = lang_form('input td3','calendar|month2_09','сенября');
$form[7][] = lang_form('input td3','calendar|month2_10','октября');
$form[7][] = lang_form('input td3','calendar|month2_11','ноября');
$form[7][] = lang_form('input td3','calendar|month2_12','декабря');
$form[7][] = '<h2>Короткие названия месяцев</h2>';
$form[7][] = lang_form('input td3','calendar|mth_01','янв');
$form[7][] = lang_form('input td3','calendar|mth_02','фев');
$form[7][] = lang_form('input td3','calendar|mth_03','мар');
$form[7][] = lang_form('input td3','calendar|mth_04','апр');
$form[7][] = lang_form('input td3','calendar|mth_05','май');
$form[7][] = lang_form('input td3','calendar|mth_06','июн');
$form[7][] = lang_form('input td3','calendar|mth_07','июл');
$form[7][] = lang_form('input td3','calendar|mth_08','авг');
$form[7][] = lang_form('input td3','calendar|mth_09','сен');
$form[7][] = lang_form('input td3','calendar|mth_10','окт');
$form[7][] = lang_form('input td3','calendar|mth_11','ноя');
$form[7][] = lang_form('input td3','calendar|mth_12','дек');
$form[7][] = '<h2>Полные дней недели</h2>';
$form[7][] = lang_form('input td3','calendar|day_1','понедельник');
$form[7][] = lang_form('input td3','calendar|day_2','вторник');
$form[7][] = lang_form('input td3','calendar|day_3','среда');
$form[7][] = lang_form('input td3','calendar|day_4','четверг');
$form[7][] = lang_form('input td3','calendar|day_5','пятница');
$form[7][] = lang_form('input td3','calendar|day_6','субота');
$form[7][] = lang_form('input td3','calendar|day_7','воскресенье');
$form[7][] = '<h2>Короткие дней недели</h2>';
$form[7][] = lang_form('input td3','calendar|d_1','пн');
$form[7][] = lang_form('input td3','calendar|d_2','вт');
$form[7][] = lang_form('input td3','calendar|d_3','ср');
$form[7][] = lang_form('input td3','calendar|d_4','чт');
$form[7][] = lang_form('input td3','calendar|d_5','пт');
$form[7][] = lang_form('input td3','calendar|d_6','сб');
$form[7][] = lang_form('input td3','calendar|d_7','вс');

$form[10][] = array('file td6','photo', 'фото директора' ,array(''=>'resize 1000x1000','p-'=>'resize 260x260'));
$form[10][] = lang_form('input td6','common|photo_name','подпись к фото');

$form[11][] = lang_form('input td12','common|investment_benefits_name','преимущество инвестирования');
$form[11][] = lang_form('tinymce td12','common|investment_benefits_txt','текст под преимущество инвестирования');
$form[11][] = lang_form('input td4','common|our_invest','стать инвестором');
$form[11][] = lang_form('input td4','common|our_invest_program','наши инвест программы');
$form[11][] = lang_form('input td4','common|other_our_invest_program','другие наши инвест программы');
$form[11][] = lang_form('input td4','common|become_investor','станьте нашим инвестором');
//Become our investor

$form[13][] = lang_form('input td6','common|finished_objects','готовые дома, квартиры');
$form[13][] = lang_form('input td6','common|objects_get_info','получить информацию');
$form[13][] = lang_form('input td6','common|get_installments_info','получить расчет рассрочки');
$form[13][] = lang_form('input td6','common|get_installments_btn','получить расчет рассрочки кнопка');

//get_installments_btn
$form[13][] = lang_form('input td6','common|objects_get','получить подборку');
$form[13][] = lang_form('input td6','common|objects_advantages','в чем мы Выгоднее');
$form[13][] = lang_form('input td6','common|objects_installments','преимущества рассрочки');

$form[13][] = lang_form('input td6','common|objects_get_installments','рассчитать рассрочку');
$form[13][] = lang_form('input td6','common|objects_banks','банки-партнеры');
$form[13][] = lang_form('input td6','common|objects_pick_up','подобрать');
$form[13][] = lang_form('input td6','common|objects_choose_option','подобрать вариант');
$form[13][] = lang_form('input td6','common|objects_try_better','стараемся лучше');


$form[14][] = lang_form('input td12','investments|name','заголовок');
$form[14][] = lang_form('tinymce td12','investments|description','описание');
$form[14][] = lang_form('tinymce td12','investments|calculation','расчет');

//Pick up

//Advantages of installments

function lang_form($type,$key,$name) {
	global $lang;
	$key = explode('|',$key);
	//автозаполнение пустых полей
	if (@$_GET['fuel'] AND !isset($lang[$key[0]][$key[1]])) $lang[$key[0]][$key[1]] = $name;
	return array ($type,'dictionary['.$key[0].']['.$key[1].']',isset($lang[$key[0]][$key[1]]) ? $lang[$key[0]][$key[1]] : '',array('name'=>$name.' <b>'.$key[0].'|'.$key[1].'</b>','title'=>$key[0].'|'.$key[1]));
}


/*
$form[0][] = lang_form('input td12','site_name','название сайта');
$form[0][] = lang_form('textarea td12','txt_meta','metatag');
$form[0][] = lang_form('textarea td12','txt_head','текст в шапке');
$form[0][] = lang_form('textarea td12','txt_index','текст на главной');
$form[0][] = lang_form('textarea td12','txt_footer','текст в подвале');
$form[0][] = lang_form('input td12','str_no_page_name','название страницы 404');
$form[0][] = lang_form('textarea td12','txt_no_page_text','текст страницы 404');
$form[0][] = lang_form('input td12','wrd_more','подробнее');
$form[0][] = lang_form('input td12','msg_no_results','нет результатов');
$form[0][] = lang_form('input td12','wrd_no_photo','нет картинки');
$form[0][] = lang_form('input td8','breadcrumb_index','хлебные крошки: на главную');
$form[0][] = lang_form('input td4','breadcrumb_separator','хлебные крошки: разделитель');
$form[0][] = lang_form('input td4','make_selection','сделайте выбор');

$form[1][] = '<h2>Форма обратной связи</h2>';
$form[1][] = lang_form('input td12','feedback_name','имя');
$form[1][] = lang_form('input td12','feedback_email','еmail');
$form[1][] = lang_form('input td12','feedback_text','сообщение');
$form[1][] = lang_form('input td12','feedback_send','отправить');
$form[1][] = lang_form('input td12','feedback_attach','прикрепить файл');
$form[1][] = lang_form('input td12','feedback_message_is_sent','сообщение отправлено');

$form[1][] = '<h2>Сообщения в формах</h2>';
$form[1][] = lang_form('input td12','msg_no_required_fields','не заполнены обязательные поля');
$form[1][] = lang_form('input td12','msg_short_login','короткий логин');
$form[1][] = lang_form('input td12','msg_not_valid_login','некорректный логин');
$form[1][] = lang_form('input td12','msg_not_valid_email','некорректный email');
$form[1][] = lang_form('input td12','msg_not_valid_password','некорректный пароль');
$form[1][] = lang_form('input td12','msg_not_valid_captcha','некорректный защитный код');
$form[1][] = lang_form('input td12','msg_not_valid_captcha2','отключены скрипты');
$form[1][] = lang_form('input td12','msg_error_email','ошибка при отправке письма');
$form[1][] = lang_form('input td12','msg_no_email','в базе нету такого email');
$form[1][] = lang_form('input td12','msg_duplicate_login','дублирование логина');
$form[1][] = lang_form('input td12','msg_duplicate_email','дублирование email');
$form[1][] = lang_form('input td12','msg_not_match_passwords','пароли не совпадают');

$form[2][] = lang_form('input td12','profile_hello','здравствуйте');
$form[2][] = lang_form('input td12','profile_link','личный кабинет');
$form[2][] = lang_form('input td12','profile_user_edit','личные данные');
$form[2][] = lang_form('input td12','profile_exit','выйти');
$form[2][] = '<h2>Форма авторизации/регистрации/редактирования</h2>';
$form[2][] = lang_form('input td3','profile_email','еmail');
$form[2][] = lang_form('input td3','profile_password','пароль');
$form[2][] = lang_form('input td3','profile_password2','подтв. пароль');
$form[2][] = lang_form('input td3','profile_new_password','новый пароль');
$form[2][] = lang_form('input td3','profile_save','сохранить');
$form[2][] = lang_form('input td3','profile_registration','регистрация');
$form[2][] = lang_form('input td3','profile_enter','войти');
$form[2][] = lang_form('input td3','profile_remember_me','запомнить меня');
$form[2][] = lang_form('input td3','profile_auth','авторизация');
$form[2][] = lang_form('input td3','profile_remind','забыли пароль');
$form[2][] = lang_form('input td12','profile_successful_registration','успешная регистрация');
$form[2][] = lang_form('input td12','profile_successful_auth','успешная авторизация');
$form[2][] = lang_form('input td12','profile_error_auth','ошибка авторизации');
$form[2][] = lang_form('input td12','profile_msg_exit','Вы вышли!');
$form[2][] = lang_form('input td12','profile_go_to_profile','перейти в профиль');
$form[2][] = '<h2>Восстановление пароля</h2>';
$form[2][] = lang_form('input td12','profile_remind_button','отправить письмо по восстановлению пароля');
$form[2][] = lang_form('input td12','profile_successful_remind','отправлено письмо по восстановлению пароля');

$form[3][] = lang_form('input td3','shop_catalog','каталог');
$form[3][] = lang_form('input td3','shop_new','новинки');
$form[3][] = lang_form('input td3','shop_brand','производитель');
$form[3][] = lang_form('input td3','shop_article','артикул');
$form[3][] = lang_form('input td3','shop_parameters','параметры');
$form[3][] = lang_form('input td3','shop_product_random','случайный товар');
$form[3][] = lang_form('input td3','shop_currency','валюта');
$form[3][] = lang_form('input td3','shop_filter_button','искать');
$form[3][] = '<h2>Отзывы</h2>';
$form[3][] = lang_form('input td3','reviews','Отзывы');
$form[3][] = lang_form('input td3','review_add','Оставить отзыв');
$form[3][] = lang_form('input td3','review_name','имя');
$form[3][] = lang_form('input td3','review_email','еmail');
$form[3][] = lang_form('input td3','review_text','сообщение');
$form[3][] = lang_form('input td3','review_send','отправить');
$form[3][] = lang_form('input td12','review_is_sent','отзыв добавлен');


$form[4][] = lang_form('input td3','basket_buy','купить');
$form[4][] = lang_form('input td3','basket','корзина');
$form[4][] = lang_form('input td12','basket_empty','пустая корзина');
$form[4][] = lang_form('input td12','basket_go_basket','перейти в корзину');
$form[4][] = lang_form('input td12','basket_go_next','продолжить покупки');
$form[4][] = lang_form('input td12','basket_product_added','товар добавлен');
$form[4][] = '<h2>Таблица товаров</h2>';
$form[4][] = lang_form('input td3','basket_product_id','id товара');
$form[4][] = lang_form('input td3','basket_product_name','название товара');
$form[4][] = lang_form('input td3','basket_product_price','цена');
$form[4][] = lang_form('input td3','basket_product_count','количество');
$form[4][] = lang_form('input td3','basket_product_summ','сумма');
$form[4][] = lang_form('input td3','basket_product_cost','стоимость');
$form[4][] = lang_form('input td3','basket_product_delete','удалить');
$form[4][] = lang_form('input td3','basket_total','итого');
$form[4][] = '<h2>Параметры заказа</h2>';
$form[4][] = lang_form('input td3','basket_profile','личные данные');
$form[4][] = lang_form('input td3','basket_delivery','доставка');
$form[4][] = lang_form('input td3','basket_delivery_cost','стоимость доставки');
$form[4][] = lang_form('input td3','basket_comment','коммен к заказу');
$form[4][] = lang_form('input td3','basket_order','оформить заказ');
$form[4][] = '<h2>Статистика заказов</h2>';
$form[4][] = lang_form('input td3','basket_orders','статистика заказов');
$form[4][] = lang_form('input td3','basket_order_name','заказ');
$form[4][] = lang_form('input td3','basket_order_from','от');
$form[4][] = lang_form('input td3','basket_order_status','статус');
$form[4][] = lang_form('input td3','basket_order_date','дата');
$form[4][] = lang_form('input td3','basket_view_order','просмотр заказа');

$form[5][] = 'Полное описание можно найти на странице <a target="_balnk" href="http://help.yandex.ru/partnermarket/shop.xml">http://help.yandex.ru/partnermarket/shop.xml</a><br /><br />';
$form[5][] = lang_form('input td12','market_name','Короткое название магазина');
$form[5][] = lang_form('input td12','market_company','Полное наименование компании');
$form[5][] = lang_form('input td12','market_currency','Валюта магазина');

$form[6][] = '<h2>Основной шаблон автоматического письма</h2>';
$form[6][] = lang_form('input td12','letter_top','Текст в шапке письма');
$form[6][] = lang_form('input td12','letter_footer','Текст в подвале письма');
$form[6][] = '<h2>Подписка</h2>';
$form[6][] = lang_form('input td12','subscribe_top','Текст в шапке рассылки');
$form[6][] = lang_form('input td12','subscribe_bottom','Текст в подвале рассылки');
$form[6][] = lang_form('input td12','subscribe_letter_failure_str','Если вы хотите отписаться от рассылки нажмите на');
$form[6][] = lang_form('input td12','subscribe_letter_failure_link','ссылку');
$form[6][] = lang_form('input td12','subscribe_on_button','Подписаться');
$form[6][] = lang_form('input td12','subscribe_on_success','Вы успешно подписаны');
$form[6][] = lang_form('input td12','subscribe_on_letter_name','Подписан новый пользователь');
$form[6][] = lang_form('input td12','subscribe_failure_text','Подтвердите, что хотите отписаться');
$form[6][] = lang_form('input td12','subscribe_failure_button','Отписаться');
$form[6][] = lang_form('input td12','subscribe_failure_success','Вы отписаны');

function lang_form($type,$key,$name) {
	global $dictionary;
	return array ($type,'dictionary['.$key.']',isset($dictionary[$key]) ? $dictionary[$key] : '',array('name'=>$name.' <b>'.$key.'</b>','title'=>$key));
}
*/