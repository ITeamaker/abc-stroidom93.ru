<?php

$module['save_as'] = true;

//исключение при редактировании модуля
if ($get['u']=='edit') {
	$post['price_table'] = serialize($post['price_table']);
}

$modules_site = array(
	'pages'			=> 'Текстовая страница',
	'index'			=> 'Главная',
	'articles'		=> 'Статьи',
	//'news'			=> 'Новости',
	//'gallery'		=> 'Галерея',

	//'basket'		=> 'Корзина',
	'feedback'		=> 'Обратная связь',
	'services'		=> 'Услуги',
	'order_services'=> 'Заказы',
	//'sitemap'		=> 'Карта сайта',
	//'profile'		=> 'Личный кабинет',
	//'login'			=> 'Авторизация',
	//'registration'	=> 'Регистрация',
	//'remind'		=> 'Восстановление пароля',
	//'subscribe'		=> 'Подписка'
	'shop'				=> 'Каталог домов',
	'plots'				=> 'Каталог участков',
	'buildings'			=> 'Строительство',
	'decoration'		=> 'Отделка',
	'internal_finish'	=> 'Внутренняя отделка',
	'exterior_finish'	=> 'Внешняя отделка',
	'designing_houses'	=> 'Проектирование домов',
	'about_us'			=> 'О нас',
	'contacts'			=> 'Контакты',
	'certificates'		=> 'Сертификаты',
	'faq'				=> 'Вопрос-ответ',
	'reviews'			=> 'Отзывы',
	'calculator'		=> 'Калькулятор',
	'projects'			=> 'Проекты',
	'investments'		=> 'Инвестиции',
	'youtube'		=> 'Видео',
	//'chose_apartments'	=> 'Подбор квартир',
	//'chose_houses'		=> 'Подбор домов',
	'finished_objects'	=> 'Готовые обьекты',
);

$a18n['menu2'] = 'меню в подвале СтройДом';
$a18n['menu3'] = 'меню в подвале Строительство';
$a18n['menu4'] = 'меню в подвале Проекты домов';
$a18n['menu5'] = 'меню в подвале Услуги';
$a18n['name_site'] = 'название в меню';
$a18n['services'] = 'услуги';
$a18n['img_about1']	= 'картинка 1 блока';
$a18n['text_about1']	= 'текст для 1 блока';
$a18n['img_about2']	= 'картинка 2 блока';
$a18n['text_about2']	= 'текст для 2 блока';
$a18n['img_about3']	= 'картинка 3 блока';
$a18n['text_about3']	= 'текст для 3 блока';
$a18n['img_about4']	= 'картинка 4 блока';
$a18n['text_about4']	= 'текст для 4 блока';
$a18n['img_about5']	= 'картинка 5 блока';
$a18n['text_about5']	= 'текст для 5 блока';

$a18n['text_seo_1']	= 'SEO текст 1';
$a18n['text_seo_2']	= 'SEO текст 2';
$a18n['decoration']	= 'отделка';
$a18n['vacancy']	= 'вакансии';
$a18n['quote']	= 'цитата';
$a18n['quote_text']	= 'текст цитаты';
$a18n['lastmod']	= 'дата обновления страницы';
$a18n['priority']	= 'приоритет страницы';
$a18n['agreement']	= 'страница соглашения';
//h2_name
$a18n['h2_name']	= 'заголовок h2';


if ($get['u']=='form') {
	if (empty($post['module'])) $post['module'] = 'pages';
	foreach ($modules_site as $k=>$v)
		if (!file_exists(ROOT_DIR.'modules/'.$k.'.php'))
			unset($modules_site[$k]);
}

if ($get['u']=='edit') {
    $post['lastmod'] = date('Y-m-d H:i:s');
}

$table = array(
	'_tree'		=> true,
	'_edit'		=> true,
	'id'		=> '',
	'name'		=> '',
	'title'		=> '',
	'url'		=> '',
	'module'	=> $modules_site,
	//'rank'	=> '',
    'priority'		=>	'',
	'services'	=> 'boolean',
	'vacancy'	=> 'boolean',
	'menu'		=> 'boolean',
	'menu2'		=> 'boolean',
	'menu3'		=> 'boolean',
	'menu4'		=> 'boolean',
	'menu5'		=> 'boolean',
    'agreement'		=> 'boolean',
    //'lastmod'	=> 'text',
    'lastmod'	=>	'date',
	'display'	=> 'display'
);

//только если многоязычный сайт
if ($config['multilingual']) {
	$languages = mysql_select("SELECT id,name FROM languages ORDER BY rank DESC", 'array');
	//приоритет пост над гет
	if (isset($post['language'])) $get['language'] = $post['language'];
	if (@$get['language'] == 0) $get['language'] = key($languages);
	$query = "
		SELECT pages.*
		FROM pages
		WHERE pages.language = '".$get['language']."'
	";
	$filter[] = array('language', $languages);
	$form[] = '<input name="language" type="hidden" value="'.$get['language'].'" />';
}

$tabs = array(
	1=>'Общее',
	2=>'Цены для услуг',
	3=>'Блоки с описанием услуг',
	4=>'Блоки с SEO текстом',
    5=>'Карта сайта',
);

$delete['confirm'] = array('pages'=>'parent');

$form[1][] = array('input td3','name',true);
$form[1][] = array('input td3','name_site',true,array('help'=>'второе название, будет отображаться в меню'));
$form[1][] = array('select td3','module',array(true,$modules_site),array('help'=>'Модуль отвечает за тип информации на странице.'));
$form[1][] = array('input td1','rank',true,array('help'=>'рейтинг для услуг'));
$form[1][] = array('checkbox','display',true);
//decoration
$form[1][] = array('parent td3 td3','parent',true);
$form[1][] = array('checkbox','services',true);
//$form[1][] = array('checkbox','decoration',true,array('help'=>'главная страница отделки, для субкатегорий не включать'));
$form[1][] = array('checkbox','menu',true);
$form[1][] = array('checkbox','menu2',true,array('help'=>'второе меню, СтройДом'));
$form[1][] = array('checkbox','menu3',true,array('help'=>'второе меню, Строительство'));
$form[1][] = array('checkbox','menu4',true,array('help'=>'второе меню, Проекты домов'));
$form[1][] = array('checkbox','menu5',true,array('help'=>'второе меню, Услуги'));
$form[1][] = array('checkbox','vacancy',true,array('help'=>'страница вакансии, другие названия кнопок и формы'));
$form[1][] = array('checkbox','quote',true,array('help'=>'цитата, на страницах модуля о нас'));
$form[1][] = array('checkbox','agreement',true);

$form[5][] = array('input td3','lastmod',true,array('help'=>'дата обновления страницы, изменяется автоматически'));
$form[5][] = array('input td3','priority',true,array('help'=>'Для главной страницы 1.00, для страниц категорий 0.8 для страниц с картинками и видео 0.6-0.2(приоритеты устанавливаются в зависимости от важности индексации той или иной страницы)'));
$form[1][] = array('input td12','quote_text',true);
$form[1][] = array('input td12','h2_name',true,array('help'=>'заголовок h2, используется только в шаблоне модуля инвестиции'));

$form[1][] = array('tinymce td12','text',true);//,array('attr'=>'style="height:500px"'));
$form[1][] = array('seo','seo url title keywords description',true);
$form[1][] = array('file','img','основное фото',array(''=>'resize 1000x1000','p-'=>'cut 360x350'));

$form[2][] = '<div style="clear:both; background:#E9E9E9; padding:5px 10px; width:875px; margin:0 -10px">';
$form[2][] = '<table class="product_list">';
$form[2][] = '<tr data-i="0">';
$form[2][] = '<th class="product_name_head">площадь</th>';
$form[2][] = '<th class="product_name_head">цена</th>';
$form[2][] = '<th><a href="#" style="background:#35B374; display:inline-block; padding:2px; border-radius:10px"><span class="sprite plus"></span></a></th>';
$form[2][] = '</tr>';

$template['product'] = '
	<tr data-i="{i}">
		<td><input name="price_table[{i}][name]" value="{name}" class="product_name" /></td>
		<td><input name="price_table[{i}][price]" value="{price}" class="product_name"/></td>
		<td><a href="#" class="sprite boolean_0"></a></td>
	</tr>
';
if (isset($post['price_table'])) {
	$basket = unserialize($post['price_table']); //print_r ($basket);
	if ($basket) foreach ($basket as $key=>$val) {
		$val['i'] = $key;
		$form[2][] = template($template['product'],$val);
	}
}
$form[2][] = '</table>';
$form[2][] = '<div class="clear"></div></div>';


//шаблоны товара используются для js
$content = '<div style="display:none">';
$content.= '<textarea id="template_product">'.htmlspecialchars($template['product']).'</textarea>';
$content.= '</div>';
$content.= '<style type="text/css">
.form .product_list {width:100%}
.form .product_list th {text-align:left; padding:0 0 5px;}
.form .product_list td {border-top:1px solid #F3F3F3; padding:5px 0; vertical-align:top;}
.form .product_list input {text-align:right; border:1px solid gray; margin:0; padding:0 2px; height:19px; width:70px}
.form .product_list .product_name_head {width:410px; text-align: center;}
.form .product_list .product_name {width:380px; text-align:left;}
.form .product_list td td {border:none}
</style>';
$content.= '<script type="text/javascript">
$(document).ready(function(){
	$(document).on("click",".product_list th a",function(){
		var i = $(this).parents("table").find("tr:last").data("i");
		i++;
		var content = $("#template_product").val();
		content = content.replace(/{i}/g,i);
		content = content.replace(/{[\w]*}/g,"");
		$(this).parents("table").append(content);
		return false;
	});
	$(document).on("click",".product_list td a",function(){
		$(this).parents("tr").remove();
		return false;
	});
});
</script>';


$form[3][] = array('file td6','img_about1',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about1',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about2',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about2',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about3',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about3',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about4',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about4',true);
$form[3][] = '<div class="clear"></div>';
$form[3][] = array('file td6','img_about5',' фото',array(''=>'resize 1000x1000','p-'=>'cut 457x300'));
$form[3][] = array('textarea td6','text_about5',true);
$form[3][] = '<div class="clear"></div>';

$form[4][] = array('tinymce td12','text_seo_1',true,array('attr'=>'style="height:300px"'));
$form[4][] = array('tinymce td12','text_seo_2',true,array('attr'=>'style="height:300px"'));

