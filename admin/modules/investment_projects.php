<?php

//$module['save_as'] = true;

if ($get['u']=='edit') {
    $post['lastmod'] = date('Y-m-d H:i:s');
}

$a18n['text_seo_1']	= 'SEO текст';
$a18n['text_seo_2']	= 'SEO текст под формой';
$a18n['name_type']	= 'дополнительное название';
$a18n['lastmod']	= 'дата обновления страницы';
$a18n['priority']	= 'приоритет страницы';

$table = array(
	'id'		=>	'rank:desc ',
	'img'		=>	'img',
	'name'		=>	'',
	'rank'		=>	'',
	'url'		=>	'',
    'priority'		=>	'',
    'lastmod'	=>	'date',
	'display'	=>	'boolean'
);


$tabs = array(
	1=>'Общее',
	//2=>'Цены',
	//3=>'Блоки с описанием',
	4=>'Блоки с SEO текстом',
    5=>'Карта сайта',
);

$form[1][] = array('input td8','name',true);
//$form[1][] = array('input td4','name_type',true,array('help'=>'второе название, будет отображаться в меню и селекте формы'));
$form[1][] = array('input td2','rank',true);
$form[1][] = array('checkbox td2','display',true);
$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('file td6','img','основное фото',array(''=>'resize 1000x1000','p-'=>'cut 555x360'));
//$form[1][] = array('file td6','img_list','картинка для списка',array(''=>'resize 1000x1000','p-'=>'cut 360x200'));
$form[1][] = array('seo','seo url title keywords description',true);

$form[4][] = array('tinymce td12','text_seo_1',true,array('attr'=>'style="height:300px"'));
$form[4][] = array('tinymce td12','text_seo_2',true,array('attr'=>'style="height:300px"'));
//$form[4][] = array('tinymce td12','text_seo_2',true,array('attr'=>'style="height:300px"'));
$form[5][] = array('input td3','lastmod',true,array('help'=>'дата обновления страницы, изменяется автоматически'));
$form[5][] = array('input td3','priority',true,array('help'=>'Для главной страницы 1.00, для страниц категорий 0.8 для страниц с картинками и видео 0.6-0.2(приоритеты устанавливаются в зависимости от важности индексации той или иной страницы)'));