<?php
$module['save_as'] = true;

$a18n['type'] = 'тип страницы';

$type = array(
	1		=> 'Главная',
	2		=> 'Строительство',
	3		=> 'Проектирование домов',
	4		=> 'Внутренняя отделка',
	5		=> 'Внешняя отделка',
	6		=> 'Текстовые страницы',
	7		=> 'О нас',
	8		=> 'Инвестиции',
	9		=> 'Подбор готовых обьектов (квартиры)',
	10		=> 'Подбор готовых обьектов',
);


$table = array(
	'id'		=>	'type rank name',
	'name'		=>	'',
	'type'		=>	$type,
	'img'		=>	'img',
	'rank'		=>	'',
	'display'	=>	'boolean'
);



$form[] = array('input td5','name',true);
$form[] = array('select td3','type',array(true,$type,''));
$form[] = array('input td2','rank',true);
$form[] = array('checkbox td2','display',true);
$form[] = array('file td6','img','основное фото',array(''=>'resize 1000x1000','p-'=>'resize 205x100'));
