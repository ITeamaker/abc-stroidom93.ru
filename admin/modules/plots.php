<?php

$module['save_as'] = true;

if ($get['u']=='edit') {
    $post['lastmod'] = date('Y-m-d H:i:s');
}

$a18n['area'] = 'площадь';
$a18n['price'] = 'общая цена';
$a18n['price2'] = 'цена за сотку';
$a18n['lat'] = 'широта';
$a18n['lng'] = 'долгота';
$a18n['text_seo_1']	= 'SEO текст';
$a18n['lastmod']	= 'дата обновления страницы';
$a18n['priority']	= 'приоритет страницы';


$regions = mysql_select("SELECT id,name FROM `regions`",'rows_id');

$table = array(
	'_edit'		=>	true,
	'id'		=>	'price:asc id name price2 area',
	'img'		=>	'img',
	'name'		=>	'',
	//'url'		=>	'',
	'price'		=>	'right',
	'price2'	=>	'right',
	'area'		=>	'right',
	'region'	=>	$regions,
    'priority'		=>	'',
    'lastmod'	=>	'date',
	'display'	=>	'boolean'
);

$where = "";
if (isset($get['search']) && $get['search']!='') $where.= "
	AND (
		LOWER(name) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
		OR LOWER(text) like '%".mysql_res(mb_strtolower($get['search'],'UTF-8'))."%'
	)
";

$query = "
	SELECT
		*
	FROM
		plots
	WHERE id IS NOT NULL $where
";

$filter[] = array('search');


$tabs = array(
	1=>'Общее',
	3=>'Картинки',
	4=>'Координаты участка',
    5=>'Карта сайта',
);
//если мультиязычный то нужно добавляем вкладки языков кроме главного языка
if ($config['multilingual']) {
	//вкладку с главным языком не показываем
	foreach ($config['languages'] as $k => $v) if ($k>0) {
		//вкладки
		$tabs['1' . $v['id']] = $v['name'];
		//поля
		$form['1' . $v['id']][] = array('input td12', 'name' . $v['id'], true, array('name' => a18n('name')));
		$form['1' . $v['id']][] = array('tinymce td12', 'text' . $v['id'], true, array('name' => a18n('text')));
		$form['1' . $v['id']][] = array('input td12','url'.$v['id'],true, array('name' => a18n('url')));
		$form['1' . $v['id']][] = array('input td12','title'.$v['id'],true, array('name' => a18n('title')));
		$form['1' . $v['id']][] = array('input td12','keywords'.$v['id'],true, array('name' => a18n('keywords')));
		$form['1' . $v['id']][] = array('input td12','description'.$v['id'],true, array('name' => a18n('description')));
	}
}

// 'SELECT id name FROM shop_materials'
$regions = mysql_select('SELECT id, name FROM regions', 'array');
$form[1][] = array('input td7','name'.$lang['i'],true);
$form[1][] = array('input td3','date',true);
$form[1][] = array('checkbox','display',true);
$form[1][] = array('input td2','price',true);
$form[1][] = array('input td2','price2',true);
$form[1][] = array('input td2','area',true);
$form[1][] = array('select td3','region',array(true,$regions,''));


$form[1][] = array('tinymce td12','text'.$lang['i'],true);
$form[1][] = array('seo','seo url title keywords description',true);
//$form[1][] = array('tinymce td12','text_seo_1',true);

$form[3][] = array('file td6','img','Основная картинка',array(''=>'resize 1000x1000','l-'=>'resize 850x500','m-'=>'cut 408x240','i-'=>'cut 360x260','p-'=>'cut 262x150','ss'=>'cut 132x77'));
$form[3][] = array('file_multi','imgs','Дополнительные картинки',array(''=>'resize 1000x1000','l-'=>'resize 850x500','p-'=>'cut 132x77'));


$form[4][] = array('input td6','lat',true);
$form[4][] = array('input td6','lng',true);


$form[4][] ="
<script type='text/javascript'>
$(document).ready(function(){    
    var yaMap, myGeoObject;
    var lat, lng;    
    // Дождёмся загрузки API и готовности DOM.
    if($('#yaMap').length){
        ymaps.ready(init);     
    }
    
    function init () {
        lat = parseFloat($('#yaMap').data('lat'));
        lng = parseFloat($('#yaMap').data('lng'));
        lat = (lat) ? lat : 43.585414268837496;
        lng = (lng) ? lng : 39.72356157391357;        
        yaMap = new ymaps.Map('yaMap', {            
            center: [lat, lng],
            zoom: 16
        });
        yaMap.setType('yandex#map');
        yaMap.behaviors.disable('scrollZoom');
        
        myGeoObject = new ymaps.GeoObject({
            geometry: {
                type: 'Point',
                coordinates: [lat, lng]
            },
            properties: {

            }
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6',
            draggable: true
        });
		yaMap.events.add('click', function (e) {
			var coords = e.get('coords');
			lat = coords[0]
			lng = coords[1]
			$('input[name=lat]').val(lat);
			$('input[name=lng]').val(lng);
			var data = [lat, lng];
//			yaMap.setCenter(data); 
			console.log(myGeoObject);
			myGeoObject.geometry.setCoordinates(data);
		});

        yaMap.geoObjects.add(myGeoObject);
        myGeoObject.events.add('dragend', function (e) {
            var coords = myGeoObject.geometry.getCoordinates();
            $('input[name=lat]').val(coords[0]);
            $('input[name=lng]').val(coords[1]);
        });        
    }
    $('select[name=apartment_settlements],#apartment_settlements, #settlements').on('change',function(){        
        $.ajax({
            type: 'GET',
            url: '/ajax.php',
            data: {
                file: 'getobjects',                
                type: 'settlement_coords',
                id: $(this).val()
            },
            dataType: 'JSON',            
            success: function(data){ 
                if($('#yaMap').length){
                    yaMap.setCenter(data); 
                    console.log(myGeoObject);
                    myGeoObject.geometry.setCoordinates(data);
                }
            }
        });
    });

});
</script>

";


$form[4][] = '<div id="yaMap" style="width:100%; height:400px; margin: 15px 0; clear: both; display: block;" data-lat="'.@$post['lat'].'" data-lng="'.@$post['lng'].'"></div>';

$form[5][] = array('input td3','lastmod',true,array('help'=>'дата обновления страницы, изменяется автоматически'));
$form[5][] = array('input td3','priority',true,array('help'=>'Для главной страницы 1.00, для страниц категорий 0.8 для страниц с картинками и видео 0.6-0.2(приоритеты устанавливаются в зависимости от важности индексации той или иной страницы)'));