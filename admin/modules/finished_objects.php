<?php

$a18n['name_head']='название над фильтром';

$a18n['installments_name1']='описание шага 1';
$a18n['installments_name2']='описание шага 2';
$a18n['installments_name3']='описание результата';

$a18n['developer_name1']='текст преимуществ';
$a18n['developer_list1']='список преимуществ';

$a18n['about_name1']='описание преимущества 1';
$a18n['about_name2']='описание преимущества 2';
$a18n['about_name3']='описание преимущества 3';
$a18n['about_name4']='описание преимущества 4';

$a18n['next_name']='названия для перелинковки в подвале';
$a18n['name_items']='текст над обьектами';
$a18n['name_btn']='название кнопки';
$a18n['form_name_btn']='название кнопки шапке и окне';


//исключение при редактировании модуля
if ($get['u']=='edit') {
	$post['lastmod'] = date('Y-m-d H:i:s');
}

$table = array(
	'id'		=>	'rank:desc date name url title id',
	'name'		=>	'',
	'title'		=>	'',
	'url'		=>	'',
	'rank'		=>	'',
	'date'		=>	'date',
	'display'	=>	'boolean'
);

$delete = array(
	'confirm'	=>	array(
		'shop_parameters'	=>	'chose_type',
	),
);

$tabs = array(
	1=>'Общее',
	2=>'Параметры страницы',
	5=>'Карта сайта',
);

$form[1][] = array('input td5','name',true);
$form[1][] = array('input td2','rank',true);
$form[1][] = array('input td3','date',true,array('attr'=>'class="datepicker"'));
$form[1][] = array('checkbox','display',true);

$form[1][] = array('input td12','name_head',true);
$form[1][] = array('input td6','name_btn',true);
$form[1][] = array('input td6','form_name_btn',true);

$form[1][] = array('input td12','name_items',true);


//next_name

$form[1][] = array('tinymce td12','text',true);
$form[1][] = array('seo','seo url title keywords description',true);

$form[1][] = array('file td6','img_head','фото для фильтра',array(''=>'resize 1920x800','l-'=>'cut 1920x640'));
$form[1][] = array('file td6','img','фото для списка',array(''=>'resize 1200x1200','p-'=>'cut 555x360'));
$form[1][] = array('file td6','next_img','фото для блока в подвале',array(''=>'resize 1920x800','l-'=>'cut 1920x374'));
$form[1][] = array('input td6','next_name',true);

$form[2][] = '<h2>Расстрочка</h2>';
$form[2][] = array('file td6','installments_img1','фото шага',array(''=>'resize 1000x800','p-'=>'cut 263x194'));
$form[2][] = array('textarea td6','installments_name1',true);
$form[2][] = '<div class="clearfix clear"></div>';
$form[2][] = array('file td6','installments_img2','фото шага',array(''=>'resize 1000x800','p-'=>'cut 263x194'));
$form[2][] = array('textarea td6','installments_name2',true);
$form[2][] = '<div class="clearfix clear"></div>';
$form[2][] = array('file td6','installments_img3','фото результат',array(''=>'resize 1000x800','p-'=>'cut 359x194'));
$form[2][] = array('textarea td6','installments_name3',true);
$form[2][] = '<div class="clearfix clear"></div>';

$form[2][] = '<h2>Выгоды</h2>';
$form[2][] = array('textarea td4','developer_name1',true);
$form[2][] = array('textarea td8','developer_list1',true);
$form[2][] = array('file td6','developer_img','фото для фона блока',array(''=>'resize 1920x800','p-'=>'cut 1920x395'));

$form[2][] = '<h2>Наши преимущества</h2>';
$form[2][] = array('file td6','about_img1','фото шага',array(''=>'resize 1000x800','p-'=>'cut 263x163'));
$form[2][] = array('textarea td6','about_name1',true);
$form[2][] = '<div class="clearfix clear"></div>';
$form[2][] = array('file td6','about_img2','фото шага',array(''=>'resize 1000x800','p-'=>'cut 263x163'));
$form[2][] = array('textarea td6','about_name2',true);
$form[2][] = '<div class="clearfix clear"></div>';
$form[2][] = array('file td6','about_img3','фото результат',array(''=>'resize 1000x800','p-'=>'cut 263x163'));
$form[2][] = array('textarea td6','about_name3',true);
$form[2][] = '<div class="clearfix clear"></div>';
$form[2][] = array('file td6','about_img4','фото результат',array(''=>'resize 1000x800','p-'=>'cut 263x163'));
$form[2][] = array('textarea td6','about_name4',true);




$form[5][] = array('input td3','lastmod',true,array('help'=>'дата обновления страницы, изменяется автоматически'));
$form[5][] = array('input td3','priority',true,array('help'=>'Для главной страницы 1.00, для страниц категорий 0.8 для страниц с картинками и видео 0.6-0.2(приоритеты устанавливаются в зависимости от важности индексации той или иной страницы)'));
