<?php

$config['admin_lang'] = 'ru'; //язык админпанели

/*
//перенес всю оплату в payments.php
$config['merchants'] = array(
	1 => 'наличный рассчет',
	2 => 'robokassa [все платежи]',
	3 => 'robokassa [yandex]',
	4 => 'robokassa [wmr]',
	5 => 'robokassa [qiwi]',
	6 => 'robokassa [терминал]',
	7 => 'robokassa [банковской картой]',
);
$config['payments'] = array(
	1 => 'наличный рассчет',
	2 => 'robokassa',
	3 => 'yandex',
);
*/

//многи ко многим
$config['depend'] = array(
	'shop_products'=>array('categories'=>'shop_products-categories'),
);

//зеркальные модули
$config['mirrors'] = array(
	//'articles'=>'news',
	'shop_products_special'=>'shop_products',
	'landing_items1'=>'landing_items',
	'landing_items2'=>'landing_items',
	'landing_items3'=>'landing_items',
);

//перечисление значений boolean массива $table к которым подвязаны классы для иконок
$config['boolean'] = array(
	'boolean','display','market','yandex_index',
);

$modules_admin = array(
	'pages' => array(
		'pages'				=> 'pages',
		'slider'			=> 'slider',
		'advantages'		=> 'advantages',
		'building_stages'	=> 'building_stages',
		'investment_benefits'=>	'investment_benefits',
		'certificates'		=> 'certificates',
		'partners'			=> 'partners',
		'banks'				=> 'banks',
		'faq'				=> 'faq',
		'reviews'			=> 'reviews',
		'vacancy'			=> 'vacancy',
        'articles'			=> 'articles',
        'video'				=> 'video',
        'lideo'				=> 'lideo',
	),
	'catalog' => array(
		'shop_products'		=> 'shop_products',
		'shop_materials'	=> 'shop_materials',
		'buildings'			=> 'buildings',
		'designing_houses'	=> 'designing_houses',
        'projects_categories'=> 'projects_categories',
		'projects'			=> 'projects',
		'internal_finish'	=> 'internal_finish',
		'exterior_finish'	=> 'exterior_finish',
		'plots'				=> 'plots',
		'regions'			=> 'regions',
		'investments'		=> 'investments',
		'investment_projects'=>	'investment_projects',
		'portfolio_slider'	=>	'portfolio_slider',
		'project_statuses'			=>	'project_statuses',

	),
	'finished_objects'			=> array(
		'finished_objects'	=>'finished_objects',
		'finished_objects_page'	=>'finished_objects_page',
		'finished_object_items'	=>'finished_object_items',
		'chose_parameters'	=>'shop_parameters',
	),
	'order_services'=> array(
		'chose_objects'	=>'chose_objects',
		'feedback'		=> 'feedback',
		'feedback_calc'		=> 'feedback_calc',
		'order_services'=>'order_services',
		'contract_application'	=> 'contract_application',
	),
	'pars'	=> 'pars',
	'dictionary'	=> 'languages',
	'users' => array(
		'users'	=> 'users',
		'user_types'	=> 'user_types',
		'user_fields'	=> 'user_fields',
	),
	'config' => array(
		'config'			=> 'config',
		'letter_templates'	=> 'letter_templates',
		'logs'				=> 'logs',
	),
	'design' => array(
		'template_css'	=> 'template_css',
		'template_images'	=> 'template_images',
		'template_includes'	=> 'template_includes',
		'template_scripts'	=> 'template_scripts'
	),
	'backup' => array(
		'backup'	=> 'backup',
		'restore'	=> 'restore'
	),
	'synchronization'=>array(
		'export'	=> 'shop_export',
		//'import'	=> 'shop_import',
		//'upload_images'	=> 'shop_upload_images',
		//'импорт категорий'=>'shop_categories_import',
	),
	'seo' => array(
		'redirects'		=> 'redirects',
		'robots.txt'	=> 'seo_robots',
		'sitemap.xml'	=> 'seo_sitemap',
		'.htaccess'		=> 'seo_htaccess',
		//сео модули, по умолчанию закоментированы (используются в основном в проектах Пестрякова)
		//'links'		=> 'seo_links',
		//'pages'		=> 'seo_pages',
		//'import'		=> 'seo_links_import',
		//'export'		=> 'seo_links_export',
	),
);