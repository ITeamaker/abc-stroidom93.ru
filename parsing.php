<?php

/**
 * файл генерирует xml документ для yandex market
 * доступен по адресу /market.xml
 * в .htaccess есть настройка RewriteRule ^market.xml$ market.php [L]
 */

// загрузка настроек *********************************************************
define('ROOT_DIR', dirname(__FILE__).'/');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

// загрузка функций **********************************************************
require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

require (ROOT_DIR.'plugins/phpquery/phpQuery/phpQuery.php');


function geturl1($url,$proxy=false) {

	//global $proxylist;

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL,str_replace(' ','%20',$url));
	curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl, CURLOPT_HEADER, 0);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($curl, CURLOPT_TIMEOUT, 15);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
/*
	$i=0;
/*
	do {
		if($proxy&&count($proxylist)>0){
			$proxys=$proxylist[rand(0,count($proxylist)-1)];
			curl_setopt($curl, CURLOPT_PROXY, $proxys);
		}
		$data = curl_exec($curl);
		if(curl_errno($curl)){$data='';//$data=false;
			echo '+'.curl_errno($curl).'+<br>';
		}
		if(!in_array(curl_getinfo($curl,CURLINFO_HTTP_CODE),array(200,404))) {$data='';//$data=false;
			echo '++'.curl_getinfo($curl,CURLINFO_HTTP_CODE).'++<br>';
		}
		if(curl_getinfo($curl,CURLINFO_EFFECTIVE_URL)=='http://upn.ru/robotprotect.aspx'){$data='';//$data=false;
			echo '+++'.curl_getinfo($curl,CURLINFO_EFFECTIVE_URL).'+++<br>';
		}
		$i++;
//} while (!$data && $i<3); //стоп если получили данные или трижды получили ошибку
	} while (strpos($data,'</html>')===false && $i<10); //стоп если получили данные или трижды получили ошибку
*/
	$data = curl_exec($curl);
	curl_close($curl);
	usleep(100);
	return $data;

}

/*
$hbr =
	file_get_contents('http://demo.abc-cms.com/');
$document = phpQuery::newDocument($hbr);
$hentry = $document->find('body');

foreach ($hentry as $el) {
	$pq = pq($el); // Это аналог $ в jQuery
	// меняем атрибуты найденого элемента
	$pq->find('.shop_product_list');
	dd($pq);
	//$pq->find('div.entry-info')->remove();//удаляем ненужный эл-нт
	//$tags = $pq->find('ul.tags > li > a');
	//$tags->append(': ')->prepend(' :'); // добавляем : по бокам
	// добавляем контент в начало найденого элемента
	//$pq->find('div.content')->prepend('<br />')->prepend($tags);
}
*/

//echo $pq;

$site='https://www.avito.ru';
$number=19;
//$url="$site/stroidom93/rossiya/";
$url="$site/stroidom93/rossiya/nedvizhimost?p=".$number." ";
//$url="https://www.avito.ru/stroidom93/rossiya/nedvizhimost";
//https://www.avito.ru/stroidom93/rossiya/nedvizhimost?p=2

$stop=0;

$db=array();

//do {

	echo "<br>$url<br>";

	$file = ROOT_DIR.'parsing'.$number.'.txt';
	if (file_exists($file) ) {
		$document = file_get_contents($file);
	}else {
		$document = geturl1($url);
		//запись в файл
		$fp = fopen(ROOT_DIR.'parsing'.$number.'.txt','w');
		fwrite($fp, $document);
		fclose($fp);
	}

//echo 1;

	//dd(geturl1($url));
//echo 2;

	if(!$document) {
		echo "error $url<br>";
		//$stop=2;
		//continue;
	}
	$document=phpQuery::newDocument($document);
//echo 3;
//dd($document);
//echo 4;
	$k=$document->find('div.item_table');

	foreach ($k as $i) {
/*
		if($stop){
			continue;
		}
*/
		$h3=pq($i)->find('h3.title > a');
/*
		if(preg_match('/(промназначения)/',pq($h3)->attr('title'))){
			continue;
		}
		preg_match('/\s(\d+)\sсот\./',pq($h3)->attr('title'),$m);
		if(isset($m[1])){
			$p['square']=$m[1];} else {$p['square']=0;}
*/
/**/
		//echo 5;
	$p = array();
		$p['name']=pq($h3)->attr('title');
		$p['title']=$site.pq($h3)->attr('title');
		$p['url']=$site.pq($h3)->attr('href');
		$p['url']=str_replace('https://','http://',$p['url']);
		$p['price']=preg_replace('/[^\d]/','',pq($i)->find('div.about')->text());
		$p['region']=trim(pq($i)->find('.data p')->text());/**/
		$region_explode=explode('р-н',$p['region'],2);
		$p['region']=trim($region_explode[1]);
		$p['category']=trim(pq($i)->find('.data p')->text());/**/
		$category_explode=explode('р-н',$p['category'],2);
		$p['category']=trim($category_explode[0]);
		//pars
		mysql_fn('insert','pars',$p);
/*
		if(pq($i)->find('p.address > i.i-metro')->attr('title')!='') {$p['address']='Екатеринбург, м.'.$p['address'];}
		*/

/*
		$date=trim(pq($i)->find('div.data > div.c-2')->text());
		if(mb_strpos($date,'Сегодня ',0,'utf-8')!==false){$date=str_replace('Сегодня',date('Y-m-d'),$date);}
		elseif(mb_strpos($date,'Вчера ',0,'utf-8')!==false){$date=str_replace('Вчера',date('Y-m-d',time()-84000),$date);}
		else{
			$date=str_replace(array('января','февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'),
				array('01','02','03','04','05','06','07','08','09','10','11','12'),$date);
			$date=preg_replace('/^(\d)\s/u','0$1 ',$date);
			$date=preg_replace('/(\d+)\s(\d+)/u',date('Y').'-$2-$1 ',$date);
		}
		$date.=':00';$p['date']=$date;
*/
/*
//    if(isset($db[$p['link']])){
		$r=mysql_select('select id,link from items where link="'.$p['link'].'"','row');
		if($r) {
			$db[$r['link']]=$r['id'];
			echo '+';
//цену не заменяем, поскольку иногда указана цена за сотку
		}
		else {
// парсер страници
*/
/*
			$document1=geturl1($p['link']);if(!$document1) {echo 'error '.$p['link'].'<br>';continue;}
			$document1=phpQuery::newDocument($document1);
			$p['text']=trim(pq($document1)->find('div#desc_text')->text());
			$p['latlon']=trim(pq($document1)->find('div.b-search-map')->attr('data-map-lat').' '.pq($document1)->find('div.b-search-map')->attr('data-map-lon'));
			if($p['latlon']!=''){$p['latlon']=str_replace(' ',', ',$p['latlon']);}
			$p['name']=trim(pq($document1)->find('div#seller > strong')->text());
			list($p['area'],$p['region'],$p['form'],$p['needm'])=explode('|',findar($p['address'].$p['text']));
			$depend=array();$p['cadastr']='';
			if(preg_match_all('/66\s*:\s*\d+\s*:\s*\d+\s*:\s*\d+/',$p['text'],$m)){
				for($cad_i=0;$cad_i<count($m[0]);$cad_i++){
					$cadelem=preg_replace('/\s+/','',$m[0][$cad_i]);
					$p['cadastr'].=$cadelem.' ';
					$samecad=mysql_select("select id,depend as name from items where cadastr like '%$cadelem%' order by id asc",'array');
					if(count($samecad)>0){
						foreach($samecad as $samecadid=>$samecadval){
							$depend[$samecadid]=1;
							if($samecadval!=''){
								$samecadval=explode(',',$samecadval);
								foreach ($samecadval as $samecadv) {
									$depend[$samecadv]=1;
								}
							}
						}
					}
				}
				$p['cadastr']=trim($p['cadastr']);
			}
*/

			//dd($p,true);
			/*
			$last_id=mysql_fn('insert','items',$p);

			if($last_id){

				$db[$p['link']]=$last_id;echo '!';

				if(count($depend)>0){

					$depend[$last_id]=1;$dependstr=implode(',',array_keys($depend));

					mysql_query("update items set depend='$dependstr' where id in ($dependstr)");

				}

			}*/

		}
$document->unloadDocument();

/*
	}
while ($stop > 0);*/
/*
	$temp=$document->find('div.pagination__nav > a.pagination__page:last');
//  if(preg_match('/Следующая\sстраница/',pq($temp)->text())){
//    $url=$site.pq($temp)->attr('href');
//  } else {$stop=1;
	if(!count($k)){$stop=2;}
	elseif(preg_match('/Следующая\sстраница/',pq($temp)->text())){
		$url=$site.pq($temp)->attr('href');
	}else{$stop=1;}
	$document->unloadDocument();
//$stop=true;
} while (!$stop);
if($stop==1){
	mysql_select('update items set archive=1 where archive=0 and link like "http://www.avito.ru/%/zemelnye_uchastki/%" and id not in ('.implode(',',$db).')');
	mysql_select('update items set archive=0 where archive=1 and link like "http://www.avito.ru/%/zemelnye_uchastki/%" and id in ('.implode(',',$db).')');
}
*/