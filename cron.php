<?php

define('ROOT_DIR', dirname(__FILE__).'/');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

$file = isset($_GET['file']) ? $_GET['file']:''; //echo $file.'<br />';

if (!preg_match("/^[a-z_]+$/", $file)) die('ошибка запроса');

if(!is_file(ROOT_DIR."cron/$file.php")) die('нет такого файла');

require_once(ROOT_DIR."cron/$file.php");
