<div class="container buildings">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<h1><?=$q['name']?></h1>
				</div>
				<div class="clearfix"></div>
                <?php
                if ($q['text2']!=''){?>
                    <div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
                        <?=$q['text2']?>
                    </div>
                    <div class="clearfix"></div>
                <?php }?>
			</div>
		</div>
        <div class="col-lg-12 col-xs-12">
            <div class="point"><div class="sprite point_horizontal"></div></div>
        </div>
        <div class="clearfix"></div>
		<?php
		if ($q['seo_name']!='' && $q['seo_text']!==''){?>
			<div class="col-lg-12 col-xs-12">
                <div class="sprite hr"></div>
				<div class="h2"><?=$q['seo_name']?></div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
				<?=$q['seo_text']?>
			</div>
			<div class="clearfix"></div>
		<?php }?>
		<?=html_query('exterior_finish/why_we',"
			SELECT *
			FROM advantages
			WHERE display=1 AND type = 5
			ORDER BY rank DESC
		",'')?>
		<div class="clearfix"></div>
		<?php
		// блок плиткой
		if (($q['img_about1']!='' && $q['text_about1']!='')
		|| ($q['img_about2']!='' && $q['text_about2']!='')
		|| ($q['img_about3']!='' && $q['text_about3']!='')
		|| ($q['img_about4']!='' && $q['text_about4']!='')
		|| ($q['img_about5']!='' && $q['text_about5']!='')) {
		?>
			<div class="col-lg-12 col-xs-12">
				<div class="h2"><?=$q['name']?></div>
			</div>
			<div class="clearfix"></div>
			<div class="about_block">
				<?php

				$count_rows = 0;
				$count_block = 0;
				//
				while ($count_block++<5){
					if ($q['img_about'.$count_block]!='' && $q['text_about'.$count_block]!=''){
						$count_rows ++;
						if ($count_rows>1){?>
							<div class="col-lg-12 col-xs-12">
								<div class="point"><div class="sprite point_vertical"></div></div>
							</div>
						<?php } //E:\OpenServer\domains\stroy-center\files\buildings\12\img_about1
						if (fmod($count_rows,2)==0){?>
							<div class="col-lg-7 col-xs-7 about_blocks">
								<?=$q['text_about'.$count_block]?>
							</div>
							<div class="col-lg-5 col-xs-5 about_blocks">
								<img src="/files/exterior_finish/<?=$q['id']?>/img_about<?=$count_block?>/p-<?=$q['img_about'.$count_block]?>" title="<?=$page['name']?>" alt="<?=$page['name']?>">
							</div>
							<?php
						}
						else{?>
							<div class="col-lg-5 col-xs-5 about_blocks">
								<img title="<?=$page['name']?>" alt="<?=$page['name']?>" src="/files/exterior_finish/<?=$q['id']?>/img_about<?=$count_block?>/p-<?=$q['img_about'.$count_block]?>">
							</div>
							<div class="col-lg-7 col-xs-7 about_blocks">
								<?=$q['text_about'.$count_block]?>
							</div>

						<?php }
					}
				}
				?>
			</div>
			<div class="clearfix"></div>
		<?php }?>
        <div class="col-lg-12 col-xs-12">
            <div class="point"><div class="sprite point_horizontal"></div></div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12 col-xs-12">
            <div class="point_bottom">
                <button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call">
                    <?=i18n('common|order_consultation');?>
                </button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?=html_query('exterior_finish/building_stages',"
	SELECT *
	FROM building_stages
	WHERE display=1 AND type = 5
	ORDER BY rank DESC
",'')?>
<div class="container buildings">
    <div class="row">
        <?php
		// таблица с ценами
		echo html_array('exterior_finish/price_table',$q);
		// сео текст
		if ($q['text_seo_1'] != ''){?>
			<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
				<?=$q['text_seo_1']?>
			</div>
			<div class="clearfix"></div>
		<?php }
		// форма заказа
		echo html_array('exterior_finish/make_order');
		// сео текст 2
		if ($q['text_seo_2'] != ''){?>
			<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block seo_text_top">
				<?=$q['text_seo_2']?>
			</div>
			<div class="clearfix"></div>
		<?php }?>
	</div>
</div>