<?php
if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$url = get_url('exterior_finish',$q);

if ($i==1){?>
	<div class="col-lg-12 col-xs-12">
        <div class="sprite hr"></div>
		<div class="h2"><?=i18n('common|we_do')?></div>
	</div>
	<div class="clearfix"></div>
<?php }?>
	<div class="building_block col-lg-6 col-xs-6" style="background-image: url(/files/exterior_finish/<?=$q['id']?>/img/p-<?=$q['img']?>);">
		<div class="name">
			<span><?=$q['name']?></span>
		</div>
		<div class="btn_wrap">
			<div class="text">
				<?=$q['text']?>
			</div>
			<a href="<?=$url?>" title="<?=i18n('common|wrd_more')?>" class="btn_silver wrd_more"><?=i18n('common|wrd_more')?></a>
		</div>
	</div>
<?php
if (fmod($i,2)==0) echo '<div class="clearfix"></div>';
?>