<?php
/*
 * v.1.1.2
 * Документация
 * https://tech.yandex.ru/money/doc/payment-solution/About-docpage/
 */
include(ROOT_DIR . 'plugins/liqpay/LiqPay.php');
$liqpay = new LiqPay($config['liqpay_public_key'], $config['liqpay_private_key']);

echo $liqpay->cnb_form(array(
	'version' => '3',
	'amount' => $q['total'],
	'currency' => 'UAH',
	'description' => $_SERVER['HTTP_HOST'].' #' . $q['id'],
	'order_id' => $q['id'],
	'server_url' => 'http://'.$_SERVER['HTTP_HOST'].'/payments.php?payment_action=liqpay_result',
	'result_url' => 'http://'.$_SERVER['HTTP_HOST'].get_url().'/' . $modules['basket'] . '/success/',
	'public_key' => $config['liqpay_public_key'],
	//'sandbox' => 1
));
