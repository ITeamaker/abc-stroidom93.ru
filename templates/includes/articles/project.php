<?php
//print_r ($q);
$img = $q['img'] ? '/files/projects/'.$q['id'].'/img/'.$q['img'] : false;
$title = $q['project_link']!='' ? filter_var($q['project_link'],FILTER_SANITIZE_STRING) : filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('projects',$q);
if ($img){
?>
    </div>
    <div class="col-lg-12 col-xs-12 article_project">
        <div class="img">
            <img src="<?=$img?>" title="<?$title?>">
        </div>
        <div class="link_wrap">
            <?=$q['project_address']!='' ? '<div class="address">'.$q['project_address'].'</div>' : '' ?>
            <?php
            if($url){?>
                <div class="link"><a href="<?=$url?>" class="project_link" title="<?=$title?>"><?=$title?></a></div>
            <?php }?>
        </div>
    </div>

    <div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1  article_text">

<?php }