<?php
if ($i==1){
    $count = 0;
    $count_row = $num_rows/3;
    //echo floor($count_row).'<br>';
}
//echo $num_rows.'<br>';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('articles',$q);
$img = $q['img'] ? '/files/articles/'.$q['id'].'/img/p-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';

?>
<div class="col-lg-4 col-md-4 col-xs-4">
    <div class="article_list">
        <div class="date"><?=date2($q['date'],'d month y')?></div>
        <div class="img">
            <a href="<?=$url?>" title="<?=$title?>">
                <img src="<?=$img?>" class="" title="<?=$title?>">
            </a>
        </div>
        <div class="name">
            <a href="<?=$url?>" title="<?=$title?>"><?=$q['name']?></a>
        </div>
    </div>
</div>
<?php
if (fmod($i,3)==0) {
    $count ++;?>
    <div class="clearfix"></div>
    <?php
    if ($count<$count_row){
    ?>
    <div class="article_list_line_wrap col-lg-12 col-md-12 col-xs-12"><div class="article_list_line"></div></div>
        <?php }
}
