<?=html_sources('footer','fancybox.js')?>

<?php
//if (access('user admin')) print_r ($q);
$img = $q['img'] ? '/files/projects/'.$q['id'].'/img/l-'.$q['img'] : '/templates/images/no_img.svg';
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);

$imgs_plan = $q['imgs_plan'] ? unserialize($q['imgs_plan']) : false;
$imgs_facade = $q['imgs_facade'] ? unserialize($q['imgs_facade']) : false;

?>
<div class="shop_product_text content">
	<div>
		<div class="product_gallery left designing_houses">
			<div class="product_img_list-for">
				<?php
				$list = $list2 = '';
				if ($q['img']) {?>
					<a class="fancybox_gallery" title="<?=$title?>" rel="gallery<?=$q['id']?>" href="/files/projects/<?=$q['id']?>/img/<?=$q['img']?>" >
                        <div class="fancybox_gallery_items">
						    <img src="<?=$img?>" alt="<?=$title?>"  title="<?=$page['name']?>" />
                            <span class="gallery-img-cover" style="background-image: url(<?=$img?>)"></span>
                        </div>
					</a>
					<?php
					$list = '<div class="img_vertical_wrap"><img width="132" height="77" src="/files/projects/'.$q['id'].'/img/ss-'.$q['img'].'" alt="'.$title.'" title="'.$title.'" /></div>';
				} ?>
				<?php if ($images) {
					$n=1;
					foreach ($images as $k=>$v) if (@$v['display']==1) {
						$n++;
						$alt2=filter_var(@$v['name'],FILTER_SANITIZE_STRING);
						$title2=filter_var(@$v['title'],FILTER_SANITIZE_STRING);
						$title2 = $title2 ? $title2 : $alt2;
						$path = '/files/projects/'.$q['id'].'/imgs/'.$k.'/';
						$list .= '<div class="img_vertical_wrap"><img width="132" height="77"  src="'.$path.'p-'.$v['file'].'" alt="'.$alt2.'" title="'.$title2.'"  /></div>';
						echo '<a  class="fancybox_gallery" title="'.$title2.'" rel="gallery'.$q['id'].'" href="'.$path.$v['file'].'" ><div class="fancybox_gallery_items"><img src="'.$path.'l-'.$v['file'].'" alt="'.$alt2.'" title="'.$title2.'" /><span class="gallery-img-cover" style="background-image: url('.$path.'l-'.$v['file'].')"></span></div></a>';
					}
				}?>
			</div>
			<div class="product_img_list-nav"><?=$list?></div>
		</div>
		<div class="product_options right projects">
			<div class="row">
				<div class="col-lg-12 col-xs-12 name_page">
					<h1><?=$q['name']?></h1>
				</div>
				<?php
				$count_blocks = 0;
				if ($q['area']!=''){
					$count_blocks++;
					?>
					<div class="col-lg-12 col-xs-12">
						<div class="data" style="display: flex; justify-content: flex-start;">
							<div class="name"><?=i18n('shop|areas')?>:&nbsp;</div>
							<div class="value">
								<span><?=$q['area']?></span>
								<?=i18n('shop|sq')?>
							</div>
						</div>
					</div>
				<?php }
				if ($q['number_floors']!=0){
					$count_blocks++;
					?>
					<div class="col-lg-12 col-xs-12">
						<div class="data"  style="display: flex; justify-content: flex-start;">
							<div class="name"><?=i18n('shop|floor')?>:&nbsp;</div>
							<div class="value"><?=$config['shop_product_floor'][$q['number_floors']]?></div>
						</div>
					</div>
				<?php }
				elseif ($q['floors']!=0 && ($q['hotel']==0 && $q['apartment_buildings']==0 && $q['townhouse_dupliksys']==0)){
					$count_blocks++;
					?>
					<div class="col-lg-12 col-xs-12">
						<div class="data"  style="display: flex; justify-content: flex-start;">
							<div class="name"><?=i18n('shop|floor')?>:&nbsp;</div>
							<div class="value"><?=$config['shop_product_floor'][$q['floors']]?></div>
						</div>
					</div>
				<?php }
				//if (fmod($count_blocks,2)==0) echo '<div class="clearfix"></div>';
				if ($q['bedrooms']!=''){
					$count_blocks++;
					?>
					<div class="col-lg-12 col-xs-12">
						<div class="data"  style="display: flex; justify-content: flex-start;">
							<div class="name"><?=$q['hotel']==1 ? 'Комнаты' : i18n('common|bedrooms')?>:&nbsp;</div>
							<div class="value"><?=$q['bedrooms']?></div>
						</div>
					</div>
				<?php }
				//if (fmod($count_blocks,2)==0) echo '<div class="clearfix"></div>';
				//[4.36] Изменение параметров проектов
				/*
				if ($q['garage']!=''){
					$count_blocks++;
					?>
					<div class="col-lg-12 col-xs-12">
						<div class="data"  style="display: flex; justify-content: flex-start;">
							<div class="name"><?=i18n('common|garage')?>:&nbsp;</div>
							<div class="value"><?=$q['garage']?></div>
						</div>
					</div>
				<?php }
				*/
				?>
				<div class="clearfix"></div>
				<?php
				if ($q['price_metr']>0 && $q['apartment_buildings']==1) {?>
					<div class="col-lg-12 col-xs-12 price_houses">
						<?=number_format($q['price_metr'],0,'.',' ')?> <?=i18n('shop|currency_metr')?>
					</div>
				<?php }
				elseif ($q['price']>0){?>
				<div class="col-lg-12 col-xs-12 price_houses">
					<?=number_format($q['price'],0,'.',' ')?> P
				</div>
				<?php }?>
				<div class="col-lg-12 col-xs-12">
					<div class="button_wrap">
						<button type="button" class="btn_red btn-primary" data-toggle="modal" data-target="#order_project"><?=i18n('common|order_project')?></button>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
	if ($q['text']!=''){
	?>
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="text" style="margin-bottom: 30px;"><?=$q['text']?></div>
		</div>
	</div>
	<?php }
	if ($imgs_plan || $imgs_facade){?>
		<div class="gallary_designing">
			<ul class="tabs tabs_js">
				<?php
				if ($imgs_plan){?>
					<li data-num="1" class="tab_product <?=(isset($_GET['tab-facade']) ? '' : 'active')?>"><a href="?tab-plan">Планировки</a></li>
				<?php }
				if ($imgs_facade){?>
					<li data-num="2" class="tab_product <?=(isset($_GET['tab-facade']) ? 'active' : '')?>"><a href="?tab-facade">Фасады</a></li>
				<?php }?>
			</ul>
			<?php
			if ($imgs_plan){?>
			<div class="show_cont" data-num="1" style="<?=(isset($_GET['tab-facade']) ? 'display: none;' : 'display: block;')?>">
				<?php
				foreach ($imgs_plan as $k=>$v) if (@$v['display']==1) {
					$alt2=filter_var(@$v['name'],FILTER_SANITIZE_STRING);
					$title2=filter_var(@$v['title'],FILTER_SANITIZE_STRING);
					$title2 = $title2 ? $title2 : $alt2;
					$path = '/files/projects/'.$q['id'].'/imgs_plan/'.$k.'/';
					echo '<a  class="fancybox_single" title="'.$title2.'" href="'.$path.$v['file'].'" ><img src="'.$path.'l-'.$v['file'].'" alt="'.$alt2.'" title="'.$title2.'" /></a>';
				}
				//print_r ($imgs_plan);
				?>
			</div>
			<?php }
			if ($imgs_facade){?>
			<div class="show_cont" data-num="2" style="<?=(isset($_GET['tab-facade']) ? 'display: block;' : 'display: none;')?>">
				<?php
				foreach ($imgs_facade as $k=>$v) if (@$v['display']==1) {
					$alt2=filter_var(@$v['name'],FILTER_SANITIZE_STRING);
					$title2=filter_var(@$v['title'],FILTER_SANITIZE_STRING);
					$title2 = $title2 ? $title2 : $alt2;
					$path = '/files/projects/'.$q['id'].'/imgs_facade/'.$k.'/';
					echo '<a  class="fancybox_single" title="'.$title2.'"  href="'.$path.$v['file'].'" ><img src="'.$path.'l-'.$v['file'].'" alt="'.$alt2.'" title="'.$title2.'" /></a>';
				}
				//print_r ($imgs_facade);
				?>
			</div>
			<?php }?>
		</div>
	<?php }?>
	<div class="point"><div class="sprite point_horizontal"></div></div>
	<div class="product_btn">
		<button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call"><?=i18n('common|order_consultation')?></button>
	</div>
</div>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		$('.product_img_list-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			prevArrow: '<div class="slick_button_wrap_prev"><button type="button" class="slick-prev">Previous</button></div>',
			nextArrow: '<div class="slick_button_wrap_next"><button type="button" class="slick-next">Next</button></div>',
			fade: true,
		});
		$('.product_img_list-nav').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			asNavFor: '.product_img_list-for',
			prevArrow: '<div class="slick_button_wrap_prev"><button type="button" class="slick-prev">Previous</button></div>',
			nextArrow: '<div class="slick_button_wrap_next"><button type="button" class="slick-next">Next</button></div>',
			dots: false,
			//centerMode: true,
			focusOnSelect: true,
            //variableWidth: true
			//vertical: true,
		});
		$(".fancybox_gallery").fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			padding: 0,
			helpers : {
				title : {
					type : 'over'
				}
			}
		});
		$(".fancybox_single").fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			padding: 0,
			helpers : {
				title : {
					type : 'over'
				}
			}
		});
	});
</script>