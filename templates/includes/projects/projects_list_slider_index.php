<?=html_sources('footer','fancybox.js')?>

<?php
if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$img = $q['img'] ? '/files/projects/'.$q['id'].'/img/p-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('projects',$q);
$statuses = mysql_select("SELECT id,name FROM `project_statuses` WHERE id = ".$q['status']." LIMIT 1",'row');

//print_r($q);

if ($i==1){ ?>

<div class="container">
	<div class="sprite hr"></div>
	<?php
	if($u[3]){?>
		<div class="h3 variants__title" style="font-size: 36px;
    font-family: 'Roboto Condensed',sans-serif;
    color: #505261;
    text-align: center;
    text-transform: uppercase;
    margin: 20px 0 50px;
    line-height: 1.2;">Наши предложения:</div>
	<?}else{
		if ($html['module']=='index'){?>
			<h3 class="variants__title">Наши предложения:</h3>
			<div class="first_objects" >Квартирные дома:</div>
		<?}else{?>
			<div class="h3 variants__title"><?=$page['name_items']?></div>
		<?}
	}?>


	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="projects_slider_wrap">
<?php }
if ($q['img']) {
	?>
	<div class="shop_product_list col-xs-12 col-sm-12 col-md-4">
		<div class="shop_product_wrap">
			<div class="img">
				<div>
					<a href="<?=$url?>" title="<?=$title?>">
						<img alt="<?= $title?>" src="<?= $img ?>" style="height: 215px"/>

					</a>
				</div>
			</div>

			<div class="product_param_wrap projects" style="min-height: 60px;">
				<div class="name_stat_flex">
					<div class="data object_name">
						<a class="name_link" href="<?=$url?>" title="<?=$title?>"><span><?=$q['name']?></span></a>
					</div>
					<?php
					if($q['status']){?>
						<div class="status_object">
							<?=$statuses['name']?>
						</div>
					<?}	?>
				</div>
				<?php
				if ($q['price_metr']>0 && $q['apartment_buildings']==1) {?>
					<div class="data big price fix_price" style="width:45%">
						<div class="value val_ind">
							<?php /*
							if($q['price']>0){?>
								<span>Квартиры от <?=number_format($q['price'],0,'.',' ')?> руб.</span><br>
							<? } /**/?>
							<?php // количество квартир
							/*
							if($q['flats']>0){?>
								<span>Кол-во квартир <?=$q['flats']?></span><br>
							<? }
								/**/?>
							<?php
							// площадь квартир
							?>
							<?php
							if($q['flats_area']){?>
								<span><?=$q['flats_area']?></span>
							<?}
							?>


							<?php
							if($q['price_metr']>0){?>
								<span>Цена от <span><?=number_format($q['price_metr'],0,'.',' ')?></span> руб. за м2</span>
							<? } ?>

						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php
	/*
				<div class="img_wrap">
					<a class="fancybox_gallery " title="<?= $title ?>" href="<?= $img ?>">
						<img src="<?=$img?>" alt="<?= $title ?>"/>
					</a>
				</div>
	   */
	?>

	<?php
}
if ($i==$num_rows){?>
			</div>
		</div>
	</div>
</div>

	<?php
if (!isset($config['slick_projects_slider_wrap'])) {
	$config['slick_projects_slider_wrap'] = 1;
	?>
<script>
	document.addEventListener("DOMContentLoaded", function() {
		$('.projects_slider_wrap').slick({
			//lazyLoad: 'progressive',
			slidesToShow: 3,
			slidesToScroll: 1,
			arrows: !0,
			prevArrow: '<button type="button" class="slick-prev">Previous</button>',
			nextArrow: '<button type="button" class="slick-next">Next</button>',
			variableWidth: !0
		});
	})
</script>
	<?php } ?>
<?php }?>