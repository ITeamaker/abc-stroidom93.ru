<?=html_sources('footer','fancybox.js')?>

<?php
//if (access('user admin')) print_r ($page);
/**/
$img = '/files/portfolio_slider/'.$q['id'].'/img/'.$q['img'];
$img_p = '/files/portfolio_slider/'.$q['id'].'/img/p-'.$q['img'];
//$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
/*
$imgs_plan = $q['imgs_plan'] ? unserialize($q['imgs_plan']) : false;
$imgs_facade = $q['imgs_facade'] ? unserialize($q['imgs_facade']) : false;
*/
if ($i==1){
    if ($page['module']=='index'){?>
        <div class="col-lg-12 col-xs-12">
            <div class="sprite hr"></div>
            <div class="h1"><?=i18n('common|portfolio')?></div>
        </div>
    <?php }
?>
<div class="col-lg-12 col-xs-12">
    <div class="portfolio_slider_wrap">
<?php }
        if ($q['img']) {
            ?>
            <div class="img_wrap">
                <a class="fancybox_gallery " title="<?= $title ?>" rel="portfolio_slider" href="<?= $img ?>">
                    <img data-lazy="<?= $img_p ?>" alt="<?= $title ?>" />
                </a>
            </div>
            <?php
        }
if ($i==$num_rows){?>
    </div>
</div>
	<?php
	if (!isset($config['slick_portfolio_slider_wrap'])) {
		$config['slick_portfolio_slider_wrap'] = 1;
		?>
	<script>
		document.addEventListener("DOMContentLoaded", function() {
			$('.portfolio_slider_wrap').slick({
				lazyLoad: 'progressive',
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: !0,
				prevArrow: '<div class="slick_button_wrap_prev"><button type="button" class="slick-prev">Previous</button></div>',
				nextArrow: '<div class="slick_button_wrap_next"><button type="button" class="slick-next">Next</button></div>',
				variableWidth: !0,
				<?=$num_rows>3?'centerMode: true,':''?>
			});

		})
	</script>
	<?php } ?>
<?php }?>