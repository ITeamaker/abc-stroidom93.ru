<?php
if ($i==1) { ?>
	<div class="building_stages_bg">
		<div class="container buildings">
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<div class="h2"><?=i18n('common|designing_stages')?></div>
				</div>
				<div class="col-lg-12 col-xs-12">
					<div class="point"><img src="/templates/images/point_vertical_white.png"></div>
				</div>
				<div class="col-lg-12 col-xs-12 building_stages_wrap">
<?php } ?>
					<div class="building_stage">
						<div class="name">
							<div class="number"><?=$i?></div>
							<div class="number_name"><?=$q['name']?></div>
						</div>
						<div class="img" style="background-image: url(/files/building_stages/<?=$q['id']?>/img/p-<?=$q['img']?>);"></div>
						<div class="text">
							<?=$q['text']?>
						</div>
					</div>
<?php
if ($i==$num_rows) { ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>