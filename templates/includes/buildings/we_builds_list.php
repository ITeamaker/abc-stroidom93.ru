<?php
$url = get_url('buildings',$q);
?>
<a href="<?=$url?>" title="<?=$q['name']?>" class="building_block col-lg-6 col-xs-6" style="background-image: url(/files/buildings/<?=$q['id']?>/img/p-<?=$q['img']?>);">
	<div class="name">
		<span><?=$q['name']?></span>
	</div>
	<div class="btn_wrap">
		<div class="text">
			<?=$q['text']?>
		</div>
		<div class="btn_silver wrd_more"><?=i18n('common|wrd_more')?></div>
	</div>
</a>
<?php
if (fmod($i,2)==0) echo '<div class="clearfix"></div>';
?>