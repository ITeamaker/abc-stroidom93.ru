<div class="container buildings">
	<div class="row">

<?php
// скрыть Рассчитать стоимость строительства дома http://workspace.abc-cms.com/proekty/stroidom93.ru/2/56/

/*
 <div class="col-lg-12 col-xs-12 ">
            <div class="sprite hr"></div>
            <div class="h2">Рассчитать стоимость строительства дома</div>
        </div>
        <div class="clearfix"></div>

//Рассчитать стоимость строительства дома
        // [4.73] В подразделы строительства добавить "Калькулятор"
        // данные по умолчанию
        $data = array(
            'width' => 10,
            'length' => 9,
            'foundation' => 1,
            'storeys' => 1,//
            'сeiling_height' => 1,//
            'roof' => 1,//
            'facade' => 1,//
            'windows' => 1,//
            'communications' => 1,//
            'trim' => 2,
            'walls'=>1,
        );

        $data_calculator = calculator($data);
        echo html_array('calculator/calculator', array_merge($data,$data_calculator));
		<div class="col-lg-12 col-xs-12">
            <div class="point"><div class="sprite point_horizontal"></div></div>
        </div>
		<div class="clearfix"></div>

  */
?>

		<div class="col-lg-12 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-xs-12">
					<h1><?=$q['name']?></h1>
				</div>
				<div class="clearfix"></div>

				<div class="row flex-align">
					<div class="clearfix"></div>
					<div class="col-xs-12 col-sm-push-8 col-sm-4 col-lg-push-9 col-lg-3 col-xs-12 sidebar-container">
						<ul class="sidebar-menu">
							<li><a href="/proekty/"><img src="/templates/images/buildings/individual_design.png" title="<?=$page['name']?>" alt="<?=$page['name']?>"><span>Индивидуальное
                            проектирование</span></a></li>
							<li><a href="/vneshnyaya-otdelka/"><img src="/templates/images/buildings/turnkey_finishing.png" title="<?=$page['name']?>" alt="<?=$page['name']?>"><span>Отделка «под ключ»</span></a></li>
							<li><a href="/yuridicheskoe-oformlenie/"><img src="/templates/images/buildings/legal_services.png" title="<?=$page['name']?>" alt="<?=$page['name']?>"><span>Юридические услуги</span></a></li>
							<li><a href="/pomoshch-v-poluchenii-ipoteki/"><img src="/templates/images/buildings/construction_loan.png" title="<?=$page['name']?>" alt="<?=$page['name']?>" ><span>Кредит на
                            строительство</span></a></li>
							<li><a href="/pomoshch-v-oformlenii-materinskogo-kapitala/"><img src="/templates/images/buildings/using_maternity_capital.png" title="<?=$page['name']?>" alt="<?=$page['name']?>"><span>Использование
                            материнского
                            капитала</span></a></li>
							<li><a href="/portfolio/"><img src="/templates/images/buildings/portfolio.png" title="<?=$page['name']?>" alt="<?=$page['name']?>"><span>Портфолио</span></a></li>
							<li><a href="/poleznye-stati/"><img src="/templates/images/buildings/useful_articles.png" title="<?=$page['name']?>" alt="<?=$page['name']?>"><span>Полезные статьи</span></a></li>
						</ul>
					</div>
					<div class="col-xs-12 col-sm-pull-4 col-sm-8 col-lg-pull-3 col-lg-9 text_block">
						<?=$q['text2']?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-12 col-xs-12 ">
            <div class="sprite hr"></div>
			<div class="h2"><?=i18n('common|why_we')?></div>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-xs-12 ">
			<div class="row">
				<?=html_query('buildings/why_we',"
					SELECT *
					FROM advantages
					WHERE display=1 AND type = 2
					ORDER BY rank DESC
				",'')?>
			</div>
		</div>
		<div class="clearfix"></div>
		<?php
		// блок плиткой
		if (($q['img_about1']!='' && $q['text_about1']!='')
		|| ($q['img_about2']!='' && $q['text_about2']!='')
		|| ($q['img_about3']!='' && $q['text_about3']!='')
		|| ($q['img_about4']!='' && $q['text_about4']!='')
		|| ($q['img_about5']!='' && $q['text_about5']!='')) {
		?>

			<div class="col-lg-12 col-xs-12">
                <div class="sprite hr"></div>
				<div class="h2"><?=$q['name']?></div>
			</div>
			<div class="clearfix"></div>
			<div class="about_block">
				<?php
				$count_rows = 0;
				$count_block = 0;
				//
				while ($count_block++<5){
					if ($q['img_about'.$count_block]!='' && $q['text_about'.$count_block]!=''){
						$count_rows ++;
						if ($count_rows>1){?>
							<div class="col-lg-12 col-xs-12">
								<div class="point"><div class="sprite point_vertical"></div></div>
							</div>
						<?php } //E:\OpenServer\domains\stroy-center\files\buildings\12\img_about1
						if (fmod($count_rows,2)==0){?>
							<div class="col-xs-push-7 col-xs-5 about_blocks">
								<img title="<?=$page['name']?>" alt="<?=$page['name']?>" src="/files/buildings/<?=$q['id']?>/img_about<?=$count_block?>/p-<?=$q['img_about'.$count_block]?>">
							</div>
							<div class="col-xs-pull-5 col-xs-7 about_blocks">
								<?=$q['text_about'.$count_block]?>
							</div>
							<?php
						}
						else{?>
							<div class="col-lg-5 col-xs-5 about_blocks">
								<img title="<?=$page['name']?>" alt="<?=$page['name']?>" src="/files/buildings/<?=$q['id']?>/img_about<?=$count_block?>/p-<?=$q['img_about'.$count_block]?>">
							</div>
							<div class="col-lg-7 col-xs-7 about_blocks">
								<?=$q['text_about'.$count_block]?>
							</div>

						<?php }
					}
				}
				?>
			</div>
			<div class="clearfix"></div>
		<?php }?>
        <div class="col-lg-12 col-xs-12">
            <div class="point"><div class="sprite point_horizontal"></div></div>
        </div>
        <div class="col-lg-12 col-xs-12">
            <div class="point_bottom">
                <button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call" id="consult_stroi">
                    <?=i18n('common|order_consultation');?>
                </button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?=html_query('buildings/building_stages',"
	SELECT *
	FROM building_stages
	WHERE display=1 AND type = 2
	ORDER BY rank DESC
",'')?>
<div class="container buildings">
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="point"><div class="sprite point_horizontal"></div></div>
        </div>
        <div class="clearfix"></div>
		<?=html_array('buildings/price_table',$q)?>
	</div>
</div>
<?=html_array('form/contract_application', $page)?>
<div class="container buildings">
	<div class="row">
		<?=html_array('buildings/make_order')?>
		<?php
		if ($page['text_seo_1'] != ''){?>
			<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block seo_text_top">
                <div class="sprite hr"></div>
				<?=$page['text_seo_1']?>
			</div>
			<div class="clearfix"></div>
		<?php }?>
	</div>
</div>