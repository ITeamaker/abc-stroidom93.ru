<?php
//страница типа строительства
if ($buildings) {?>
	<?=$html['content']?>
	<div class="container buildings">
		<div class="row">

			<div class="col-lg-12 col-xs-12 we_also_build">
                <div class="sprite hr"></div>
				<div class="h2"><?=i18n('common|we_also_build')?></div>
			</div>
			<?=$html['buildings_list']?>
            <div class="col-lg-12 col-xs-12 we_also_build">
                <?php
				//калькулятор

				/*
				<div class="feedback_wrap"
                    <div class="calculator">
                        <?php
                        $calculator = mysql_select("SELECT url, name,module, id FROM pages WHERE module='calculator' AND display=1",'row',60*60);
                        ?>
                        <a href="/<?=$modules['calculator']?>/" class=" btn-primary btn_silver_head" title="<?=$calculator['name']?>" style="margin: 0 auto;justify-content: center;"><?=$calculator['name']?></a>
                    </div>
                </div>
				   */
				?>

            </div>
		</div>
	</div>
<?php }
//страница строительства общая
else {
	//print_r($page);
	?>
	<div class="container buildings">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<h1><?=$page['name_site']!='' ? $page['name_site'] : $page['name']?></h1>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
				<?=$page['text']?>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
				<div class="point"><div class="sprite point_horizontal"></div></div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
                <div class="sprite hr"></div>
				<div class="h2"><?=i18n('common|we_builds')?></div>
			</div>
			<div class="clearfix"></div>
			<?=$html['buildings_list']?>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
				<div class="point"><div class="sprite point_horizontal"></div></div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
				<div class="point_bottom">
					<button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call" id="consult_stroi">
						<?=i18n('common|order_consultation')?>
					</button>
				</div>
			</div>
		</div>
		<?=$html['build_block']?>
		<div class="row">
			<?=html_array('buildings/make_order')?>
		</div>
	</div>
	<?php
}
?>
