<?php
if ($i==1) { ?>
	<div class="faq_wrap row">
<?php }
$answer = iconv_strlen($q['answers'])>90 ? iconv_substr($q['answers'],0,90,"UTF-8").'...' : $q['answers'];
?>
		<div class="col-lg-12 col-xs-12">
			<div class="question">Вопрос: <?=$q['question']?></div>
			<div class="answers row">
				<div class="icon"></div>
				<div class="col-lg-11 col-xs-11 col-lg-offset-1 col-xs-offset-1 answer">
					<div class="short"><?=$answer?></div>
					<div class="full"><?=$q['answers']?></div>
				</div>
			</div>
		</div>

<?php if ($i==$num_rows) { ?>
	</div>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function () {
			$('body').on('click','.faq_wrap .answers .icon', function () {
				var parent = $(this).parents('.answers'),
					short = parent.find('.short'),
					full = parent.find('.full');
				if ($(this).hasClass('active')){
					full.slideUp(300);
					setTimeout(function() {
						short.css('display','block');
					}, 300);
				}
				else{
					short.css('display','none');
					full.slideDown(300);
				}
				$(this).toggleClass('active');
				return false;
			});
		});
	</script>
<?php }