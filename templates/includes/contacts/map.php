<?php
//print_r($q);
?>


<div class="map_wrap">
	<div id="yaMapAll<?=$q['id']?>" style="width:100%; height:100%;"></div>
</div>

<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		/* скрипт по работе с картами */
		var yaMap<?=$q['id']?>,placemark<?=$q['id']?>;
		// Дождёмся загрузки API и готовности DOM.
		if($("#yaMapAll<?=$q['id']?>").length){
			ymaps.ready(init<?=$q['id']?>);
		}
		function init<?=$q['id']?> () {
			yaMap<?=$q['id']?> = new ymaps.Map('yaMapAll<?=$q['id']?>', {
				// При инициализации карты обязательно нужно указать
				// её центр и коэффициент масштабирования.
                center: [<?=i18n('common|coordinates'.$q['id'])?>],
				zoom: 16,
//				controls: []
			});
			yaMap<?=$q['id']?>.setType('yandex#map');
			yaMap<?=$q['id']?>.behaviors.disable('scrollZoom');
            var placemark<?=$q['id']?> = new ymaps.Placemark([<?=i18n('common|coordinates'.$q['id'])?>], {
                hintContent: '<?=str_replace(":", "", i18n('common|address'.$q['id']))?>',
                //balloonContent: '<img src="http://img-fotki.yandex.ru/get/6114/82599242.2d6/0_88b97_ec425cf5_M" />',
            }, {
                // Задаем стиль метки (метка в виде круга).
                preset: "islands#redDotIconWithCaption",
                // Задаем цвет метки (в формате RGB).
                iconColor: '#ed4543'
            });
			yaMap<?=$q['id']?>.geoObjects.add(placemark<?=$q['id']?>);
		}
	});
</script>
