<div class="map_wrap">
	<div id="yaMapAll<?=$q['map_init']?>" style="width:100%; height:100%;"></div>
</div>

<script type="text/javascript">
    var yaMap<?=$q['map_init']?>, placemark<?=$q['map_init']?>;
    yaMap<?=$q['map_init']?> = new ymaps.Map('yaMapAll<?=$q['map_init']?>', {
        center: [<?=i18n('common|coordinates' . $q['id'])?>],
        zoom: 16,
    });
    yaMap<?=$q['map_init']?>.setType('yandex#map');
    yaMap<?=$q['map_init']?>.behaviors.disable('scrollZoom');
    var placemark<?=$q['map_init']?> = new ymaps.Placemark([<?=i18n('common|coordinates' . $q['id'])?>], {
        hintContent: '<?=str_replace(":", "", i18n('common|address' . $q['id']))?>',
    }, {
        preset: "islands#redDotIconWithCaption",
        iconColor: '#ed4543'
    });
    yaMap<?=$q['map_init']?>.geoObjects.add(placemark<?=$q['map_init']?>);

</script>
