<?php
$office_type = isset($_COOKIE['office_type']) && $_COOKIE['office_type']>0 && $_COOKIE['office_type']<6 ? $_COOKIE['office_type'] : 3;
?>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div class="container contacts">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<h1><?=$page['name']?></h1>
		</div>
		<div class="clearfix"></div>
		<?php
		/*
		if ($page['text']!=''){?>
			<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
				<?=$page['text']?>
			</div>
			<div class="clearfix"></div>
		<?php }
		*/?>
		<?php
		/*

		          <div class="select_office">
            <div class="name">Выберите город:</div>
            <div class="select_wrap">
                <select class="form-control" name="office">
                    <?php
                    foreach ($config['offices'] as $k=>$v){
                        if ($office_type==$k){
                            echo '<option value="'.$k.'" selected>'.$v.'</option>';
                        }
                        else{
                            echo '<option value="'.$k.'">'.$v.'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

		    */
		?>
        <?php
        foreach ($config['offices'] as $ko => $vo){
            $office_data['id'] = $ko;
            $office_data['type'] = $office_type;
            echo html_array('contacts/office_data',$office_data);
        }
        ?>
        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function () {
                $('body').on('change','.select_office .select_wrap select',function () {
                    var office = $(this).val();
                        //office_data_wrap = $('.office_data_wrap');
                    if (office>0 && office<6) {
                        $('.office_data_wrap').hide();
                        $('.office_data_wrap[data-id="'+office+'"]').show();
                    }
                });
            });
        </script>
	</div>
</div>
<div class="requisites_wrap">
	<div class="container contacts">
		<div class="row">
			<div class="col-lg-5 col-xs-5 requisites_people"></div>
			<div class="col-lg-7 col-xs-7 requisites">
				<div class="name"><?=i18n('common|requisites')?></div>
				<div class="value"><?=i18n('common|requisites_value')?></div>
			</div>
		</div>
	</div>
</div>
<div class="container contacts">
	<div class="row">

		<?=html_array('contacts/make_order')?>
		<div class="clearfix"></div>
	</div>
</div>