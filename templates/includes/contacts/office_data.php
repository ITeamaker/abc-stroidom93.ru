<?php
//print_r ($q);?>
<?php
/*
http://workspace.abc-cms.com/proekty/stroidom93.ru/1/2/
<div class="office_data_wrap" data-id="<?=$q['id']?>" <?=$q['id']==$q['type'] ? ' style="display:block;" ' : ' style="display:none;" '?>>
  */
?>
<div class="office_data_wrap" data-id="<?=$q['id']?>" <?=$q['id']==$q['type'] ? ' style="display:block;" ' : ' style="display:block;" '?>>
    <div class="contacts_icons col-lg-12 col-xs-12">
        <?php		
		if (i18n('common|address'.$q['id'])!=''){?>
            <div class="col-lg-3 col-xs-3 address">
                <div class="name"><?=i18n('common|address_name')?>: <?=$q['id']==3 ? '<span>Центральный офис</span>' : ''?></div>
                <div class="value"><?=str_replace(":", "", i18n('common|address'.$q['id']));?></div>
            </div>
        <?php }
        if (i18n('common|phone'.$q['id'])!=''){?>
            <div class="col-lg-3 col-xs-3 phone">
                <div class="name"><?=i18n('common|phone_name')?>:</div>
                <div class="value"><a <?=$q['id']==5 ? ' class="ya-phone-1" ' : ''?> href="tel:<?=i18n('common|phone'.$q['id'].'_link')?>" title="<?=i18n('common|phone'.$q['id'])?>"><?=i18n('common|phone'.$q['id'])?></a></div>
                <div class="value" style="margin-top: 10px"><a href="tel:8-(918)-001-23-73" title="<?=i18n('common|phone'.$q['id'])?>">8-(918)-001-23-73</a></div>
            </div>
        <?php }
        if (i18n('common|email'.$q['id'])!=''){?>
            <div class="col-lg-3 col-xs-3 email">
                <div class="name"><?=i18n('common|email_name')?>:</div>
                <div class="value"><a href="mailto:<?=i18n('common|email'.$q['id'])?>" title="<?=i18n('common|email'.$q['id'])?>"><?=i18n('common|email'.$q['id'])?></a></div>
            </div>
        <?php }
        if (i18n('common|working_hours'.$q['id'])!=''){?>
            <div class="col-lg-3 col-xs-3 working_hours">
                <div class="name"><?=i18n('common|working_hours_name')?>:</div>
                <div class="value"><?=i18n('common|working_hours'.$q['id'])?></div>
            </div>
        <?php }?>
        <div class="clearfix"></div>
    </div>
    <?php
    if (i18n('common|coordinates'.$q['id'])!=''){
        echo '<div class="col-lg-12 col-xs-12">';
        //$data['map_init'] = $_SESSION['map_init'];
        $data['id'] = $q['id'];
        echo html_array('contacts/map',$data);
        echo '</div><div class="clearfix"></div>';
    }
    ?>
</div>