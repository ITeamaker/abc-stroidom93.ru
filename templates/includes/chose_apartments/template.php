<?=html_sources('return','chose.css')?>

<div class="chose_block">

<section class="selection">
	<div class="container">
		<div class="h2">Подберите свою квартиру в Cочи
			<small>по лучшей цене от застройщика</small></div>
		<form action="" class="selection-form">
			<div class="selection-form__title">Получите подробную информацию:</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 selection-form__item">
					<strong>Выберите район:</strong>
					<ul class="selection-form__list">
						<li>
							<input type="checkbox" class="selection-form__check" id="1-1">
							<label for="1-1" class="selection-form__label">Все районы</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="1-2">
							<label for="1-2" class="selection-form__label">Адлерский</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="1-3">
							<label for="1-3" class="selection-form__label">Сочи</label>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-6 selection-form__item">
					<strong>Стоимость квартиры:</strong>
					<ul class="selection-form__list">
						<li>
							<input type="checkbox" class="selection-form__check" id="2-1">
							<label for="2-1" class="selection-form__label">до 1 млн. рублей</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="2-2">
							<label for="2-2" class="selection-form__label">до 2 млн. рублей</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="2-3">
							<label for="2-3" class="selection-form__label">до 3 млн. рублей</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="2-4">
							<label for="2-4" class="selection-form__label">более 3 млн. рублей</label>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-6 selection-form__item">
					<strong>Вид оплаты:</strong>
					<ul class="selection-form__list">
						<li>
							<input type="checkbox" class="selection-form__check" id="3-1">
							<label for="3-1" class="selection-form__label">Наличные</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="3-2">
							<label for="3-2" class="selection-form__label">Рассрочка</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="3-3">
							<label for="3-3" class="selection-form__label">Ипотека</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="4-3">
							<label for="4-3" class="selection-form__label">Мат. Капитал</label>
						</li>
					</ul>
				</div>
				<div class="col-md-3 col-sm-6 selection-form__item">
					<strong>Срок сдачи:</strong>
					<ul class="selection-form__list">
						<li>
							<input type="checkbox" class="selection-form__check" id="4-1">
							<label for="4-1" class="selection-form__label">ЖК уже сдан</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="4-2">
							<label for="4-2" class="selection-form__label">ЖК сдается в 2017 году</label>
						</li>
						<li>
							<input type="checkbox" class="selection-form__check" id="4-3">
							<label for="4-3" class="selection-form__label">ЖК сдается после 2017 года</label>
						</li>
					</ul>
				</div>
			</div>
			<button type="submit" class="btn selection-form__btn">Получите подборку квартир</button>
		</form>
	</div>
</section>

<section class="variants">
	<div class="container">
		<div class="sprite hr"></div>
		<h3 class="variants__title">
			<small>10 квартир и студий по цене от застройщика! Ваша выгода от 150 000 руб.!</small>
			БЕЗ КОМИССИЙ И ПЕРЕПЛАТ ПОСРЕДНИКАМ</h3>
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="variants-item">
					<img src="/templates/images/chose/variants_1.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>" class="variants-item__photo">
					<div class="variants-item__title">Квартира студия</div>
					<div class="variants-item__old-price">1 305 900 P</div>
					<div class="variants-item__price">1 164 900 p</div>
					<div class="variants-item__square">29 м<sup>2</sup></div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="variants-item">
					<img src="/templates/images/chose/variants_1.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>" class="variants-item__photo">
					<div class="variants-item__title">Квартира студия</div>
					<div class="variants-item__old-price">1 305 900 P</div>
					<div class="variants-item__price">1 164 900 p</div>
					<div class="variants-item__square">29 м<sup>2</sup></div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="variants-item">
					<img src="/templates/images/chose/variants_1.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>" class="variants-item__photo">
					<div class="variants-item__title">Квартира студия</div>
					<div class="variants-item__old-price">1 305 900 P</div>
					<div class="variants-item__price">1 164 900 p</div>
					<div class="variants-item__square">29 м<sup>2</sup></div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="variants-item">
					<img src="/templates/images/chose/variants_1.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>" class="variants-item__photo">
					<div class="variants-item__title">Квартира студия</div>
					<div class="variants-item__old-price">1 305 900 P</div>
					<div class="variants-item__price">1 164 900 p</div>
					<div class="variants-item__square">29 м<sup>2</sup></div>
				</div>
			</div>
		</div>
		<a href="javascript:void(0)" class="btn variants__btn">Подобрать вариант</a>
	</div>
</section>

<section class="profit">
	<div class="container">
		<div class="sprite hr"></div>
		<div class="h2">В чем мы Выгоднее!</div>
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="profit-item">
					<div class="profit-item__img-wrap">
						<img src="/templates/images/chose/profit_1.png" title="<?=$page['name']?>" alt="<?=$page['name']?>">
					</div>
					<div class="profit-item__text">Материал стен керамические блоки</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="profit-item">
					<div class="profit-item__img-wrap">
						<img src="/templates/images/chose/profit_2.png" title="<?=$page['name']?>" alt="<?=$page['name']?>">
					</div>
					<div class="profit-item__text">Большой выбор готовых объектов</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="profit-item">
					<div class="profit-item__img-wrap">
						<img src="/templates/images/chose/profit_3.png" title="<?=$page['name']?>" alt="<?=$page['name']?>">
					</div>
					<div class="profit-item__text">За каждой квартирой закреплен огороженный участок земли</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="profit-item">
					<div class="profit-item__img-wrap">
						<img src="/templates/images/chose/profit_4.png" title="<?=$page['name']?>" alt="<?=$page['name']?>">
					</div>
					<div class="profit-item__text">Стоимость от 34 тыс. <br> за м<sup>2</sup></div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="advantages">
	<div class="container">
		<div class="sprite hr"></div>
		<div class="h2">Преимущества рассрочки</div>
		<div class="row">
			<div class="col-md-3 col-sm-5">
				<div class="advantages-item">
					<img src="/templates/images/chose/advantages_1.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>">
					<div class="advantages-item__text">
						<strong>Первый взнос</strong>
						<span>от 375 000 руб.</span>
					</div>
				</div>
			</div>
			<div class="col-md-1 col-sm-2">
				<div class="advantages__symbol">+</div>
			</div>
			<div class="col-md-3 col-sm-5">
				<div class="advantages-item">
					<img src="/templates/images/chose/advantages_2.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>">
					<div class="advantages-item__text">
						<strong>Ежемесячный взнос</strong>
						<span>на 6 мес. от 62 500 руб.</span>
					</div>
				</div>
			</div>
			<div class="col-md-1 col-sm-12">
				<div class="advantages__symbol advantages__symbol_2">=</div>
			</div>
			<div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2">
				<div class="advantages-item">
					<img src="/templates/images/chose/advantages_3.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>">
					<div class="advantages-item__text">
						<strong>Твоя квартира или студия</strong>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-md-offset-4">
				<a href="javascript:void(0)" class="btn advantages__btn">Рассчитать рассрочку</a>
			</div>
		</div>
	</div>
</section>

<section class="partner-banks">
	<div class="container">
		<div class="sprite hr"></div>
		<div class="h2">Наши банки-партнеры</div>
		<div class="row">
			<div class="col-xs-6 col-sm-3">
				<div class="partner-banks-item">
					<img src="/templates/images/chose/bank_1.png" title="<?=$page['name']?>" alt="<?=$page['name']?>">
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="partner-banks-item">
					<img src="/templates/images/chose/bank_2.png" title="<?=$page['name']?>" alt="<?=$page['name']?>">
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="partner-banks-item">
					<img src="/templates/images/chose/bank_3.png" title="<?=$page['name']?>" alt="<?=$page['name']?>">
				</div>
			</div>
			<div class="col-xs-6 col-sm-3">
				<div class="partner-banks-item">
					<img src="/templates/images/chose/bank_4.png" title="<?=$page['name']?>" alt="<?=$page['name']?>">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="developer">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-sm-6 ">
				<div class="developer__text">
					<strong>Мы застройщик!</strong>
					16 лет профессионально занимаемся проектированием и строительством домов.
				</div>
			</div>
			<div class="col-lg-6 col-lg-offset-1 col-sm-6 ">
				<ul class="developer__list">
					<li>Свои рабочие бригады</li>
					<li>Подбираем качественные материалы по выгодным ценам</li>
					<li>Строим дома за короткий срок От "3" месяцев.</li>
					<li>Делаем ремонт под ключ с полной комплектацией.</li>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="about">
	<div class="container">
		<div class="sprite hr"></div>
		<div class="h2">Все стараются, но мы стараемся лучше</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="about-item">
					<div class="row">
						<div class="col-md-6">
							<img src="/templates/images/chose/about_1.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>">
						</div>
						<div class="col-md-6">
							<div class="about-item__descr">
								<strong>Трансфер по объектам</strong>
								<span>Встретим, разместим в гостинице, провезем на авто по всем объектам</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="about-item">
					<div class="row">
						<div class="col-md-6">
							<img src="/templates/images/chose/about_2.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>">
						</div>
						<div class="col-md-6">
							<div class="about-item__descr">
								<strong>Юридическая помощь</strong>
								<span>Проконсультируем по всем вопросам и окажем юридическую помощь</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="about-item">
					<div class="row">
						<div class="col-md-6">
							<img src="/templates/images/chose/about_3.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>">
						</div>
						<div class="col-md-6">
							<div class="about-item__descr">
								<strong>Избавим от хлопот с ремонтом</strong>
								<span>Сделаем качественный ремонт от 8 000 руб. за м<sup>2</sup> (с материалами)</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="about-item">
					<div class="row">
						<div class="col-md-6">
							<img src="/templates/images/chose/about_4.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>">
						</div>
						<div class="col-md-6">
							<div class="about-item__descr">
								<strong>Поможем быстро окупить вложения</strong>
								<span>Сдадим вашу квартиру в аренду посуточно или помесячно</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="text_block seo_text">
	<div class="container">
		<div class="sprite hr"></div>
		<h2>У кого можно заказать квартиру в Сочи?</h2>
		<p>Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Точках свою, снова маленькая необходимыми! Языкового деревни, сих, несколько большой текстов щеке коварный текста, страна вопроса залетают первую его выйти. Семь злых грамматики большой. Решила правилами маленькая подзаголовок залетают все, предупреждал ему грамматики дорогу прямо, ее осталось мир это маленький он даль его, наш гор, своего себя силуэт свое текстами однажды взгляд! О которой ручеек собрал даже составитель заголовок всеми города, за пунктуация предложения речью большой его эта там на берегу своих рыбного продолжил великий жизни строчка lorem большого! Свое несколько агенство реторический города сбить первую обеспечивает наш эта толку рыбного.</p>
		<p>Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Точках свою, снова маленькая необходимыми! Языкового деревни, сих, несколько большой текстов щеке коварный текста, страна вопроса залетают первую его выйти. Семь злых грамматики большой. Решила правилами маленькая подзаголовок залетают все, предупреждал ему грамматики дорогу прямо, ее осталось мир это маленький он даль его, наш гор, своего себя силуэт свое текстами однажды взгляд! О которой ручеек собрал даже составитель заголовок всеми города, за пунктуация предложения речью большой его эта там на берегу своих рыбного продолжил великий жизни строчка lorem большого! Свое несколько агенство реторический города сбить первую обеспечивает наш эта толку рыбного.</p>
		<p>Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Точках свою, снова маленькая необходимыми! Языкового деревни, сих, несколько большой текстов щеке коварный текста, страна вопроса залетают первую его выйти. Семь злых грамматики большой. Решила правилами маленькая подзаголовок залетают все, предупреждал ему грамматики дорогу прямо, ее осталось мир это маленький он даль его, наш гор, своего себя силуэт свое текстами однажды взгляд! О которой ручеек собрал даже составитель заголовок всеми города, за пунктуация предложения речью большой его эта там на берегу своих рыбного продолжил великий жизни строчка lorem большого! Свое несколько агенство реторический города сбить первую обеспечивает наш эта толку рыбного.</p>
		<p>Далеко-далеко за словесными горами в стране, гласных и согласных живут рыбные тексты. Точках свою, снова маленькая необходимыми! Языкового деревни, сих, несколько большой текстов щеке коварный текста, страна вопроса залетают первую его выйти. Семь злых грамматики большой. Решила правилами маленькая подзаголовок залетают все, предупреждал ему грамматики дорогу прямо, ее осталось мир это маленький он даль его, наш гор, своего себя силуэт свое текстами однажды взгляд! О которой ручеек собрал даже составитель заголовок всеми города, за пунктуация предложения речью большой его эта там на берегу своих рыбного продолжил великий жизни строчка lorem большого! Свое несколько агенство реторический города сбить первую обеспечивает наш эта толку рыбного.</p>
	</div>
</section>

<section class="finished-houses">
	<h2>Мы так же предлагаем Вам готовые дома!</h2>
	<a href="javascript:void(0)" class="btn finished-houses__btn">Подобрать</a>
</section>

</div>