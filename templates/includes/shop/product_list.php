<?php

if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$img = $q['img'] ? '/files/projects/'.$q['id'].'/img/p-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('projects',$q);
if ($i==1) {?>
    <div class="inner_container">
    <style>
        .shop_product_list .product_param_wrap {padding-top: 12px;}
        .data {font-size: 14px !important; min-height: 35px; margin-bottom: 0 !important;}
        .shop_product_list .product_param_wrap .name_link{ height: auto; line-height: 35px; margin: 0; }
        .data.big {font-size: 14px !important;}
        .data.big.price {    color: #fff;  background: #b12828;}
        .data.big span {font-size: 24px;line-height: 35px;}
        .data.big .value_name {font-size: 14px;}
    </style>
<?php } ?>
    <div class="shop_product_list col-xs-4 col-sm-4 col-md-4">
        <div class="shop_product_wrap">
            <div class="img">
                <div>
                    <a href="<?=$url?>" title="<?=$title?>">
                        <img src="<?=$img?>" alt="<?=$title?>" />
                    </a>
                </div>
            </div>
            <div class="product_param_wrap projects" style="min-height: 60px;">
                <div class="data" style="width:25%">
                    <a class="name_link" href="<?=$url?>" title="<?=$title?>"><span><?=$q['name']?></span></a>
                </div>
                <?php if ($q['area']!='') {?>
                    <div class="data big" style="width:25%">
                        <div class="value">
                            <span><?=number_format($q['area'],0,'.','')?></span>
                            <?=i18n('shop|sq')?>
                        </div>
                    </div>
                <?php }
                if ($q['price_metr']>0 && $q['apartment_buildings']==1) {?>
                    <div class="data big price" style="width:50%">
                        <div class="value">
                            <span><?=number_format($q['price_metr'],0,'.',' ')?></span>
                            <?=i18n('shop|currency_metr')?>
                        </div>
                    </div>
                <?php }
                elseif ($q['price']>0) {?>
                    <div class="data big price" style="width:50%">
                        <div class="value">
                            <span><?=number_format($q['price'],0,'.',' ')?></span>
                            <?=i18n('shop|currency')?>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
    </div>
<?php
if (fmod($i,3)==0) echo '<div class="clearfix"></div>';
if ($i==$num_rows) echo '</div>';






/* [4.61] Добавить возможность добавлять проекты самостоятельно в раздел портфолио
if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$img = $q['img'] ? '/files/shop_products/'.$q['id'].'/img/m-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('shop_product',$q);
if ($i==1) {?>
	<div class="inner_container">
		<div class="row">
<?php } ?>
			<div class="shop_product_list col-xs-4 col-sm-4 col-md-4">
				<div class="shop_product_wrap">
					<div class="img">
						<div>
							<a href="<?=$url?>" title="<?=$title?>">
								<img src="<?=$img?>" alt="<?=$title?>" />
							</a>
						</div>
					</div>
					<div class="product_param_wrap">
						<a class="name_link" href="<?=$url?>" title="<?=$title?>"><span><?=$q['name']?></span></a>
						<?php if ($q['price']>0) {?>
						<div class="data">
							<div class="name"><?=i18n('shop|costs')?></div>
							<div class="value"><span><?=number_format($q['price'],0,',',' ')?></span> <?=i18n('shop|currency')?></div>
						</div>
						<?php }
						if ($q['area']>0) {?>
							<div class="data">
								<div class="name"><?=i18n('shop|area')?></div>
								<div class="value"><span><?=number_format($q['area'],1,',',' ')?></span> <?=i18n('shop|sq')?></div>
							</div>
						<?php }
						if ($q['material']>0) {?>
							<div class="data">
								<div class="name"><?=i18n('shop|material')?></div>
								<div class="value"><?=$q['material_name']?></div>
							</div>
						<?php }
						if ($q['floors']>0) {?>
							<div class="data">
								<div class="name"><?=i18n('shop|floor')?></div>
								<div class="value"><span><?=$q['floors'].' '.plural($q['floors'],i18n('shop|floor1'),i18n('shop|floor2'),i18n('shop|floors5'))?></span></div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
<?php
if (fmod($i,3)==0) echo '<div class="clearfix"></div>';
if ($i==$num_rows) echo '</div></div>';
?>
*/