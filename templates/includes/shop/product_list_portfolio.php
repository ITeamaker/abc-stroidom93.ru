<?php

if ($i==1) { ?>
<div class="col-lg-12 col-xs-12">
    <div class="sprite hr"></div>
	<h1><?=i18n('common|portfolio')?></h1>
</div>
<div class="col-lg-12 col-xs-12">
<div class="portfolio-wrap">
	<div class="portfolio_elements">
		<div class="portfolio_arrows portfolio_prev"></div>
		<div class="portfolio_dots"></div>
		<div class="portfolio_arrows portfolio_next"></div>
	</div>
	<div class="portfolio_slider slick_slider">
<?php }
$img = '/files/projects/'.$q['id'].'/img/l-'.$q['img'];
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('projects',$q);
?>
		<div class="portfolio_item">
			<div class="data left">
				<div class="data_wrap">
					<div class="short_description">
						<?=$q['short_description']?>
					</div>
					<div class="delimiter"><span></span></div>
					<?php
					if ($q['area']>0){?>
					<div class="area_wrap">
						<div class="name"><?=i18n('shop|area')?></div>
						<div class="value"><span><?=number_format($q['area'],1,',',' ')?></span> <?=i18n('shop|sq')?></div>
					</div>
					<?php } ?>
					<?php
					if ($q['number_months']>0){?>
						<div class="price_wrap">
							<div class="name"><?=i18n('shop|number_months')?></div>
							<div class="value"><?=$q['number_months'].' '.plural($q['number_months'],i18n('shop|months1'),i18n('shop|months2'),i18n('shop|months5'))?></div>
						</div>
					<?php } ?>
					<?php
					if ($q['price']>0){?>
						<div class="price_wrap">
							<div class="name"><?=i18n('shop|costs')?></div>
							<div class="value"><span><?=number_format($q['price'],0,'.',' ')?></span> <?=i18n('shop|currency')?></div>
						</div>
					<?php } ?>
				</div>
				<a class="btn_silver wrd_more" href="<?=$url?>" title="<?=$title?>"><?=i18n('common|wrd_more')?></a>
			</div>
			<a href="<?=$url?>" title="<?=$title?>" class="image right" >
                <img src="<?=$img?>" title="<?=$page['name']?>" alt="<?=$page['name']?>">
                <div class="name">
                    <?=$q['name']?>
                </div>
                <span class="gallery-img-cover" style="background-image: url(<?=$img?>)"></span>
			</a>
		</div>
<?php if ($i==$num_rows) { ?>
	</div>
</div>
</div>
<?php } ?>


