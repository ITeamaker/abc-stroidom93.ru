<div class="container">
	<div class="row">
<?php
//страница товара
if ($product) {
	echo '<div class="col-lg-12 col-xs-12">';
	echo $html['content'];
	echo '</div>';
//страница категории
}
else {
	?>
		<div class="col-lg-12 col-xs-12">
			<h1<?=$news ? editable('news|name|'.$page['id']) : editable('pages|name|'.$page['id'])?>><?=$page['name']?></h1>
		</div>
    <?php /* [4.53] Изменить раздел портфолио
		<div class="col-lg-3 col-xs-3">
			<?=$html['filter']?>
		</div>
 */?>

<?php /* [4.53] Изменить раздел портфолио
            <?=$html['product_list_map']?>
 */?>
        <?=$html['slider_portfolio']?>
        <div class="col-lg-12 col-xs-12">
            <div class="point"><img style="margin-bottom: 5px;" src="/templates/images/point.png"></div>
            <div class="portfolio_arrow_down"><img src="/templates/images/arrows_slider_down.png" alt="arrows down"></div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12 col-xs-12">
            <div class="point_bottom" style="margin-bottom: 40px;">
                <button type="button" class="btn-consultation btn_margin" data-toggle="modal" data-target="#make_excursion">
                    <?=i18n('common|order_consultation_shop');?>
                </button>
            </div>
        </div>
        <?=i18n('shop|we_do_for_you')!='' ? '<div class="clearfix"></div><div class="sprite hr"></div><div class="h2">'.i18n('shop|we_do_for_you').'</div>' : ''?>
        <?=$html['product_list']?>

	<div class="col-lg-12 col-xs-12">
		<div class="h2" style=" margin-top: -10px;">Скачав наше приложение вы сможете оценить наш дом в коттеджном посёлке!</div>
		<div class="point_bottom api_stroidom" >
			<span class="api_stroidom_item">
				<a target="_blank" href="https://drive.google.com/open?id=1IV_DYvxjT7NRyYcfvF8kY907-bLZBtKv" class="btn-consultation btn_margin" data-toggle="modal" >
				Скачать приложение для Windows
				</a>
				<div class="instruction">
					<ul>
						<li>Краткая инструкция по установке (только для ОС Windows):</li>
						<li>1. Перейдите по ссылке и скачайте архив на компьютер</li>
						<li>2. Распакуйте архив</li>
						<li>3. Откройте папку Windows и запустите файл home shooter.exe</li>
						<li>4. После распаковки нажмите клавишу "Play!"</li>
					</ul>
				</div>
			</span>
			<span class="api_stroidom_item">
				<a target="_blank" href="https://play.google.com/store/apps/details?id=com.vuforia.samples.Stroidom93" class="btn-consultation btn_margin" data-toggle="modal" >
				Скачать приложение в Google Play
				</a>

			</span>
		</div>
	</div>

	<?php
}
?>
	</div>
</div>