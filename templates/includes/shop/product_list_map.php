<?php
//print_r($q);
?>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div class="map_wrap index">
	<div id="yaMapAll" data-id="<?=implode(',',$q)?>" style="width:100%; height:100%;"></div>
</div>

<script>
	document.addEventListener("DOMContentLoaded", function () {
		/* скрипт по работе с картами */
		var yaMap, myGeoObject;
		var lat, lng;
		var yaMapAll;
		// Дождёмся загрузки API и готовности DOM.
		if($("#yaMapAll").length){
			ymaps.ready(init);
		}
		function init () {
			yaMapAll = new ymaps.Map('yaMapAll', {
				// При инициализации карты обязательно нужно указать
				// её центр и коэффициент масштабирования.
				center: [43.585819969581635, 39.72656534029233],
				zoom: 14
			});
			yaMapAll.setType('yandex#map');
			yaMapAll.behaviors.disable('scrollZoom');
			var $id = $("#yaMapAll").data('id');
			$.ajax({
				type: "GET",
				url: "/ajax.php",
				data: {
					file: 'getobjects',
					type: 'portfolio',
					id: $id
				},
				dataType: 'JSON',
				success: function(data){
					var objectManager = new ymaps.ObjectManager({
						clusterize: true
					});
					objectManager.add(data.objects);
					yaMapAll.geoObjects.add(objectManager);
					//yaMapAll.setCenter(objectManager.objects.getById(0).geometry.coordinates);
				}
			});
		}
	});
</script>