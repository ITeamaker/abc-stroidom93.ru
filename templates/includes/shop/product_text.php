<?=html_sources('footer','fancybox.js')?>
<?php
$img = $q['img'] ? '/files/shop_products/'.$q['id'].'/img/l-'.$q['img'] : '/templates/images/no_img.svg';
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
?>
<div class="sprite hr"></div>
<h1><?=$q['name']?></h1>
<div class="shop_product_text content">
	<div class="product_gallery ">
		<div class="product_img_list-for left">
			<?php
			$list = $list2 = '';
			if ($q['img']) {?>
				<a class="fancybox_gallery" title="<?=$title?>" rel="gallery<?=$q['id']?>" href="/files/shop_products/<?=$q['id']?>/img/<?=$q['img']?>" >
					<img src="<?=$img?>" alt="<?=$title?>"  />
				</a>
				<?php
				$list = '<div class="img_vertical_wrap"><img width="262" height="150" src="/files/shop_products/'.$q['id'].'/img/i-'.$q['img'].'" alt="'.$title.'" /></div>';
			} ?>
			<?php if ($images) {
				$n=1;
				foreach ($images as $k=>$v) if (@$v['display']==1) {
					$n++;
					$alt2=filter_var(@$v['name'],FILTER_SANITIZE_STRING);
					$title2=filter_var(@$v['title'],FILTER_SANITIZE_STRING);
					$title2 = $title2 ? $title2 : $alt2;
					$path = '/files/shop_products/'.$q['id'].'/imgs/'.$k.'/';
					$list .= '<div class="img_vertical_wrap"><img width="262" height="150"  src="'.$path.'p-'.$v['file'].'" alt="'.$alt2.'" title="'.$title2.'"  /></div>';
					echo '<a  class="fancybox_gallery" rel="gallery'.$q['id'].'" title="'.$title2.'"  href="'.$path.$v['file'].'" ><img src="'.$path.'l-'.$v['file'].'" alt="'.$alt2.'" title="'.$title2.'" /></a>';
				}
			}?>
		</div>
		<div class="product_img_list-nav right"><?=$list?></div>
		<div class="clearfix"></div>
	</div>
	<div class="product_options">
		<div class="row">
			<div class="col-lg-2 col-xs-2 ">
				<?php if ($q['price']>0) {?>
					<div class="data">
						<div class="name"><?=i18n('shop|price')?></div>
						<div class="value"><span><?=number_format($q['price'],0,',',' ')?></span> <?=i18n('shop|currency')?></div>
					</div>
				<?php }
				if ($q['material']>0) {?>
					<div class="data">
						<div class="name"><?=i18n('shop|material')?></div>
						<div class="value"><?=$q['material_name']?></div>
					</div>
				<?php }?>
			</div>
			<div class="col-lg-2 col-xs-2 ">
			<?php
			if ($q['area']>0) {?>
				<div class="data">
					<div class="name"><?=i18n('shop|areas')?></div>
					<div class="value"><span><?=number_format($q['area'],1,',',' ')?></span> <?=i18n('shop|sq')?></div>
				</div>
			<?php }
			if ($q['floors']>0) {?>
				<div class="data">
					<div class="name"><?=i18n('shop|floor')?></div>
					<div class="value"><span><?=$q['floors'].' '.plural($q['floors'],i18n('shop|floor1'),i18n('shop|floor2'),i18n('shop|floors5'))?></span></div>
				</div>
			<?php } ?>
			</div>
			<div class="col-lg-8 col-xs-8">
				<div class="text" <?=editable('shop_products|text|'.$q['id'],'text')?>><?=$q['text']?></div>
			</div>
		</div>
	</div>
	<div class="product_btn">
		<div class="row">
			<div class="col-lg-12 col-xs-12 ">
				<button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call"><?=i18n('shop|want_same')?></button>
			</div>
		</div>
	</div>
</div>
<script>
	document.addEventListener("DOMContentLoaded", function () {
		$('.product_img_list-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
		});
		$('.product_img_list-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.product_img_list-for',
			prevArrow: '<div class="slick_button_wrap_prev"><button type="button" class="slick-prev">Previous</button></div>',
			nextArrow: '<div class="slick_button_wrap_next"><button type="button" class="slick-next">Next</button></div>',
			dots: false,
			centerMode: true,
			focusOnSelect: true,
			vertical: true,
		});
		$(".fancybox_gallery").fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			padding: 0,
			helpers : {
				title : {
					type : 'over'
				}
			}
		});
		$(".fancybox_single").fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			padding: 0,
			helpers : {
				title : {
					type : 'over'
				}
			}
		});
	});
</script>