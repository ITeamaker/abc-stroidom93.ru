<?php
//print_r ($q);
$url = get_url('projects',$q);
$img = '/files/projects/'.$q['id'].'/img/l-'.$q['img'];

if ($i==1) { ?>
	<div class="our_prices_wrap">
<?php } ?>
		<div class="our_price col-lg-4 col-xs-4">
			<a href="<?=$url?>" title="<?=$q['name']?>" class="our_price_img" style="background-image: url(/files/projects/<?=$q['id']?>/img/l-<?=$q['img']?>);">
				<div class="name">
					<div class="area_name"><?=i18n('shop|area')?></div>
					<?php
					if ($q['area']>0){?>
						<div class="area"><span><?=number_format($q['area'],1,',',' ')?></span> <?=i18n('shop|sq')?></div>
					<?php } ?>
				</div>
			</a>
			<div class="our_price_text">
				<div class="our price_text">
					<div class="name"><?=i18n('shop|our_price')?></div>
					<div class="value"><?=number_format($q['price'],0,'.',' ')?> <?=i18n('shop|currency')?></div>
				</div>
				<div class="competitors price_text">
					<div class="name"><?=i18n('shop|competitors_price')?></div>
					<div class="value"><?=number_format($q['price2'],0,'.',' ')?> <?=i18n('shop|currency')?></div>
				</div>
			</div>
		</div>
<?php if ($i==$num_rows) { ?>
	</div>
<?php } ?>