<?php
//print_r ($q);
?>
<form method="get" class="shop_filter form_clear">
	<div class="filter_block">
		<div class="name">
			<?=i18n('shop|areas',true)?>
		</div>
		<?php
		// площадь
		if ($config['filter_areas']) {
			$filter_areas = array();
			foreach ($config['filter_areas'] as $k => $v){
				$filter_areas[$k] = $v['name'];
			}
			echo html_array('form/multi_checkbox_new',array(
				//'caption'=>i18n('shop|areas',true),
				'name'=>'areas',
				'value'=>@$q['areas'],
				'data'=>$filter_areas
			));
		}
		/*
		$i = 0;
		if($config['filter_areas']){
			foreach ($config['filter_areas'] as $k => $v) {
				$i++;
				$checked = (isset($_GET['areas']) AND @$_GET['areas'] == $k) ? ' checked="checked"' : '';
				//$checked = $i == 1 ? ' checked="checked"' : $checked;
				?>
				<div class="radio">
					<input type="radio" name="areas" class="radio_btn" id="areas<?= $k ?>" value="<?= $k ?>" <?= $checked ?>>
					<label for="areas<?= $k ?>">
						<span><?= $v['name'] ?></span>
					</label>
				</div>
		<?php }
		}
		*/
		?>
	</div>
	<div class="filter_block">
		<div class="name">
			<?=i18n('shop|material',true)?>
		</div>
		<?php
		//материал
		if ($material = mysql_select("
				SELECT sm.id,sm.name
				FROM shop_materials sm, shop_products sp
				WHERE sp.material = sm.id AND sm.display = 1 AND sp.display=1
				GROUP BY sm.id
				ORDER BY sm.rank DESC, sm.name
			",'array')) {
			echo html_array('form/multi_checkbox_new',array(
				//'caption'=>i18n('shop|material',true),
				'name'=>'material',
				'value'=>@$q['material'],
				'data'=>$material
			));
		}
		?>
	</div>
	<div class="filter_block">
		<div class="name">
			<?=i18n('shop|costs',true)?>
		</div>
		<?php
		// цена
		if ($config['filter_price']) {
			$filter_price = array();
			foreach ($config['filter_price'] as $k => $v){
				$filter_price[$k] = $v['name'];
			}
			echo html_array('form/multi_checkbox_new',array(
				//'caption'=>i18n('shop|costs',true),
				'name'=>'costs',
				'value'=>@$q['costs'],
				'data'=>$filter_price
			));
		}
		?>
	</div>
	<div class="filter_block">
		<div class="name">
			<?=i18n('shop|parameters',true)?>
		</div>
		<div class="checkbox_new_wrap">
			<?php
			// комплектация
			echo html_array('form/checkbox_new',array(
				'units'	=>	i18n('shop|garage',true),
				'name'	=>	'garage',
				'class'	=>	'checkbox_new',
				'value'	=>	@$q['garage'],
			));
			echo html_array('form/checkbox_new',array(
				'units'	=>	i18n('shop|loft',true),
				'name'	=>	'loft',
				'class'	=>	'checkbox_new',
				'value'	=>	@$q['loft'],
			));
			echo html_array('form/checkbox_new',array(
				'units'	=>	i18n('shop|terrace',true),
				'name'	=>	'terrace',
				'class'	=>	'checkbox_new',
				'value'	=>	@$q['terrace'],
			));
			?>
		</div>
	</div>
	<div class="filter_block">
		<div class="name"><?=i18n('shop|floor',true)?></div>
		<?php
		// этажность
		if ($config['shop_product_floor']) {
			$filter_floor = array();
			foreach ($config['shop_product_floor'] as $k => $v){
				$filter_floor[$k] = $k.' '.i18n('shop|floor_unit');
			}
			echo html_array('form/multi_checkbox_new',array(
				//'caption'=>i18n('shop|floor',true),
				'name'=>'floor',
				'value'=>@$q['floor'],
				'data'=>$filter_floor
			));
		}
		?>
	</div>

	<?php
	echo html_array('form/button',array(
		'name' =>	i18n('shop|filter_button'),
		'class' => 'btn_red ',
	));
	?>
	<a class="btn_silver btn-default" href="/<?=$modules['shop']?>/" title="<?=i18n('shop|clear')?>" >Сбросить<?=i18n('shop|clear')?></a>
</form>
