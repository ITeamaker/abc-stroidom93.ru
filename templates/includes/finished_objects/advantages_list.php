<?php
$img = $q['img'] ? '/files/advantages/'.$q['id'].'/img/p-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';

if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
if ($i==1){?>
	<section class="profit">
		<div class="container">
			<div class="sprite hr"></div>
			<div class="h2"><?=i18n('common|objects_advantages')?></div>
			<div class="row">
<?php } ?>
				<div class="col-md-4 col-sm-6">
					<div class="profit-item">
						<div class="profit-item__img-wrap">
							<img class="lazyload" data-src="<?=$img?>" alt="<?=$q['name']?>"/>
						</div>
						<div class="profit-item__text"><?=$q['name']?></div>
					</div>
				</div>
<?php
if (fmod($i,3)==0) echo '<div class="clearfix"></div>';

if ($i==$num_rows){
	echo '</div></div></section>';
}