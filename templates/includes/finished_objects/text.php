<div class="finished_wrap">
	<div class="modal make_finished fade" id="make_installments" >
		<div class="modal-dialog modal-lg">
			<form class="modal-content validate" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				</div>
				<div class="modal-body selection-form">
					<div class="selection-form__title"><?=i18n('common|get_installments_info')?></div>
					<div class="row">
						<div class="col-lg-6 col-sm-8 col-xs-12 col-lg-offset-3 col-xs-offset-0 col-sm-offset-2 plashka">
							Мы перезвоним Вам в течении двух минут и предоставим выгодный варинт рассрочки.
						</div>
					</div>
					<div class="row ">
						<div class="col-lg-4 col-sm-6 col-xs-12 col-lg-offset-2 col-sm-offset-0">
							<?=html_array('form/input',array(
								'caption'	=>	'Имя<span> *</span>',
								'name'		=>	'name',
								'value'		=>	'',
								'attr'		=>	' required',
							));?>
						</div>
						<?php /*
						<div class="col-lg-4 col-sm-6 col-xs-12">
							<?=html_array('form/input',array(
								'caption'	=>	'E-mail<span> *</span>',
								'name'		=>	'email',
								'value'		=>	'',
								'attr'		=>	' required email',
							));?>
						</div>
 */?>
						<div class="col-lg-4 col-sm-6 col-xs-12 ">
							<?=html_array('form/input',array(
								'caption'	=>	'Телефон<span> *</span>',
								'name'		=>	'phone',
								'value'		=>	'',
								'attr'		=>	' required mask_phone',
								'placeholder'		=>	' placeholder="+7 (_ _ _) _ _ _-_ _-_ _"',
							));?>
						</div>
						<input name="chose_type" type="hidden" value="<?=$q['id']?>" />
					</div>
					<div class="row ">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 message_block"></div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m_b_50">
							<div class="list-group-item-text_checkbox">
								<label style="display: flex;margin: 0;justify-content: center;align-items: center;">
									<input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
									<span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
							<input type="submit" class="btn selection-form__btn button_agreement" value="<?=i18n('common|get_installments_btn')?>" />
						</div>
					</div>
				</div>
			</form>
			<script type="text/javascript">
				document.addEventListener("DOMContentLoaded", function () {
					$('body').on('submit','#make_installments form', function(){
						var form = $(this),
							content_form = form.find('.modal-body'),
							message_block = form.find('.message_block');
						if (form.valid()) {
							var $finish='';
							$('.selection-form.parameters_js input').each(function(){
								var $name = $(this).attr("name");
								var $val = $(this).val();
								if($val != '' && $name != ''){
									$finish = $finish+'&'+$name+'='+$val;
								}
							});
							//alert($finish);
							//return false;
							form.find('input[type=submit]').val('Отправляем...');
							form.find('input[type=submit]').prop('disabled', true);
							form.ajaxSubmit({
								url:	'/<?=$modules['finished_objects']?>/<?=$page['url']?>?modal2'+$finish,
								type: 	"POST",
								success:	function (data){
									form.find('input[type=submit]').prop('disabled', false);
									form.find('input[type=submit]').val('<?=i18n('feedback|send')?>');
									if (data==1){
										$(content_form).html('<div class="h2"><?=i18n('feedback|message_is_sent')?></div><div class="col-lg-4 col-md-6 col-sm-8 col-xs-12 col-lg-offset-4 col-md-offset-3 col-xs-offset-0 col-sm-offset-2" ><a class="btn " href="#" data-dismiss="modal" aria-hidden="true">Спасибо!</a></div>');
									}
									else $(message_block).html(data);
								}
							});
							return false;
						}
					});
				});
			</script>
		</div>
	</div>
	<section class="selection" style="background-image: url('/files/finished_objects/<?=$q['id']?>/img_head/l-<?=$q['img_head']?>');">
		<div class="container">
			<div class="h2" style="text-shadow: #000000 1px 0px 2px;"><?=$q['name_head']?></div>
			<div class="selection-form parameters_js" data-action="/<?=$modules['finished_objects']?>/<?=$page['url']?>/" >
				<div class="selection-form__title"><?=i18n('common|objects_get_info')?></div>
				<div class="row row_flex">
					<?php
					$parameters = mysql_select('SELECT * FROM shop_parameters WHERE display = 1 AND chose_type = '.$q['id'].' ORDER BY rank DESC', 'rows_id');
					if (count($parameters)>0){
						foreach ($parameters as $k=>$v){
							if (in_array($v['type'],array(1,4))) {
								$values = unserialize($v['values']);
								//показываем если есть параметры
								if (count($values)>0) {
									//v1.2.19 - для мультивыбора нужно достать все параметры, так ключ может содержать несколько значений через запятую
									if ($v['type'] == 4) {
										foreach ($values as $k1=>$v1) {
											if (strripos($v1,',')) {
												$v2 = explode(',',$v1);
												foreach ($v2 as $k3=>$v3) {
													$values[$v3] = $v3;
												}
												unset($values[$k1]);
											}
										}
									}
									foreach ($values as $k1=>$v1) if (isset($array[$k1])) $values[$k1]=$array[$k1];
									echo html_array('form/multi_checkbox_finished',array(
										'caption'=>$v['name'].':',
										'name'=>'p'.$k.'',
										'data'=>$values,
										'value'=>'',
										'class'=> $v['type'] == 1 ? 'single_checkbox' : '',
									));
								}
							}
							//да/нет
							elseif($v['type']==3)
								echo html_array('form/select',array(
									'caption'=>$v['name'],
									'name'=>'p'.$k.'',
									'select'=>select($q['p'.$k],unserialize($v['values']),''),
								));
						}
					}
					?>
				</div>
				<div class="row ">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
						<a href="#" data-toggle="modal" data-target="#make_finished" class="btn selection-form__btn button_agreement"><?=@$page['form_name_btn']!='' ? $page['form_name_btn'] : i18n('common|objects_get')?></a>
						<p style="margin: 15px 0 0;">*в течение 2-х минут мы предоставим 10 самых выгодных предложений</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	//http://workspace.abc-cms.com/proekty/stroidom93.ru/2/43/

	if($u[2]=='doma-i-kottedzhi'){
		echo html_query('finished_objects/items_list_houses',"
		SELECT *
		FROM finished_object_items
		WHERE display = 1 AND `type` = ".$q['id']."
		ORDER BY rank DESC
	",'',60*60);
	}
	elseif ($u[2]=='kottedzhi'){
		// слайдер http://workspace.abc-cms.com/proekty/stroidom93.ru/2/71/
		echo html_query('projects/projects_slider_finished_obj',
			"SELECT *
				FROM finished_object_items
				WHERE display = 1 AND `type` = 5
				ORDER BY rank DESC",
			'');
	}
	elseif ($u[2]=='podbor-kvartir'){
		$query = "SELECT sp.*, sb.url category_url
		FROM projects sp
		LEFT JOIN projects_categories sb ON sb.id=sp.category
		WHERE sp.display = 1 AND sb.display=1 AND sp.category=3
		ORDER BY sp.rank DESC
		LIMIT 6";
		//echo html_query('projects/projects_list_index',$query,'');
		/**/
		//http://workspace.abc-cms.com/proekty/stroidom93.ru/2/46/
		echo html_query('projects/projects_list_slider_index',$query,'');
	}
	else{
		// слайдер http://workspace.abc-cms.com/proekty/stroidom93.ru/2/71/

		echo html_query('projects/projects_slider_finished_obj',
			"SELECT *
				FROM finished_object_items
				WHERE display = 1 AND `type` = 5
				ORDER BY rank DESC",
			'');
	}

	$type_advantages_list = $q['id']==1 ? 9 : 10;

	echo html_query('finished_objects/advantages_list',"
		SELECT *
		FROM advantages
		WHERE display = 1 AND `type` = ".$type_advantages_list."
		ORDER BY rank DESC
	",'',60*60);

	/*
	http://workspace.abc-cms.com/proekty/stroidom93.ru/2/25/

	if ($q['installments_img1']!='' && $q['installments_img2']!='' && $q['installments_img3']!='' &&
		$q['installments_name1']!='' && $q['installments_name2']!='' && $q['installments_name3']!=''){

		$installments_img1 = $q['installments_img1'] ? '/files/finished_objects/'.$q['id'].'/installments_img1/p-'.$q['installments_img1'] : '/'.$config['style'].'/images/no_img.svg';
		$installments_img2 = $q['installments_img2'] ? '/files/finished_objects/'.$q['id'].'/installments_img2/p-'.$q['installments_img2'] : '/'.$config['style'].'/images/no_img.svg';
		$installments_img3 = $q['installments_img3'] ? '/files/finished_objects/'.$q['id'].'/installments_img3/p-'.$q['installments_img3'] : '/'.$config['style'].'/images/no_img.svg';

		?>
		<section class="advantages">
			<div class="container">
				<div class="sprite hr"></div>
				<div class="h2"><?=i18n('common|objects_installments')?></div>
				<div class="row">
					<div class="col-md-3 col-sm-5">
						<div class="advantages-item">
							<img src="<?=$installments_img1?>" alt="<?=$q['installments_img1']?>">
							<div class="advantages-item__text">
								<?=$q['installments_name1']?>
							</div>
						</div>
					</div>
					<div class="col-md-1 col-sm-2">
						<div class="advantages__symbol">+</div>
					</div>
					<div class="col-md-3 col-sm-5">
						<div class="advantages-item">
							<img src="<?=$installments_img2?>" alt="<?=$q['installments_img2']?>">
							<div class="advantages-item__text">
								<?=$q['installments_name2']?>
							</div>
						</div>
					</div>
					<div class="col-md-1 col-sm-12">
						<div class="advantages__symbol advantages__symbol_2">=</div>
					</div>
					<div class="col-md-4 col-md-offset-0 col-sm-8 col-sm-offset-2">
						<div class="advantages-item">
							<img src="<?=$installments_img3?>" alt="<?=$q['installments_img3']?>">
							<div class="advantages-item__text">
								<?=$q['installments_name3']?>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3 col-md-offset-4">
						<a href="#" data-toggle="modal" data-target="#make_installments" class="btn advantages__btn"><?=i18n('common|objects_get_installments')?></a>
					</div>
				</div>
			</div>
		</section>
	<?php } /**/
	echo '<div style="margin-top: -50px"></div>';
	echo html_query('finished_objects/banks_list',"
		SELECT *
		FROM banks
		WHERE display = 1
		ORDER BY rank DESC
	",'',60*60);

	if ($q['developer_name1']!='' && $q['developer_list1']!=''){ ?>
		<section class="developer" style="background-image: url('/files/finished_objects/<?=$q['id']?>/developer_img/p-<?=$q['developer_img']?>');">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-sm-6 ">
						<div class="developer__text">
							<?=$q['developer_name1']?>
						</div>
					</div>
					<div class="col-lg-6 col-lg-offset-1 col-sm-6 ">
						<ul class="developer__list">
							<?=$q['developer_list1']?>
						</ul>
					</div>
				</div>
			</div>
		</section>
	<?php } ?>
	<?php //кнопка Хочу свой дом обратная связь?>
	<div class="container investments">
		<div class="row">

			<div class="col-lg-12 col-xs-12">
				<div class="point_bottom">
					<button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_finished" style="margin-bottom: 0; margin-top: 50px">Хочу свой дом</button>
				</div>
			</div>
		</div>
	</div>
	<?php
	if (($q['about_img1']!='' && $q['about_name1']!='')
		|| ($q['about_img2']!='' && $q['about_name2']!='')
		|| ($q['about_img3']!='' && $q['about_name3']!='')
		|| ($q['about_img4']!='' && $q['about_name4']!='')
	) {
		$about_img1 = $q['about_img1'] ? '/files/finished_objects/'.$q['id'].'/about_img1/p-'.$q['about_img1'] : '/'.$config['style'].'/images/no_img.svg';
		$about_img2 = $q['about_img2'] ? '/files/finished_objects/'.$q['id'].'/about_img2/p-'.$q['about_img2'] : '/'.$config['style'].'/images/no_img.svg';
		$about_img3 = $q['about_img3'] ? '/files/finished_objects/'.$q['id'].'/about_img3/p-'.$q['about_img3'] : '/'.$config['style'].'/images/no_img.svg';
		$about_img4 = $q['about_img4'] ? '/files/finished_objects/'.$q['id'].'/about_img4/p-'.$q['about_img4'] : '/'.$config['style'].'/images/no_img.svg';

		?>
		<section class="about">
			<div class="container">
				<div class="sprite hr"></div>
				<div class="h2"><?=i18n('common|objects_try_better')?></div>
				<div class="row">
					<?php
					if ($q['about_img1']!='' && $q['about_name1']!=''){
					?>
					<div class="col-sm-6">
						<div class="about-item">
							<div class="row">
								<div class="col-md-6">
									<img src="<?=$about_img1?>" alt="<?=$q['about_img1']?>">
								</div>
								<div class="col-md-6">
									<div class="about-item__descr">
										<?=$q['about_name1']?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php }
					if ($q['about_img1']!='' && $q['about_name1']!=''){?>
					<div class="col-sm-6">
						<div class="about-item">
							<div class="row">
								<div class="col-md-6">
									<img src="<?=$about_img2?>" alt="<?=$q['about_img2']?>">
								</div>
								<div class="col-md-6">
									<div class="about-item__descr">
										<?=$q['about_name2']?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php }
					if ($q['about_img1']!='' && $q['about_name1']!=''){?>
					<div class="col-sm-6">
						<div class="about-item">
							<div class="row">
								<div class="col-md-6">
									<img src="<?=$about_img3?>" alt="<?=$q['about_img3']?>">
								</div>
								<div class="col-md-6">
									<div class="about-item__descr">
										<?=$q['about_name3']?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php }
					if ($q['about_img1']!='' && $q['about_name1']!=''){?>
					<div class="col-sm-6">
						<div class="about-item">
							<div class="row">
								<div class="col-md-6">
									<img src="<?=$about_img4?>" alt="<?=$q['about_img4']?>">
								</div>
								<div class="col-md-6">
									<div class="about-item__descr">
										<?=$q['about_name4']?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>

		<?php
	}


	/*

	http://workspace.abc-cms.com/proekty/stroidom93.ru/2/25/
/**/

	if ($q['text']){
	?>
		<section class="text_block seo_text">
			<div class="container">
				<div class="sprite hr"></div>
				<?=$q['text']?>
			</div>
		</section>
	<?php }

	//объекты третей вложености

	$finished_objects = mysql_select("SELECT * FROM finished_objects_page WHERE category = '" . $q['id'] . "' AND display = 1 ", 'rows_id');
?>
	<div class="container">
	<?= html_query('finished_objects/projects_list',$finished_objects);?>
	</div>
	<?

/**/
	//$next_objects = mysql_select('SELECT * FROM finished_objects WHERE display = 1 AND id !='.$page['id'].' ORDER BY RAND ('.$page['id'].')','row');
/*
	if ($next_objects){
		$next_url = get_url('finished_objects',$next_objects);
		?>
		<section class="finished-houses" style="background-image: url('/files/finished_objects/<?=$next_objects['id']?>/next_img/l-<?=$next_objects['next_img']?>')">
			<div class="h2"><?=$next_objects['next_name']?></div>
			<a href="<?=$next_url?>" class="btn finished-houses__btn"><?=@$next_objects['name_btn']!='' ? $next_objects['name_btn'] : i18n('common|objects_pick_up')?></a>
		</section>
	<?php }
/**/
?>
</div>