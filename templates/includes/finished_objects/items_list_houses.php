<?php
$img = $q['img'] ? '/files/finished_object_items/'.$q['id'].'/img/p-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';

if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
if ($i==1){?>
<section class="variants">
	<div class="container">
		<div class="hr"></div>
		<?php
		if ($html['module']=='index'){?>
			<h3 class="variants__title">Наши предложения:</h3>
		<?}else{?>
			<h3 class="variants__title"><?=$page['name_items']?></h3>
		<?}?>


		<div class="row">
			<div class="items_list_finished_object_houses">
				<?php } ?>
				<div class="img_wrap col-xs-12 col-sm-6 col-md-3">
					<a href="#" data-toggle="modal" data-target="#make_finished" class="variants-item">
						<img src="<?=$img?>" alt="<?=$q['name']?>" class="variants-item__photo" >
						<div class="variants-item__title"><?=$q['name']?></div>
						<?php
						if($q['price2']!=0){ ?>
							<div class="variants-item__old-price"><?=number_format($q['price2'],0,',',' ')?> руб.</div>
						<? } ?>
						<?php
						/*
						<div class="variants-item__square"><?=number_format($q['area'],1,',',' ')?> м<sup>2</sup></div>
						   */
						?>

						<div class="variants-item__square"><?=number_format($q['area'],0,',',' ')?> м<sup>2</sup></div>
						<div class="variants-item__price"><?=number_format($q['price'],0,',',' ')?> pуб.</div>

					</a>
				</div>
				<?php
				//if (fmod($i,4)==0) echo '<div class="clearfix"></div>';

				if ($i==$num_rows){?>
			</div>
		</div>
		<div class="text_finished_houses">
			*Цена на дом указана с учетом земельного участка 3 сотки, проведенными коммуникациями и документацией
		</div>
		<a href="#" data-toggle="modal" data-target="#make_finished" class="btn variants__btn"><?=@$page['name_btn']!='' ? $page['name_btn'] : i18n('common|objects_choose_option')?></a>
	</div>
</section>
<?php } ?>

