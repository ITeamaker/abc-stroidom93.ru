<?php
$img = $q['img'] ? '/files/banks/'.$q['id'].'/img/p-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';

if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
if ($i==1){?>
	<section class="partner-banks">
		<div class="container">
			<div class="sprite hr"></div>
			<div class="h2"><?=i18n('common|objects_banks')?></div>
				<div class="row">
<?php } ?>
					<div class="col-xs-6 col-sm-3">
						<div class="partner-banks-item">
							<img src="<?=$img?>" title="<?=$page['name']?>" alt="<?=$page['name']?>">
						</div>
					</div>
<?php
if (fmod($i,4)==0) echo '<div class="clearfix"></div>';

if ($i==$num_rows){
	echo '</div></div></section>';
}