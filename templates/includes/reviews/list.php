<?php
if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$url = get_url('reviews',$q);
$q['text'] = strip_tags($q['text']);
$text = iconv_strlen($q['text'])>250 ? iconv_substr($q['text'],0,250,"UTF-8").'...' : $q['text'];

$photo = $q['photo'] ? '/files/reviews/'.$q['id'].'/photo/p-'.$q['photo'] : '/templates/images/photo.png';
if ($i==1){?>
	<?=html_sources('footer','highslide_gallery')?>
	<div class="container reviews">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
                <div class="sprite hr"></div>
				<h1><?=$page['name']?></h1>
			</div>
		</div>
<?php }?>
		<div class="row">
			<?php
			if ($i!=1){?>
				<div class="col-lg-12 col-xs-12">
					<div class="point"><div class="sprite point_vertical"></div></div>
				</div>
			<?php }?>
			<div class="col-lg-12 col-xs-12 ">
				<div class="review_item <?=$i==$num_rows ? 'last' : ''?>">
					<div class="row">
						<div class="col-lg-3 col-xs-3">
							<a href="<?=$url?>" title="<?=$q['name']?>" class="review_name">
								<img src="<?=$photo?>">
								<div class="name"><?=$q['name']?></div>
							</a>
						</div>
						<div class="col-lg-6 col-xs-6 text_wrap">
							<div class="text"><?=$text?></div>
							<a href="<?=$url?>" title="<?=i18n('common|wrd_more')?>" class="btn_silver wrd_more"><?=i18n('common|wrd_more')?></a>
						</div>
						<div class="col-lg-3 col-xs-3">
							<?php
							if ($q['img']) {?>
								<a class="fancybox_single img_review" title="<?=$q['name']?>"  href="/files/reviews/<?=$q['id']?>/img/<?=$q['img']?>" >
									<img src="/files/reviews/<?=$q['id']?>/img/p-<?=$q['img']?>" alt="<?=$q['name']?>"  />
								</a>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
if ($i==$num_rows){?>
	</div>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function () {
			$(".fancybox_single").fancybox({
				openEffect	: 'elastic',
				closeEffect	: 'elastic',
				padding: 0,
				helpers : {
					title : {
						type : 'over'
					}
				}
			});
		});
	</script>
<?php }?>