<?php
$photo = $q['photo'] ? '/files/reviews/'.$q['id'].'/photo/p-'.$q['photo'] : '/templates/images/photo.png';
?>
<?=html_sources('footer','fancybox.js')?>
<div class="container reviews">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<h1><?=$q['name']?></h1>
		</div>
		<div class="col-lg-3 col-xs-3 item_name">
			<div class="review_name">
				<img src="<?=$photo?>">
				<div class="name"><?=$q['name']?></div>
			</div>
			<?php
			if ($q['img']) {?>
				<a class="img_review fancybox_single" title="<?=$q['name']?>"  href="/files/reviews/<?=$q['id']?>/img/<?=$q['img']?>" >
					<img src="/files/reviews/<?=$q['id']?>/img/l-<?=$q['img']?>" alt="<?=$q['name']?>"  />
				</a>
				<?php
			}
			?>
		</div>
		<div class="col-lg-9 col-xs-9 text_block">
			<?=$q['text']?>
		</div>
		<div class="col-lg-12 col-xs-12">
			<a href="/<?=$modules['reviews']?>/" class="slider_btn_arrows" title="<?=i18n('common|back_review_btn')?>"><?=i18n('common|back_review_btn')?></a>
		</div>
	</div>
</div>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		$(".fancybox_single").fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			padding: 0,
			helpers : {
				title : {
					type : 'over'
				}
			}
		});
	});
</script>