<?php
//print_r ($page);
if ($page['agreement']==1) {
    ?>
    <div class="container pages">
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <h1><?=$page['name']?></h1>
            </div>
            <div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block pages_wraps">
                <?=$page['text']?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <?php
}
else{?>
    <div class="container pages">
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                <h1><?=$page['name']?></h1>
            </div>
            <div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block pages_wraps">
                <?=$html['page_children']?>
                <?=$page['text']?>
                <?php
                if ($page['vacancy']==1){
                    echo html_query('pages/list_vacancy',"
                        SELECT *
                        FROM vacancy
                        WHERE display=1
                        ORDER BY rank DESC
                    ",'');
                }?>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 col-xs-12">
                <div class="point"><div class="sprite point_horizontal"></div></div>
            </div>
            <div class="col-lg-12 col-xs-12">
                <div class="point_bottom">
                    <button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call">
                        <?=$page['vacancy']==1 ? i18n('common|order_consultation_vacancy') : i18n('common|order_consultation');?>
                    </button>
                </div>
            </div>
            <?=html_query('common/why_we',"
                SELECT *
                FROM advantages
                WHERE display=1 AND type = 6
                ORDER BY rank DESC
            ",'')?>
            <?=html_array('common/about_block',$page)?>
            <?=html_array('common/price_table',$page)?>
            <?php
            if ($page['text_seo_1'] != ''){?>
                <div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
                    <?=$page['text_seo_1']?>
                </div>
                <div class="clearfix"></div>
            <?php }?>
            <?=html_array('form/make_order',$page)?>
        </div>
    </div>

<?php }