<div class="col-lg-12 col-xs-12">
	<div class="h2"><?=i18n('common|order_service')?></div>
</div>
<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 form_wrap">
	<noscript><?=i18n('validate|not_valid_captcha2')?></noscript>
	<div class="row">
		<div class="col-lg-12 col-xs-12 message_block">
			<?=isset($q['message']) ? html_array('form/message',$q['message']) : ''?>
		</div>
	</div>
	<form method="post" class="form validate row make_order_form" id="make_order_form<?=@$q['class'] ? $q['class'] : ''?>">
		<div class="col-lg-6 col-xs-6">
			<?php
			echo html_array('form/input',array(
				'caption'	=>	i18n('feedback|name',true).'<span> *</span>',
				'name'		=>	'name',
				'value'		=>	isset($q['name']) ? $q['name'] : '',
				'attr'		=>	' required',
			));
			echo html_array('form/input',array(
				'caption'	=>	i18n('feedback|phone',true).'<span> *</span>',
				'name'		=>	'phone',
				'value'		=>	isset($q['phone']) ? $q['phone'] : '',
				'attr'		=>	' required mask_phone',
			));
			echo html_array('form/input',array(
				'caption'	=>	i18n('common|email_name')/*.'<span> *</span>'*/,
				'name'		=>	'email',
				'value'		=>	isset($q['email']) ? $q['email'] : '',
				'attr'		=>	' email',
			));
			/*
			$value_services = array();
			$value_services = mysql_select('SELECT id,name FROM pages WHERE display = 1 AND services=1 ORDER BY rank DESC', 'array'); //запрос на услуги
			echo html_array('form/select',array(
				'caption'=>	i18n('feedback|services',true).'<span> *</span>',
				'name'	=>	'services',
				'select'=>	select($page['pid'], $value_services,''),
				'attr'	=>	' required ',
			));
			*/
			?>
		</div>
		<div class="col-lg-6 col-xs-6">
			<?php
			echo html_array('form/textarea',array(
				'name'		=>	'text',
				'caption'	=>	i18n('feedback|text',true),
				'value'		=>	isset($q['text']) ? $q['text'] : '',
				//'attr'		=>	' required',
			));
			//echo html_array('form/captcha2');//скрытая капча
			?>
		</div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="list-group-item-text_checkbox">
                <label style="display: flex;margin: 0;justify-content: center;align-items: center;">
                    <input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
                    <span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
            </div>
        </div>
		<div class="col-lg-12 col-xs-12 ">
            <input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
            <input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
			<?php
			//print_r ($modules);
			echo html_array('form/button',array(
				'name'	=>	i18n('feedback|send'),
				'class'	=>	'btn_red right button_agreement',
			));
			?>
			<div class="clearfix"></div>
		</div>
	</form>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function () {
			$('#make_order_form<?=@$q['class'] ? $q['class'] : ''?>').submit(function(){
				var form = $(this),
					form_wrap = $(this).parents('.form_wrap'),
					message_block = form_wrap.find('.message_block');
				if (form.valid()) {
                    form.find('input[type=submit]').text('Отправляем...');
                    form.find('input[type=submit]').prop('disabled', true);
					form.ajaxSubmit({
						url:	'/<?=$modules['contacts']?>/?action=order_services',
						type: 	"POST",
						success:	function (data){
                            form.find('input[type=submit]').prop('disabled', false);
                            form.find('input[type=submit]').text('<?=i18n('feedback|send')?>');
							if (data==1) $(form).html('<?=i18n('feedback|order_is_sent')?>');
							else $(message_block).html(data);
						},
						error:	function(xhr,txt,err){
                            form.find('input[type=submit]').prop('disabled', false);
                            form.find('input[type=submit]').text('<?=i18n('feedback|send')?>');
							alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
						}
					});
					return false;
				}
			});
		});
	</script>
</div>
