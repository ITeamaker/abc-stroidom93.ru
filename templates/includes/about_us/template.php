<div class="container about_us">
	<div class="row">
        <?php
        if ($page['quote']==1 && $page['quote_text']!=''){?>
            <div class="col-lg-12 col-xs-12 quote">
                <blockquote>
                    <cite>Миссия компании:</cite>
                    <p>"<?=$page['quote_text']?>"</p>
                </blockquote>
            </div>
            <div class="clearfix"></div>
        <?php }?>
        <div class="col-lg-12 col-xs-12">
			<h1><?=$page['name']?></h1>
		</div>
		<div class="clearfix"></div>
		<?php /* [4.48] Убрать контакты на странице услуги, передвинуть картинки по центру
		<div class="col-lg-3 col-xs-3 photo">
			<addres>
				<div class="addres-line">
					<div class="addres-icon" style="font-size:30px;color:#ff0000; background-color:#ffffff">
						<span class="fa fa-map-marker"></span>
					</div>
					<div class="addres-text">
						<p class="addres-title">Контактные данные</p>
						<p>Ул. Труда 27а, г.Сочи, Краснодарский край</p>
					</div>
				</div>
				<div class="addres-line">
					<div class="addres-icon" style="font-size:30px;color:#ff0000; background-color:#ffffff">
						<span class="fa fa-phone"></span>
					</div>
					<div class="addres-text">
						<p class="addres-title">Телефон</p>
						<p>8 (918) 3000 435</p>
					</div>
				</div>
				<div class="addres-line">
					<div class="addres-icon" style="font-size:30px;color:#ff0000; background-color:#ffffff">
						<span class="fa fa-phone"></span>
					</div>
					<div class="addres-text">
						<p class="addres-title">Добавочный телефон</p>
						<p>8 (918) 915 7211</p>
					</div>
				</div>
				<div class="addres-line">
					<div class="addres-icon" style="font-size:26px;color:#ff0000; background-color:#ffffff">
						<span class="fa fa-envelope-o"></span>
					</div>
					<div class="addres-text">
						<p class="addres-title">Почта</p>
						<p>info@stroicentr1.ru</p>
					</div>
				</div>
			</addres>
			<?php /*
			<div class="img" style="background-image: url('/files/languages/<?=$lang['id']?>/photo/p-<?=$lang['photo']?>')"></div>
			<div class="name"><?=i18n('common|photo_name')?></div>
			*//* ?>
		</div>
*/?>
		<div class="col-lg-12 col-xs-12 text_block">
			<?=$page['text']?>
		</div>
		<div class="clearfix"></div>
		<?=html_query('about_us/why_we',"
			SELECT *
			FROM advantages
			WHERE display=1 AND type = 7
			ORDER BY rank DESC
		",'')?>
	</div>
</div>
<?=html_array('form/contract_application', $page)?>
<div class="container about_us">
	<div class="row">
		<?=html_array('about_us/certificates')?>
        <div class="clearfix"></div>
        <?php
        if ($page['id']==63){?>
            <?=html_array('designing_houses/make_order')?>
            <div class="clearfix"></div>
        <?php }?>
	</div>
</div>