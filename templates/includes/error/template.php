<div class="container">
	<div class="row">
		<div class="col-lg-12 col-xs-12 ">
			<h1<?=editable('dictionary|common|str_no_page_name|'.$lang['id'],'editable_str')?>><?=i18n('common|str_no_page_name')?></h1>
			<div class="text">
				<div<?=editable('dictionary|common|txt_no_page_text|'.$lang['id'],'editable_text')?>><?=i18n('common|txt_no_page_text')?></div>
			</div>
		</div>
	</div>
</div>