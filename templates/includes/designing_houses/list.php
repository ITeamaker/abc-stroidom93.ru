<?php
$q['text'] = strip_tags($q['text']);
$text = iconv_strlen($q['text'])>300 ? iconv_substr($q['text'],0,300,"UTF-8").'..' : $q['text'];
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('news',$q);
?>
<div class="news_list">
	<div class="date"><?=date2($q['date'],'%d.%m.%y')?></div>
	<div class="name">
		<a href="<?=$url?>" title="<?=$title?>"><?=$q['name']?></a>
	</div>
	<div class="text">
		<?=$text?>
	</div>
	<div class="next">
		<a href="<?=$url?>"><?=i18n('common|wrd_more')?> <i class="icon-caret-right"></i></a>
	</div>
</div>