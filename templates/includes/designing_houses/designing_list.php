<?php

if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$img = $q['img'] ? '/files/designing_houses/'.$q['id'].'/img/p-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('designing_houses',$q);
if ($i==1) {?>
	<div class="inner_container">
		<div class="row">
<?php } ?>
			<div class="shop_product_list col-xs-4 col-sm-4 col-md-4">
				<div class="shop_product_wrap">
					<div class="img">
						<div>
							<a href="<?=$url?>" title="<?=$title?>">
								<img src="<?=$img?>" title="<?=$page['name']?>" alt="<?=$page['name']?>" />
							</a>
						</div>
					</div>
					<div class="product_param_wrap">
						<a class="name_link" href="<?=$url?>" title="<?=$title?>"><?=$q['name']?></a>
						<?php if ($q['style']!='') {?>
						<div class="data">
							<div class="name"><?=i18n('common|style')?></div>
							<div class="value"><?=$q['style']?></div>
						</div>
						<?php }
						if ($q['area']!='') {?>
							<div class="data">
								<div class="name"><?=i18n('shop|areas')?></div>
								<div class="value"><?=$q['area']?></div>
							</div>
						<?php }
						if ($q['bedrooms']!='') {?>
							<div class="data">
								<div class="name"><?=i18n('common|bedrooms')?></div>
								<div class="value"><?=$q['bedrooms']?></div>
							</div>
						<?php }
						if ($q['floors']!='') {?>
							<div class="data">
								<div class="name"><?=i18n('shop|floor')?></div>
								<div class="value"><span><?=$q['floors'].' '.plural($q['floors'],i18n('shop|floor1'),i18n('shop|floor2'),i18n('shop|floors5'))?></span></div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
<?php
if (fmod($i,3)==0) echo '<div class="clearfix"></div>';
if ($i==$num_rows) echo '</div></div>';
?>