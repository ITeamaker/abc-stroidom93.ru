<?php
//print_r ($q);
?>
<div class="col-lg-12 col-xs-12 ">
	<form method="get" class="designing_houses_filter form_clear">
		<div class="filter_block">
			<?php
			//этажность
			if ($config['shop_product_floor_filter']) {
				echo '<div class="checkbox_new_wrap">';
				echo html_array('form/multi_checkbox',array(
					'name'=>'type_floor',
					'value'=>@$q['type_floor'],
					'data'=>$config['shop_product_floor_filter']
				));
				echo '</div>';
			}
			?>
		</div>
		<?php /*
		<div class="filter_block sort slide_filter" >
			<div class="blocks">
				<div class="name"><?=i18n('shop|size')?>:</div>
				<div class="slide_wrap">
					<div id="area_slide"></div>
					<input type="hidden" name="area_min" id="area_slide_min">
					<input type="hidden" name="area_max" id="area_slide_max">
					<div class="name_linear_wrap">
						<div class="name_linear left_n">1 кв. м.</div>
						<div class="name_linear right_n">10,000 кв.м.</div>
					</div>
				</div>
			</div>
			<div class="blocks">
				<div class="name"><?=i18n('shop|costs')?>:</div>
				<div class="slide_wrap">
					<div id="price_slide"></div>
					<input type="hidden" name="price_min" id="price_slide_min">
					<input type="hidden" name="price_max" id="price_slide_max">
					<div class="name_linear_wrap">
						<div class="name_linear left_n">1,000 руб.</div>
						<div class="name_linear right_n">10,000,000 руб.</div>
					</div>
				</div>
			</div>
		</div>
		*/?>
	</form>
</div>
<?php
if (isset($_GET['type_floor']) || isset($_GET['price_min']) || isset($_GET['price_max']) || isset($_GET['area_min']) || isset($_GET['area_max'])){?>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded", function () {
			var $mm = $('.designing_houses_filter').offset().top-50;
			$("html, body").animate({ scrollTop: $mm }, 400);
		});
	</script>
<?php }?>
<script>
	document.addEventListener("DOMContentLoaded", function () {
		var nonLinearSlider = document.getElementById('price_slide');
		var input0 = document.getElementById('price_slide_min');
		var input1 = document.getElementById('price_slide_max');
		var inputs = [input0, input1];
		noUiSlider.create(nonLinearSlider, {
			behaviour: 'tap',
			connect: true,
			start: [<?=@$q['price_min'] ? $q['price_min'] : '3000'?>, <?=@$q['price_max'] ? $q['price_max'] : '9000000'?>],
			//tooltips:true,
			tooltips: [wNumb({decimals: 0, thousand: ','}), wNumb({decimals: 0, thousand: ','})],
			range: {
				'min': 1000,
				'max': 10000000
			}
		});
		nonLinearSlider.noUiSlider.on('update', function (values, handle) {
			inputs[handle].value = values[handle];
		});
		var areaSlider = document.getElementById('area_slide');
		var inputa0 = document.getElementById('area_slide_min');
		var inputa1 = document.getElementById('area_slide_max');
		var inputas = [inputa0, inputa1];
		noUiSlider.create(areaSlider, {
			behaviour: 'tap',
			connect: true,
			start: [<?=@$q['area_min'] ? $q['area_min'] : '10'?>, <?=@$q['area_max'] ? $q['area_max'] : '6000'?>],
			//tooltips:true,
			tooltips: [wNumb({decimals: 0, thousand: ','}), wNumb({decimals: 0, thousand: ','})],
			range: {
				'min': 1,
				'max': 10000
			}
		});
		areaSlider.noUiSlider.on('update', function (values, handle) {
			inputas[handle].value = values[handle];
		});
	});
</script>
