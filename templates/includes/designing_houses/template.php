<?php
//страница типа строительства
if ($designing_houses) {?>
	<?=$html['content']?>
	<?=$html['list']?>
<?php }
//страница строительства общая
else {
	//print_r($page);
	?>
	<div class="container buildings">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<h1><?=$page['name_site']!='' ? $page['name_site'] : $page['name']?></h1>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
				<?=$page['text']?>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
				<div class="point"><div class="sprite point_horizontal"></div></div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
                <div class="sprite hr"></div>
				<div class="h2"><?=i18n('common|we_designing')?></div>
			</div>
			<div class="clearfix"></div>
			<?=$html['list']?>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
				<div class="point"><div class="sprite point_horizontal"></div></div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
				<div class="point_bottom">
					<button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call" id="consult_proekt">
						<?=i18n('common|order_consultation');?>
					</button>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12 ">
                <div class="sprite hr"></div>
				<div class="h2"><?=i18n('common|why_we')?></div>
			</div>
			<div class="clearfix"></div>
			<?=html_query('designing_houses/why_we',"
				SELECT *
				FROM advantages
				WHERE display=1 AND type = 3
				ORDER BY rank DESC
			",'')?>
			<div class="clearfix"></div>
		</div>
		<?=$html['build_block']?>
		<?php
		if ($page['text_seo_1'] != ''){?>
			<div class="row">
				<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
                    <div class="sprite hr"></div>
					<?=$page['text_seo_1']?>
				</div>
			</div>
		<?php }?>
		<div class="clearfix"></div>
		<div class="row">
			<?=html_array('designing_houses/make_order')?>
		</div>
	</div>
	<?php
}
echo '<div style="margin-top: 50px"></div>';
echo html_array('form/contract_application', $page);
