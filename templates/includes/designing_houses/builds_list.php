<?php
if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$url = get_url('designing_houses',$q);

if ($i==1){?>
	<div class="container buildings">
		<div class="row">
			<div class="col-lg-12 col-xs-12 we_also_build">
                <div class="sprite hr"></div>
				<div class="h2"><?=i18n('common|we_also_designing')?></div>
			</div>
<?php }?>
			<div class="col-lg-4 col-xs-4">
				<div class="building_block_product" style="background-image: url(/files/designing_houses/<?=$q['id']?>/img_list/p-<?=$q['img_list']?>);">
					<a href="<?=$url?>" title="<?=$q['name']?>" class="name">
						<span><?=$q['name']?></span>
					</a>
				</div>
			</div>
<?php
if (fmod($i,3)==0) echo '<div class="clearfix"></div>';
if ($i==$num_rows) echo '</div></div>';
?>