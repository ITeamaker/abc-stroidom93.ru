<div class="col-lg-12 col-xs-12">
	<div class="h2"><?=i18n('common|certificates',true)?></div>
</div>
<div class="clearfix"></div>
<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1">
	<div class="row">
		<?=html_query('certificates/certificates_list',"
			SELECT *
			FROM certificates
			WHERE display=1
			ORDER BY rank ASC
		",'');?>
	</div>
</div>
<div class="clearfix"></div>