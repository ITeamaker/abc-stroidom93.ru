<div class="container certificates">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<h1><?=$page['name']?></h1>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
			<?=$page['text']?>
		</div>
		<div class="clearfix"></div>
		<div class="col-lg-12 col-xs-12">
			<div class="point"><div class="sprite point_horizontal"></div></div>
		</div>
		<?=html_array('certificates/certificates')?>
	</div>
</div>