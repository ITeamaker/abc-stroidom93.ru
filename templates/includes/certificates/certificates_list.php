<?php
if ($i==1) {?>
	<?=html_sources('footer','fancybox.js')?>
	<div class="certificates_wrap">
<?php } ?>
		<div class="col-lg-3 col-xs-3">
			<div class="certificate_img">
				<a class="fancybox_gallery img" rel="gallery" title="<?=$q['name']?>" style="display: block; " onclick="return hs.expand(this, config1)" href="/files/certificates/<?=$q['id']?>/img/<?=$q['img']?>" >
					<img width="100%" src="/files/certificates/<?=$q['id']?>/img/p-<?=$q['img']?>" alt="<?=$q['name']?>"  />
				</a>
			</div>
		</div>

<?php if ($i==$num_rows) { ?>
	</div>
	<script type="text/javascript">
		document.addEventListener("DOMContentLoaded",function(){$(".fancybox_gallery").fancybox({openEffect:"elastic",closeEffect:"elastic",padding:0,helpers:{title:{type:"over"}}}),$(".fancybox_single").fancybox({openEffect:"elastic",closeEffect:"elastic",padding:0,helpers:{title:{type:"over"}}})});
	</script>
<?php }