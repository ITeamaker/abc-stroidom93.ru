
<div class="calculator_wrap col-lg-8 col-xs-8 col-lg-offset-2 col-xs-offset-2">
	<form class="calculator row" method="post" id="calculator_form">
        <div class="col-lg-6 col-xs-6">
            <div class="row">
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_width"></i>
                    <div class="name">Ширина дома, м:</div>
                    <?php
                    echo html_array('form/input',array(
                        'name'		=>	'width',
                        'value'		=>	isset($q['width']) ? $q['width'] : 10,
                        'attr'		=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_length"></i>
                    <div class="name">Длина дома, м:</div>
                    <?php
                    echo html_array('form/input',array(
                        'name'		=>	'length',
                        'value'		=>	isset($q['length']) ? $q['length'] : 10,
                        'attr'		=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_storeys"></i>
                    <div class="name">Количество этажей:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'storeys',
                        'select'=>	select(@$q['storeys'], $config['calculator_storeys']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_сeiling_height"></i>
                    <div class="name">Высота потолков,м:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'сeiling_height',
                        'select'=>	select(@$q['сeiling_height'], $config['calculator_сeiling_height']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12 ">
                    <i class="ico_wall"></i>
                    <div class="name">Материал cтен:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'walls',
                        'select'=>	select(@$q['walls'], $config['calculator_walls']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12 ">
                    <i class="ico_foundation"></i>
                    <div class="name">Фундамент:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'foundation',
                        'select'=>	select(@$q['foundation'], $config['calculator_foundation']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="row">
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_roof"></i>
                    <div class="name">Кровля:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'roof',
                        'select'=>	select(@$q['roof'], $config['calculator_roof']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_windows"></i>
                    <div class="name">Окна:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'windows',
                        'select'=>	select(@$q['windows'], $config['calculator_windows']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_facade"></i>
                    <div class="name">Фасад:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'facade',
                        'select'=>	select(@$q['facade'], $config['calculator_facade']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_communications"></i>
                    <div class="name">Коммуникации:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'communications',
                        'select'=>	select(@$q['communications'], $config['calculator_communications']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
                <div class="calc_row col-lg-12 col-xs-12">
                    <i class="ico_trim"></i>
                    <div class="name">Внутренняя отделка:</div>
                    <?php
                    echo html_array('form/select',array(
                        'name'	=>	'trim',
                        'select'=>	select(@$q['trim'], $config['calculator_trim']),
                        'attr'	=>	' required ',
                    ));
                    ?>
                </div>
            </div>
        </div>
		<div class="result_block col-lg-12 col-xs-12">
			<div class="name">Ориентировочная стоимость строительства:</div>
			<div class="result_cost">
				<span class="result_cost_main"><?=number_format($q['total'],0,'.',' ')?> р.</span>
				<span class="cost_sq">(<?=number_format($q['total']/$q['sum_area'],0,'.',' ')?> р./м2)</span>
			</div>
		</div>
		<div class="attansion  col-lg-12 col-xs-12">
            В стоимость входит: проект, строительные работы, все материалы и транспортные расходы.
		</div>
	</form>
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="point_bottom">
				<button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call_calculator" id="consult_stroi">Получить расчет</button>
			</div>
		</div>
	</div>
</div>
<?php // хочу инвестировать выгодно ?>

<div class="modal make_call fade" id="make_call_calculator" >
	<div class="modal-dialog">
		<form class="modal-content validate" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<div class="h1 fix_h1">Оставьте свои контактные данные и мы свяжемся с Вами.</div>
				<div class="message_block"></div>
				<?php
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|name',true).'<span> *</span>',
					'name'		=>	'name',
					'value'		=>	isset($q['name']) ? $q['name'] : '',
					'attr'		=>	' required',
				));
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|phone',true).'<span> *</span>',
					'name'		=>	'phone',
					'value'		=>	isset($q['phone']) ? $q['phone'] : '',
					'attr'		=>	' required mask_phone',
					'placeholder'		=>	' placeholder="+7 (_ _ _) _ _ _-_ _-_ _"',
				));?>
				<input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
				<input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
				<div class="list-group-item-text_checkbox">
					<label style="display: flex;margin: 0;justify-content: center;align-items: center;">
						<input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
						<span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
				</div>
			</div>
			<input type="hidden" name="width_calc" value="">
			<input type="hidden" name="length_calc" value="">
			<input type="hidden" name="storeys_calc" value="">
			<input type="hidden" name="сeiling_height_calc" value="">
			<input type="hidden" name="walls_calc" value="">
			<input type="hidden" name="foundation_calc" value="">
			<input type="hidden" name="roof_calc" value="">
			<input type="hidden" name="windows_calc" value="" >
			<input type="hidden" name="facade_calc" value="">
			<input type="hidden" name="communications_calc" value="">
			<input type="hidden" name="trim_calc" value="">

			<div class="modal-footer">
				<button type="submit" class=" btn-default btn_red button_agreement" title="Связаться со мной">Связаться со мной</button>
			</div>
		</form>
		<script type="text/javascript">
			document.addEventListener("DOMContentLoaded", function () {
				//$('#make_call form').submit(function(){
				$('body').on('submit','#make_call_calculator form', function(){
					var form = $(this),
						content_form = form.find('.modal-body'),
						message_block = form.find('.message_block');
					if (form.valid()) {
						form.find('button[type=submit]').text('Отправляем...');
						form.find('button[type=submit]').prop('disabled', true);
						form.ajaxSubmit({
							url:	'/<?=$modules['feedback']?>/?action=make_call_calculator',
							type: 	"POST",
							success:	function (data){
								form.find('button[type=submit]').prop('disabled', false);
								form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								if (data==1){
									$(content_form).html('<div class="h2"><?=i18n('feedback|message_is_sent')?></div>');
									$(form).find('.modal-footer').html('');
									yaCounter42381919.reachGoal('call_order_send');
									ga('send', 'event', 'form', 'send', 'modal_call');

								}
								else $(message_block).html(data);
							},
							error:	function(xhr,txt,err){
								form.find('button[type=submit]').prop('disabled', false);
								form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
							}
						});
						return false;
					}
				});
			});
		</script>
	</div>
</div>


<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		$('body').on('change','#calculator_form',function () {

			var input_width = $('input[name="width"]').val();
			$('input[name="width_calc"]').val(input_width);
			var input_length = $('input[name="length"]').val();
			$('input[name="length_calc"]').val(input_length);

			var select_storeys = $('select[name="storeys"]').val();
			$('input[name="storeys_calc"]').val(select_storeys);

			var select_сeiling_height = $('select[name="сeiling_height"]').val();
			$('input[name="сeiling_height_calc"]').val(select_сeiling_height);

			var select_walls = $('select[name="walls"]').val();
			$('input[name="walls_calc"]').val(select_walls);

			var select_foundation = $('select[name="foundation"]').val();
			$('input[name="foundation_calc"]').val(select_foundation);

			var select_roof = $('select[name="roof"]').val();
			$('input[name="roof_calc"]').val(select_roof);

			var select_windows = $('select[name="windows"]').val();
			$('input[name="windows_calc"]').val(select_windows);

			var select_facade = $('select[name="facade"]').val();
			$('input[name="facade_calc"]').val(select_facade);

			var select_communications = $('select[name="communications"]').val();
			$('input[name="communications_calc"]').val(select_communications);

			var select_trim = $('select[name="trim"]').val();
			$('input[name="trim_calc"]').val(select_trim);

		});


		$('body').on('change','#calculator_form',function () {
			//alert(777);
			//return false;
			var form = $(this),
				form_rezult = form.find('.result_cost');
			if (form.valid()) {
				form.ajaxSubmit({
					url:		'/<?=$modules['calculator']?>/',
					dataType: 'JSON',
					success:	function (data){
						if (data.done){
							form_rezult.html(data.html);
						}
						else form_rezult.html(data.message);
					},
					error:	function(xhr,txt,err){
						alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
					}
				});
				return false;
			}
		});
	});
</script>