<div class="form-section contract_aplication_wrap" style="background-image: url(/templates/images/bg_contract_aplication.jpg)">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-xs-12 col-lg-offset-2 col-xs-offset-0">
				<div class="contract_aplication">
					<?php
					/*
					 // http://workspace.abc-cms.com/proekty/stroidom93.ru/1/3/
					<div class="h1"><?=i18n('feedback|name_contract')?></div>
					   */
					?>
					<div class="h1">Оставьте свой E-mail и получите подборку квартир с ценами</div>
					<form method="post" class="form validate row">
						<div class="col-xs-12 flex_block">
							<?php
							echo html_array('form/input',array(
								//'caption'	=>	i18n('feedback|email',true).'<span> *</span>',
								'name'		=>	'email',
								'value'		=>	'',
								'placeholder'=> ' placeholder="Email" ',
								'attr'		=>	' required email',
							));
							echo html_array('form/button',array(
								'name'	=>	'Получить подборку',
								'class'	=>	'btn_red right button_agreement',
							));
							?>
						</div>
						<div class="clearfix"></div>
						<?php
						// конфиденцыальность

						/*
						 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="list-group-item-text_checkbox">
								<label style="display: flex;margin: 0;justify-content: center;align-items: center;">
									<input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
									<span style="max-width: 90%">я согласен/согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
							</div>
						</div>
						   */
						?>

						<div class="col-lg-12 col-xs-12 ">
							<input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
							<input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
							<div class="clearfix"></div>
						</div>
					</form>
					<div class="row">
						<div class="col-lg-12 col-xs-12 message_block">
							<?=isset($q['message']) ? html_array('form/message',$q['message']) : ''?>
						</div>
					</div>
					<script async>
						document.addEventListener("DOMContentLoaded", function () {
							$('body').on('submit','.contract_aplication form',function () {
								var form = $(this),
									form_wrap = $(this).parents('.form_wrap'),
									message_block = form_wrap.find('.message_block');
								if (form.valid()) {
									form.find('input[type=submit]').text('Отправляем...');
									form.find('input[type=submit]').prop('disabled', true);
									form.ajaxSubmit({
										url:	'/ajax.php?file=contract_application',
										type: 	"POST",
										success:	function (data){
											form.find('input[type=submit]').prop('disabled', false);
											form.find('input[type=submit]').text('<?=i18n('feedback|send')?>');
											if (data==1) {
												$(form).html('<?=i18n('feedback|contract_is_sent')?>');
											}
											else {
												$(message_block).html(data);
											}
										},
										error:	function(xhr,txt,err){
											form.find('input[type=submit]').prop('disabled', false);
											form.find('input[type=submit]').text('<?=i18n('feedback|send_contract')?>');
										}
									});
									return false;
								}
							});
						});
					</script>
				</div>
			</div>
		</div>
	</div>
</div>
