<div class="form-group form_multi_checkbox clearfix <?=@$q['class']?>">
	<?php
	if (isset($q['caption'])) {
		?>
	<label><?=$q['caption']?></label>
		<?php
	}
	?>
	<div class="data">
		<?php
		$data = is_array($q['data']) ? $q['data'] : mysql_select($q['data'],'array');
		$value = @$q['value']!='' ? explode(',',$q['value']) : array();
		foreach ($data as $k=>$v) {
			$checked = in_array($k,$value) ? ' checked="checked" ' : '';
			?>
			<div class="checkbox_wrap">
				<input id="multi_checkbox_<?=$q['name'].$k?>" name="" type="checkbox" value="<?=$k?>"<?=$checked?>/>
				<label for="multi_checkbox_<?=$q['name'].$k?>" class="checkbox-inline <?=$q['label_class']?>"><?=$v?></label>
			</div>
		<?php }?>
	</div>
	<input name="<?=$q['name']?>" type="hidden" value="<?=$q['value']?>">
</div>

