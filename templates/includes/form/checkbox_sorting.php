<div class="form-group checkbox form-checkbox <?=@$q['class']?>">
	<?php
	$checked = (isset($q['value']) AND $q['value']==1) ? ' checked="checked" ' : '';
	if (isset($q['caption'])) {
		?>
	<div class="label"><?=$q['caption']?></div>
		<?php
		}
	?>
	<div class="data ">
		<input id="checkbox_<?=$q['name']?>" name="<?=$q['name']?>" <?=isset($q['attr']) ? $q['attr'] : ''?> type="checkbox" value="1"<?=$checked?>/>
		<label for="checkbox_<?=$q['name']?>">
			<?=isset($q['units']) ? $q['units'] : ''?><span></span>
		</label>
	</div>
	<div class="clear"></div>
</div>


