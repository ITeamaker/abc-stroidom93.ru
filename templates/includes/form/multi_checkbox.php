<div class="form-group form_multi_checkbox clearfix <?=@$q['class']?>">
	<?php
	if (isset($q['caption'])) {
		?>
		<label><?=$q['caption']?></label>
		<?php
	}
	?>
	<div class="data ">
		<?php
		$data = is_array($q['data']) ? $q['data'] : mysql_select($q['data'],'array');
		$value = @$q['value']!='' ? explode(',',$q['value']) : array();
		foreach ($data as $k=>$v) {
			$checked = in_array($k,$value) ? ' checked="checked" ' : '';
			?>
			<div class="checkbox_wrap checkbox_new">
				<input id="multi_checkbox_<?=$q['name'].$k?>" name="" type="checkbox" value="<?=$k?>"<?=$checked?>/>
				<label for="multi_checkbox_<?=$q['name'].$k?>" class="checkbox-inline <?=$q['label_class']?>"><?=$v?></label>
			</div>
		<?php }
		if ($page['module']=='plots' /*|| $page['module']=='projects'*/){
			echo '<div class="checkbox_wrap"  style="text-align: right;">';
			echo html_array('form/button',array(
				'name' =>	i18n('shop|filter_button'),
				'class' => 'btn_red ',
			));
			echo '</div>';
		}
		?>
	</div>
	<input name="<?=$q['name']?>" type="hidden" value="<?=$q['value']?>">
</div>

