<?php

if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$img = $q['img'] ? '/files/plots/'.$q['id'].'/img/m-'.$q['img'] : '/'.$config['style'].'/images/no_img.svg';
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('plots',$q);
if ($i==1) {?>
	<div class="inner_container col-lg-12 col-xs-12" style="margin-bottom: 20px; margin-top: 40px;">
	<div class="row">
<?php } ?>
	<div class="shop_product_list col-xs-4 col-sm-4 col-md-4">
		<div class="shop_product_wrap">
			<div class="img">
				<div>
					<a href="<?=$url?>" title="<?=$title?>" style="background-image: url(<?=$img?>);">
                        <div class="names"></div>
                        <span class="btn_wrap">
                            <span class="btn_silver wrd_more"><?=i18n('common|wrd_more')?></span>
                        </span>
                    </a>
				</div>
			</div>
			<div class="product_param_wrap">
				<a class="name_link" href="<?=$url?>" title="<?=$title?>"><?=$q['name']?></a>
				<?php if ($q['price']!='') {?>
					<div class="data">
						<div class="name"><?=i18n('shop|price')?></div>
						<div class="value"><span><?=number_format($q['price'],0,',',' ')?></span> <?=i18n('shop|currency')?></div>
					</div>
				<?php }
                if ($q['area']!='') {?>
                    <div class="data">
                        <div class="name"><?=i18n('shop|areas')?></div>
                        <div class="value"><span><?=number_format($q['area'],1,',',' ')?></span> <?=i18n('shop|sq')?></div>
                    </div>
                <?php }
				if ($q['price2']!='') {?>
					<div class="data">
						<div class="name"><?=i18n('shop|price_for_sq')?></div>
						<div class="value"><span><?=number_format($q['price2'],0,',',' ')?></span> <?=i18n('shop|currency')?></div>
					</div>
				<?php }
				if ($q['name_region']!='') {?>
					<div class="data">
						<div class="name"><?=i18n('shop|name_region')?></div>
						<div class="value"><?=$q['name_region']?></div>
					</div>
				<?php }?>
			</div>
		</div>
	</div>
<?php
if (fmod($i,3)==0) echo '<div class="clearfix"></div>';
if ($i==$num_rows) echo '</div></div>';
?>