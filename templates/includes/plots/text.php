<?=html_sources('footer','fancybox.js')?>

<?php
$img = $q['img'] ? '/files/plots/'.$q['id'].'/img/l-'.$q['img'] : '/templates/images/no_img.svg';
$images = $q['imgs'] ? unserialize($q['imgs']) : false;
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);

?>
<div class="shop_product_text content">
	<div>
		<div class="product_gallery left designing_houses">
			<div class="product_img_list-for">
				<?php
				$list = $list2 = '';
				if ($q['img']) {?>
					<a class="fancybox_gallery" title="<?=$title?>" rel="gallery<?=$q['id']?>" href="/files/plots/<?=$q['id']?>/img/<?=$q['img']?>" >
						<div class="fancybox_gallery_items">
							<img src="<?=$img?>" alt="<?=filter_var(@$v['name'],FILTER_SANITIZE_STRING);?>"	title="<?=$title?>" />
							<span class="gallery-img-cover" style="background-image: url(<?=$img?>)"></span>
						</div>
					</a>
					<?php
					$list = '<div class="img_vertical_wrap"><img width="132" height="77" src="/files/plots/'.$q['id'].'/img/ss'.$q['img'].'" alt="'.$title.'" title="'.$title.'" /></div>';
				} ?>
				<?php if ($images) {
					$n=1;
					foreach ($images as $k=>$v) if (@$v['display']==1) {
						$n++;
						$alt2=filter_var(@$v['name'],FILTER_SANITIZE_STRING);
						$title2=filter_var(@$v['title'],FILTER_SANITIZE_STRING);
						$title2 = $title2 ? $title2 : $alt2;
						$path = '/files/plots/'.$q['id'].'/imgs/'.$k.'/';
						$list .= '<div class="img_vertical_wrap"><img width="132" height="77"	src="'.$path.'p-'.$v['file'].'" alt="'.$alt2.'" title="'.$title2.'"	/></div>';
						echo '<a	class="fancybox_gallery" title="'.$title2.'" rel="gallery'.$q['id'].'" href="'.$path.$v['file'].'" ><div class="fancybox_gallery_items"><img src="'.$path.'l-'.$v['file'].'" alt="'.$alt2.'" title="'.$title2.'" /><span class="gallery-img-cover" style="background-image: url('.$path.'l-'.$v['file'].')"></span></div></a>';
					}
				}?>
			</div>
			<div class="product_img_list-nav"><?=$list?></div>
		</div>
		<div class="product_options right projects">
			<div class="row">
				<div class="col-lg-12 col-xs-12 name_page">
					<h1><?=$q['name']?></h1>
				</div>
				<?php
								if ($q['price']>0){
										?>
										<div class="col-lg-12 col-xs-12">
												<div class="data" style="display: flex; justify-content: flex-start;">
														<div class="name"><?=i18n('shop|price')?>:&nbsp;</div>
														<div class="value">
																<span><?=number_format($q['price'],0,',',' ')?></span> <?=i18n('shop|currency')?>
														</div>
												</div>
										</div>
								<?php }
				if ($q['area']!=''){
					?>
					<div class="col-lg-12 col-xs-12">
						<div class="data" style="display: flex; justify-content: flex-start;">
							<div class="name"><?=i18n('shop|areas')?>:&nbsp;</div>
							<div class="value">
								<span><?=$q['area']?></span>
								<?=i18n('shop|sq')?>
							</div>
						</div>
					</div>
				<?php }
								if ($q['price2']>0){
										?>
										<div class="col-lg-12 col-xs-12">
												<div class="data" style="display: flex; justify-content: flex-start;">
														<div class="name"><?=i18n('shop|price_for_sq')?>:&nbsp;</div>
														<div class="value">
																<span><?=number_format($q['price2'],0,',',' ')?></span> <?=i18n('shop|currency')?>
														</div>
												</div>
										</div>
								<?php }
								if ($q['name_region']!=''){
										?>
										<div class="col-lg-12 col-xs-12">
												<div class="data" style="display: flex; justify-content: flex-start;">
														<div class="name"><?=i18n('shop|name_region')?>:&nbsp;</div>
														<div class="value">
																<?=$q['name_region']?>
														</div>
												</div>
										</div>
								<?php } ?>
				<div class="clearfix"></div>
				<?php
								/*
								?>
				<div class="col-lg-12 col-xs-12">
					<div class="button_wrap">
						<button type="button" class="btn_red btn-primary" data-toggle="modal" data-target="#order_project"><?=i18n('common|order_project')?></button>
					</div>
				</div>
								*/?>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<?php
	if ($q['text']!=''){
	?>
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="text" style="margin-bottom: 30px;"><?=$q['text']?></div>
		</div>
	</div>
	<?php }?>
		<?=html_array('plots/map',$q)?>
		<div class="product_btn">
				<div class="row">
						<div class="col-lg-12 col-xs-12">
								<div class="point"><div class="sprite point_horizontal"></div></div>
						</div>
						<div class="clearfix"></div>
						<div class="col-lg-12 col-xs-12">
								<div class="point_bottom">
										<button type="button" class="btn-consultation btn_margin" data-toggle="modal" data-target="#make_call" id="consult_zemlya">
												<?=i18n('common|order_consultation');?>
										</button>
								</div>
						</div>
				</div>
		</div>
</div>
<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		$('.product_img_list-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			prevArrow: '<div class="slick_button_wrap_prev"><button type="button" class="slick-prev">Previous</button></div>',
			nextArrow: '<div class="slick_button_wrap_next"><button type="button" class="slick-next">Next</button></div>',
			fade: true,
		});
		$('.product_img_list-nav').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			asNavFor: '.product_img_list-for',
			prevArrow: '<div class="slick_button_wrap_prev"><button type="button" class="slick-prev">Previous</button></div>',
			nextArrow: '<div class="slick_button_wrap_next"><button type="button" class="slick-next">Next</button></div>',
			dots: false,
			//centerMode: true,
			focusOnSelect: true,
			//vertical: true,
		});
		$(".fancybox_gallery").fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			padding: 0,
			helpers : {
				title : {
					type : 'over'
				}
			}
		});
		$(".fancybox_single").fancybox({
			openEffect	: 'elastic',
			closeEffect	: 'elastic',
			padding: 0,
			helpers : {
				title : {
					type : 'over'
				}
			}
		});
	});
</script>