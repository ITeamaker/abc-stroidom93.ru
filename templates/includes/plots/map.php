<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div class="map_wrap">
	<div id="yaMapAll" style="width:100%; height:100%;"></div>
</div>

<script type="text/javascript">
	document.addEventListener("DOMContentLoaded", function () {
		/* скрипт по работе с картами */
		var yaMap,placemark;
		// Дождёмся загрузки API и готовности DOM.
		if($("#yaMapAll").length){
			ymaps.ready(init);
		}
		function init () {
			yaMap = new ymaps.Map('yaMapAll', {
				// При инициализации карты обязательно нужно указать
				// её центр и коэффициент масштабирования.
				center: [<?=$q['lat']?>, <?=$q['lng']?>],
				zoom: 14,
//				controls: []
			});
			yaMap.setType('yandex#map');
			yaMap.behaviors.disable('scrollZoom');
			var placemark = new ymaps.Placemark([<?=$q['lat']?>, <?=$q['lng']?>], {}, {
				// Задаем стиль метки (метка в виде круга).
				preset: "islands#dotCircleIcon",
				// Задаем цвет метки (в формате RGB).
				iconColor: '#ed4543'
			});
			yaMap.geoObjects.add(placemark);
		}
	});
</script>
