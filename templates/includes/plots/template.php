<?php
//страница товара
if ($plots) {?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<?=$html['content']?>
			</div>
		</div>
	</div>
<?php 
//страница категории
}
else {
	?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<h1><?=$page['name']?></h1>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="container">
		<?=$html['filter']?>
	</div>
	<div class="container">
		<div class="row">
			<?=$html['list']?>
            <div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 text_block">
                <?=$page['text']?>
            </div>
            <div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
				<div class="point"><div class="sprite point_horizontal"></div></div>
			</div>
			<div class="clearfix"></div>
			<div class="col-lg-12 col-xs-12">
				<div class="point_bottom">
					<button type="button" class="btn-consultation btn_margin" data-toggle="modal" data-target="#make_call" id="consult_zemlya">
						<?=i18n('common|order_consultation');?>
					</button>
				</div>
			</div>
			<?php
			if ($page['text_seo_1'] != ''){?>
				<div class="col-lg-12 col-xs-12 text_block seo_text_top">
					<?=$page['text_seo_1']?>
				</div>
				<div class="clearfix"></div>
			<?php }?>
		</div>
	</div>
	<?php
}
?>