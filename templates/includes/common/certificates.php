<div class="container certificates">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
            <div class="sprite hr"></div>
			<div class="h1"><?=i18n('common|certificates',true)?></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<?=html_query('common/certificates_list',"
				SELECT *
				FROM certificates
				WHERE display=1
				ORDER BY rank ASC
				LIMIT 4
			",'');?>
		</div>
	</div>
</div>