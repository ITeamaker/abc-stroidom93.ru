<!DOCTYPE html>
<html class="main" data-lines="3" data-cart="/personal/cart/" lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="msvalidate.01" content="6DDF0F8B80C6CA9AC7F1AA85E4FA438E" />

	<title>Подарочные сертификаты в Москве от "PRESENTSTAR": самые необычные, оригинальные и запоминающиеся подарки для вас и ваших близких.</title>
	<meta name="description" content="Интернет-магазин подарочных сертификатов PRESENTSTAR. Всегда в наличии огромный выбор сертификатов на любой вкус и по любому поводу: мужчинам, женщинам, парням, девушкам, любимым, родителям; на свадьбу и на корпоративным праздник, на 23 февраля и 8 марта, на юбилей, годовщину, новый год. Бесплатная доставка и возможность дарить &quot;онлайн&quot;. Подарочный сертификат от PRESENTSTAR — незабываемые впечатления гарантированы!" />


	<meta name="yandex-verification" content="77ed98074347ce69" />

	<!--<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">-->
	<!--<meta http-equiv="Cache-Control" content="no-cache">-->
	<meta name="viewport" content="width=1000">
	<!--<meta name="viewport" content="width=1000, user-scalable=no">-->
	<!--<meta http-equiv="pragma" content="no-cache">-->
	<!--<meta name="format-detection" content="telephone=no">-->
	<meta name="format-detection" content="address=no">

	<meta property="og:title" content="Подарочные сертификаты в Москве от "PRESENTSTAR": самые необычные, оригинальные и запоминающиеся подарки для вас и ваших близких." />
	<meta property="og:url" content="https://www.presentstar.ru/" />
	<meta property="og:image" content="https://www.presentstar.ru/img/og_img.jpg" />
	<meta property="og:description" content="Интернет-магазин подарочных сертификатов PRESENTSTAR. Всегда в наличии огромный выбор сертификатов на любой вкус и по любому поводу: мужчинам, женщинам, парням, девушкам, любимым, родителям; на свадьбу и на корпоративным праздник, на 23 февраля и 8 марта, на юбилей, годовщину, новый год. Бесплатная доставка и возможность дарить &quot;онлайн&quot;. Подарочный сертификат от PRESENTSTAR — незабываемые впечатления гарантированы!" />



	<link rel="stylesheet" type="text/css" href="/css/style.css?b64f26b2165e95b6e7c577e2acb4fadd">
	<script src="/js/lib/kick_sovetnik.min.js"></script>
	<script src="/js/lib/modernizr.js"></script>
	<!--[if IE 9]>
	<link rel="stylesheet" type="text/css" href="/css/ie9-fix.css">
	<![endif]-->
</head>

<body>

<div class="container catalog-tag-page _initial js-catalog-tag-page">
	<div class="promoblock js-promoblock __loading">
		<div class="promoblock-i">
			<ul class="promoblock-list js-promoblock-list">

				<li class="promoblock-item js-promoblock-item">
					<a onclick="window.yaCounter.reachGoal('clickOnBannerOnMainPage'); return true;" href="/catalog/multipodarki-20-v-1/" class="promoblock-item-i">
						<div class="promoblock-item-image js-promoblock-item-image"><img src="https://www.presentstar.ru/tinify/w2000/upload/iblock/669/present_star2_2000x408_min_min.jpg?a0d29220" alt=""></div>
						<div class="promoblock-item-text">
							<span>Подарочные наборы</span><br>
							<strong>Яркие впечатления в стильной упаковке</strong>
						</div>
					</a>
				</li>


				<li class="promoblock-item js-promoblock-item">
					<a onclick="window.yaCounter.reachGoal('clickOnBannerOnMainPage'); return true;" href="https://www.presentstar.ru/catalog/extreme/" class="promoblock-item-i">
						<div class="promoblock-item-image js-promoblock-item-image"><img src="https://www.presentstar.ru/resize/w2000/upload/iblock/ce8/kater-crownline_min.jpg?4f5e0a54" alt=""></div>
						<div class="promoblock-item-text">
							<span>Экстрим</span><br>
							<strong>Экстремальные подарки на любой вкус</strong>
						</div>
					</a>
				</li>


				<li class="promoblock-item js-promoblock-item">
					<a onclick="window.yaCounter.reachGoal('clickOnBannerOnMainPage'); return true;" href="/catalog/podarochnye-nabory/" class="promoblock-item-i">
						<div class="promoblock-item-image js-promoblock-item-image"><img src="https://www.presentstar.ru/tinify/w2000/upload/iblock/f37/presentstar_2000x408_robo_min.jpg?eeedc46c" alt=""></div>
						<div class="promoblock-item-text">
							<span>При заказе от 3500р</span><br>
							<strong>Бесплатная доставка</strong>
						</div>
					</a>
				</li>


				<li class="promoblock-item js-promoblock-item">
					<a onclick="window.yaCounter.reachGoal('clickOnBannerOnMainPage'); return true;" href="/catalog/gifts/vysshiy-pilotazh-na-istrebitele-l-39-20-min/" class="promoblock-item-i">
						<div class="promoblock-item-image js-promoblock-item-image"><img src="https://www.presentstar.ru/resize/w2000/upload/iblock/6d2/111bigbluesky_min.jpg?8ee79001" alt=""></div>
						<div class="promoblock-item-text">
							<span>Почувствуйте себя пилотом</span><br>
							<strong>Высший пилотаж на истребителе L-39</strong>
						</div>
					</a>
				</li>


				<li class="promoblock-item js-promoblock-item">
					<a onclick="window.yaCounter.reachGoal('clickOnBannerOnMainPage'); return true;" href="/custom/" class="promoblock-item-i">
						<div class="promoblock-item-image js-promoblock-item-image"><img src="https://www.presentstar.ru/tinify/w2000/upload/iblock/852/soberi-podarok-sam-2_min.jpg?17719f4f" alt=""></div>
						<div class="promoblock-item-text">
							<span>Не нашли подходящий подарок?</span><br>
							<strong>Собери подарок сам!</strong>
						</div>
					</a>
				</li>

			</ul>
			<div class="promoblock-nav js-promoblock-nav">
				<div class="promoblock-arrow js-promoblock-arrow __up"></div>
				<div class="promoblock-arrow js-promoblock-arrow __down"></div>
			</div>
		</div>
	</div>
</div>

<div class="js-jivosite-url" data-url="/ajax/device-info.php"></div>

<script src="/js/lib/jquery.js"></script>
<script src="/js/script.js?b64f26b2165e95b6e7c577e2acb4fadd"></script>

</body>
</html>
