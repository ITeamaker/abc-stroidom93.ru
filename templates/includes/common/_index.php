<?php
if (access('user admin')) {
	echo html_query('common/slider', "
		SELECT *
		FROM slider
		WHERE display=1
		ORDER BY rank DESC
	", '');
	echo html_query('common/slider_phone', "
		SELECT *
		FROM slider
		WHERE display=1
		ORDER BY rank DESC
	", '');
}
else{

?>


<div class="header_images" >
	<div class="container_max">
		<div class="bootom_img_text">
			<div class="bootom_img_price">
				<div class="price"><?=i18n('common|txt_slider_price')?></div>
				<div class="under"><?=i18n('common|txt_slider_price_und')?></div>
			</div>
			<div class="bootom_img_link">
				<div class="count"><?=i18n('common|txt_slider_count')?></div>
				<div class="name"><?=i18n('common|txt_slider_count_name')?></div>
				<a href="/portfolio/" class="slider_btn" title="<?=i18n('common|txt_slider_count_link')?>"><?=i18n('common|txt_slider_count_link')?></a>
			</div>
		</div>
		<div class="row">
			<div class="slider_head col-lg-12 col-xs-12">
				<div class="text">
					<?=i18n('common|txt_slider')?>
				</div>
			</div>
			<div class="text_info_round"><?=i18n('common|txt_slider_round')?></div>
		</div>
	</div>
</div>
<?php } ?>
<div class="we_builds">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<div class="point"><div class="sprite point_horizontal"></div></div>
                <div class="sprite hr"></div>
				<h2><?=i18n('common|we_builds')?></h2>
			</div>
		</div>
		<div class="advantage_products row">
			<?=html_query('buildings/we_builds_list',"
			    SELECT *
			    FROM buildings
			    WHERE display=1 
			    ORDER BY rank DESC
			",'')?>
			<div class="col-lg-12 col-xs-12">
				<div class="point"><div class="sprite point_horizontal"></div></div>
			</div>
			<div class="col-lg-12 col-xs-12">
				<div class="point_bottom">
					<button type="button" class="btn-consultation" data-toggle="modal" data-target="#make_call" id="consult_glav">
						<?=i18n('common|order_consultation');?>
					</button>
				</div>
			</div>
			<?php
			if ($page['text_seo_1'] != ''){?>
				<div class="col-lg-12 col-xs-12 text_block seo_text">
                    <div class="sprite hr"></div>
					<?=$page['text_seo_1']?>
				</div>
				<div class="clearfix"></div>
			<?php }?>
		</div>
	</div>
</div>

<div class="header_content">
	<div class="container_menu">
		<h1><?=i18n('common|why_we')?></h1>
		<?=html_query('advantages/why_we_list',"
			SELECT *
			FROM advantages
			WHERE display=1 AND type=1
			ORDER BY rank DESC
		",'')?>
	</div>
</div>
<div class="services_wrap">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
                <div class="sprite hr"></div>
				<h1><?=i18n('common|our_services')?></h1>
			</div>
        </div>
            <?=i18n('common|services_list')?>
			<?php
            /*=html_query('common/services_list',"
				SELECT *
				FROM pages
				WHERE display=1 AND services=1
				ORDER BY rank DESC
			",'')*/?>
        <div class="row">
            <?=html_array('form/make_order',array('class'=>'second'))?>
			<?php
			if ($page['text_seo_2'] != ''){?>
				<div class="col-lg-12 col-xs-12 text_block seo_text">
                    <div class="sprite hr"></div>
					<?=$page['text_seo_2']?>
				</div>
				<div class="clearfix"></div>
			<?php }?>
		</div>
	</div>
</div>

<div class="ask_questions">
	<div class="container">
		<div class="row">
			<div class="ask_questions_info col-lg-12 col-xs-12">
				<p><?=i18n('common|ask_questions_txt')?></p>
				<a href="#" data-toggle="modal" data-target="#make_call" id="ask_questions_button"><?=i18n('common|ask_questions_btn')?></a>
			</div>
		</div>
	</div>
</div>
<div class="our_prices">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
                <div class="sprite hr"></div>
				<h1><?=i18n('common|our_prices')?></h1>
			</div>
			<?=html_query('shop/product_list_price',"
				SELECT sp.*, sb.url category_url
                FROM projects sp
                LEFT JOIN projects_categories sb ON sb.id=sp.category
                WHERE sp.display = 1 AND sb.display=1 AND sp.portfolio_index=1 AND sp.price>0 AND sp.price2>0 AND sp.price2>sp.price
                ORDER BY sp.price ASC
                LIMIT 3
			",'');?>
			<div class="col-lg-12 col-xs-12">
				<a href="#" title="<?=i18n('common|wrd_more_price')?>" class="btn_silver wrd_more"><?=i18n('common|wrd_more_price')?></a>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="point"><div class="sprite point_horizontal"></div></div>
		</div>
	</div>
</div>
<div class="portfolio">
	<div class="container">
		<div class="row">
            <?=html_query('projects/projects_slider_top','
                SELECT *
                FROM portfolio_slider
                WHERE display = 1
                ORDER BY rank DESC
            ','');
            ?>
			<?php
            /*=html_query('shop/product_list_portfolio',"
				SELECT p.*, pc.url category_url
				FROM projects p
				LEFT JOIN projects_categories pc ON pc.id=p.category
                WHERE pc.display = 1 AND p.display=1 AND p.portfolio_index=1
				ORDER BY p.price ASC
				LIMIT 6
			",''); */
			?>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="point"><div class="sprite point_horizontal"></div></div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<?=html_array('form/make_order')?>
	</div>
</div>
<div class="certificates_partners">
	<?=html_array('common/certificates')?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-xs-12">
				<div class="point"><div class="sprite point_vertical"></div></div>
			</div>
		</div>
	</div>
	<?=html_array('common/partners')?>
</div>

