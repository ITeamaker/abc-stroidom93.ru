<div class="container partners">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
            <div class="sprite hr"></div>
			<div class="h1"><?=i18n('common|partners',true)?></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-xs-12 banks_wrap">
			<?=html_query('common/banks_list',"
				SELECT *
				FROM banks
				WHERE display=1
				ORDER BY rank DESC
				LIMIT 4
			",'');?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="point"><div class="sprite point_vertical"></div></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1">
			<?=html_query('common/partners_list',"
				SELECT *
				FROM partners
				WHERE display=1
				ORDER BY rank DESC
			",'');?>
		</div>
	</div>
</div>