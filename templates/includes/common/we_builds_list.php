<?php
if ($config['multilingual']) $q['name'] = $q['name'.$lang['i']];
$url = get_url('buildings',$q);
?>
<div class="building_block col-lg-6 col-xs-6" style="background-image: url(/files/buildings/<?=$q['id']?>/img/p-<?=$q['img']?>);">
		<div class="name">
			<span><?=$q['name']?></span>
		</div>
		<div class="btn_wrap">
			<div class="text">
				<?=$q['text']?>
			</div>
			<a href="<?=$url?>" title="<?=i18n('common|wrd_more')?>" class="btn_silver wrd_more"><?=i18n('common|wrd_more')?></a>
		</div>
	</a>
</div>
<?php
if (fmod($i,2)==0) echo '<div class="clearfix"></div>';
?>