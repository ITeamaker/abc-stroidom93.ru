<?php
$office_type = isset($_COOKIE['office_type']) && $_COOKIE['office_type']>0 && $_COOKIE['office_type']<6 ? $_COOKIE['office_type'] : 3 ;
?>

<div class="logo_wrap" >
    <?php
    if ($page['module']=='index'){?>
        <span class="logo" title="<?=i18n('common|site_name')?>">
            <img src="/<?=$config['style']?>/images/logo_head.png" alt="<?=i18n('common|site_name')?>" title="<?=$page['name']?>" />

        </span>
		<div class="descr"><?=i18n('common|site_name')?></div>
    <?php }
    else{?>
        <a href="/" class="logo" title="<?=i18n('common|site_name')?>">
            <img src="/<?=$config['style']?>/images/logo_head.png" alt="<?=i18n('common|site_name')?>" title="<?=$page['name']?>" />

        </a>
		<div class="descr"><?=i18n('common|site_name')?></div>
    <?php }
    ?>
</div>
<?php
// http://workspace.abc-cms.com/proekty/stroidom93.ru/2/90/
?>
<div class="mobile_company_data" styl>
	<a href="/nashi-video/">
		<div class="mobile_btns_cam"></div>
	</a>
	<?php
	/*
	 	<a href="/calculator/">
		<div class="mobile_btns_calc"></div>
	</a>
	  */
	?>

</div>
<div class="company_data">
	<div class="company_data_wrap">
		<div class="social_styles">
			<?= i18n('common|social')?>
		</div>
	</div>

	<div class="company_data_wrap">
		<div class="phone_wrap company_data_item">
			<a href="<?=get_url('contacts')?>">
			<div class="img_wrap"></div>
			</a>
		</div>
		<div class="address_wrap company_data_item">
            <div class="img_wrap">
				<a href="<?=get_url('contacts')?>">
					<div class="sprite location"></div>
				</a>

            </div>
            <div class="address_item">
				<?php
				//http://workspace.abc-cms.com/proekty/stroidom93.ru/1/1/
				/*
				                  <span style="width: 155px;display: inline-block;">Выберите город:</span><span class="phone_data" style="margin-left: 10px;"><a href="tel:88005503016" title="8 (800) 550-30-16" style="font-weight: bold;">8 (800) 550-30-16</a></span>
				  */
				?>

				<div class="adress_list">
					<div class="filter_block_head ">
						<div class="address-wrap">
						<?php
						$address_style = '';
						foreach ($config['offices'] as $k=>$v){
							$link_phone = $k==5 ? '<a class="ya-phone-1" href="tel:'.i18n('common|phone'.$k.'_link').'">'.i18n('common|phone'.$k).'</a>' : ($k==4 ? '<a class="ya-phone-2" href="tel:'.i18n('common|phone'.$k.'_link').'">'.i18n('common|phone'.$k).'</a>' : '<a href="tel:'.i18n('common|phone'.$k.'_link').'">'.i18n('common|phone'.$k).'</a>');
							?>
							<div class="address address_block<?=$k?>" data-address="<?=$k?>" <?=$k==$office_type ? "style='display:block;'" : "style='display:none;'"?>>
								<span class="name_wrap">
									<span class="name" style="font-weight: bold;">
										<?=i18n('common|address_city'.$k)?>
									</span>
								</span>
								<div class="header_phones">
									<span style="margin-left: 10px;"><?=$link_phone?></span>
									<span class="phone_data" style="margin-left: 10px;"><a href="tel:89180012373" title="8-(918)-001-23-73" style="font-weight: bold;">8-(918)-001-23-73</a></span>

								</div>
							</div>
						<?php }?>
						</div>
<?php
/*
// филиалы
<div class="filter_parametrs_head" style="width: 180px; padding: 0 0 0 4px;">
							<?php
							foreach ($config['offices'] as $k=>$v) {
								$address_style = $office_type==$k ? 'style="display: none;"' : ($office_type==1 && $k==1 ? 'style="display: none;"' : '');
								if (i18n('common|phone'.$k)!='' && i18n('common|address_city'.$k)!=''){
									echo '<div class=" checkbox_sorting" data-address="'.$k.'" '.$address_style.' >'.i18n('common|address_city'.$k).'</div>';
								}
							}
							?>
						</div>

  */
?>

					</div>
				</div>
            </div>
        </div>
        <div class="email_wrap company_data_item" style="display: none!important;">
            <div class="img_wrap">
				<div class="sprite email_header"></div>
            </div>
            <span class="email_data"><a href="mailto:<?= i18n('common|email' . $office_type) ?>"><?= i18n('common|email' . $office_type) ?></a></span>
        </div>
        <div class="working_hours_wrap company_data_item">
            <div class="img_wrap">
				<a href="<?=get_url('contacts')?>">
					<div class="sprite working_hours"></div>
				</a>
            </div>
            <div class="working_days_data"><?=i18n('common|working_days_name')?> <?=i18n('common|working_hours'.$office_type)?></div>
        </div>
    </div>

</div>
<div class="feedback_wrap">
    <div class="feedback">
        <button type="button" class="btn_red_head btn-primary" data-toggle="modal" data-target="#make_call" id="header_button">
            <?=i18n('common|get_free_consultation')?>
        </button>
    </div>

	<div class="online_video">
		<a class="btn-primary btn_silver_head" href="/nashi-video/">
			Онлайн трансляция строящихся объектов
		</a>
	</div>

    <?php
	//http://workspace.abc-cms.com/proekty/stroidom93.ru/1/1/
	/*
<div class="calculator">
		<?php
		$calculator = mysql_select("SELECT url, name,module, id FROM pages WHERE module='calculator' AND display=1",'row',60*60);
		?>
		<a href="/<?=$modules['calculator']?>/" class=" btn-primary btn_silver_head" title="<?=$calculator['name']?>"><?=$calculator['name']?></a>
	</div>
	   */
	?>

</div>
<script async>
	document.addEventListener("DOMContentLoaded",function(){$('body').on('click','.filter_block_head .name',function(){if($(this).parents('.filter_block_head').hasClass('open_js')){$(this).parents('.filter_block_head').removeClass('open_js');$(this).removeClass('open')}
	else{$('.filter_block_head').removeClass('open_js');$(this).parents('.filter_block_head').addClass('open_js');$('.filter_block_head .name').removeClass('open');$(this).addClass('open')}});$('body').on('click','.filter_parametrs_head .checkbox_sorting',function(){var id_office=$(this).data('address'),address_wrap=$('.filter_block_head .address-wrap');address_wrap.find('.address').hide();address_wrap.find('.address_block'+id_office+'').show(); $(this).parents('.filter_block_head').removeClass('open_js');$('.filter_parametrs_head .checkbox_sorting').show();$('.filter_parametrs_head .checkbox_sorting[data-address='+id_office+']').hide();document.cookie="office_type="+id_office+"; path=/;"})})
</script>