<?php
$office_type = isset($_COOKIE['office_type']) && $_COOKIE['office_type']>0 && $_COOKIE['office_type']<6 ? $_COOKIE['office_type'] : 1 ;
?>

<div class="logo_wrap" >
    <?php
    if ($page['module']=='index'){?>
        <span class="logo" title="<?=i18n('common|site_name')?>">
            <img src="/<?=$config['style']?>/images/logo_head.png" alt="<?=i18n('common|site_name')?>" />
            <div><?=i18n('common|site_name')?></div>
        </span>
    <?php }
    else{?>
        <a href="/" class="logo" title="<?=i18n('common|site_name')?>">
            <img src="/<?=$config['style']?>/images/logo_head.png" alt="<?=i18n('common|site_name')?>" />
            <div><?=i18n('common|site_name')?></div>
        </a>
    <?php }
    ?>
</div>
<div class="company_data">
	<div class="company_data_wrap">
		<div class="phone_wrap company_data_item">
			<div class="img_wrap"></div>
		</div>
		<div class="address_wrap company_data_item">
			<div class="img_wrap">
				<img src="/templates/images/location.png">
			</div>
			<div class="address_item">
				<span style="width: 165px;display: inline-block;">Выберите город:</span><span class="phone_data"><a href="tel:88005503016" title="8 (800) 550-30-16" style="font-weight: bold;">8 (800) 550-30-16</a></span>
				<div class="adress_list">
					<div class="filter_block_head ">
						<div class="address-wrap">
							<div class="address" data-address="<?=$office_type?>">
								<span class="name_wrap"><span class="name" style="font-weight: bold;"><?=i18n('common|address_city'.$office_type)?></span></span><a href="tel:<?=i18n('common|phone'.$office_type.'_link')?>"><?=i18n('common|phone'.$office_type)?></a>
							</div>
						</div>
						<div class="filter_parametrs_head" style="width: 180px; padding: 0 0 0 4px;">
							<?php
							$address_style = '';
							foreach ($config['offices'] as $k=>$v){
								//for ($x=1; $x<=5; $x++){
								$address_style = $office_type==$k ? 'style="display: none;"' : ($office_type==1 && $k==1 ? 'style="display: none;"' : '');
								if (i18n('common|phone'.$k)!='' && i18n('common|address_city'.$k)!=''){
									echo '<div class=" checkbox_sorting" data-address="'.$k.'" '.$address_style.' >'.i18n('common|address_city'.$k).'</div>';
								}
							}
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="email_wrap company_data_item" style="display: none!important;">
			<div class="img_wrap">
				<img src="/templates/images/email.png">
			</div>
			<span class="email_data"><a href="mailto:<?= i18n('common|email' . $office_type) ?>"><?= i18n('common|email' . $office_type) ?></a></span>
		</div>
		<div class="working_hours_wrap company_data_item">
			<div class="img_wrap">
				<img src="/templates/images/working_hours.png">
			</div>
			<div class="working_days_data"><?=i18n('common|working_days_name')?> <?=i18n('common|working_hours'.$office_type)?></div>
		</div>
	</div>
</div>
<div class="feedback_wrap">
    <div class="feedback">
        <button type="button" class="btn_red_head btn-primary" data-toggle="modal" data-target="#make_call" id="header_button">
            <?=i18n('common|get_free_consultation')?>
        </button>
    </div>
    <div class="calculator">
        <?php
        $calculator = mysql_select("SELECT url, name,module, id FROM pages WHERE module='calculator' AND display=1",'row',60*60);
        ?>
        <a href="/<?=$modules['calculator']?>/" class=" btn-primary btn_silver_head" title="<?=$calculator['name']?>"><?=$calculator['name']?></a>
    </div>
</div>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        $('body').on('click','.filter_block_head .name',function () {
            if ($(this).parents('.filter_block_head').hasClass('open_js')){
                $(this).parents('.filter_block_head').removeClass('open_js');
                $(this).removeClass('open');
            }
            else{
                $('.filter_block_head').removeClass('open_js');
                $(this).parents('.filter_block_head').addClass('open_js');
                $('.filter_block_head .name').removeClass('open');
                $(this).addClass('open');
            }
        });
        $('body').on('click','.filter_parametrs_head .checkbox_sorting',function () {
            var id_office = $(this).data('address'),
                address_wrap = $('.filter_block_head .address-wrap'),
                email_wrap = $('.email_wrap .email_data'),
                working_hours_wrap = $('.working_hours_wrap .working_days_data');
            $.ajax({
                type: "GET",
                url: "/ajax.php",
                data: {
                    file: 'office_head',
                    id: id_office
                },
                dataType: 'JSON',
                success: function(data){
                    if (data.done) {
                        address_wrap.html(data.address_data);
                        email_wrap.html(data.email_data);
                        working_hours_wrap.html(data.working_hours_data);
                    }
                }
            });
            $(this).parents('.filter_block_head').removeClass('open_js');
            $('.filter_parametrs_head .checkbox_sorting').show();
            $('.filter_parametrs_head .checkbox_sorting[data-address='+id_office+']').hide();
            document.cookie = "office_type="+id_office+"; path=/;";
        });
    });
</script>