<?php
$title = htmlspecialchars($q['name']);

$img = '/files/slider/'.$q['id'].'/img/'.$q['img'];
$img_p = '/files/slider/'.$q['id'].'/img/p-'.$q['img'];
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
if ($i==1){?>
	<div class="slider_phone_wrap ">
		<div class="slider_phone">
			<?php }
			if ($q['img']!='') {
				if ($q['url'] != '') {
					?>
					<a class="img_wrap" title="<?= $title ?>" href="<?=$q['url']?>" style="background-image: url('<?= $img_p ?>');">
						<div class="promoblock-item-text">
							<?=$q['text']?>
						</div>
					</a>
				<?php
				} else {
					?>
					<div class="img_wrap" style="background-image: url('<?= $img_p ?>');">
						<div class="promoblock-item-text">
							<?=$q['text']?>
						</div>
					</div>
				<?php }
			}
			if ($i==$num_rows){?>
		</div>
		<script type="text/javascript">
			document.addEventListener("DOMContentLoaded", function () {
				$('.slider_phone').slick({
					infinite: true,
					speed: 500,
					fade: true,
					cssEase: 'linear',
					arrows: true,
					prevArrow: '<div class="slick_button_wrap_prev"><button type="button" class="slick-prev">Previous</button></div>',
					nextArrow: '<div class="slick_button_wrap_next"><button type="button" class="slick-next">Next</button></div>',
				});
			});
		</script>
	</div>
<?php }