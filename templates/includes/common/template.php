<?php
$page['title']			= isset($page['title']) ? filter_var($page['title'], FILTER_SANITIZE_STRING) : filter_var($page['name'],FILTER_SANITIZE_STRING);
$page['description']	= isset($page['description']) ? filter_var($page['description'], FILTER_SANITIZE_STRING) : $page['title'];
$page['keywords']		= isset($page['keywords']) ? filter_var($page['keywords'], FILTER_SANITIZE_STRING) : $page['title'];
?><!DOCTYPE html>
<html lang="<?=$lang['localization']?>">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<?php
if(isset($_GET['n']) && $_GET['n']>1 && intval($_GET['n'])){
	$page_number = intval($_GET['n']);
	$page['description'].= '. Страница № '.$page_number;
	$page['title'].= ' Страница № '.$page_number;
} ?>
<title><?=$page['title']?></title>
<meta name="description" content="<?=$page['description']?>" />
<meta name="keywords" content="<?=$page['keywords']?>" />
<meta name="yandex-verification" content="8ba539d7616216ee" />
<link rel="icon" sizes="16x16" href="/templates/images/favicon.ico"/>
<script async type="text/javascript" src="/templates/scripts/modernizr.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1:1, maximum-scale=1, minimal-ui">
	

	<?php
// <meta name="viewport" content="width=device-width, initial-scale=1">

//v1.1.15 - запрет индексации на тестовом
/*if(strripos($_SERVER['HTTP_HOST'], '.abc-cms.com') OR strripos($_SERVER['HTTP_HOST'], '.tyt.kz') OR $page['module']=='finished_objects') {?>
	<meta name="robots" content="noindex, follow" />
<?php }*/
$config['http'] = 'https';
$config['domain'] = $_SERVER['HTTP_HOST'];
$config['http_domain'] = $config['http'].'://'.$config['domain'];
//v1.2.31 - canonical,next,prev
if (@$page['canonical'] AND $page['canonical']==$_SERVER['REQUEST_URI']) unset($page['canonical']);
foreach (array('canonical','next','prev') as $k=>$v) {
	if (@$page[$v]) echo '<link rel="'.$v.'" href="'.$config['http_domain'].$page[$v].'">';
}
?>
	<style>
		<?=file_get_contents(ROOT_DIR.'templates/css/critical/main.min.css')?>
	</style>
	<?=html_sources('head')?>
</head>

<body>
<script>
	setTimeout(function(){
		<?=i18n('common|script_body_start')?>
	}, 3000);
</script>

<?php
$office_type = isset($_COOKIE['office_type']) && $_COOKIE['office_type']>0 && $_COOKIE['office_type']<6 ? $_COOKIE['office_type'] : 3 ;
?>
<div id="body">
	<?=html_array('common/modals')?>
		<div class="header_new">
			<div class="container header_wrap">
				<?=html_array('common/head')?>
				<button type="button" class="navbar-toggle collapsed visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
		</div>
		<div class="header_menu fix navbar-collapse">
			<?php
			$mob_link_class = $office_type==5 ? ' ya-phone-1 ' : ($office_type==4 ? ' ya-phone-2 ' : '');
			?>
<?php
/*
 <div class="phone_fix_wrap">
				<div class="phone_fix">
					<div class="city_name">
						<div class="address" data-address="<?=$office_type?>"><span class="name">г.<?=$config['offices'][$office_type]?></span> <a class="<?=$mob_link_class?>" href="tel:<?=i18n('common|phone'.$office_type.'_link')?>"><?=i18n('common|phone'.$office_type)?></a><span class="arrows"></span>
						</div>
					</div>
					<div class="city_name_filter">
						<?php
						$address_style = '';
						foreach ($config['offices'] as $k=>$v){
							//for ($x=1; $x<=5; $x++){
							$address_style = $office_type==$k ? 'style="display: none;"' : ($office_type==3 && $k==3 ? 'style="display: none;"' : '');
							$mob_link_class = $k==5 ? ' ya-phone-1 ' : ($k==4 ? ' ya-phone-2 ' : '');
							if (i18n('common|phone'.$k)!='' && i18n('common|address_city'.$k)!=''){
								echo '<div class=" checkbox_sorting" data-address="'.$k.'" '.$address_style.' >г.'.$config['offices'][$k].' <a class="'.$mob_link_class.'" href="tel:'.i18n('common|phone'.$office_type.'_link').'">'.i18n('common|phone'.$office_type).'</a></div>';
							}
						}
						?>
					</div>
				</div>
				<script type="text/javascript">
					document.addEventListener("DOMContentLoaded", function () {
						$('body').on('click','.phone_fix .city_name span.arrows',function () {
							if ($(this).parents('.phone_fix').hasClass('open')){
								$(this).parents('.phone_fix').removeClass('open');
								$('.phone_fix .city_name_filter').hide();
							}
							else{
								$('.phone_fix').removeClass('open');
								$(this).parents('.phone_fix').addClass('open');
								$('.phone_fix .city_name_filter').show();
							}
						});
						$('body').on('click','.navbar-toggle', function () {
							$('.phone_fix').removeClass('open');
							$('.phone_fix .city_name_filter').hide();
						})
					});
				</script>
			</div>

	*/
?>

			<div class="container">
				<div class="row">
					<div class=" col-lg-12 col-xs-12">
						<button type="button" class="navbar-toggle collapsed visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="head">
							<a href="tel:+79189157211" class="coll">
								<div class="sprite phone"></div>
							</a>
							<?php
								// <a href="/calculator/" class="calc">Калькулятор</a>
							?>

							<a href="#" class="close" data-toggle="collapse" data-target=".navbar-collapse"><img src="/templates/images/close.jpg" title="<?=$page['name']?>" alt="<?=$page['name']?>"></a>
						</div>

							<?=html_query('menu/list',"
								SELECT name, name_site, url, module, level, decoration
								FROM pages
								WHERE display=1 AND level < 3 AND menu = 1
								ORDER BY left_key
							",'')/**/?>

					</div>
				</div>
			</div>
		</div>
		<div class="header_menu">
			<div class="container">
				<div class="row">
					<div class=" col-lg-12 col-xs-12">
						<?/*=html_query('menu/list',"
							SELECT name, name_site, url, module, level, decoration
							FROM pages
							WHERE display=1 AND level < 3 AND menu = 1
							ORDER BY left_key
						",'')/**/?>
						<?=html_array('menu/list_new')?>
					</div>
				</div>
			</div>
		</div>

		<?php
		if ($u[1] != $modules['investments']){
			if (isset($breadcrumb)) echo html_array('common/breadcrumb',$breadcrumb);
		}

		?>
		<?php
		if ($html['module']=='index') include(ROOT_DIR.$config['style'].'/includes/common/index.php');
		else {
			if (file_exists(ROOT_DIR.$config['style'].'/includes/'.$html['module'].'/template.php')) include(ROOT_DIR.$config['style'].'/includes/'.$html['module'].'/template.php');
			elseif($html['module']=='basket') echo $html['content'];
			else {
				?>
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-xs-12 ">
							<h1><?=$page['name']?></h1>
							<div class="text">
								<?=$html['content']?>
							</div>
						</div>
					</div>
				</div>
			<?php
			}
		}?>
</div>
<?php
$footer_class = (
		$page['module']== 'finished_objects'
		|| $page['module']== 'designing_houses'
		|| $page['module']== 'projects'
		//|| $page['module']== 'projects'
) ? 'margin_t_0' : '';
?>
<div id="footer" class="<?=$footer_class?>">
	<div class="container">
		<div class="footer_row">
			<div class="footer_item logo_wrap">
				<?php
				//print_r($page);
				if ($page['module']=='index'){?>
					<span class="logo" title="<?=i18n('common|site_name')?>">
						<img src="/<?=$config['style']?>/images/logo_footer.png" alt="<?=i18n('common|site_name')?>" title="<?=$page['name']?>" />
						<div style="text-align: center; margin-top: 10px; width: 168px;"><?=i18n('common|site_name')?></div>
					</span>
				<?php }
				else{?>
					<a href="/" class="logo" title="<?=i18n('common|site_name')?>">
						<img src="/<?=$config['style']?>/images/logo_footer.png" alt="<?=i18n('common|site_name')?>" title="<?=$page['name']?>" />
						<div style="text-align: center; margin-top: 10px; width: 168px;"><?=i18n('common|site_name')?></div>
					</a>
				<?php }
				?>

			</div>
			<div class="footer_item company_data_item">
				<div class="company_data_wrap">
					<div class="social_styles">
						<?= i18n('common|social')?>
					</div>
				</div>
				<div class="footer_phone">
					<div class="img_wrap">
						<div class="sprite location"></div>
					</div>
					<div class="address_item" >
						<a href="tel:89180012373" title="8-(918)-001-23-73" style="margin-left: 0;">8-(918)-001-23-73</a>
						<?php
						foreach ($config['offices'] as $k=>$v) {
							?>
							<div class="address" style="		display: flex;
		flex-direction: column-reverse;"><?= i18n('common|address'.$k) ?>
								<a class="<?=$k==5 ? ' ya-phone-1 ' : ($k==4 ? ' ya-phone-2 ' : '')?>" href="tel:<?= i18n('common|phone'.$k.'_link') ?>"><?= i18n('common|phone'.$k) ?></a></div>
							<?php
						}?>
					</div>

				</div>
			</div>

			<div class="footer_item feedback_wrap">
				<div class="button_wrap ">
					<button type="button" class="btn_red_head btn-primary" data-toggle="modal" data-target="#make_call" id="footer_button">
						<?=i18n('common|make_call2')?>
					</button>
				</div>
			</div>
			<?php /*
						<div class="footer_item social_wrap"><?=i18n('common|social')?></div>
			*/?>
		</div>
		<div class="footer_row">
			<div class="menu_footer_wrap">
				<div class="menu_footer">
					<div class="menu_footer_item">
						<?=html_query('menu/list2',"
							SELECT name,name_site,url,module, '1' as type
							FROM pages
							WHERE display=1 AND menu2 = 1 
							ORDER BY left_key
						",'',60*60,'json')?>
					</div>
					<div class="menu_footer_item">
						<?=html_query('menu/list2',"
							SELECT *, '2' as `type`
							FROM buildings 
							WHERE display=1 
							ORDER BY rank DESC
						",'',60*60,'json')?>
						<?=html_query('menu/list2',"
							SELECT `name`,name_site,url,`module`, '5' as `type`
							FROM pages
							WHERE display=1 AND menu3 = 1 
							ORDER BY left_key
						",'',60*60,'json')?>
					</div>
					<div class="menu_footer_item">
						<?=html_query('menu/list2',"
							SELECT name,name_site,url,module, '3' as type
							FROM pages
							WHERE display=1 AND menu4 = 1 
							ORDER BY left_key
						",'',60*60,'json')?>
					</div>
					<div class="menu_footer_item">
						<?=html_query('menu/list2',"
							SELECT name,name_site,url,module, '4' as type
							FROM pages
							WHERE display=1 AND menu5 = 1 
							ORDER BY left_key
						",'',60*60,'json')?>
					</div>
				</div>
			</div>
		</div>
		<div class="txt_footer">
			<?=i18n('common|txt_footer',array('Y'=>date('Y')))?>
		</div>
	</div>
</div>
<div class="finished_wrap">
	<div class="modal make_finished fade" id="make_finished" >
		<div class="modal-dialog modal-lg" style="max-width: 500px;">
			<form class="modal-content validate" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				</div>
				<div class="modal-body selection-form">
					<div class="selection-form__title">
						Введите номер телефона
						<?//=i18n('common|objects_get_info')?>
					</div>
					<?php
					/*
					// http://workspace.abc-cms.com/proekty/stroidom93.ru/2/46/
					<div class="row">
						<div class="col-lg-12 col-sm-12 col-xs-12 col-lg-offset-3 col-xs-offset-0 col-sm-offset-2 plashka" style="margin-left: 0">
							В течение двух минут мы перезвоним и предоставим информацию о наших домах.
						</div>
					</div>
						 */
					?>

					<div class="row ">
						<?php
						/*
						 						<div class="col-lg-4 col-sm-6 col-xs-12">
							<?=html_array('form/input',array(
								'caption'	=>	'Имя<span> *</span>',
								'name'		=>	'name',
								'value'		=>	'',
								'attr'		=>	' required',
							));?>
						</div>
						<div class="col-lg-4 col-sm-6 col-xs-12">
							<?=html_array('form/input',array(
								'caption'	=>	'E-mail<span> *</span>',
								'name'		=>	'email',
								'value'		=>	'',
								'attr'		=>	' required email',
							));?>
						</div>

							 */
						?>
						<div class="col-lg-12 col-sm-12 col-xs-12 col-lg-offset-0 col-xs-offset-0 col-sm-offset-3">
							<?=html_array('form/input',array(
								//'caption'	=>	'Телефон<span> *</span>',
								'name'		=>	'phone',
								'value'		=>	'',
								'attr'		=>	' required mask_phone',
								'placeholder'		=>	' placeholder="+7 (_ _ _) _ _ _-_ _-_ _"',
							));?>
						</div>
						<?php
						if($page['module']=='index' OR $page['module']=='finished_objects'){
							$page['url'] = 'podbor-kvartir';
							$q['id'] = 1;
							?>
							<input name="chose_type" type="hidden" value="1" />
						<?}else{?>
							<input name="chose_type" type="hidden" value="<?=$q['id']?>" />
						<? 	} ?>
					</div>

					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 message_block"></div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m_b_50">
							<div class="list-group-item-text_checkbox">
								<label style="display: flex;margin: 0;justify-content: center;align-items: center;">
									<input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
									<input type="hidden" name="count" value="7">

									<span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 25px">
							<input type="submit" class="btn selection-form__btn button_agreement" value=" Получить подборку<?//=@$page['form_name_btn']!=''?$page['form_name_btn']:i18n('common|objects_get')?>" />
						</div>
					</div>
				</div>
			</form>
			<script async type="text/javascript">
				document.addEventListener("DOMContentLoaded",function(){$('body').on('submit','#make_finished form',function(){var form=$(this),content_form=form.find('.modal-body'),message_block=form.find('.message_block');if(form.valid()){var $finish='';$('.selection-form.parameters_js input').each(function(){var $name=$(this).attr("name");var $val=$(this).val();if($val!=''&&$name!=''){$finish=$finish+'&'+$name+'='+$val}});form.find('input[type=submit]').val('Отправляем...');form.find('input[type=submit]').prop('disabled',!0);form.ajaxSubmit({url:'/<?=$modules['finished_objects']?>/<?=$page['url']?>?modal'+$finish,type:"POST",success:function(data){form.find('input[type=submit]').prop('disabled',!1);form.find('input[type=submit]').val('<?=i18n('feedback|send')?>');if(data==1){$(content_form).html('<div class="h2"><?=i18n('feedback|message_is_sent')?></div><div class="col-lg-4 col-md-6 col-sm-8 col-xs-12 col-lg-offset-4 col-md-offset-3 col-xs-offset-0 col-sm-offset-2" ><a class="btn " href="#" data-dismiss="modal" aria-hidden="true">Спасибо!</a></div>')}
				else $(message_block).html(data)}});return!1}})})
			</script>
		</div>
	</div>
</div>



<?//=html_sources('return','bootstrap.css slick.css nouislider.css fancybox.css common.css newslider.css')?>
<?//=html_sources('return','jquery.js bootstrap.js mask.js nouislider.js newslider.js')?>
<link href="/common.css.php?lang=<?=$lang['localization']?>" rel="stylesheet" type="text/css">

<script type="text/javascript" src="/common.js.php?lang=<?=$lang['localization']?>"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-beta.2/lazyload.js"></script>
<script>
	//document.addEventListener("DOMContentLoaded", function () {
	window.addEventListener('load', function(){
		$("img.lazyload").lazyload();
	});
</script>
<?php
/*
<link href="/templates/css/newslider.css?1" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/templates/scripts/newslider.js?2"></script>

 */
?>
<?php
//callbackkiller

/*
http://workspace.abc-cms.com/proekty/stroidom93.ru/2/65/
<link rel="stylesheet" href="https://cdn.envybox.io/widget/cbk.css">
<script type="text/javascript" src="https://cdn.envybox.io/widget/cbk.js?wcb_code=c8b4b650a4de7f023a784ef1567f1ba2" charset="UTF-8" async></script>
 */
?>

<?=access('editable scripts') ? html_sources('footer','tinymce.js editable.js') : ''?>
<?=html_sources('footer')?>
<?//=html_sources('return','common.js')?>
<script>
	setTimeout(function(){
		<?=i18n('common|script_body_end')?>
	}, 3000);
</script>

<noscript><div><img src="https://mc.yandex.ru/watch/42381919"; style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<?php
/*
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'f8gfWqFnFy';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->

 */
?>

</body>
</html>