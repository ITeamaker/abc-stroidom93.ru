<?php
$page['title']			= isset($page['title']) ? filter_var($page['title'], FILTER_SANITIZE_STRING) : filter_var($page['name'],FILTER_SANITIZE_STRING);
$page['description']	= isset($page['description']) ? filter_var($page['description'], FILTER_SANITIZE_STRING) : $page['title'];
$page['keywords']		= isset($page['keywords']) ? filter_var($page['keywords'], FILTER_SANITIZE_STRING) : $page['title'];
?><!DOCTYPE html>
<html lang="<?=$lang['localization']?>">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title><?=$page['title']?></title>
	<?php /*
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,700,900&subset=cyrillic-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&subset=cyrillic" rel="stylesheet">*/?>
<meta name="description" content="<?=$page['description']?>" />
<meta name="keywords" content="<?=$page['keywords']?>" />
<meta name="yandex-verification" content="8ba539d7616216ee" />
<link rel="icon" sizes="16x16" href="/templates/images/favicon.ico"/>
<meta name="viewport" content="width=device-width, initial-scale=1:1, maximum-scale=1, minimal-ui">
	<script async type="text/javascript" src="/templates/scripts/modernizr.js"></script>
<?php
// <meta name="viewport" content="width=device-width, initial-scale=1">

//v1.1.15 - запрет индексации на тестовом
if(strripos($_SERVER['HTTP_HOST'], '.abc-cms.com') OR strripos($_SERVER['HTTP_HOST'], '.tyt.kz')) {?>
	<meta name="robots" content="noindex, follow" />
<?php }
//v1.2.31 - canonical,next,prev
if (@$page['canonical'] AND $page['canonical']==$_SERVER['REQUEST_URI']) unset($page['canonical']);
foreach (array('canonical','next','prev') as $k=>$v) {
		if (@$page[$v]) echo '<link rel="' . $v . '" href="https://' . $_SERVER['HTTP_HOST'] . $page[$v] . '" />';
}
?>
	<style>
		<?=file_get_contents(ROOT_DIR.'templates/css/critical/main.min.css')?>
	</style>
	<?=html_sources('head')?>
</head>

<body>
<?=i18n('common|script_body_start')?>
<?php
$office_type = isset($_COOKIE['office_type']) && $_COOKIE['office_type']>0 && $_COOKIE['office_type']<6 ? $_COOKIE['office_type'] : 1 ;
?>
<div id="body">
	<?=html_array('common/modals')?>
		<div class="header_new">
			<div class="container header_wrap">
				<?=html_array('common/head')?>
				<button type="button" class="navbar-toggle collapsed visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
		</div>
		<div class="header_menu fix navbar-collapse">
			<div class="phone_fix_wrap">
				<div class="phone_fix">
					<div class="city_name">
						<div class="address" data-address="<?=$office_type?>"><span class="name">г.<?=$config['offices'][$office_type]?></span> <a href="tel:<?=i18n('common|phone'.$office_type.'_link')?>"><?=i18n('common|phone'.$office_type)?></a><span class="arrows"></span>
						</div>
					</div>
					<div class="city_name_filter">
						<?php
						$address_style = '';
						foreach ($config['offices'] as $k=>$v){
							//for ($x=1; $x<=5; $x++){
							$address_style = $office_type==$k ? 'style="display: none;"' : ($office_type==1 && $k==1 ? 'style="display: none;"' : '');
							if (i18n('common|phone'.$k)!='' && i18n('common|address_city'.$k)!=''){
								echo '<div class=" checkbox_sorting" data-address="'.$k.'" '.$address_style.' >г.'.$config['offices'][$k].' <a class="'.($k==5 ? ' ya-phone-1 ' : '').'" href="tel:'.i18n('common|phone'.$office_type.'_link').'">'.i18n('common|phone'.$office_type).'</a></div>';
							}
						}
						?>
					</div>
				</div>
				<script type="text/javascript">
					document.addEventListener("DOMContentLoaded", function () {
						$('body').on('click','.phone_fix .city_name span.arrows',function () {
							if ($(this).parents('.phone_fix').hasClass('open')){
								$(this).parents('.phone_fix').removeClass('open');
								$('.phone_fix .city_name_filter').hide();
							}
							else{
								$('.phone_fix').removeClass('open');
								$(this).parents('.phone_fix').addClass('open');
								$('.phone_fix .city_name_filter').show();
							}
						});
						$('body').on('click','.navbar-toggle', function () {
							$('.phone_fix').removeClass('open');
							$('.phone_fix .city_name_filter').hide();
						})
					});
				</script>
			</div>
			<div class="container">
				<div class="row">
					<div class=" col-lg-12 col-xs-12">

						<button type="button" class="navbar-toggle collapsed visible-xs" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<div class="head">
							<a href="tel:+79189157211" class="coll"><img src="/templates/images/phone.png"></a>
							<a href="/calculator/" class="calc">Калькулятор</a>
							<a href="#" class="close" data-toggle="collapse" data-target=".navbar-collapse"><img src="/templates/images/close.jpg"></a>
						</div>
							<?=html_query('menu/list',"
								SELECT name, name_site, url, module, level, decoration
								FROM pages
								WHERE display=1 AND level < 3 AND menu = 1
								ORDER BY left_key
							",'')?>
					</div>
				</div>
			</div>
		</div>
		<div class="header_menu">
			<div class="container">
				<div class="row">
					<div class=" col-lg-12 col-xs-12">
						<?=html_query('menu/list',"
							SELECT name, name_site, url, module, level, decoration
							FROM pages
							WHERE display=1 AND level < 3 AND menu = 1
							ORDER BY left_key
						",'')?>
					</div>
				</div>
			</div>
		</div>
		<?php if (isset($breadcrumb)) echo html_array('common/breadcrumb',$breadcrumb);?>
		<?php
		if ($html['module']=='index') include(ROOT_DIR.$config['style'].'/includes/common/index.php');
		else {
			if (file_exists(ROOT_DIR.$config['style'].'/includes/'.$html['module'].'/template.php')) include(ROOT_DIR.$config['style'].'/includes/'.$html['module'].'/template.php');
			elseif($html['module']=='basket') echo $html['content'];
			else {
				?>
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-xs-12 ">
							<h1><?=$page['name']?></h1>
							<div class="text">
								<?=$html['content']?>
							</div>
						</div>
					</div>
				</div>
			<?php
			}
		}?>
</div>
<div id="footer" class="<?=$page['module']== 'finished_objects' && $u[2]!='' ? 'margin_t_0' : ''?>">
	<div class="container">
		<div class="footer_row">
			<div class="footer_item logo_wrap">
				<?php
				//print_r($page);
				if ($page['module']=='index'){?>
					<span class="logo" title="<?=i18n('common|site_name')?>">
						<img src="/<?=$config['style']?>/images/logo_footer.png" alt="<?=i18n('common|site_name')?>" />
						<div style="text-align: center; margin-top: 10px; width: 168px;"><?=i18n('common|site_name')?></div>
					</span>
				<?php }
				else{?>
					<a href="/" class="logo" title="<?=i18n('common|site_name')?>">
						<img src="/<?=$config['style']?>/images/logo_footer.png" alt="<?=i18n('common|site_name')?>" />
						<div style="text-align: center; margin-top: 10px; width: 168px;"><?=i18n('common|site_name')?></div>
					</a>
				<?php }
				?>

			</div>
			<div class="footer_item company_data_item">
				<div class="img_wrap">
					<img src="/templates/images/location.png">
				</div>
				<div class="address_item">
					<a href="tel:88005503016" title="8 (800) 550-30-16" style="margin-left: 0;">8 (800) 550-30-16</a>
					<?php
					foreach ($config['offices'] as $k=>$v) {
						?>
						<div class="address"><?= i18n('common|address'.$k) ?> <a
									href="tel:<?= i18n('common|phone'.$k.'_link') ?>"><?= i18n('common|phone'.$k) ?></a></div>
						<?php
					}?>
				</div>
			</div>
			<div class="footer_item feedback_wrap">
				<div class="button_wrap ">
					<button type="button" class="btn_red_head btn-primary" data-toggle="modal" data-target="#make_call" id="footer_button">
						<?=i18n('common|make_call2')?>
					</button>
				</div>
			</div>
			<?php /*
						<div class="footer_item social_wrap"><?=i18n('common|social')?></div>
			*/?>
				</div>
				<div class="footer_row">
						<div class="menu_footer_wrap">
								<div class="menu_footer">
										<div class="menu_footer_item">
												<?=html_query('menu/list2',"
														SELECT name,name_site,url,module, '1' as type
														FROM pages
														WHERE display=1 AND menu2 = 1 
														ORDER BY left_key
												",'',60*60,'json')?>
										</div>
										<div class="menu_footer_item">
											<?=html_query('menu/list2',"
														SELECT *, '2' as `type`
														FROM buildings 
														WHERE display=1 
														ORDER BY rank DESC
												",'',60*60,'json')?>
										<?=html_query('menu/list2',"
														SELECT `name`,name_site,url,`module`, '5' as `type`
														FROM pages
														WHERE display=1 AND menu3 = 1 
														ORDER BY left_key
												",'',60*60,'json')?>
										</div>
										<div class="menu_footer_item">
										<?=html_query('menu/list2',"
														SELECT name,name_site,url,module, '3' as type
														FROM pages
														WHERE display=1 AND menu4 = 1 
														ORDER BY left_key
												",'',60*60,'json')?>
										</div>
										<div class="menu_footer_item">
										<?=html_query('menu/list2',"
														SELECT name,name_site,url,module, '4' as type
														FROM pages
														WHERE display=1 AND menu5 = 1 
														ORDER BY left_key
												",'',60*60,'json')?>
					</div>
				</div>
			</div>
		</div>
		<div class="txt_footer">
			<?=i18n('common|txt_footer',array('Y'=>date('Y')))?>
		</div>
	</div>
</div>

<?=html_sources('return','bootstrap.css slick.css nouislider.css fancybox.css common.css')?>
<?=html_sources('return','jquery.js bootstrap.js slick.js mask.js nouislider.js')?>

<link href="/templates/css/newslider.css?12" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/templates/scripts/newslider.js?236"></script>

<script async src="https://use.fontawesome.com/04a7aa3c3a.js"></script>
<?=access('editable scripts') ? html_sources('footer','tinymce.js editable.js') : ''?>
<?=html_sources('footer')?>
<?=html_sources('return','common.js')?>
<?=i18n('common|script_body_end')?>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'f8gfWqFnFy';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</body>
</html>