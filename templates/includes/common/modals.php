<?//=html_sources('footer','jquery_validate.js jquery_form.js')?>


<?php
// хочу инвестировать выгодно
?>
<div class="modal make_call fade" id="make_call" >
	<div class="modal-dialog">
		<form class="modal-content validate" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<div class="h1 fix_h1"><?=i18n('common|make_call')?></div>
				<div class="message_block"></div>
				<?php
				/*
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|name',true).'<span> *</span>',
					'name'		=>	'name',
					'value'		=>	isset($q['name']) ? $q['name'] : '',
					'attr'		=>	' required',
				));
				/**/
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|phone',true).'<span> *</span>',
					'name'		=>	'phone',
					'value'		=>	isset($q['phone']) ? $q['phone'] : '',
					'attr'		=>	' required mask_phone',
					'placeholder'		=>	' placeholder="+7 (_ _ _) _ _ _-_ _-_ _"',
				));?>
                <input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                <input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
                <div class="list-group-item-text_checkbox">
                    <label style="display: flex;margin: 0;justify-content: center;align-items: center;">
                        <input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
                        <span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
                </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class=" btn-default btn_red button_agreement" title="<?=i18n('feedback|send')?>">Перезвоните мне</button>
			</div>
		</form>
		<script async >
			document.addEventListener("DOMContentLoaded", function () {
				//$('#make_call form').submit(function(){
				$('body').on('submit','#make_call form', function(){
					var form = $(this),
						content_form = form.find('.modal-body'),
						message_block = form.find('.message_block');
					if (form.valid()) {
                        form.find('button[type=submit]').text('Отправляем...');
                        form.find('button[type=submit]').prop('disabled', true);
						form.ajaxSubmit({
							url:	'/<?=$modules['feedback']?>/?action=make_call',
							type: 	"POST",
							success:	function (data){
                                form.find('button[type=submit]').prop('disabled', false);
                                form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								if (data==1){
									$(content_form).html('<div class="h2"><?=i18n('feedback|message_is_sent')?></div>');
									$(form).find('.modal-footer').html('');
							 		yaCounter42381919.reachGoal('call_order_send');
							 		ga('send', 'event', 'form', 'send', 'modal_call');

								}
								else $(message_block).html(data);
							},
							error:	function(xhr,txt,err){
                                form.find('button[type=submit]').prop('disabled', false);
                                form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
							}
						});
						return false;
					}
				});
			});
		</script>
	</div>
</div>
<div class="modal make_call fade" id="make_call_consult" >
	<div class="modal-dialog">
		<form class="modal-content validate" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>

			<div class="modal-body">
				<div class="modal_banner">Оставь заявку прямо сейчас и получите </br> <span>личный земельный участок рядом с домом!</span> </div>
				<div class="message_block"></div>
				<?php
				/*
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|name',true).'<span> *</span>',
					'name'		=>	'name',
					'value'		=>	isset($q['name']) ? $q['name'] : '',
					'attr'		=>	' required',
				));
				/**/
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|phone',true).'<span> *</span>',
					'name'		=>	'phone',
					'value'		=>	isset($q['phone']) ? $q['phone'] : '',
					'attr'		=>	' required mask_phone',
					'placeholder'		=>	' placeholder="+7 (_ _ _) _ _ _-_ _-_ _"',
				));?>
				<input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
				<input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
				<?php
				/*
				// конфиденцыальность
				<div class="list-group-item-text_checkbox">
					<label style="display: flex;margin: 0;justify-content: center;align-items: center;">
						<input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
						<span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
				</div>
				   */
				?>

			</div>
			<div class="modal-footer">
				<button type="submit" class=" btn-default btn_red button_agreement" title="<?=i18n('feedback|send')?>">Получить участок</button>
			</div>

		</form>
		<script async >
			document.addEventListener("DOMContentLoaded", function () {
				//$('#make_call form').submit(function(){
				$('body').on('submit','#make_call_consult form', function(){
					var form = $(this),
						content_form = form.find('.modal-body'),
						message_block = form.find('.message_block');
					if (form.valid()) {
                        form.find('button[type=submit]').text('Отправляем...');
                        form.find('button[type=submit]').prop('disabled', true);
						form.ajaxSubmit({
							url:	'/<?=$modules['feedback']?>/?action=make_call',
							type: 	"POST",
							success:	function (data){
                                form.find('button[type=submit]').prop('disabled', false);
                                form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								if (data==1){
									$(content_form).html('<div class="h2"><?=i18n('feedback|message_is_sent')?></div>');
									$(form).find('.modal-footer').html('');
							 		yaCounter42381919.reachGoal('call_order_send');
							 		ga('send', 'event', 'form', 'send', 'modal_call');

								}
								else $(message_block).html(data);
							},
							error:	function(xhr,txt,err){
                                form.find('button[type=submit]').prop('disabled', false);
                                form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
							}
						});
						return false;
					}
				});
			});
		</script>
	</div>
</div>

<div class="modal make_call fade" id="make_excursion" >
    <div class="modal-dialog">
        <form class="modal-content validate" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            </div>
            <div class="modal-body">
                <div class="h1" style="font-size: 22px;line-height: 30px;"><?=i18n('common|make_call_shop')?></div>
                <div class="message_block"></div>
                <?php
				/*
                echo html_array('form/input',array(
                    'caption'	=>	i18n('feedback|name',true).'<span> *</span>',
                    'name'		=>	'name',
                    'value'		=>	isset($q['name']) ? $q['name'] : '',
                    'attr'		=>	' required',
                ));
				/**/
                echo html_array('form/input',array(
                    'caption'	=>	i18n('feedback|phone',true).'<span> *</span>',
                    'name'		=>	'phone',
                    'value'		=>	isset($q['phone']) ? $q['phone'] : '',
                    'attr'		=>	' required mask_phone',
                    'placeholder'		=>	' placeholder="+7 (_ _ _) _ _ _-_ _-_ _"',
                ));?>
                <input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                <input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
                <div class="list-group-item-text_checkbox">
                    <label style="display: flex;margin: 0;justify-content: center;align-items: center;">
                        <input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
                        <span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class=" btn-default btn_red button_agreement" title="<?=i18n('common|order_consultation_shop')?>"><?=i18n('common|order_consultation_shop')?></button>
            </div>
        </form>
        <script async >
            document.addEventListener("DOMContentLoaded", function () {
                //$('#make_call form').submit(function(){
                $('body').on('submit','#make_excursion form', function(){
                    var form = $(this),
                        content_form = form.find('.modal-body'),
                        message_block = form.find('.message_block');
                    if (form.valid()) {
                        form.find('button[type=submit]').text('Отправляем...');
                        form.find('button[type=submit]').prop('disabled', true);
                        form.ajaxSubmit({
                            url:	'/<?=$modules['feedback']?>/?action=make_call',
                            type: 	"POST",
                            success:	function (data){
                                form.find('button[type=submit]').prop('disabled', false);
                                form.find('button[type=submit]').text('<?=i18n('common|order_consultation_shop')?>');
                                if (data==1){
                                    $(content_form).html('<div class="h2"><?=i18n('feedback|message_is_sent')?></div>');
                                    $(form).find('.modal-footer').html('');
                                    yaCounter42381919.reachGoal('call_order_send');
                                    ga('send', 'event', 'form', 'send', 'modal_call');

                                }
                                else $(message_block).html(data);
                            },
                            error:	function(xhr,txt,err){
                                form.find('button[type=submit]').prop('disabled', false);
                                form.find('button[type=submit]').text('<?=i18n('common|order_consultation_shop')?>');
                                alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
                            }
                        });
                        return false;
                    }
                });
            });
        </script>
    </div>
</div>

<div class="modal make_call fade" id="order_project" >
	<div class="modal-dialog">
		<form class="modal-content validate" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>
			<div class="modal-body">
				<div class="h1"><?=i18n('common|order_project')?></div>
				<div class="message_block"></div>
				<?php
				/*
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|name',true).'<span> *</span>',
					'name'		=>	'name',
					'value'		=>	isset($q['name']) ? $q['name'] : '',
					'attr'		=>	' required',
				));
				/**/
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|phone',true).'<span> *</span>',
					'name'		=>	'phone',
					'value'		=>	isset($q['phone']) ? $q['phone'] : '',
					'attr'		=>	' required mask_phone',
					'placeholder'		=>	' placeholder="+7 (_ _ _) _ _ _-_ _-_ _"',
				));?>
                <input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                <input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
                <div class="list-group-item-text_checkbox">
                    <label style="display: flex;margin: 0;justify-content: center;align-items: center;">
                        <input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
                        <span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
                </div>
			</div>
			<div class="modal-footer">
				<button type="submit" class=" btn-default btn_red button_agreement" title="<?=i18n('feedback|send')?>"><?=i18n('feedback|send')?></button>
			</div>
		</form>
		<script async >
			document.addEventListener("DOMContentLoaded", function () {
				//$('#make_call form').submit(function(){
				$('body').on('submit','#order_project form', function(){
					var form = $(this),
						content_form = form.find('.modal-body'),
						message_block = form.find('.message_block');
					if (form.valid()) {
                        form.find('button[type=submit]').text('Отправляем...');
                        form.find('button[type=submit]').prop('disabled', true);
						form.ajaxSubmit({
							url:	'/<?=$modules['feedback']?>/?action=make_call',
							type: 	"POST",
							success:	function (data){
                                form.find('button[type=submit]').prop('disabled', false);
                                form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								if (data==1){
									$(content_form).html('<div class="h2"><?=i18n('feedback|order_is_sent')?></div>');
									$(form).find('.modal-footer').html('');
									yaCounter42381919.reachGoal('call_order_send');
									ga('send', 'event', 'form', 'send', 'modal_call');

								}
								else $(message_block).html(data);
							},
							error:	function(xhr,txt,err){
                                form.find('button[type=submit]').prop('disabled', false);
                                form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
							}
						});
						return false;
					}
				});
			});
		</script>
	</div>
</div>

<div class="modal make_call fade" id="make_call_investing" >
	<div class="modal-dialog">
		<form class="modal-content validate" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			</div>

			<div class="modal-body">
				<div class="modal_banner" style="line-height: 1.5; font-size: 19px;">Получить предложение по <br> выгодному инвестированию  </div>
				<div class="message_block"></div>
				<?php
				/*
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|name',true).'<span> *</span>',
					'name'		=>	'name',
					'value'		=>	isset($q['name']) ? $q['name'] : '',
					'attr'		=>	' required',
				));
				/**/
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|phone',true).'<span> *</span>',
					'name'		=>	'phone',
					'value'		=>	isset($q['phone']) ? $q['phone'] : '',
					'attr'		=>	' required mask_phone',
					'placeholder'		=>	' placeholder="+7 (_ _ _) _ _ _-_ _-_ _"',
				));?>
				<input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
				<input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
				<?php
				/*
				// конфиденцыальность
				<div class="list-group-item-text_checkbox">
					<label style="display: flex;margin: 0;justify-content: center;align-items: center;">
						<input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
						<span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
				</div>
				   */
				?>

			</div>
			<div class="modal-footer">
				<button type="submit" class=" btn-default btn_red button_agreement" title="<?=i18n('feedback|send')?>">Получить предложение</button>
			</div>

		</form>
		<script async >
			document.addEventListener("DOMContentLoaded", function () {
				//$('#make_call form').submit(function(){
				$('body').on('submit','#make_call_investing form', function(){
					var form = $(this),
						content_form = form.find('.modal-body'),
						message_block = form.find('.message_block');
					if (form.valid()) {
						form.find('button[type=submit]').text('Отправляем...');
						form.find('button[type=submit]').prop('disabled', true);
						form.ajaxSubmit({
							url:	'/<?=$modules['feedback']?>/?action=make_call',
							type: 	"POST",
							success:	function (data){
								form.find('button[type=submit]').prop('disabled', false);
								form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								if (data==1){
									$(content_form).html('<div class="h2"><?=i18n('feedback|message_is_sent')?></div>');
									$(form).find('.modal-footer').html('');
									yaCounter42381919.reachGoal('call_order_send');
									ga('send', 'event', 'form', 'send', 'modal_call');

								}
								else $(message_block).html(data);
							},
							error:	function(xhr,txt,err){
								form.find('button[type=submit]').prop('disabled', false);
								form.find('button[type=submit]').text('<?=i18n('feedback|send')?>');
								alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
							}
						});
						return false;
					}
				});
			});
		</script>
	</div>
</div>
