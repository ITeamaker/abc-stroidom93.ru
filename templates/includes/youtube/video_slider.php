<?=html_sources('footer','fancybox.js')?>

<?php
$q['text'] = strip_tags($q['text']);
$text = mb_strlen($q['text'], "UTF-8")>300 ? mb_substr($q['text'],0,300,"UTF-8").'..' : $q['text'];
$title = filter_var($q['name'],FILTER_SANITIZE_STRING);
$url = get_url('articles',$q);

//print_r($q);
if($q['youtube'] !=''){
	// старая ссылка
	if (count(explode("=", $q['youtube'])) >1) {
		$q_video = explode("=", $q['youtube']);
		$counts = 1;
	}else{
		//новая ссылка
		$q_video = explode("/", $q['youtube']);
		$counts = count($q_video) - 1;
	}
}

if ($i==1){ ?>
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-xs-12">
			<div class="sprite hr"></div>
			<div class="h1">Видео портфолио</div>
		</div>

	<div class="col-lg-12 col-xs-12">
		<div class="portfolio_slider_wrap">
			<?php }
				?>
				<div class="img_wrap">
					<div class="youtube" data-href="<?=$q_video[$counts]?>" data-id="<?=$q['id']?>" data-img="<?=$q['img']?>">
					</div>

				</div>

				<?php
			if ($i==$num_rows){?>
		</div>
	</div>
	</div>
</div>
	<script async>
		document.addEventListener("DOMContentLoaded", function() {
			$(".youtube").each(function() {
				$(this).css('background-image', 'url(/files/video/' + $(this).data('id') + '/img/' + $(this).data('img'));
				$(this).append($('<div />', {
					'class': 'sprite play'
				}));
				$(document).on("click", '.youtube', function() {
					var iframe_url = "https://www.youtube.com/embed/" + $(this).data('href');
					var iframe = '<iframe width="' + $(this).width() + '" height="' + $(this).height() + '" src="' + iframe_url + '?autoplay=1" frameborder="0" allowfullscreen>';
					$(this).replaceWith(iframe)
				})
			});
			$(".fancybox_gallery").fancybox({
				openEffect: 'elastic',
				closeEffect: 'elastic',
				padding: 0,
				helpers: {
					title: {
						type: 'over'
					}
				}
			})
		})
	</script>
	<?php
	if (!isset($config['slick_portfolio_slider_wrap'])) {
		$config['slick_portfolio_slider_wrap'] = 1;
		?>
		<script>
			document.addEventListener("DOMContentLoaded", function() {
				$('.portfolio_slider_wrap').slick({
					lazyLoad: 'progressive',
					slidesToShow: 3,
					slidesToScroll: 1,
					arrows: !0,
					prevArrow: '<div class="slick_button_wrap_prev"><button type="button" class="slick-prev">Previous</button></div>',
					nextArrow: '<div class="slick_button_wrap_next"><button type="button" class="slick-next">Next</button></div>',
					variableWidth: !0,
					<?=$num_rows>3?'centerMode: true,':''?>
				});

			})
		</script>
	<?php } ?>

<?php }?>