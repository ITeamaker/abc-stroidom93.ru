<?php
$services_type = false;
$services = mysql_select('SELECT id as name, module AS id FROM pages WHERE display = 1 AND services=1 AND (module = "buildings" OR module = "internal_finish" OR module = "exterior_finish" OR module = "designing_houses") ORDER BY rank DESC', 'array');
if (isset($services[$page['module']])) {
	$services_type = mysql_select('SELECT IF (`name_type`="",name,name_type) name, id FROM `'.$page['module'].'` WHERE display = 1 ORDER BY rank DESC', 'array');
}
?>


<div class="order_service ">
	<div class="col-lg-12 col-xs-12">
        <div class="sprite hr"></div>
		<div class="h2"><?=i18n('common|become_investor')?></div>
	</div>
	<div class="col-lg-10 col-xs-10 col-lg-offset-1 col-xs-offset-1 form_wrap">
		<noscript><?=i18n('validate|not_valid_captcha2')?></noscript>
		<div class="row">
			<div class="col-lg-12 col-xs-12 message_block">
				<?=isset($q['message']) ? html_array('form/message',$q['message']) : ''?>
			</div>
		</div>
		<form method="post" class="form validate row make_order_form" id="make_order_form<?=@$q['class'] ? $q['class'] : ''?>">
			<div class="col-lg-6 col-xs-6">
				<?php
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|name',true).'<span> *</span>',
					'name'		=>	'name',
					'value'		=>	isset($q['name']) ? $q['name'] : '',
					'attr'		=>	' required',
				));
				echo html_array('form/input',array(
					'caption'	=>	i18n('feedback|phone',true).'<span> *</span>',
					'name'		=>	'phone',
					'value'		=>	isset($q['phone']) ? $q['phone'] : '',
					'attr'		=>	' required mask_phone',
				));
				/*
				// существуют подуслуги
				if ($services_type){
					$id =  $u[2]!='' ? $page['id'] : 0 ;
					echo html_array('form/select',array(
						'caption'=>	i18n('feedback|services',true).'<span> *</span>',
						'name'	=>	'type',
						'select'=>	select($id, $services_type,''),
						'attr'	=>	' required ',
					));
					echo '<input type="hidden" name="services" value="'.$services[$page['module']].'">';
				}
				else{
					$value_services = array();
					$value_services = mysql_select('SELECT id,name FROM investment_projects WHERE display = 1 ORDER BY rank DESC', 'array'); //запрос на услуги
					echo html_array('form/select',array(
						'caption'=>	'Выберите инвест программу из списка<span> *</span>',
						'name'	=>	'services',
						'select'=>	select($page['id'], $value_services,''),
						'attr'	=>	' required ',
					));
				}
				/**/
				?>
			</div>
			<div class="col-lg-6 col-xs-6">
				<?php
				echo html_array('form/textarea',array(
					'name'		=>	'text',
					'caption'	=>	i18n('feedback|text',true),
					'value'		=>	isset($q['text']) ? $q['text'] : '',
					//'attr'		=>	' required',
				));
				//echo html_array('form/captcha2');//скрытая капча
				?>
			</div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="list-group-item-text_checkbox">
                    <label style="display: flex;margin: 0;justify-content: center;align-items: center;">
                        <input type="checkbox" name="" checked="checked" style="width: inherit;height: inherit;margin: 0 5px 0 0;">
                        <span style="max-width: 90%">я согласен/ согласна с <a href="/<?=$config['agreement_page']['url']?>/" target="_blank">условиями конфиденциальности личных данных</a></span></label>
                </div>
            </div>
			<div class="col-lg-12 col-xs-12 ">
                <input type="hidden" name="form_page_url" class="required" value="https://<?=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>">
                <input type="hidden" name="form_page_name" class="required" value="<?=$page['name']?>">
				<div style="display: flex; flex-direction: row; justify-content: center;">
					<?php
					//print_r ($modules);
					echo html_array('form/button',array(
						'name'	=>	i18n('feedback|send'),
						'class'	=>	'btn_red right button_agreement',
					));
					?>
				</div>

				<div class="clearfix"></div>
			</div>
		</form>
		<script type="text/javascript">
			document.addEventListener("DOMContentLoaded", function () {
				$('#make_order_form<?=@$q['class'] ? $q['class'] : ''?>').submit(function(){
					var form = $(this),
						form_wrap = $(this).parents('.form_wrap'),
						message_block = form_wrap.find('.message_block');
					if (form.valid()) {
                        form.find('input[type=submit]').text('Отправляем...');
                        form.find('input[type=submit]').prop('disabled', true);
					    form.ajaxSubmit({
							url:	'/<?=$modules['order_services']?>/?action=order_investment_projects',
							type: 	"POST",
							success:	function (data){
                                if (data==1) {
                                    form.find('input[type=submit]').prop('disabled', false);
                                    form.find('input[type=submit]').text('<?=i18n('feedback|send')?>');
                                    $(form).html('<?=i18n('feedback|order_building_is_sent')?>');
                                    yaCounter42381919.reachGoal('order_service_stroi');
                                    ga('send', 'event', 'form', 'send', 'order_service_stroi');
                                }
                                else {
                                    $(message_block).html(data);
                                }
							},
							error:	function(xhr,txt,err){
                                form.find('input[type=submit]').prop('disabled', false);
                                form.find('input[type=submit]').text('<?=i18n('feedback|send')?>');
								alert('Ошибка ('+txt+(err&&err.message ? '/'+err.message : '')+')');
							}
						});
						return false;
					}
				});
			});
		</script>
	</div>
	<div class="clearfix"></div>
</div>