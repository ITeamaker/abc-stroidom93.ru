<?php

$img = '/files/investments/'.$q['id'].'/img/';

$images = $q['img_list'] ? unserialize($q['img_list']) : false;
//print_r ();

//$url = get_url('investments',$q);

if ($i==1){
    echo html_sources('footer','fancybox.js');
    }
?>
<a href="#" title="<?=$q['name']?>" class="investments_list investments_gallery_js col-lg-6 col-xs-6" data-slider="<?=$q['id']?>" style="background-image: url(<?=$img?>p-<?=$q['img']?>);" rel="gallery<?=$q['id']?>">
	<div class="name_head"></div>
	<div class="name">
		<div class="name_wrap">
			<span><?=$q['name']?></span>
		</div>
	</div>
</a>
<?php
if ($images){
    foreach ($images as $k=>$v) if (@$v['display']==1) {
        $alt2=filter_var(@$v['name'],FILTER_SANITIZE_STRING);
        $title2=filter_var(@$v['title'],FILTER_SANITIZE_STRING);
        $title2 = $title2 ? $title2 : $alt2;
        $path = '/files/investments/'.$q['id'].'/img_list/'.$k.'/';
        echo '<a class="fancybox_gallery investments_galery" title="'.$title2.'" data-slider="'.$q['id'].'" rel="gallery'.$q['id'].'" href="'.$path.$v['file'].'" ></a>';
    }
}

if (fmod($i,2)==0) echo '<div class="clearfix"></div>';
if ($i==$num_rows) {?>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        $('body').on('click','a.investments_gallery_js',function () {
            var $slider_id = $(this).data('slider');
            $('.investments_galery[data-slider='+$slider_id+']:first').trigger("click");
            return false;
        });
        $(".fancybox_gallery").fancybox({
            openEffect	: 'elastic',
            closeEffect	: 'elastic',
            padding: 0,
            helpers : {
                title : {
                    type : 'over'
                }
            }
        });
    });
</script>
<?php }

/*?>

<a class="fancybox_gallery" title="<?=$title?>" rel="gallery<?=$q['id']?>" href="/files/projects/<?=$q['id']?>/img/<?=$q['img']?>" >
    <img src="<?=$img?>" alt="<?=$title?>"  />
</a>
*/