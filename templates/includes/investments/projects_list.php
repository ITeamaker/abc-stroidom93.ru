<?php
$url = get_url('investment_projects',$q);

if ($i==1) {?>
	<div class="col-lg-12 col-xs-12 ">
        <div class="sprite hr"></div>
		<div class="h2"><?=$u[2]!='' && $u[3]=='' ? i18n('common|other_our_invest_program') : i18n('common|our_invest_program')?></div>
	</div>
	<div class="clearfix"></div>
	<div class="investment_projects_wrap">
<?php }?>
	<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
		<a href="<?=$url?>" title="<?=$q['name']?>" class="investments_list projects" style="background-image: url(/files/investment_projects/<?=$q['id']?>/img/p-<?=$q['img']?>);">
			<div class="name_head"></div>
			<div class="name">
				<div class="name_wrap">
					<span><?=$q['name']?></span>
				</div>
			</div>
		</a>
	</div>
<?php
if (fmod($i,4)==0) echo '<div class="clearfix"></div>';
if ($i==$num_rows) echo '</div>';
