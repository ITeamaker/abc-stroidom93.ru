<?php
//print_r ($q);
$price_table = $q['price_table']!='' ? unserialize($q['price_table']) : array() ;
//print_r ($price_table);
if ($price_table){
	?>
	<div class="col-lg-8 col-xs-8 col-lg-offset-2 col-xs-offset-2 text_block">
		<div class="h2"><?=i18n('common|our_prices')?></div>
		<div class="table_our_price">
			<div class="area head" style="border-top: none;"><?=i18n('shop|area')?></div>
			<div class="price head" style="border-top: none;"><?=i18n('shop|price_from')?> <?=i18n('shop|currency')?></div>
			<?php
			foreach ($price_table as $k=>$v){
				if ($v['name']!= '' && $v['price']!= ''){?>
					<div class="area"><?=$v['name']?></div><div class="price"><?=number_format($v['price'],0,'.',' ')?></div>
				<?php }
			}?>
		</div>
	</div>
	<div class="clearfix"></div>
<?php }?>