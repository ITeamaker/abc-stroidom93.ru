<div class="guarantee_h2"><?=i18n('investments|name')?></div>
<div class="guarantee_images">

	<img class="first_photo" src="/templates/images/photo1.jpg">
	<img class="second_photo" src="/templates/images/foto-1.jpg"/>

</div>

<div class="container">
	<div class="row">

		<div class="col-xs-12">
			<div class="description_guarantee">
				<?=i18n('investments|description')?>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="calculation_guarantee">
				<?=i18n('investments|calculation')?>
			</div>
		</div>

	</div>
</div>


