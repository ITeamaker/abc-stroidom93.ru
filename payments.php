<?php

// загрузка настроек *********************************************************
define('ROOT_DIR', dirname(__FILE__).'/');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

// загрузка функций **********************************************************
//require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
//require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
//require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
//require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
//require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
//require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
//require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
//require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
//require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

//заккоментировать в боевом режиме
$config['debug'] = true;

$action = @$_GET['payment_action']; //тип действия
$order = false; //массив заказа переданного от мерчанта
$data = false; //массив для обновления заказов
$date = date('Y-m-d H:i:s');
//массив данных для лога
$log = array(
	'date'=>$date,
	'ip'=>get_ip(),
);

//робокасса
if ($action=='robokassa_result') {
	//строка лога
	$log['merchant'] = 'robokassa';

	//собираем параметры
	$shp_item = @$_REQUEST["Shp_item"];
	$crc = @$_REQUEST["SignatureValue"];

	////синхронизируем данные мерчанта с полями заказа
	$order = array(
		'id' =>  @$_REQUEST["InvId"],
		'total' => $_REQUEST["OutSum"]
	);

	//подписи
	$crc = strtoupper($crc);
	$my_crc = strtoupper(md5($order['total'].':'.$order['id'].':'.$config['robokassa_password2'].':Shp_item='.$shp_item));
	// проверка корректности подписи - check signature
	if ($my_crc != $crc) {
		echo 'ERROR';
		$log['error'] = 'invalid_crc';
	}
	else {
		// признак успешно проведенной операции
		// success
		echo $order['id'];
		// проверка есть ли такой заказ в базе
		$check = check_order ($order);
		if ($check=='success') {
			echo ' OK';
			$data['payment'] = 100; //$config['payments']'
		}
		else {
			echo $check;
			$log['error'] = $check;
		}
	}
}
//liqpay (privat24) v.1.1.2
elseif ($action=='liqpay_result') {
	//строка лога
	$log['merchant'] = 'liqpay';

	//собираем параметры
	$post = array(
		'data' => (string)@$_POST['data'],
		'signature'	=> (string)@$_POST['signature'],
	);
	$json = base64_decode($post['data']);
	$data2 = json_decode($json,true);
	$sign = base64_encode( sha1(
		$config['liqpay_private_key'] .
		$post['data'] .
		$config['liqpay_private_key']
		, 1 ));

	//синхронизируем данные мерчанта с полями заказа
	$order = array(
		'id' =>  @$data2['order_id'],
		'total' => @$data2['amount']
	);

	// проверка корректности подписи - check signature
	if ($sign!=$post['signature']) {
		$log['error'] = 'invalid_signature';
	}
	else {
		// признак успешно проведенной операции
		if ($data2['status']=='success' OR $data2['status']=='wait_accept') {
			echo $order['id'];
			// проверка есть ли такой заказ в базе
			$check = check_order($order);
			if ($check == 'success') {
				$data['payment'] = 500; //$config['payments']'
			} else {
				$log['error'] = $check;
			}
		}
		else {
			$log['error'] = $data2['status'];
		}
		/*
		 * еще вот есть статусы с которыми не понятно что делать, они ни о чем и не понятно как с ними поступать
		processing    Платеж обрабатывается
		prepared    Платеж создан, ожидается его завершение отправителем
		wait_bitcoin    Ожидается перевод bitcoin от клиента
		wait_secure    Платеж на проверке
		wait_accept    Деньги с клиента списаны, но магазин еще не прошел проверку
		wait_lc    Аккредитив. Деньги с клиента списаны, ожидается подтверждение доставки товара
		hold_wait    Сумма успешно заблокирована на счету отправителя
		cash_wait    Ожидается оплата наличными в ТСО.
		wait_qr    Ожидается сканировани QR-кода клиентом.
		wait_sender    Ожидается подтверждение оплаты клиентом в приложении Privat24/Sender.
		wait_card    Не установлен способ возмещения у получателя
		wait_compensation    Платеж успешный, будет зачислен в ежесуточной проводке
		invoice_wait    Инвойс создан успешно, ожидается оплата
		wait_reserve    Средства по платежу зарезервированы для проведения возврата по ранее поданной заявке
		 */
	}
}
//яндекс - проверка заказа
elseif ($action=='yandex_checkOrder') {
	$password = $config['yandex_shopPassword'];
	$post = $_POST;

	//синхронизируем данные мерчанта с полями заказа
	$order = array(
		'id'=>@$post['orderNumber'],
		'total'=>@$post['orderSumAmount'],
	);

	//Ошибка разбора запроса
	if (!isset($post['md5']) OR !isset($post['orderNumber'])) {
		$code = 200;
	}
	else {
		$md5 = md5($post['action'].';'.$post['orderSumAmount'].';'.$post['orderSumCurrencyPaycash'].';'.$post['orderSumBankPaycash'].';'.$post['shopId'].';'.$post['invoiceId'].';'.$post['customerNumber'].';'.$password);
		//не совпадает мд5
		if (strtoupper($post['md5'])!=strtoupper($md5)) {
			$code = 1;
		}
		else {
			// проверка есть ли такой заказ в базе
			$check = check_order ($order);
			if ($check=='success') {
				$code = 0;
			}
			else {
				$code = 100;
				$log['error'] = $check;
			}
		}
	}
	$xml = '<?xml version="1.0" encoding="UTF-8"?>';
	$xml.= '<checkOrderResponse performedDatetime="'.$post['requestDatetime'].'" code="'.$code.'" invoiceId="'.$post['invoiceId'].'" shopId="'.$post['shopId'].'"/>';
	header('Content-type: text/xml; charset=UTF-8');
	echo $xml;
	//проверка существования платежа, потому дальше скрипт не отрабатываем
	die();
}
//яндекс - оплата заказа
elseif ($action=='yandex_paymentAviso') {
	//строка лога
	$log['merchant'] = 'yandex';

	//подготавливаем данные
	$password = $config['yandex_shopPassword'];
	$post = $_POST;

	//синхронизируем данные мерчанта с полями заказа
	$order = array(
		'id' => @$post['orderNumber'],
		'total' => @$post['orderSumAmount'],
	);

	//Ошибка разбора запроса
	if (!isset($post['md5']) OR !isset($post['orderNumber'])) {
		$code = 200;
		$log['error'] = 'invalid_request';
	}
	else {
		$md5 = md5($post['action'].';'.$post['orderSumAmount'].';'.$post['orderSumCurrencyPaycash'].';'.$post['orderSumBankPaycash'].';'.$post['shopId'].';'.$post['invoiceId'].';'.$post['customerNumber'].';'.$password);
		if (strtoupper($post['md5'])!=strtoupper($md5)) {
			$code = 1;
			$log['error'] = 'invalid_md5';
		}
		//не совпадает мд5
		else {
			$check = check_order ($order);
			if ($check=='success') {
				$code = 0;
				$data['payment'] = 200; //$config['payments']
				if ($post['paymentType'] == 'PC') $data['payment'] = 201; //yandex
				if ($post['paymentType'] == 'AC') $data['payment'] = 202; //карта
				if ($post['paymentType'] == 'WM') $data['payment'] = 203; //webmoney
				if ($post['paymentType'] == 'QW') $data['payment'] = 204; //qiwi
			}
			else {
				echo $check;
				$log['error'] = $check;
			}
		}
	}
	$xml = '<?xml version="1.0" encoding="UTF-8"?>';
	$xml.= '<paymentAvisoResponse performedDatetime="'.$post['requestDatetime'].'" code="'.$code.'" invoiceId="'.$post['invoiceId'].'" shopId="'.$post['shopId'].'"/>';
	header('Content-type: text/xml; charset=UTF-8');
	echo $xml;
}
//paypal
elseif ($action=='paypal_result') {
	//строка лога
	$log['merchant'] = 'paypal';

	//обрабатываем данные
	$post = $_POST;
	//смотрим все что запрашивает пейлпал
	log_add('paypal.txt',serialize($post),true);
	$postdata = '';
	//обратный запрос на пейпал для проверки заказа
	foreach ($_POST as $key=>$value) $postdata.= $key.'='.urlencode($value).'&';
	$postdata .= "cmd=_notify-validate";
	$curl = curl_init("https://www.paypal.com/cgi-bin/webscr");
	curl_setopt ($curl, CURLOPT_HEADER, 0);
	curl_setopt ($curl, CURLOPT_POST, 1);
	curl_setopt ($curl, CURLOPT_POSTFIELDS, $postdata);
	curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt ($curl, CURLOPT_RETURNTRANSFER, 1);
	//нужно чтобы небыло ошибки You don't have permission to access "http://www.paypal.com/cgi-bin/webscr" on this server.
	curl_setopt ($curl, CURLOPT_HTTPHEADER, array('Connection: Close', 'User-Agent: company-name'));
	//расскоментировать на боевом, на локале ругается на эту строку
	//curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 1);
	$response = curl_exec ($curl);
	//$response = "VERIFIED"; $post['item_number'] = 1;
	curl_close ($curl);
	//фиксируем ответ от пейпала
	log_add('paypal.txt',$response,true);

	//синхронизируем данные мерчанта с полями заказа
	$order = array(
		'id'=>@$post['item_number'],
		'total'=>@$post['payment_gross'],
	);

	//успешный платеж
	if ($response == "VERIFIED"){
		if (mb_strtolower($config['paypal_email'],'UTF-8')==mb_strtolower($post['receiver_email'],'UTF-8')) {
			$check = check_order ($order);
			if ($check=='success') {
				$code = 0;
				$data['payment'] = 600; //$config['payments']
			}
			else {
				echo $check;
				$log['error'] = $check;
			}
		}
		else {
			$log['error'] = 'invalid email';
		}
	}
	else {
		$log['error'] = 'not VERIFIED';
	}
}
//2checkout
//todo - доработать
elseif ($action=='2checkout_result') {
	$log['merchant'] = 'paypal';
}
//неизвестный мерчант (ошибка)
else {
	$log['merchant'] = 'unknown';
}

//записываем в лог ИД и стоимость заказа
//todo правильно бы добавить еще и валюту
if ($order) {
	$log['id'] = $order['id'];
	$log['total'] = $order['total'];
}

//если успешно то проводим оплату заказа
if ($data AND $order) {
	$data['id'] = $order['id'];
	$data['paid']= 1;
	$data['date_paid'] = $date;
	mysql_fn('update', 'orders', $data);
	log_add('payment_success_'.date('Y-m').'.txt',$log);
}
//ошибка
else {
	log_add('payment_error_'.date('Y-m').'.txt',$log);
}

//функция проверки заказа
//todo - валюты заказа
function check_order ($data) {
	if ($order = mysql_select("SELECT * FROM orders WHERE id=".intval($data['id']),'row')) {
		//заказ уже оплачен
		if ($order['paid']>0) {
			return 'paid'; //оплачен
		}
		//не совпадает сумма оплаты
		elseif ($order['total']!=$data['total']) {
			return 'total';
		}
		//все успешно
		else {
			return 'success';
		}
	}
	//нет такого заказа
	else return 'error';
}


