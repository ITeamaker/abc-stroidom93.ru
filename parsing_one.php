<?php

/**
 * файл генерирует xml документ для yandex market
 * доступен по адресу /market.xml
 * в .htaccess есть настройка RewriteRule ^market.xml$ market.php [L]
 */

// загрузка настроек *********************************************************
define('ROOT_DIR', dirname(__FILE__).'/');
require_once(ROOT_DIR.'_config.php');	//динамические настройки
require_once(ROOT_DIR.'_config2.php');	//установка настроек

// загрузка функций **********************************************************
require_once(ROOT_DIR.'functions/admin_func.php');	//функции админки
require_once(ROOT_DIR.'functions/auth_func.php');	//функции авторизации
require_once(ROOT_DIR.'functions/common_func.php');	//общие функции
require_once(ROOT_DIR.'functions/file_func.php');	//функции для работы с файлами
require_once(ROOT_DIR.'functions/html_func.php');	//функции для работы нтмл кодом
require_once(ROOT_DIR.'functions/form_func.php');	//функции для работы со формами
require_once(ROOT_DIR.'functions/image_func.php');	//функции для работы с картинками
require_once(ROOT_DIR.'functions/lang_func.php');	//функции словаря
require_once(ROOT_DIR.'functions/mail_func.php');	//функции почты
require_once(ROOT_DIR.'functions/mysql_func.php');	//функции для работы с БД
require_once(ROOT_DIR.'functions/string_func.php');	//функции для работы со строками

require (ROOT_DIR.'plugins/phpquery/phpQuery/phpQuery.php');
function geturl1($url,$proxy=false) {

	//global $proxylist;
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL,str_replace(' ','%20',$url));
	curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl, CURLOPT_HEADER, 0);
	curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($curl, CURLOPT_TIMEOUT, 15);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	/*
		$i=0;
	/*
		do {
			if($proxy&&count($proxylist)>0){
				$proxys=$proxylist[rand(0,count($proxylist)-1)];
				curl_setopt($curl, CURLOPT_PROXY, $proxys);
			}
			$data = curl_exec($curl);
			if(curl_errno($curl)){$data='';//$data=false;
				echo '+'.curl_errno($curl).'+<br>';
			}
			if(!in_array(curl_getinfo($curl,CURLINFO_HTTP_CODE),array(200,404))) {$data='';//$data=false;
				echo '++'.curl_getinfo($curl,CURLINFO_HTTP_CODE).'++<br>';
			}
			if(curl_getinfo($curl,CURLINFO_EFFECTIVE_URL)=='http://upn.ru/robotprotect.aspx'){$data='';//$data=false;
				echo '+++'.curl_getinfo($curl,CURLINFO_EFFECTIVE_URL).'+++<br>';
			}
			$i++;
	//} while (!$data && $i<3); //стоп если получили данные или трижды получили ошибку
		} while (strpos($data,'</html>')===false && $i<10); //стоп если получили данные или трижды получили ошибку
	*/
	$data = curl_exec($curl);
	curl_close($curl);
	usleep(100);
	return $data;
}

/*
$hbr =
	file_get_contents('http://demo.abc-cms.com/');
$document = phpQuery::newDocument($hbr);
$hentry = $document->find('body');

foreach ($hentry as $el) {
	$pq = pq($el); // Это аналог $ в jQuery
	// меняем атрибуты найденого элемента
	$pq->find('.shop_product_list');
	dd($pq);
	//$pq->find('div.entry-info')->remove();//удаляем ненужный эл-нт
	//$tags = $pq->find('ul.tags > li > a');
	//$tags->append(': ')->prepend(' :'); // добавляем : по бокам
	// добавляем контент в начало найденого элемента
	//$pq->find('div.content')->prepend('<br />')->prepend($tags);
}
*/

//echo $pq;

$site='https://www.avito.ru';

//$url="$site/sochi/kvartiry/1-k_kvartira_20_m_13_et._1247970440";
//https://www.avito.ru/stroidom93/rossiya/nedvizhimost?p=2
$https = 'https:';
$stop=0;

$db=array();

$document1=phpQuery::newDocument($document1);
// парсер страници
//$p['link'] = "$site/sochi/kvartiry/1-k_kvartira_20_m_13_et._1247970440";

$update_pars = mysql_select("SELECT * FROM pars WHERE id_advert = ''", "row");

dd($update_pars['id']);

$p['link'] = $update_pars['url'];
//echo "<br>".$update_pars['url']."<br>";
dd($update_pars['url']);

$file = ROOT_DIR.'parsing_one_'.$update_pars['id'].'.txt';
//dd($file);

if (file_exists($file) ) {
	$document1 = file_get_contents($file);
} else {
	$document1 = geturl1($p['link']);
	//запись в файл
	$fp = fopen(ROOT_DIR.'parsing_one_'.$update_pars['id'].'.txt','w');
	fwrite($fp, $document1);
	fclose($fp);
}

//dd($document1);
//die();
if(!$document1) {
	echo 'Объекты для обновления отсутвуют!'.$p['link'].'<br>';
}

$document1=phpQuery::newDocument($document1);
$parameters=nl2br(trim(pq($document1)->find('li.item-params-list-item')->text()));
$parameters2=explode('br',$parameters);

//dd($parameters2);
foreach ($parameters2 as $k=>$v){
	$v = str_replace(array('/>','>','<'),'',$v);
	//echo strip_tags($v);
	//dd($v);
	if(strpos($v,':')){
		//dd($v);
		$explode = explode(':',$v);
		//dd($explode);
		if(strpos($explode[0],'в доме')){
			$p['floors']=trim($explode[1]);
		}elseif (strpos($explode[0],'таж')){
			$p['floor']=trim($explode[1]);
		}elseif (strpos($explode[0],'ип дома')){
			$p['housetype']=trim($explode[1]);
		}elseif (strpos($explode[0],'площадь')){
			$explode2 = explode('м',$explode[1]);
			$p['square']=trim($explode2[0]);
		}elseif (strpos($explode[0],'дь дома')){
			$explode2 = explode('м',$explode[1]);
			$p['square']=trim($explode2[0]);
		}elseif (strpos($explode[0],'комнат')){
			$explode2 = explode('-',$explode[1]);
			//dd($explode2[0]);
			$p['rooms']=trim($explode2[0]);
		}elseif (strpos($explode[0],'дь участка')){
			$explode2 = explode('сот',$explode[1]);
			$p['landarea']=trim($explode2[0]);
		}elseif (strpos($explode[0],'ал стен')){
			$p['wallstype']=trim($explode[1]);
		}elseif (strpos($explode[0],'д объекта')){
			$p['objecttype']=trim($explode[1]);
		}
	}
}

$unic_id=nl2br(trim(pq($document1)->find('.title-info-metadata-item')->text()));

$explode_unic_id = explode(',',$unic_id);
$explode_unic_id_next = explode('№',$explode_unic_id[0]);

$p['name'] = trim(pq($document1)->find('h1.title-info-title span')->text());

$p['id_advert'] = trim($explode_unic_id_next[1]);
$p['operationtype'] = 'продам';
$p['markettype'] = 'Новостройка';
$p['propertyrights'] = 'собственник';
$p['companyname'] = 'СтройДом93';

//$p['category'] = "Квартиры";
//	$p['datebegin']
//	$p['dateend']
$p['address'] = trim(pq($document1)->find('span.item-map-address')->text());
//$p['region'] = "Адлерский";
$p['city'] = "Сочи";
$p['description']=trim(pq($document1)->find('div.item-description-html')->text());
$p['managername'] =trim(pq($document1)->find('span.sticky-header-seller-text')->attr('title'));

//$p['id_advert'] = substr($p['link'], -10, 10);
if($p['managername']=='Марина') $p['contactphone'] = 89183000525;
if($p['managername']=='Елена') $p['contactphone'] = 89884000905;
if($p['managername']=='Алексей') $p['contactphone'] = 89183000469;
if($p['managername']=='Кристина') $p['contactphone'] = 89884000874;
if($p['managername']=='Евгений') $p['contactphone'] = 89183000769;
if($p['managername']=='Анна') $p['contactphone'] = 89183000415;
if($p['managername']=='Яна') $p['contactphone'] = 89183000469;
if($p['managername']=='Наталья') $p['contactphone'] = 89884000905;
if($p['managername']=='Владимир') $p['contactphone'] = 89183000769;
if($p['managername']=='Любовь') $p['contactphone'] = 89885000902;
if($p['managername']=='Лина') $p['contactphone'] = 89884000905;
if($p['managername']=='Екатерина') $p['contactphone'] = 89183000525;
if($p['managername']=='Алекскей') $p['contactphone'] = 89183000469;
if($p['managername']=='Александр') $p['contactphone'] = 89885000902;

//$k=$document1->find('div.js-gallery-img-frame');
//dd(trim(pq($document1)->find('div.js-gallery-img-frame')));
//dd($update_pars['id']);

//$id_file = 381;
//dd($id_file);
//mkdir(ROOT_DIR."files/pars/381/");

$images = trim(pq($document1)->find('div.gallery-imgs-container')->find('.gallery-img-wrapper'));
//foreach($images as $k=>$v){
//dd($v);
//}
//$images = trim(pq($document1)->find('div.gallery-img-frame')->attr('data-url'));

$qqq = explode('data-url="',$images);
$img_arr = array();
foreach ($qqq as $k=>$v){
	$i++;
	$j=$i-1;

	if(strpos($v,'"')){
		$explode = explode('" data-ti',$v);
		$p['images']=$https.trim($explode[0]);

		$img_url = explode('480/',$p['images']);
		// Get the file
		$content = file_get_contents($p['images']);
		// Store in the filesystem
		$fp = fopen(ROOT_DIR.$img_url[1], "w");
		fwrite($fp, $content);
		fclose($fp);

		if($j>0){
			mkdir(ROOT_DIR."files/pars/".$update_pars['id']."/");
			mkdir(ROOT_DIR."files/pars/".$update_pars['id']."/images/");
			mkdir(ROOT_DIR."files/pars/".$update_pars['id']."/images/".$j);
		}
		if(copy(ROOT_DIR.$img_url[1],ROOT_DIR."files/pars/".$update_pars['id']."/images/".$j."/".$img_url[1])){
			$img_arr[$j] = $img_url[1];
			foreach ($img_arr as $key=>$val){
				$val_name = explode('.',$val);

				$new_val = array(
					'name' => $val_name[0],
					'display' => 1,
					'file' => $val
				);
				$img_arr[$j] = $new_val;
			}

		};
	}
}

$p['images'] = serialize($img_arr);
$p['id'] = $update_pars['id'];

$p['update_pars'] = 2;

$where = " AND id = ".$update_pars['id']." ";
//dd($where);
//dd($p);
mysql_fn('update','pars',$p);

$document1->unloadDocument();

/*
	}
while ($stop > 0);*/
/*
	$temp=$document->find('div.pagination__nav > a.pagination__page:last');
//  if(preg_match('/Следующая\sстраница/',pq($temp)->text())){
//    $url=$site.pq($temp)->attr('href');
//  } else {$stop=1;
	if(!count($k)){$stop=2;}
	elseif(preg_match('/Следующая\sстраница/',pq($temp)->text())){
		$url=$site.pq($temp)->attr('href');
	}else{$stop=1;}
	$document->unloadDocument();
//$stop=true;
} while (!$stop);
if($stop==1){
	mysql_select('update items set archive=1 where archive=0 and link like "http://www.avito.ru/%/zemelnye_uchastki/%" and id not in ('.implode(',',$db).')');
	mysql_select('update items set archive=0 where archive=1 and link like "http://www.avito.ru/%/zemelnye_uchastki/%" and id in ('.implode(',',$db).')');
}
*/
?>
<script type="text/javascript">
	setTimeout(function() {
		location.reload();
	}, 5000);
</script>
